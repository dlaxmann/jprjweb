package com.javacodegeeks.spring.mail;

import java.util.Properties;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class MailTest {
	/**
	 * @param args
	 */
	public static void main(String[] args) {
	
	        String host = "hm2ntns03.farmersinsurance.com";
	        String from = "FBIExpress@farmersinsurance.com";

	        try {
	        Properties props = System.getProperties();
	        props.put("mail.smtp.host", host);
	        props.put("mail.smtp.user", from);
	        props.put("mail.debug", "true");

	        Session session = Session.getDefaultInstance(props, null);
	        session.setDebug(true);
	        Transport transport = session.getTransport("smtp");

	        MimeMessage message = new MimeMessage(session);
	        Address fromAddress = new InternetAddress("FBIExpress@farmersinsurance.com");

	        message.setFrom(fromAddress);

	        //InternetAddress to = new InternetAddress("mbinigeri@csc.com");
	        InternetAddress to = new InternetAddress("ldevasani@csc.com");
	        message.addRecipient(Message.RecipientType.TO, to);

	        message.setSubject("Email Details Sending");
	        message.setText("This is my testing content.");

	        transport.connect(host, from);
	        message.saveChanges();
	        Transport.send(message);
	        transport.close();
	    } catch(Exception e){
	    }finally { 
	    }
	    //    out.close();
	    }
}
