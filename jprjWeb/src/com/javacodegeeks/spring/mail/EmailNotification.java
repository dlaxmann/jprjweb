package com.javacodegeeks.spring.mail;

import org.springframework.mail.MailException;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;

public class EmailNotification {

	private MailSender mailSender;
private SimpleMailMessage simpleMailMessage;
	
	public void setSimpleMailMessage(SimpleMailMessage simpleMailMessage) {
		this.simpleMailMessage = simpleMailMessage;
	}

	public void setMailSender(MailSender mailSender) {
		this.mailSender = mailSender;
	}
	
	public void sendMail(Taxpayer taxpayer) {

		SimpleMailMessage message = new SimpleMailMessage(simpleMailMessage);
		
		message.setTo(taxpayer.getEmailAddress());
		message.setText("Dear " + taxpayer.getTaxpayerName() + ",\n" + 
				"Your profile has been created.");

		try{
			mailSender.send(message);
		} catch(MailException e) {
			System.err.println(e.getMessage());
		}
		
	}
}
