package com.javacodegeeks.spring.mail;

import org.springframework.context.ApplicationContext;

import com.frame.util.AppUtils;
import com.frame.util.LoadContext;

public class MailServiceTest {
	static ApplicationContext appContext;
	

	
	public static MailService getMailService(){
		LoadContext lct = new LoadContext();		
		lct.laodContext();
		MailService mailService;
		appContext = AppUtils.getContext();
		mailService = (MailService) appContext.getBean("mailService");
		return mailService;
	}
	
	public static void main(String[] args) {
		
		
		//ApplicationContext context = new FileSystemXmlApplicationContext("conf/spring.xml");
		//MailService mailService = (MailService) appContext.getBean("mailService");
		
		MailService mailService = MailServiceTest.getMailService();
		System.out.println("@@@@@ mailService= "+mailService);
		mailService.sendMail("imlaxman@gmail.com", "imlaxman@gmail.com",
				"titanic@123", "Testing only \n\n Hello Spring Email Sender");
		
		mailService.sendAlertMail("Exception occurred");
		System.out.println(".....Sent");
	}
	
}
