package com.frame.logging;

import org.springframework.context.ApplicationContext;

import com.app.domain.bean.Stock;
import com.frame.bo.StockBo;
import com.frame.util.AppUtils;
import com.frame.util.LoadContext;

public class TestStock {
	static ApplicationContext appContext  ;
	
    public static void main(String args[])     {
    	LoadContext lct = new LoadContext();
    	lct.laodContext();
    	appContext = AppUtils.getContext();
    	StockBo stockBo = (StockBo)appContext.getBean("stockBo");
    	
    	/** insert **/
    	Stock stock = new Stock();
    	stock.setStockCode("766815");
    	stock.setStockName("HAIOO3");
    	stockBo.save(stock);
    	
    	/** select **/
    	Stock stock2 = stockBo.findByStockCode("7668");
    	System.out.println(stock2);
    	
    	/** update **/
    	stock2.setStockName("HAIO-2");
    	stockBo.update(stock2);
    	
    	/** delete **/
    	stockBo.delete(stock2);
    	
    	System.out.println("Done");
    }
}
