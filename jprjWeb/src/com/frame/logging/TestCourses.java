package com.frame.logging;

import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.context.ApplicationContext;

import com.app.domain.bean.Course;
import com.frame.util.HibernateUtil;


public class TestCourses {
	private static Logger _logger = Logger.getLogger(TestCourses.class);
	
	
	
	public static void main(String[] args) {
		TestCourses obj = new TestCourses();
	
		//Session session = obj.getSession();
		Long courseId1 = obj.saveCourse("Physics");
		Long courseId2 = obj.saveCourse("Chemistry");
		Long courseId3 = obj.saveCourse("Maths");
		obj.listCourse();
		obj.updateCourse(courseId3, "Mathematics");
		obj.deleteCourse(courseId2);
		obj.listCourse();
	}
	
	
	
	
	
	
	
	
	
	
	
	public Long saveCourse(String courseName)
	{
		Session session = HibernateUtil.getSession();
		Transaction transaction = null;
		Long courseId = null;
		try {
			
			transaction = session.beginTransaction();
			Course course = new Course();
			course.setCourseName(courseName);
			courseId = (Long) session.save(course);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return courseId;
	}
	
	public void listCourse()
	{
		Session session = HibernateUtil.getSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			List courses = session.createQuery("from Course").list();
			for (Iterator iterator = courses.iterator(); iterator.hasNext();)
			{
				Course course = (Course) iterator.next();
				System.out.println(course.getCourseName());
			}
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}
	
	public void updateCourse(Long courseId, String courseName)
	{
		Session session = HibernateUtil.getSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			Course course = (Course) session.get(Course.class, courseId);
			course.setCourseName(courseName);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}
	
	public void deleteCourse(Long courseId)
	{
		Session session = HibernateUtil.getSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			Course course = (Course) session.get(Course.class, courseId);
			session.delete(course);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}
	
	/*public Session getSession(){
	LoadContext lct = new LoadContext();		
	lct.laodContext();
	appContext = AppUtils.getContext();
	SessionFactory sessionFactory = (SessionFactory)appContext.getBean("sessionFactory");
	Session ss= sessionFactory.openSession();
	MailService mailService;
	mailService = (MailService) appContext.getBean("mailService");
	_logger.info("@@@@@ MailServiceTest= "+mailService);
	return ss;
}*/
}
