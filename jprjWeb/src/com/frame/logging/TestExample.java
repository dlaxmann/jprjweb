package com.frame.logging;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.app.domain.bean.Employee;
import com.frame.util.HibernateUtil;

public class TestExample {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		/** Getting the Session Factory and session */
		//SessionFactory session = HibernateUtil.getSessionFactory();
		Session sess = HibernateUtil.getSession();
		/** Starting the Transaction */
		Transaction tx = sess.beginTransaction();
		/** Creating Pojo */
		Employee pojo = new Employee();
		pojo.setId(new Integer(5));
		pojo.setName("XYZ");
		/** Saving POJO */
		sess.save(pojo);
		//sess.Load(pojo);
		/** Commiting the changes */
		tx.commit();
		System.out.println("Record Inserted");	
		/** Closing Session */
		sess.close();
	}

}
