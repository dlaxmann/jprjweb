package com.frame.logging;

import com.frame.util.HibernateUtil;


public class LoggerTest {
	private static final Logger log = LoggerFactory.getLog(LoggerTest.class.getName());  
	
	public void test() {
		int a = 10;
		int b = 0;
		try {
			int c = a/b;
		} catch(Exception e) {
			log.error("Error TEST", e);
		}
	}
	
	public static void main(String[] args) throws ClassNotFoundException {
		Class.forName(HibernateUtil.class.getName());
		LoggerTest lt = new LoggerTest();
		lt.test();
	}
}
