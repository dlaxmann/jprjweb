package com.frame.logging;
import org.apache.log4j.Logger;




public class LogSampleReports {

	static Logger logger = Logger.getLogger(LogSampleReports.class); 
	public static void main (String[] args){
		logger.info("Sample info message"); 
		LogSampleReports smpl = new LogSampleReports();
		System.out.println("hai hello");
		smpl.generateReport();
	} 
	public void generateReport() 
	{ 
	logger.debug("Sample debug message"); 
	logger.info("Sample info message"); 
	logger.warn("Sample warn message"); 
	logger.error("Sample error message"); 
	logger.fatal("Sample fatal message"); 
	
	}
}
