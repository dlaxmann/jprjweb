package com.frame.logging;

import java.sql.Date;
import java.sql.Timestamp;

import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.spi.LocationInfo;
import org.apache.log4j.spi.LoggingEvent;
import org.apache.log4j.spi.ThrowableInformation;

import com.frame.dao.BaseDAO;
import com.frame.dao.DAOManager;

/**
 * This class is used to log the information into the database. By extending the
 * JDBCAppeneder class we can get the base class method functionality. The
 * methods inside the Base class gets called internally and read the
 * log4j.properties file defined.
 * 
 * @author ksivagnanam
 * 
 */
public class DBAppender extends AppenderSkeleton {

	/**
	 * Default Constructor for the DBAppender class
	 */
	public DBAppender() {

	}

	
	public final int MAX_LENGTH_MESSAGE = 3000;

	/**
	 * Internal method. Appends the message to the database table.
	 * 
	 * @param event
	 *            Description of Parameter
	 */
	@Override
	protected void append(LoggingEvent event) {

		BaseDAO eventDao = DAOManager.getBaseDAO();
		if (eventDao == null) {
			return;
		}
		long timestamp = event.getTimeStamp();
		LoggerTable loggerTable = new LoggerTable();
		Date date = new Date(event.getTimeStamp());
		loggerTable.setLogDate(date);
		Timestamp time = new Timestamp(timestamp);
		loggerTable.setLogTime(time);
		loggerTable.setLogger(event.getLoggerName());
		loggerTable.setSeverity(event.getLevel().toString());

		String message = replaceProblematicChars(event.getMessage() == null ? "null"
				: event.getMessage().toString());
		loggerTable.setLogMessage(message);
		loggerTable.setThreadName(event.getThreadName());

		LocationInfo locinfo = event.getLocationInformation();
		if (locinfo != null) {
			loggerTable.setLogClassName(locinfo.getClassName());
			loggerTable.setLogFileName(locinfo.getFileName());
			loggerTable.setLogLineNumber(locinfo.getLineNumber());
			loggerTable.setLogMethodName(locinfo.getMethodName());
		}
		ThrowableInformation throwableinfo = event.getThrowableInformation();

		StringBuffer throwableStringBuffer = new StringBuffer();

		if (throwableinfo != null) {
			String[] lines = throwableinfo.getThrowableStrRep();
			for (int index = 0; index < lines.length; index++) {
				throwableStringBuffer = (StringBuffer) throwableStringBuffer
						.append(lines[index] + "\r\n");
			}
		}

		String locThrowable = replaceProblematicChars(throwableStringBuffer
				.toString());
		loggerTable.setLogThrowable(locThrowable);

		try {
			eventDao.save(loggerTable);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	protected String replaceProblematicChars(String aString) {
		String result = new String(aString);
		// use replace function
		result = replace(result, "'", "''");
		if (result.length() > MAX_LENGTH_MESSAGE) {
			result = result.substring(0, MAX_LENGTH_MESSAGE);
		}

		return result;
	}


	/**
	 * String-replacer
	 * 
	 * @param source
	 *            Description of Parameter
	 * @param find
	 *            Description of Parameter
	 * @param replacement
	 *            Description of Parameter
	 * @return Description of the Returned Value
	 */
	public String replace(String source, String find, String replacement) {
		int i = 0;
		int j;
		int k = find.length();
		int m = replacement.length();

		while (i < source.length()) {
			j = source.indexOf(find, i);

			if (j == -1) {
				break;
			}

			source = replace(source, j, j + k, replacement);

			i = j + m;
		}

		return source;
	}

	/**
	 * String-replacer
	 * 
	 * @param source
	 *            Description of Parameter
	 * @param start
	 *            Description of Parameter
	 * @param end
	 *            Description of Parameter
	 * @param replacement
	 *            Description of Parameter
	 * @return Description of the Returned Value
	 */
	private String replace(String source, int start, int end, String replacement) {
		if (start == 0) {
			source = replacement + source.substring(end);
		} else if (end == source.length()) {
			source = source.substring(0, start) + replacement;
		} else {
			source = source.substring(0, start) + replacement
					+ source.substring(end);
		}

		return source;
	}

	public void close() {
		// TODO Auto-generated method stub
		
	}

	public boolean requiresLayout() {
		// TODO Auto-generated method stub
		return false;
	}

}
