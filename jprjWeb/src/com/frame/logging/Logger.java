package com.frame.logging;

/**
 * This Logger interface contains all the abstract methods declaration,
 * which the Implementation class has to override and provide the implementation codes for all the methods.
 * @author 
 * 
 */
public interface Logger {

	/**
	 * This method returns true, if the isTrace option is enabled for the log.
	 * @return boolean
	 * 
	 */
	public boolean isTraceEnabled();

	/**
	 * This method returns true, if the isDebug option is enabled for the log.
	 * @return boolean
	 * 
	 */
	public boolean isDebugEnabled();

	/**
	 * This method returns true, if the isInfo option is enabled for the log.
	 * @return boolean
	 * 
	 */
	public boolean isInfoEnabled();

	/**
	 * This method returns true, if the isWarning option is enabled for the log.
	 * @return boolean
	 * 
	 */
	public boolean isWarningEnabled();

	/**
	 * This method returns true, if the isError option is enabled for the log.
	 * @return boolean
	 */
	public boolean isErrorEnabled();

	/**
	 * This method returns true, if the isFatal option is enabled for the log.
	 * @return boolean
	 * 
	 */
	public boolean isFatalEnabled();

	/**
	 * This method get the log object and log the message details into the log file
	 * @param message
	 * @return java.lang.String
	 * 
	 */
	public void trace(Object message);

	/**
	 * 
	 * @param message
	 * @param t
	 * @return java.lang.String
	 * 
	 */
	public void trace(Object message, Throwable t);

	/**
	 * @param message
	 * @return java.lang.String
	 * 
	 */
	public void debug(Object message);

	/**
	 * This method get the log object and log the message details into the log file
	 * @param message
	 * @param t
	 * @return java.lang.String
	 * 
	 */
	public void debug(Object message, Throwable t);

	/**
	 * This method get the log object and log the message details into the log file
	 * @param message
	 * 
	 */
	public void info(Object message);

	/**
	 * @param message
	 * 
	 */
	public void info(Object message, Throwable t);

	/**
	 * @param message
	 *  
	 */
	public void warn(Object message);

	/**
	 * @param message
	 * @param t
	 * 
	 */
	public void warn(Object message, Throwable t);

	/**
	 * 
	 * @param message
	 * 
	 */
	public void error(Object message);

	/**
	 * @param message
	 * 
	 */
	public void error(Object message, Throwable t);

	/**
	 * @param message
	 */
	public void fatal(Object message);

	/**
	 * This method logs the fatal error message into the log file
	 * @param message
	 * 
	 */
	public void fatal(Object message, Throwable t);

}
