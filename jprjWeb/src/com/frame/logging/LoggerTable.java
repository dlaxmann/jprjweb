package com.frame.logging;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="LOGGER_TABLE")
public class LoggerTable implements Serializable {
	private static final long serialVersionUID = 2869849133306070143L;
	
	private int logId;
	private Date logDate;
	private Timestamp logTime;
	private String logger = null;
	private String severity = null;
	private String logMessage = null;
	private String threadName = null;
	private String logClassName = null;
	private String logMethodName = null;
	private String logFileName = null;
	private String logLineNumber = null;
	private String logThrowable = null;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public int getLogId() {
		return logId;
	}
	
	public void setLogId(int logId) {
		this.logId = logId;
	}
	
	@Column(name="LOG_DATE")
	public Date getLogDate() {
		return logDate;
	}
	
	public void setLogDate(Date logDate) {
		this.logDate = logDate;
	}
	
	@Column(name="LOG_TIME")
	public Timestamp getLogTime() {
		return logTime;
	}
	
	public void setLogTime(Timestamp logTime) {
		this.logTime = logTime;
	}
	
	@Column(name="LOGGER")
	public String getLogger() {
		return logger;
	}
	
	public void setLogger(String logger) {
		this.logger = logger;
	}
	
	@Column(name="SEVERITY")
	public String getSeverity() {
		return severity;
	}
	
	public void setSeverity(String severity) {
		this.severity = severity;
	}
	
	@Column(name="LOG_MESSAGE")
	public String getLogMessage() {
		return logMessage;
	}
	
	public void setLogMessage(String logMessage) {
		this.logMessage = logMessage;
	}
	
	@Column(name="THREAD_NAME")
	public String getThreadName() {
		return threadName;
	}
	
	public void setThreadName(String threadName) {
		this.threadName = threadName;
	}
	
	@Column(name="LOG_CLASS_NAME")
	public String getLogClassName() {
		return logClassName;
	}
	
	public void setLogClassName(String logClassName) {
		this.logClassName = logClassName;
	}
	
	@Column(name="LOG_METHOD_NAME")
	public String getLogMethodName() {
		return logMethodName;
	}
	
	public void setLogMethodName(String logMethodName) {
		this.logMethodName = logMethodName;
	}
	
	@Column(name="LOG_FILE_NAME")
	public String getLogFileName() {
		return logFileName;
	}
	
	public void setLogFileName(String logFileName) {
		this.logFileName = logFileName;
	}
	
	@Column(name="LOG_LINE_NUMBER")
	public String getLogLineNumber() {
		return logLineNumber;
	}
	
	public void setLogLineNumber(String logLineNumber) {
		this.logLineNumber = logLineNumber;
	}
	
	@Column(name="LOG_THROWABLE", length=3000)
	public String getLogThrowable() {
		return logThrowable;
	}
	
	public void setLogThrowable(String logThrowable) {
		this.logThrowable = logThrowable;
	}
}
