package com.frame.cache;

public class CachingObjects {

	public void cacheSingleObject(){
		
	}
	
	public void cacheMultipleObjects(){
		
	}
	
	public static void main (String[] args){
		CachingObjects caching = new CachingObjects();
		caching.cacheSingleObject();
		caching.cacheMultipleObjects();
	}
}
