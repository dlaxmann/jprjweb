package com.frame.cache;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.jcs.engine.control.CompositeCacheManager;

import com.frame.exception.CacheException;
import com.frame.logging.Logger;
import com.frame.logging.LoggerFactory;
import com.frame.util.AppUtils;
/**
 * This the Cache Manager Factory class used to create the Instance of the CacheManagerImpl class and wrap it into
 * ICacheManager and return the ICacheManager instance.
 * So the client accessing this factory won't be aware of the class that's processing it request.
 * 
 * @author ksivagnanam
 *
 */

public class CacheManagerFactory
{

  private static final Logger log = LoggerFactory.getLog(CacheManagerFactory.class.getName());
  private static final String cacheFile = "cache.ccf";
  private static Map<String, ICacheManager> regions = new HashMap<String, ICacheManager>();
  private static Properties props;
  
  static {
	  props = new Properties();
	  boolean error = true;
	  
	  try {
		  props.load(CacheManagerFactory.class.getClassLoader().getResourceAsStream(cacheFile));
		  error = false;
	  } catch(Exception e) {
		  log.error("Error while loading cache.ccf file", e);
	  }
	  
	  if (error) {
		  	log.debug("Loading default cache configuration");
		  	//Define Cache Region
			props.put("jcs.region.$REGION$", "$REGION$_DC");
			props.put("jcs.region.$REGION$.cacheattributes", 
					"org.apache.jcs.engine.CompositeCacheAttributes");
			props.put("jcs.region.$REGION$.cacheattributes.MaxObjects", "0");
			props.put("jcs.region.$REGION$.cacheattributes.MemoryCacheName", 
			"org.apache.jcs.engine.memory.lru.LRUMemoryCache");
			
			//Indexed Disk Cache
			props.put("jcs.auxiliary.$REGION$_DC", 
					"org.apache.jcs.auxiliary.disk.indexed.IndexedDiskCacheFactory");
			props.put("jcs.auxiliary.$REGION$_DC.attributes", 
			"org.apache.jcs.auxiliary.disk.indexed.IndexedDiskCacheAttributes");
			props.put("jcs.auxiliary.$REGION$_DC.attributes.DiskPath", 
					"$REGION$_cache");
			props.put("jcs.auxiliary.$REGION$_DC.attributes.MaxPurgatorySize", "-1");
			props.put("jcs.auxiliary.$REGION$_DC.attributes.MaxKeySize", "1000000");
			props.put("jcs.auxiliary.$REGION$_DC.attributes.OptimizeAtRemoveCount", "300000");
			props.put("jcs.auxiliary.$REGION$_DC.attributes.OptimizeOnShutdown", "true");
			props.put("jcs.auxiliary.$REGION$_DC.attributes.MaxRecycleBinSize", "7500");
			props.put("jcs.auxiliary.$REGION$_DC.attributes.EventQueueType", "POOLED");
			props.put("jcs.auxiliary.$REGION$_DC.attributes.EventQueuePoolName", "disk_cache_event_queue");
			
			props.put("thread_pool.disk_cache_event_queue.useBoundary", "false");
			props.put("thread_pool.remote_cache_client.maximumPoolSize", "15");
			props.put("thread_pool.disk_cache_event_queue.minimumPoolSize", "1");
			props.put("thread_pool.disk_cache_event_queue.keepAliveTime", "1");
			props.put("thread_pool.disk_cache_event_queue.startUpSize", "1");
	  }
  }
  
/**
 * This method creates the Instance of the CacheManagerImpl class and Instantiate the class by passing the region name
 * The constructor of the CacheManagerImpl will get the JCS cache instance for the region passed. 
 * @param region
 * @return ICacheManager
 */
  
  public static ICacheManager getInstance(String region) throws CacheException {
		ICacheManager cache = regions.get(region);
		if (cache != null) {
			return cache;
		}

		CompositeCacheManager ccm = CompositeCacheManager.getUnconfiguredInstance();
		Properties ps = new Properties();
		
		Enumeration<Object> en = props.keys();
		String key;
		String value;
		
		while (en.hasMoreElements()) {
			key = (String) en.nextElement();
			value = props.getProperty(key);
			
			key = key.replace("$REGION$", region);
			value = value.replace("$REGION$", region);
			
			ps.put(key, value);
		}

		key = "jcs.auxiliary." + region + "_DC.attributes.DiskPath";
		value = AppUtils.getRegionAbsolutePath(region + "_cache");
		ps.setProperty(key, value);
		ccm.configure(ps);
		
		cache = new CacheManagerImpl(region);
		regions.put(region, cache);
		
		return cache;
  	}
  
  	public static void main(String[] args) throws Exception {
		ICacheManager cache = CacheManagerFactory.getInstance("down111111");
		cache.putSafe("abc", "1236666666666");
		
		Thread.sleep(1000 * 1);
		System.out.println(cache.get("abc"));
	}
}