package com.frame.cache;

import com.frame.exception.CacheException;
/**
 * Interface for that contains the cache methods,
 * Any class that implements this interface has to implement the cache methods
 * @author ksivagnanam
 *
 */
public interface ICacheManager 
{
   
   /**
   @param key
   @param value
   @throws com.farmers.peachtree.framework.exception.CacheException
   
    */
   public void putSafe(Object key, Object value)  throws CacheException;
   
   /**
   @param key
   @return java.lang.Object@throws 
   com.farmers.peachtree.framework.Exception.CacheException
   
    */
   public Object get(String key) throws CacheException;
   
   /**
   @param key
   @throws com.farmers.peachtree.framework.exception.CacheException
   
    */
   public void remove (Object key) throws CacheException;
   
   /**
    * 
    * @throws com.farmers.peachtree.framework.exception.CacheException
    */
   public void clear() throws CacheException;
   
  /**
   * 
   */
   public void dispose();
}
