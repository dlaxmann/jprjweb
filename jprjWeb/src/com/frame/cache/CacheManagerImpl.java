package com.frame.cache;

import org.apache.jcs.JCS;

import com.frame.exception.CacheException;
import com.frame.logging.Logger;
import com.frame.logging.LoggerFactory;

/**
 * This is the Cache Implementation class,which implement the ICacheManager
 * interface and provide implementation codes for the cache methods.
 * 
 * @author ksivagnanam
 * 
 */
public class CacheManagerImpl implements ICacheManager {
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory
			.getLog(CacheManagerImpl.class.getName());
	private JCS jcsCache;

	/**
	 * Default Constructor
	 */
	public CacheManagerImpl() {
	}

	/**
	 * The argumented constructor,This creates the JCS cache instance for the
	 * region passed.
	 * 
	 * @param region
	 */
	public CacheManagerImpl(String region) throws CacheException {
		try {
			jcsCache = JCS.getInstance(region);
		} catch (org.apache.jcs.access.exception.CacheException ex) {
			throw new CacheException(ex.getMessage(), ex.getCause());
		}
	}

	/**
	 * This method will put the Object into the JCS cache using key as
	 * Identifier.
	 * 
	 * @param key
	 * @param value
	 * @throws com.farmers.peachtree.framework.exception.CacheException
	 * 
	 */
	public void putSafe(Object key, Object value) throws CacheException {
		try {
			jcsCache.put(key, value);
		} catch (org.apache.jcs.access.exception.CacheException ex) {
			throw new CacheException(ex.getMessage(), ex.getCause());
		}
	}

	/**
	 * This method is used to get the Object from the JCS Cache by passing the
	 * Key
	 * 
	 * @param key
	 * @return java.lang.Object@throws
	 *         com.farmers.peachtree.framework.Exception.CacheException
	 * 
	 */
	public Object get(String key) throws CacheException {
		Object value = new Object();
		try {
			value = jcsCache.get(key);

		} catch (Exception ex) {
			throw new CacheException(ex.getMessage(), ex.getCause());
		}
		return value;
	}

	/**
	 * This method removes the Object from the JCS cache for the key passed
	 * 
	 * @param key
	 * @throws com.farmers.peachtree.framework.exception.CacheException
	 * 
	 */
	public void remove(Object key) throws CacheException {
		try {
			jcsCache.remove(key);
		} catch (org.apache.jcs.access.exception.CacheException ex) {
			log.error(ex.getMessage());
			throw new CacheException(ex.getMessage(), ex.getCause());
		}
	}

	/**
	 * This method removes all of the elements from a region.
	 * 
	 * @throws com.farmers.peachtree.framework.exception.CacheException
	 * 
	 */
	public void clear() throws CacheException {
		try {
			jcsCache.clear();
		} catch (org.apache.jcs.access.exception.CacheException e) {
			log.error(e.getMessage());
			throw new CacheException(e.getMessage(), e.getCause());
		}
	}

	/**
	 * 
	 */
	public void dispose() {
		jcsCache.dispose();
	}
}
