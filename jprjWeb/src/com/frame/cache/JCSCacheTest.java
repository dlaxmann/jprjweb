package com.frame.cache;

import java.io.IOException;
import java.util.Date;
import java.util.Properties;

import org.apache.jcs.JCS;
import org.apache.jcs.access.exception.CacheException;
import org.apache.log4j.Logger;

public class JCSCacheTest {
//private static Logger log = LoggerFactory.getLog(JCSCacheTest.class.getName());
	private static Logger log = Logger.getLogger(JCSCacheTest.class.getName());
	private static final String cachePrpFile = "com/frame/config/props/cache.ccf";
	private boolean error = true;
	
	
	public void loadCacheProps(){
		Properties prp = new Properties();
		try {
			prp.load(JCSCacheTest.class.getClassLoader().getResourceAsStream(cachePrpFile));
			error = false;
		} catch (IOException e) {
			log.error("Error occured while loading ccs properties file");
			e.printStackTrace();
		}
	}
	
	public static boolean setBlog(int bId, String author, Date date, String title, String content) {
		  BlogObject blog = new BlogObject(bId, author, date, title, content);
		  JCS blogCache = null;
		  String blogCacheRegion = "blogCacheRegion";
		  try {
		    blogCache = JCS.getInstance(blogCacheRegion);
		    blogCache.put(bId, blog);
		    return true;
		  } catch (CacheException ce) {
		    return false;
		  }
		}
	
	public static BlogObject getBlog(int id) {
		  BlogObject blog = null;
		  JCS blogCache = null;
		  String blogCacheRegion = "blogCacheRegion";
		  try {
		    blogCache = JCS.getInstance(blogCacheRegion);
		    blog = (BlogObject)blogCache.get(id);
		  } catch (CacheException ce) {
		    blog = null;
		  }
		  if (blog == null) {
			  blog = new BlogObject (1,"anchor",new Date(),"title","content");
			  setBlog(1,"anchor",new Date(),"title","content");
		  }

		  return blog;
		}
	
	public static void testCache(){
		try {
			JCSCacheTest test = new JCSCacheTest();
			test.loadCacheProps();
			JCS jcsCache = JCS.getInstance("region");
			String key="key1";
			String value = "value1";
			
			jcsCache.putSafe(key, value);
			
			String retrvDataFrmCach;
			retrvDataFrmCach = (String) jcsCache.get(key);
			System.out.println(retrvDataFrmCach);
			
			
			
		} catch (CacheException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		getBlog(1);
	}

}
