package com.frame.cache;

import java.io.Serializable;
import java.util.Date;

public class BlogObject  implements Serializable{
	private static final long serialVersionUID = 6392376146163510046L;
	  private int blogId;
	  private String author;
	  private Date date;
	  private String title;
	  private String content;

	  public BlogObject(int blogId, String author, Date date, String title, String content) {
	    this.blogId = blogId;
	    this.author = author;
	    this.date = date;
	    this.title = title;
	    this.content = content;
	  }

	  public int getBlogId() {
	    return this.blogId;
	  }

	  public String getAuthor() {
	    return this.author;
	  }

	  public Date getDate() {
	    return this.date;
	  }

	  public String getTitle() {
	    return this.title;
	  }

	  public String getContent() {
	    return this.content;
	  }
}
