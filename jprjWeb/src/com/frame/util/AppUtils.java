package com.frame.util;

import java.io.File;
import java.io.IOException;

import org.springframework.context.ApplicationContext;

public class AppUtils {
	private static ApplicationContext context;
	
	private AppUtils() {
		
	}
	
	public static void setContext(ApplicationContext context) {
		AppUtils.context = context;
	}
	
	public static ApplicationContext getContext() {
		return context;
	}
	
	public static Object getBean(String id) {
		return context.getBean(id);
	}
	
	public static File getWebinfPath() throws IOException {
		//return context.getResource("src").getFile();
		return new File("src");
	}
	
	public static String getRegionAbsolutePath(String region) {
		String filePath = null;
		try {
			File file = getWebinfPath();
			String cacheDir = AppConfig.getProperty("cache.dir");
			/*File cacheDirFile = new File(cacheDir);
			if (cacheDirFile.isAbsolute()) {
				file = new File(cacheDirFile.getAbsolutePath() 
						+ File.separator + region);
			} else {
				file = new File(file.getAbsolutePath() + File.separator 
						+ cacheDir + File.separator + region);
			}*/
			file = new File(file.getAbsolutePath() + File.separator 
					+ cacheDir + File.separator + region);
			filePath = file.getAbsolutePath();
		} catch(Exception e) {
			e.printStackTrace();
		}
		return filePath;
	}
	
	public static String getLayoutFileDir() {
		String filePath = null;
		try {
			File file = getWebinfPath();
			String layoutDir = AppConfig.getProperty("layouts.dir");
			File lf = new File(layoutDir);
			if (!lf.isAbsolute()) {
				file = new File(file.getAbsolutePath() + File.separator 
						+ layoutDir);
			} else {
				file = lf;
			}
			
			filePath = file.getAbsolutePath();
		} catch(Exception e) {
			e.printStackTrace();
		}
		return filePath;
	}
	
	public static String getOutputDir() {
		String filePath = null;
		try {
			File file = getWebinfPath();
			String outputDir = AppConfig.getProperty("output.dir");
			/*File of = new File(outputDir);
			if (!of.isAbsolute()) {
				file = new File(file.getAbsolutePath() + File.separator + outputDir);
			} else {
				file = of;
			}*/
			file = new File(file.getAbsolutePath() + File.separator + outputDir);
			if (!file.exists()) {
				file.mkdir();
			}
			filePath = file.getAbsolutePath();
		} catch(Exception e) {
			e.printStackTrace();
		}
		return filePath;
	}
	public static String getInputDir() {
		String filePath = null;
		try {
			File file = getWebinfPath();
			String inputDir = AppConfig.getProperty("input.dir");
			File of = new File(inputDir);
			if (!of.isAbsolute()) {
				file = new File(file.getAbsolutePath() + File.separator + inputDir);
			} else {
				file = of;
			}

			filePath = file.getAbsolutePath();
		} catch(Exception e) {
			e.printStackTrace();
		}
		return filePath;
	}
}
