package com.frame.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;
import org.springframework.context.ApplicationContext;

import com.frame.dao.BaseDAO;
import com.frame.dao.BaseDAOImpl;
import com.frame.dao.DAOManager;
import com.frame.dao.NotificationDAO;
import com.frame.logging.Logger;
import com.frame.logging.LoggerFactory;

public class HibernateUtil {

	private static final Logger log = LoggerFactory.getLog(HibernateUtil.class.getName());  
	public static SessionFactory sessionFactory;
	public static ApplicationContext appContext  ;
	
	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	
	public static Session getSession(){
		LoadContext lct = new LoadContext();		
		lct.laodContext();
		appContext = AppUtils.getContext();
		SessionFactory sessionFactory = (SessionFactory)appContext.getBean("sessionFactory");
		Session ss= sessionFactory.openSession();
		return ss;
	}
	
	static {
	    try {
	        //Create the SessionFactory from hibernate.cfg.xml
			Configuration config = new AnnotationConfiguration().configure();
			sessionFactory = config.buildSessionFactory();
			
			BaseDAO baseDAO = new BaseDAOImpl();
			baseDAO.setSessionFactory(sessionFactory);
			NotificationDAO notificationDAO = new NotificationDAO();
			notificationDAO.setBaseDAO(baseDAO);
			DAOManager daoManager = new DAOManager();
			daoManager.setBaseDAO(baseDAO);
			daoManager.setNotificationDAO(notificationDAO);
	  
	    } catch (Throwable ex) {	    	 
	    	log.error("Initial SessionFactory creation failed." + ex);	     
	        	//throw new ExceptionInInitializerError(ex);
	    }
	}
}