package com.frame.util;

import java.io.InputStream;
import java.util.Enumeration;
import java.util.Properties;

public class AppConfig {
	
	/** Configuration filename. */
	private static String configFile = "Appconfig.properties";
	
	/** Properties. */
	private static Properties props = null;

	/**
	Constructor.
	*/
	static {
		props = loadProperties();
	}

	/**
	This method loads the properties from the config file.
	@return Properties object with loaded properties.
	*/
	private static Properties loadProperties() {
		Properties props = null;
		InputStream is = null;
		try {
			is = AppConfig.class.getClassLoader().getResourceAsStream(configFile);
			
			props = new Properties();
			props.load(is);
			
			String filePrefix = props.getProperty("file.prefix");
			String fileQualifier = props.getProperty("file.qualifier");
			String fileExt = props.getProperty("file.ext");
			
			Enumeration<String> em = (Enumeration<String>) props.propertyNames();
			String key;
			String value;
			while (em.hasMoreElements()) {
				key = em.nextElement();
				if (key.endsWith(".outputfile")) {
					value = props.getProperty(key);					
					value = value.replace("{file.prefix}", filePrefix);
					value = value.replace("{file.qualifier}", fileQualifier);
					value = value.replace("{file.ext}", fileExt);
					
					props.setProperty(key, value);					
				}
			}
		} catch (Exception ex) {
			
		} finally {
			try {
				if (is != null) {
					is.close();
				}
			} catch (Exception ex) {
			}
		}

		return (props == null) ? new Properties() : props;
	}

	/**
	Constructor.
	*/
	private AppConfig() {
	}

	/**
	Reloads the properties from config file.
	*/
	public static void reload() {
		loadProperties();
	}

	/**
	Returns the value of the property with given name.
	@param name property name
	@return value of property with given name	
	*/
	public static String getProperty(String name) {
		return props.getProperty(name);
	}

	/**
	Returns the value of the property with given name.
	@param name property name
	@param defaultValue default value for property
	@return value of property with given name	
	*/
	public static String getProperty(String name, String defaultValue) {
		return props.getProperty(name, defaultValue);
	}

	/**
	Returns Enumeration of property names.
	@return Enumeration object containing property names
	*/
	public static Enumeration getPropertyNames() {
		return props.elements();
	}

	/**
	Main method.
	@param args String[] for arguments.
	*/
	public static void main(String[] args) {
		
	}
}
