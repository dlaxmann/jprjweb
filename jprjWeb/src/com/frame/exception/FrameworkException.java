package com.frame.exception;

import java.util.HashMap;

import com.frame.logging.Logger;
import com.frame.logging.LoggerFactory;


@SuppressWarnings("unchecked")
public class FrameworkException extends BaseException
{

  private static final long serialVersionUID = 2260015439901445612L;

  static Logger log = LoggerFactory.getLog(FrameworkException.class
      .getName());

  public FrameworkException()
  {
    super();
  }

  public FrameworkException(String message)
  {
    super(message);
  }

  public FrameworkException(Throwable cause)
  {
    super(cause);
  }

  public FrameworkException(String errorMsg, Throwable cause)
  {
    super(errorMsg, cause);
  }

  public FrameworkException(String errorMsg, String p_errorCode)
  {
    super(errorMsg, p_errorCode);
  }

  public FrameworkException(String errorMsg, String p_errorCode, Throwable cause)
  {
    super(errorMsg, p_errorCode, cause);
  }

  public FrameworkException(String errorMsg, String p_errorCode,
      HashMap p_KeyValuepair)
  {
    super(errorMsg, p_errorCode, p_KeyValuepair);
  }

  public FrameworkException(String errorMsg, String p_errorCode,
      HashMap p_KeyValuepair, Throwable cause)
  {
    super(errorMsg, p_errorCode, p_KeyValuepair, cause);
  }
}
