package com.frame.exception;

import java.util.HashMap;
import java.util.Map;

/**
 * This class contains technical error codes and their corresponding error messages.
 * @author Sohail, CSC
 * 
 */
@SuppressWarnings("unchecked")
public class TechnicalErrorCodes
{

	@SuppressWarnings("unchecked")
  private static Map TECHNICAL_CODES_MESSAGES = new HashMap();

  /**
   * As a service
   *
   */

  /** Unexpected application error */
	public static final String TE_ERR_UNKOWN = "30001";
  /** Technical error with detailmessage */
  public static final String TE_ERR_WITH_DETAIL_MESSAGE = "40001";
  /** Database error with detailmessage */
  public static final String TE_ERR_DB_DETAIL_MESSAGE = "40002";
  /** No network connection */
  public static final String TE_NO_CONNECTION = "40003";
  /** Mail could not be sent */
  public static final String TE_MAIL_NOT_SENT = "40004";
  /** for Transanction Exception*/
  public static final String TE_TRANSACTION_ERROR = "40005";
  /**  validation error */
  public static final String TE_VALIDATION_ERROR = "40006";


 /**
   * As a client
   *
   */

  /** Connection to server could not be created */
  public static final String TE_ERR_CON_CREATION = "40901";
  /** Service call failed */
  public static final String TE_ERR_SERVICE_CALL_FAILED = "40902";
  /** Unknown Exception while calling server */
  public static final String TE_ERR_SERVICE_UNKNOWN = "40903";
  /** Unknown Returncode received from server */
  public static final String TE_SERVER_RC_UNKNOWN = "40904";
  
  
  /** 
   * key constants
   * 
   */
  
  /** message key */
  public static final String KEY_MESSAGE = "message";
  
  /** trace key */
  public static final String KEY_TRACE = "trace";

  /** message key */
  public static final String KEY_DETAIL = "detail";

  /** field name key */
  public static final String KEY_FIELD = "field";
  
  /** value key */
  public static final String KEY_VALUE = "value";
  
 
  
  // class must not be instantiated
  private TechnicalErrorCodes()
  {
    super();
  }

	// mapping between error codes and messages
	static
	{
    TECHNICAL_CODES_MESSAGES.put(TE_ERR_UNKOWN, "Unexpected application error");
    TECHNICAL_CODES_MESSAGES.put(TE_ERR_WITH_DETAIL_MESSAGE,"Failed to create client connection");
    TECHNICAL_CODES_MESSAGES.put(TE_ERR_DB_DETAIL_MESSAGE, "Database error with detailmessage");
    TECHNICAL_CODES_MESSAGES.put(TE_NO_CONNECTION, "No network connection");
    TECHNICAL_CODES_MESSAGES.put(TE_MAIL_NOT_SENT, "Mail could not be sent");
    TECHNICAL_CODES_MESSAGES.put(TE_ERR_CON_CREATION, "Connection to server could not be created");
    TECHNICAL_CODES_MESSAGES.put(TE_ERR_SERVICE_CALL_FAILED, "Could not call Service");
    TECHNICAL_CODES_MESSAGES.put(TE_ERR_SERVICE_UNKNOWN, "Unknown Exception while calling server");
    TECHNICAL_CODES_MESSAGES.put(TE_SERVER_RC_UNKNOWN, "Unknown Returncode received from Service");
    TECHNICAL_CODES_MESSAGES.put(TE_TRANSACTION_ERROR, "Transaction failure");
    TECHNICAL_CODES_MESSAGES.put(TE_VALIDATION_ERROR, "Validation error occured");
    }

  /**
   * retrieve Message for errorCode
   * 
   * @param errorCode
   * @return String
   */
	public static String getErrorMessage(String errorCode)
	{
		return (String) TECHNICAL_CODES_MESSAGES.get(errorCode);
	}

}
