package com.frame.exception;

import java.util.HashMap;

import com.frame.logging.Logger;
import com.frame.logging.LoggerFactory;

/**
 * @author Sohail
 * 
 */

@SuppressWarnings("unchecked")
public class CacheException extends FrameworkException
{

  private static final long serialVersionUID = 3042424000642726750L;

  static Logger log = LoggerFactory
      .getLog(CacheException.class.getName());

  /**
   * Represents the original exception that triggered this exception. Might
   * possibly contain implementation specific exception.
   */
  private Throwable cause;

  public CacheException()
  {
    super();
  }

  /**
   * Creates an instance of this class with the specified error message and root
   * cause.
   * 
   * @param message A descriptive error message indicating the failure.
   * @param cause Root cause that triggered this exception.
   */
  public CacheException(String message, Throwable cause)
  {
    super(message, cause);

  }

  public CacheException(Throwable cause)
  {
    super(cause);
  }

  public CacheException(String errorMsg, String p_errorCode)
  {
    super(errorMsg, p_errorCode);
  }

  public CacheException(String errorMsg, String p_errorCode, Throwable cause)
  {
    super(errorMsg, p_errorCode, cause);
  }

  public CacheException(String errorMsg, String p_errorCode,
      HashMap p_KeyValuepair)
  {
    super(errorMsg, p_errorCode, p_KeyValuepair);
  }

  public CacheException(String errorMsg, String p_errorCode,
      HashMap p_KeyValuepair, Throwable cause)
  {
    super(errorMsg, p_errorCode, p_KeyValuepair, cause);
  }

  /**
   * Gets the root cause that triggered this exception. This could be
   * implementation specific exception that needs to encapsulated.
   * 
   * @return The root cause that triggered this exception.
   */
  public Throwable getCause()
  {
    return cause;
  }

  /**
   * Prints the stack trace of this exception including that of root cause.
   */
  public void printStackTrace()
  {
    super.printStackTrace();
    if (cause != null)
    {
      System.err.println("<< Root Cause >>");
      cause.printStackTrace();
    }
  }
}
