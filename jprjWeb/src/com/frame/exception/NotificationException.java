package com.frame.exception;

import java.util.HashMap;

import com.frame.logging.Logger;
import com.frame.logging.LoggerFactory;

@SuppressWarnings("unchecked")
public class NotificationException extends FrameworkException 
{
	
  private static final long serialVersionUID = 3136340649426406052L;

  static Logger log = LoggerFactory.getLog(NotificationException.class.getName());
   
  /**
	 * Represents the original exception that triggered this exception. Might
	 * possibly contain implementation specific exception.
	 */
  private Throwable cause;


	  public NotificationException()
	  {
	    super();
	  }
	  
/**
	 * Creates an instance of this class with the specified error message and
	 * root cause.
	 * 
	 * @param message
	 *            A descriptive error message indicating the failure.
	 * @param cause
	 *            Root cause that triggered this exception.
	 */
	  public NotificationException(String message, Throwable cause)
	  {
	    super(message, cause);

	  }

	  public NotificationException(Throwable cause)
	  {
	    super(cause);
	  }

	  public NotificationException(String errorMsg, String p_errorCode)
	  {
	    super(errorMsg, p_errorCode);
	  }

	  public NotificationException(String errorMsg, String p_errorCode, Throwable cause)
	  {
	    super(errorMsg, p_errorCode, cause);
	  }

	  public NotificationException(String errorMsg, String p_errorCode,
	      HashMap p_KeyValuepair)
	  {
	    super(errorMsg, p_errorCode, p_KeyValuepair);
	  }

	  public NotificationException(String errorMsg, String p_errorCode,
	      HashMap p_KeyValuepair, Throwable cause)
	  {
	    super(errorMsg, p_errorCode, p_KeyValuepair, cause);
	  }


	/**
	 * Gets the root cause that triggered this exception. This could be
	 * implementation specific exception that needs to encapsulated.
	 * 
	 * @return The root cause that triggered this exception.
	 */
	public Throwable getCause() {
		return cause;
	}

	/**
	 * Prints the stack trace of this exception including that of root cause.
	 */
	public void printStackTrace() {
		super.printStackTrace();
		if (cause != null) {
			System.err.println("<< Root Cause >>");
			cause.printStackTrace();
		}
	}
}
