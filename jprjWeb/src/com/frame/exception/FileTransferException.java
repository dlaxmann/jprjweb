package com.frame.exception;

import java.util.HashMap;

import com.frame.logging.Logger;
import com.frame.logging.LoggerFactory;

@SuppressWarnings("unchecked")
public class FileTransferException extends FrameworkException
{

  private static final long serialVersionUID = -6363944008170330451L;

  static Logger log = LoggerFactory.getLog(FileTransferException.class.getName());

  /**
   * Represents the original exception that triggered this exception. Might
   * possibly contain implementation specific exception.
   */
  private Throwable cause;

  public FileTransferException()
  {
    super();
  }

  /**
   * Creates an instance of this class with the specified error message and root
   * cause.
   * 
   * @param message A descriptive error message indicating the failure.
   * @param cause Root cause that triggered this exception.
   */
  public FileTransferException(String message, Throwable cause)
  {
    super(message, cause);

  }

  public FileTransferException(Throwable cause)
  {
    super(cause);
  }

  public FileTransferException(String errorMsg, String p_errorCode)
  {
    super(errorMsg, p_errorCode);
  }

  public FileTransferException(String errorMsg, String p_errorCode, Throwable cause)
  {
    super(errorMsg, p_errorCode, cause);
  }

  public FileTransferException(String errorMsg, String p_errorCode,
      HashMap p_KeyValuepair)
  {
    super(errorMsg, p_errorCode, p_KeyValuepair);
  }

  public FileTransferException(String errorMsg, String p_errorCode,
      HashMap p_KeyValuepair, Throwable cause)
  {
    super(errorMsg, p_errorCode, p_KeyValuepair, cause);
  }

  /**
   * Gets the root cause that triggered this exception. This could be
   * implementation specific exception that needs to encapsulated.
   * 
   * @return The root cause that triggered this exception.
   */
  public Throwable getCause()
  {
    return cause;
  }

  /**
   * Prints the stack trace of this exception including that of root cause.
   */
  public void printStackTrace()
  {
    super.printStackTrace();
    if (cause != null)
    {
      System.err.println("<< Root Cause >>");
      cause.printStackTrace();
    }
  }
}
