package com.frame.exception;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

import com.frame.logging.Logger;
import com.frame.logging.LoggerFactory;

/**
 * Simple utilities to return the stack trace of an
 * exception as a String.
 * @author Sohail, CSC
 */
public class StacktraceUtil
{
	static Logger log = LoggerFactory.getLog(StacktraceUtil.class.getName());
  /**
   * Simple toString Converter
   * 
   * @param aThrowable
   * @return String stacktrace
   */
  public static String getStackTrace(Throwable aThrowable)
  {
    final Writer result = new StringWriter();
    final PrintWriter printWriter = new PrintWriter(result);
    aThrowable.printStackTrace(printWriter);
    return result.toString();
  }

}
