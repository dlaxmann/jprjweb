package com.frame.exception;

import java.util.HashMap;

import com.frame.logging.Logger;
import com.frame.logging.LoggerFactory;

/**
 * @author Sohail
 * 
 */

@SuppressWarnings("unchecked")
public class BaseException extends Exception
{

  private static final long serialVersionUID = 4027572284001205757L;

  private String error;

  /** errorCode of exception */
  protected String errorCode;

  /** map to store all key value pairs */
  protected HashMap keyValuePair;

  static Logger log = LoggerFactory.getLog(BaseException.class.getName());

  public BaseException()
  {
    super();
  }

  /**
   * @param error
   * 
   */

  public BaseException(String errorMsg)
  {
    super(errorMsg);
    this.keyValuePair = new HashMap();
    this.keyValuePair.put(TechnicalErrorCodes.KEY_MESSAGE, errorMsg);
    this.keyValuePair.put(TechnicalErrorCodes.KEY_TRACE, StacktraceUtil
        .getStackTrace(this));
    handleException(errorMsg, this);
    
  }

  public BaseException(Throwable cause)
  {
    super(cause);
  }

  public BaseException(String errorMsg, Throwable cause)
  {
    super(errorMsg, cause);
    this.keyValuePair = new HashMap();
    this.keyValuePair.put(TechnicalErrorCodes.KEY_MESSAGE, errorMsg);
    this.keyValuePair.put(TechnicalErrorCodes.KEY_TRACE, StacktraceUtil
        .getStackTrace(this));
    handleException(errorMsg, this);

  }

  public BaseException(String errorMsg, String p_errorCode)
  {
    super(errorMsg);
    this.errorCode = p_errorCode;
    this.keyValuePair = new HashMap();
    this.keyValuePair.put(TechnicalErrorCodes.KEY_MESSAGE, errorMsg);
    this.keyValuePair.put(TechnicalErrorCodes.KEY_TRACE, StacktraceUtil
        .getStackTrace(this));
    handleException(errorMsg, this);
  }

  public BaseException(String errorMsg, String p_errorCode, Throwable cause)
  {
    super(errorMsg, cause);
    this.errorCode = p_errorCode;
    this.keyValuePair = new HashMap();
    this.keyValuePair.put(TechnicalErrorCodes.KEY_MESSAGE, errorMsg);
    this.keyValuePair.put(TechnicalErrorCodes.KEY_TRACE, StacktraceUtil
        .getStackTrace(this));
    handleException(errorMsg, this);
  }

  public BaseException(String errorMsg, String p_errorCode,
      HashMap p_KeyValuepair)
  {
    super(errorMsg);
    this.errorCode = p_errorCode;
    this.keyValuePair = p_KeyValuepair;
    this.keyValuePair.put(TechnicalErrorCodes.KEY_MESSAGE, errorMsg);
    this.keyValuePair.put(TechnicalErrorCodes.KEY_TRACE, StacktraceUtil
        .getStackTrace(this));
    handleException(errorMsg, this);
  }

  public BaseException(String errorMsg, String p_errorCode,
      HashMap p_KeyValuepair, Throwable cause)
  {
    super(errorMsg, cause);
    this.errorCode = p_errorCode;
    this.keyValuePair = p_KeyValuepair;
    this.keyValuePair.put(TechnicalErrorCodes.KEY_MESSAGE, errorMsg);
    if (cause != null)
    {
      this.keyValuePair.put(TechnicalErrorCodes.KEY_TRACE, StacktraceUtil
          .getStackTrace(cause));
    }
    else
    {
      this.keyValuePair.put(TechnicalErrorCodes.KEY_TRACE, "");
    }
    handleException(errorMsg, this);
  }

  /**
   * @return java.lang.String
   * 
   */
  public String getError()
  {
    return error;
  }

  /**
   * @param p_errorCode
   * @return the message belonging to the error code
   */
  public String getErrorMessage(String p_errorCode)
  {
    return TechnicalErrorCodes.getErrorMessage(p_errorCode);
  }

  /**
   * @return the message belonging to the error code
   */
  public String getErrorMessage()
  {
    return getErrorMessage(this.errorCode);
  }

  /**
   * Returns the value of field keyValuePair.
   * @return value of field keyValuePair.
   */
  public HashMap getKeyValuePair()
  {
    return this.keyValuePair;
  }

  /**
   * @param error
   * 
   */
  private void handleException(String error)
  {
	  log.error(error);
  }

  /**
   * @param error
   * @param t
   * 
   */
  private void handleException(String error, Throwable t)
  {
	  log.error(error,t);
  }
}
