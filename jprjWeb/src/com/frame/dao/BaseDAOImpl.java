package com.frame.dao;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.app.core.app.bean.Student;
import com.frame.exception.DataAccesException;
import com.frame.logging.Logger;
import com.frame.logging.LoggerFactory;

public class BaseDAOImpl implements BaseDAO {
	private static final Logger log = LoggerFactory.getLog(BaseDAOImpl.class.getName());
	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void save(Object object) throws DataAccesException {
		Session session = null;
		Transaction t = null;
		try {
			session = sessionFactory.openSession();
			t = session.beginTransaction();
			session.save(object);
			t.commit();
		} catch(Exception e) {
			t.rollback();
			throw new DataAccesException(e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}
	
	public void save(List items) throws DataAccesException {
		Session session = null;
		Transaction t = null;
		try {
			session = sessionFactory.openSession();
			t = session.beginTransaction();
			
			for (Object object: items) {
				session.save(object);
			}
			
			t.commit();
		} catch(Exception e) {
			t.rollback();
			throw new DataAccesException(e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}
	
	
	public void saveOrUpdate(Object object) throws DataAccesException {
		Session session = null;
		Transaction t = null;
		try {
			session = sessionFactory.openSession();
			t = session.beginTransaction();
			session.saveOrUpdate(object);
			t.commit();
		} catch(Exception e) {
			t.rollback();
			throw new DataAccesException(e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}
	
	
	public List<Student> getRegUserForDay(Date d1, Date d2){
		return null;
		
	}
	
	
	
	public void delete(Object object) throws DataAccesException {
		Session session = null;
		Transaction t = null;
		try {
			session = sessionFactory.openSession();
			t = session.beginTransaction();
			session.delete(object);
			t.commit();
		} catch(Exception e) {
			t.rollback();
			throw new DataAccesException(e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}
	
	public Object getObject(Class clazz, Serializable id) throws DataAccesException {
		Session session = null;
		Object object = null;
		try {
			session = sessionFactory.openSession();
			object = session.get(clazz, id);
		} catch(Exception e) {
			throw new DataAccesException(e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return object;
	}
	
	
	public List getList(String hqlQuery, List<Object> parameters) throws DataAccesException {
		Session session = null;
		List list = null;
		try {
			session = sessionFactory.openSession();
			Query query = session.createQuery(hqlQuery);
			int index = 0;
			if (parameters != null) {
				for (Object object: parameters) {
					setObject(query, index++, object);
				}
			}
			list = query.list();
		} catch(Exception e) {
			throw new DataAccesException(e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return list;
	}

	
	public List getList(String hqlQuery, Map<String, Object> parameters) throws DataAccesException {
		Session session = null;
		List list = null;
		try {
			session = sessionFactory.openSession();
			Query query = session.createQuery(hqlQuery);
			int index = 0;
			if (parameters != null) {
				for (String key: parameters.keySet()) {
					setObject(query, index++, parameters.get(key));
				}
			}
			list = query.list();
		} catch(Exception e) {
			throw new DataAccesException(e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return list;
	}
	
	
	public List getListByNamedQuery(String queryName, List<Object> parameters) throws DataAccesException {
		Session session = null;
		List list = null;
		try {
			session = sessionFactory.openSession();
			Query query = session.getNamedQuery(queryName);
			int index = 0;
			if (parameters != null) {
				for (Object object: parameters) {
					setObject(query, index++, object);
				}
			}
			list = query.list();
		} catch(Exception e) {
			throw new DataAccesException(e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return list;
	}
	
	
	public List getListByNamedQuery(String queryName, Map<String, Object> parameters) throws DataAccesException {
		Session session = null;
		List list = null;
		try {
			session = sessionFactory.openSession();
			Query query = session.getNamedQuery(queryName);
			int index = 0;
			if (parameters != null) {
				for (String key: parameters.keySet()) {
					setObject(query, index++, parameters.get(key));
				}
			}
			list = query.list();
		} catch(Exception e) {
			throw new DataAccesException(e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return list;
	}
	
	private void setObject(Query query, int index, Object object) {
		if (object instanceof Integer) {
			query.setInteger(index, (Integer) object);
		} else if (object instanceof BigInteger) {
			query.setBigInteger(index, (BigInteger) object);
		} else if (object instanceof String) {
			query.setString(index, (String) object);
		} else if (object instanceof Long) {
			query.setLong(index, (Long) object);
		} else if (object instanceof BigDecimal) {
			query.setBigDecimal(index, (BigDecimal) object);
		} else if (object instanceof Boolean) {
			query.setBoolean(index, (Boolean) object);
		} else if (object instanceof Double) {
			query.setDouble(index, (Double) object);
		} else if (object instanceof Float) {
			query.setFloat(index, (Float) object);
		} else if (object instanceof Date) {
			query.setDate(index, (Date) object);
		} else if (object instanceof Calendar) {
			query.setCalendar(index, (Calendar) object);
		} else if (object instanceof Short) {
			query.setShort(index, (Short) object);
		} else if (object instanceof Byte) {
			query.setByte(index, (Byte) object);
		} else if (object instanceof Character) {
			query.setCharacter(index, (Character) object);
		}
	}
	
	private void setObject(Query query, String key, Object object) {
		if (object instanceof Integer) {
			query.setInteger(key, (Integer) object);
		} else if (object instanceof BigInteger) {
			query.setBigInteger(key, (BigInteger) object);
		} else if (object instanceof String) {
			query.setString(key, (String) object);
		} else if (object instanceof Long) {
			query.setLong(key, (Long) object);
		} else if (object instanceof BigDecimal) {
			query.setBigDecimal(key, (BigDecimal) object);
		} else if (object instanceof Boolean) {
			query.setBoolean(key, (Boolean) object);
		} else if (object instanceof Double) {
			query.setDouble(key, (Double) object);
		} else if (object instanceof Float) {
			query.setFloat(key, (Float) object);
		} else if (object instanceof Date) {
			query.setDate(key, (Date) object);
		} else if (object instanceof Calendar) {
			query.setCalendar(key, (Calendar) object);
		} else if (object instanceof Short) {
			query.setShort(key, (Short) object);
		} else if (object instanceof Byte) {
			query.setByte(key, (Byte) object);
		} else if (object instanceof Character) {
			query.setCharacter(key, (Character) object);
		}
	}
}
