package com.frame.dao;


public class DAOManager {
	private static BaseDAO baseDAO;
	private static NotificationDAO notificationDAO;
	
	public void setBaseDAO(BaseDAO baseDAO) {
		DAOManager.baseDAO = baseDAO;
	}

	public static BaseDAO getBaseDAO() {
		return baseDAO;
	}

	public void setNotificationDAO(NotificationDAO notificationDAO) {
		DAOManager.notificationDAO = notificationDAO;
	}

	public static NotificationDAO getNotificationDAO() {
		return notificationDAO;
	}
}
