package com.frame.dao;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.hibernate.SessionFactory;

import com.app.core.app.bean.Student;
import com.frame.exception.DataAccesException;

public interface BaseDAO {
	public void setSessionFactory(SessionFactory sessionFactory);
	public SessionFactory getSessionFactory();
	public void save(Object object) throws DataAccesException;
	public void save(List items) throws DataAccesException;
	public void saveOrUpdate(Object object) throws DataAccesException;
	public void delete(Object object) throws DataAccesException;
	public Object getObject(Class clazz, Serializable id) throws DataAccesException;
	public List getList(String hqlQuery, List<Object> parameters) throws DataAccesException;
	public List getList(String hqlQuery, Map<String, Object> parameters) throws DataAccesException;
	public List getListByNamedQuery(String queryName, List<Object> parameters) throws DataAccesException;
	public List getListByNamedQuery(String queryName, Map<String, Object> parameters) throws DataAccesException;
	
	public List<Student> getRegUserForDay(Date d1, Date d2);
	
}
