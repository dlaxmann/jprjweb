package com.frame.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.app.domain.bean.Stock;
import com.frame.dao.StockDao;
import com.frame.util.CustomHibernateDaoSupport;

@Repository("stockDao")
public class StockDaoImpl extends CustomHibernateDaoSupport implements StockDao{
	
	public void save(Stock stock){
		getHibernateTemplate().save(stock);
	}
	
	public void update(Stock stock){
		getHibernateTemplate().update(stock);
	}
	
	public void delete(Stock stock){
		getHibernateTemplate().delete(stock);
	}
	
	public Stock findByStockCode(String stockCode){
		List list = getHibernateTemplate().find("from Stock where stockCode=?",stockCode);
		if(list != null && list.size()>0){
			return (Stock)list.get(0);
		}
		
		return null;
	}

}