package com.frame.dao;

import java.util.ArrayList;
import java.util.List;

import javax.management.Notification;

import com.frame.exception.DataAccesException;
import com.frame.exception.NotificationException;
import com.frame.logging.Logger;
import com.frame.logging.LoggerFactory;

public class NotificationDAO {
	private static final Logger log = LoggerFactory
			.getLog(NotificationDAO.class.getName());

	private static final String NOTIFICATIONTOLIST_SQL = "SELECT toList FROM NotificationtoList where id=";
	private static final String NOTIFICATIONTXT_SQL = "SELECT MsgTxt FROM NotificationTxt  where id=";

	private BaseDAO baseDAO;
	
	public void setBaseDAO(BaseDAO baseDAO) {
		this.baseDAO = baseDAO;
	}

	public BaseDAO getBaseDAO() {
		return baseDAO;
	}
	
	/**
	 * @param id
	 * @return String
	 */
	public String getNotificationTxt(String id) throws NotificationException {
		log.info(" getNotificationTxt Method");
		String txtMsg = "";

		List notiftxt;
		try {
			notiftxt = baseDAO.getList(NOTIFICATIONTXT_SQL + id, (List<Object>)null);
		} catch (DataAccesException e) {
			throw new NotificationException(e.getMessage(), e);
		}
		if (notiftxt.size() > 0) {
			txtMsg =(String)notiftxt.get(0);
		}
		
		return txtMsg;
	}

	/**
	 * @param id
	 * @return List
	 */
	public List getNotificationList(String id) throws NotificationException {
		log.info(" getNotificationList Method");
		ArrayList toList = new ArrayList();

		List notiftxt;
		try {
			notiftxt = baseDAO.getList(NOTIFICATIONTOLIST_SQL + id, (List<Object>)null);
		} catch (DataAccesException e) {
			throw new NotificationException(e.getMessage(), e);
		}
		for (int i = 0; i < notiftxt.size(); i++) {
			toList.add((String) notiftxt.get(i));
		}

		return toList;
	}

	/**
	 * @param notification
	 */
	/*public void buildNotification(Notification notification) throws NotificationException {
		notification
				.setNotificationTxt(getNotificationTxt(notification.getId()));
		notification.setNotificationList(getNotificationList(notification
				.getId()));
	}*/
}
