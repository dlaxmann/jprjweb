package com.app.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class POCStringUtil {

	
	// PROGRAM -1
	 //All common characters like "&.-_/" are replaced by SPACES
	public static void getM503StringBuilder(String str){
		
		char  c1 = ' ';
		 StringBuffer sBuffer = new StringBuffer("");
		 int i =0;
		 String blank = " ";
		 for (char ch : str.toCharArray()){
				String stringChar = Character.toString(ch);
				 if (stringChar.equalsIgnoreCase("&")) {
					 stringChar = blank;
				}else if (stringChar.equalsIgnoreCase(".")) {
					 stringChar = blank;
				}else if (stringChar.equalsIgnoreCase("-")) {
						 stringChar = blank;
				}else if (stringChar.equalsIgnoreCase("_")) {
							 stringChar = blank;
				}else if (stringChar.equalsIgnoreCase("/")) {
					 stringChar = blank;
		}
				 sBuffer.append(stringChar);
				 i++;
		    }
		
		String st = sBuffer.toString();
		System.out.println("After Removing Speacial chars: "+st+ "and Length= "+st.length());
		chekAvilaBleString(st);
	}
	
	
	
	//2 79 Fields
	public static String  chekAvilaBleString(String veryfyString){
		
		Map<String,String> mapp = new HashMap<String,String>();
		ArrayList<String> byWords = new ArrayList<String>();
		
		String[] arr = veryfyString.split(" ");    
		 for ( String ss : arr) {
			 if(ss != null){
				 mapp.put(ss.trim(), ss.trim());
				 byWords.add(ss.trim());
			 }
		  }
		System.out.println("After Split___MapSize: "+mapp.size()+"   List Size: "+byWords.size());
		String[] listOfWords = {
				"P P",
				"M H P",
				"D ",
				"O ",
				"P ",
				"R ",
				"S ",
				"AT",
				"DE",
				"EL",
				"IN",
				"LA",
				"MH",
				"OF",
				"ON",
				"OR",
				"PK",
				"PO",
				"RR",
				"RT",
				"SP",
				"AND",
				"BOX",
				"DEL",
				"FOR",
				"HWY",
				"INC",
				"LAS",
				"LOT",
				"MHP",
				"MOB",
				"OFF",
				"POB",
				"PRK",
				"RIO",
				"SAN",
				"SOL",
				"THE",
				"TLR",
				"CAMP",
				"CASA",
				"CITY",
				"CLUB",
				"EAST",
				"HOME",
				"LAKE",
				"PALM",
				"PARK",
				"PRIV",
				"PROP",
				"WEST",
				"(THE)",
				"ACRES",
				"BEACH",
				"COURT",
				"MANOR",
				"NORTH",
				"PALMS",
				"ROUTE",
				"SOUTH",
				"SPACE",
				"VILLA",
				"DESERT",
				"INSIDE",
				"LIMITS",
				"MOBILE",
				"RANCHO",
				"SPACES",
				"TRAVEL",
				"VALLEY",
				"WITHIN",
				"COUNTRY",
				"ESTATES",
				"PRIVATE",
				"TRAILER",
				"PROPERTY",
				"CAMPGROUND",
				"RESIDENTIAL",
				"SUBDIVISION",
		};
		
		//ArrayList<String> wrdsList = (ArrayList<String>) Arrays.asList(listOfWords);
		List wrdsList = Arrays.asList(listOfWords);
		
		ArrayList<String> listOfWordings = new ArrayList<String>();
		String stringWord="";
		StringBuffer finalWordBuffer = new StringBuffer("");
		String finalWord="";
		char c=' ';
		List<String> stringss = new ArrayList<String>();
		
		
		StringBuffer sbf = new StringBuffer("");
		StringBuffer sbf1 = new StringBuffer("");
		String spaceText = null;
		String spaceText1 = null;
		int i = 1;
		for(String strr: byWords){
			if(strr != null && strr.length()>0){
				
				/*if(mapp.containsKey(strr)){
					for (char cc : strr.toCharArray())	    {
				    	sbf.append(" ");
				    }
				    spaceText = sbf.toString();
				  System.out.println("^^^^^^^^^spaceText length"+spaceText.length());
					mapp.put(strr,spaceText);
				}*/
				
				strr = strr.trim();
				
				if(wrdsList.contains(strr)){
					System.out.println("___The Matched String: "+strr+" Length: "+strr.length());
					sbf1 = new StringBuffer("");
					for (char cc : strr.toCharArray())	{
				    	sbf1.append(" ");
				    	 spaceText1 = sbf1.toString();
				    }
					strr = spaceText1;
				}
				i++;
				if(i>1){
					System.out.println("in if i>1");
					String spc = " ";
					sbf.append(spc);
				}
				sbf.append(strr);
				System.out.println("Un Matched String "+strr+" Length: "+strr.length());
			}
		}
		
		System.out.println("The Final Output: "+sbf.toString());
		System.out.println("======="+sbf.toString().length());
		return finalWord;
	}
	
	
	
	
	//MG20M504:       PROGRAM-2
		//This is used to split the text field into 3 separate fields of 20 bytes each. 
		//2. If the first field is SPACES, then the 2nd field is moved to first field, 3rd to second field and 
		//if there is any remaining field, it is moved to third field and is sent back to calling program. 
		public static void splitStringIntoBytes(String inputString){
			
		    byte[] bytes = inputString.getBytes();
		    System.out.println("Text : " + inputString.length());
		    System.out.println("Text [Byte Format] : " + bytes.length);
		    
		    String strAftrRmvngSpaces ="";
		    String string1 ="";
		    String string2 ="";
		    String string3 ="";
		    StringBuffer sbf = new StringBuffer("");
		    
		    for (char c : inputString.toCharArray())	    {
		        if (!Character.isSpace(c)) {
		        	sbf.append(c);
		        }
		    }
		    
		    strAftrRmvngSpaces = sbf.toString();
		    System.out.println("string ="+strAftrRmvngSpaces+"__" +strAftrRmvngSpaces.length());
	        
	        if(strAftrRmvngSpaces.length() >20){
	        	string1 = strAftrRmvngSpaces.substring(0, 20);
	        }
	        if(strAftrRmvngSpaces.length() >40){
	        	string2 = strAftrRmvngSpaces.substring(20, 40);
	        }
	        if(strAftrRmvngSpaces.length() >60){
	        	string3 = strAftrRmvngSpaces.substring(40, 60);
	        } 
	        if(strAftrRmvngSpaces.length() < 20){
	        	string1 = strAftrRmvngSpaces;
	        }
	       
	        System.out.println("string1 ="+string1+"__" +string1.length());
	        System.out.println("string2 ="+string2+"__"  +string2.length());
	        System.out.println("string3 ="+string3+"__"  +string3.length());
		    
		}
		
		
		
		
		
		//MG20M505:    PROGRAM - 3
				//This is used to compress special characters from text field.
				//2. If the first field is SPACES, then the 2nd field is moved to first field, 3rd to second field and 
				//if there is any remaining field, it is moved to third field and is sent back to calling program. 
				public static void verifyStringForMG20M505(String inputString){
					
				    byte[] bytes = inputString.getBytes();
				    System.out.println("Input String Lenght: " + inputString.length());
				    System.out.println("Input String [Byte Format] : " + bytes.length);
				    
				    String strAftrremvlSplChrs ="";
				    StringBuffer sbf = new StringBuffer("");
				    
				    for (char c : inputString.toCharArray())	    {
				       /* if (Character.isDigit(c) || Character.isAlphabetic(c)) {
				        	sbf.append(c);
				        }*/
				    }
				    
				    strAftrremvlSplChrs = sbf.toString();
				    System.out.println("strAftrremvlSplChrs ="+strAftrremvlSplChrs+"__" +strAftrremvlSplChrs.length());
				}
				
		
		
		
		
	
	public static boolean chekInputValNumeric(String str){
		for (char c : str.toCharArray())	    {
	        if (!Character.isDigit(c)) return false;
	    }
	    return true;
	}
	
	
	
	public static void main(String args[]){
		
		/*boolean b = false;
		String stt = "a";
		b = chekInputValNumeric(stt);
		System.out.println(b);*/
		
		
		//while testing....comment the other two method so that it will be easy to test
		
		//Program -1 
		//String str = "Deploying & application.in - domain_ failed/& .-_/";
		
		//String str = "SUNRISE TRAILER PARK %% WEB &&";
		//String str = "SUNRISE TRAILER PARK && WEB &&ABC";
		String str = "SUNRISE TRAILER PARK && WEB &&ABC ESTATES";
		System.out.println("String1: "+str);
		System.out.println("Initital String length: "+str.length());
		getM503StringBuilder(str);
		
		String st="PROP";
		//chekAvilaBleString(st);
		
		
		/*//Program -2
		String inputString = "This is an example to split word into bytes abcdefghij";
		splitStringIntoBytes(inputString);
		
		//Program -3
		String inputString1 = "chek valid String+-...";
		verifyStringForMG20M505(inputString1);
		*/
		
		
	}
}
