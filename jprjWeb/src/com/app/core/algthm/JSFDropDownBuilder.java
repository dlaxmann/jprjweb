package com.app.core.algthm;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class JSFDropDownBuilder {
	
	
	public static void buildStringValForDropDownItems(){
		File file = new File("files/skills.txt");
		StringBuffer sbf=  new StringBuffer();;
		BufferedReader bfrdr =  null;
		String lineOneText = null;
		
		try {
			bfrdr = new BufferedReader(new FileReader(file));
			while(((lineOneText = bfrdr.readLine())) != null){
			
			sbf.append("\"");
			if(lineOneText !=null)
			sbf.append(lineOneText.trim()+"\",");
			
			String item =  sbf.toString();
			System.out.println(item);
			sbf = new StringBuffer();
			}
			
			//writeToFile(notExistDmnList);
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public static void buildDropDownItems(){
		File file = new File("files/Items.txt");
		StringBuffer sbf=  new StringBuffer();;
		BufferedReader bfrdr =  null;
		String lineOneText = null;
		
		InetAddress inetAddress =null;
		List<String> notExistDmnList = new ArrayList<String>();
		try {
			bfrdr = new BufferedReader(new FileReader(file));
			while(((lineOneText = bfrdr.readLine())) != null){
			
			sbf.append("<f:selectItem itemValue=\"");
			sbf.append(lineOneText+"\"  ");
			sbf.append("itemLabel=\"");
			sbf.append(lineOneText+"\"/>");
			
			String item =  sbf.toString();
			System.out.println(item);
			sbf = new StringBuffer();
			notExistDmnList.add(item);
			}
			
			//writeToFile(notExistDmnList);
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	
	
	public static void writeToFile(List<String> dmnList){
		//File dmnFile = new File("domn/dmn3.txt");
		File dmnFile = new File("files/DropDownItmes.txt");
		//File dmnFile = new File("domn/dmn.txt");
		try {
			BufferedWriter bfwtr = null;
			bfwtr = new BufferedWriter(new FileWriter(dmnFile));
			StringBuffer sbf = new StringBuffer();
					bfwtr.write(sbf.append(System.getProperty("line.separator")).toString());
	
			
			bfwtr.flush();
			bfwtr.close();
		}catch(Exception e){
			
		}
		
		System.out.println("____Done written all domains to file");
	}
	
	public static void main(String[] args){
		
		//buildDropDownItems();
		buildStringValForDropDownItems();
	}
	
}
