package com.app.core.algthm;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;
//take words from file.. the add build name with each word and existance
//EXUCTE THIS FILE
public class DomainNameBuilderTest {

	
	
	public static void build1LtrDmn(String st){

		File file = new File("domn/1.txt");
		StringBuffer sbf=  new StringBuffer();
		BufferedReader bfrdr =  null;
		String lineOneText = null;
		
		InetAddress inetAddress =null;
		List<String> notExistDmnList = new ArrayList<String>();
		try {
			bfrdr = new BufferedReader(new FileReader(file));
			while(((lineOneText = bfrdr.readLine())) != null){
				
			String str = lineOneText.substring(0,1);
			sbf.append("www.");
			//sbf.append(str.trim()+"job");
			sbf.append(str.trim()+st);
			//sbf.append(str.trim()+"infoLabs");
			sbf.append(".com");
			String dmn =  sbf.toString();
			sbf = new StringBuffer();
			try{
			 inetAddress = InetAddress.getByName(dmn);
			}catch(UnknownHostException e){
				System.out.println(dmn+"_____This Domain not existed");
				notExistDmnList.add(dmn);
			} 
		}
			
			writeToFile(notExistDmnList);
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	
	}
	
	public static void build2LtrDmn(String st){

		File file = new File("domn/2.txt");
		StringBuffer sbf=  new StringBuffer();
		BufferedReader bfrdr =  null;
		String lineOneText = null;
		
		InetAddress inetAddress =null;
		List<String> notExistDmnList = new ArrayList<String>();
		try {
			bfrdr = new BufferedReader(new FileReader(file));
			while(((lineOneText = bfrdr.readLine())) != null){
				
			String str = lineOneText.substring(0,2);
			sbf.append("www.");
			//sbf.append(str.trim()+"job");
			//sbf.append(str.trim()+"tech");
			sbf.append(str.trim()+st);
			sbf.append(".com");
			String dmn =  sbf.toString();
			sbf = new StringBuffer();
			try{
			 inetAddress = InetAddress.getByName(dmn);
			}catch(UnknownHostException e){
				System.out.println(dmn+"_____This Domain not existed");
				notExistDmnList.add(dmn);
			} 
		}
			
			writeToFile(notExistDmnList);
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	
	}
	
	public static void build3LtrDmn(String st){
		File file = new File("domn/3.txt");
		StringBuffer sbf=  new StringBuffer();
		BufferedReader bfrdr =  null;
		String lineOneText = null;
		
		InetAddress inetAddress =null;
		List<String> notExistDmnList = new ArrayList<String>();
		try {
			bfrdr = new BufferedReader(new FileReader(file));
			while(((lineOneText = bfrdr.readLine())) != null){
				
			String str = lineOneText.substring(0,4);
			sbf.append("www.");
			//sbf.append(str.trim()+"job");
			//sbf.append(str.trim()+"tek");
			sbf.append(str.trim()+st);
			
			sbf.append(".com");
			String dmn =  sbf.toString();
			sbf = new StringBuffer();
			try{
			 inetAddress = InetAddress.getByName(dmn);
			}catch(UnknownHostException e){
				System.out.println(dmn+"_____This Domain not existed");
				notExistDmnList.add(dmn);
			} 
		}
			
			writeToFile(notExistDmnList);
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public static void build4LtrDmn(String st){
		File file = new File("domn/44.txt");
		
		BufferedReader bfrdr =  null;
		String lineOneText = null;
		InetAddress inetAddress =null;
		List<String> notExistDmnList = new ArrayList<String>();
		
		try {
			bfrdr = new BufferedReader(new FileReader(file));
			
			while(((lineOneText = bfrdr.readLine())) != null){
				if(lineOneText.trim().length() >4){
			String str = lineOneText.substring(0,4);
			System.out.println("___line:::"+lineOneText);
		
			StringBuffer sbff= new StringBuffer();
			sbff.append("www.");
			for(char c: lineOneText.toCharArray()){
				if(!Character.isSpace(c)){
					sbff.append(c);
					//System.out.println("________"+str);
					if(sbff.toString().length() ==8){
						sbff.append(st);
						sbff.append(".com");
						//System.out.println(sbff.toString());
						//stringList.add(sbff.toString());
						String domain = sbff.toString();
						sbff= new StringBuffer();
						sbff.append("www.");
						try{
							domain = removeCharAt(domain, 7);
							 inetAddress = InetAddress.getByName(domain);
							//System.out.println(inetAddress.getHostName()+"__"+inetAddress.getHostAddress());
							}catch(UnknownHostException e){
								System.out.println(domain+"_____This Domain not existed");
								notExistDmnList.add(domain);
							} 
					}
				}
			}

			}
			}
			System.out.println("_______dmns list: " +notExistDmnList.size());
			writeToFile(notExistDmnList);
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	
	public static String removeCharAt(String s, int pos) {
	      return s.substring(0, pos) + s.substring(pos + 1);
	   }
	
	
	
	public static void buil5LtrDmn(String st){
		File file = new File("domn/5.txt");
		File dmnFile = new File("domn/dmn.txt");
		StringBuffer sbf=  new StringBuffer();;
		BufferedReader bfrdr =  null;
		String lineOneText = null;
		
		InetAddress inetAddress =null;
		List<String> stringList = new ArrayList<String>();
		List<String> notExistDmnList = new ArrayList<String>();
		
		
		BufferedWriter bfwtr = null;
		
		try {
			bfrdr = new BufferedReader(new FileReader(file));
			bfwtr = new BufferedWriter(new FileWriter(dmnFile));
			
			while(((lineOneText = bfrdr.readLine())) != null){
			String str = lineOneText.substring(0,5);
			System.out.println("___line:::"+lineOneText);
		
			StringBuffer sbff= new StringBuffer();
			sbff.append("www.");
			for(char c: lineOneText.toCharArray()){
				if(!Character.isSpace(c)){
					sbff.append(c);
					if(sbff.toString().length() ==9){
						//sbff.append("job");
						sbff.append(st);
						
						sbff.append(".com");
						System.out.println(sbff.toString());
						stringList.add(sbff.toString());
						
						StringBuffer sb1= new StringBuffer();
						sb1.append(sbff.toString());
						bfwtr.write(sb1.append(System.getProperty("line.separator")).toString());
						sbff= new StringBuffer();
						sbff.append("www.");
						
					}
				}
			}
			
			
/*			sbf.append("www.");
			sbf.append(str.trim()+"job");
			sbf.append(".com");
			String dmn =  sbf.toString();
			System.out.println(dmn);
			sbf = new StringBuffer();
			 boolean isExist = false;
			int i =2;
			 
			 if(i ==3){
			try{
			
			 inetAddress = InetAddress.getByName(dmn);
			System.out.println(inetAddress.getHostName());
			System.out.println(inetAddress.getHostAddress());
			}catch(UnknownHostException e){
				System.out.println("......dmn not existed");
				notExistDmnList.add(inetAddress.getHostName());
			} 
		    
			}*/
			 
			}
			/*InetAddress[] inetAddress = InetAddress.getAllByName("job.com");
			System.out.println(inetAddress.length);*/
			
			bfwtr.flush();
			bfwtr.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("____dmn not existed list size:"+notExistDmnList.size());
	}
	
	
	public static void writeToFile(List<String> dmnList){
		File dmnFile = new File("site/2ilabs.txt");
		//File dmnFile = new File("domn/dmn4.txt");
		//File dmnFile = new File("domn/dmn.txt");
		try {
			BufferedWriter bfwtr = null;
			bfwtr = new BufferedWriter(new FileWriter(dmnFile));
			StringBuffer sbf = new StringBuffer();
			int i = 0;
			for (String string : dmnList) {
				sbf.append(string);
				sbf.append("        ");
				i++;
				if(i==4){
					bfwtr.write(sbf.append(System.getProperty("line.separator")).toString());
					i =0;
				}
				
			}
			
			bfwtr.flush();
			bfwtr.close();
		}catch(Exception e){
			
		}
		
		System.out.println("____Done written all domains to file");
	}
	
	
	
	public static String generateString(Random rng, String characters, int length) 	{
	    char[] text = new char[length];
	    for (int i = 0; i < length; i++) 	    {
	        text[i] = characters.charAt(rng.nextInt(characters.length()));
	    }
	    return new String(text);
	}
	
	public static void randomString2LtrDmn(){
		int length =2;
		String characters ="abcdefghijklmnopqrstuvwxyz";
		Random rng = new Random();
		StringBuffer sbf=  new StringBuffer();
		InetAddress inetAddress =null;
		List<String> notExistDmnList = new ArrayList<String>();
		
		for (int i = 0; i < 1000; i++) {
		String s =generateString(rng, characters, length);
			//unqStr.add(s);
		//	if(unqStr.size()>0){
			//	String myStringAtIndex = (String) unqStr.toArray()[unqStr.size()-1];
						sbf.append("www.");
						//sbf.append(s.trim());
						sbf.append(""+s.trim()+"scs");
						sbf.append(".com");
						String dmn =  sbf.toString();
						sbf = new StringBuffer();
						try{
						 inetAddress = InetAddress.getByName(dmn);
						 System.out.println(dmn);
						}catch(UnknownHostException e){
							System.out.println(dmn+"_____This Domain not existed");
							notExistDmnList.add(dmn);
						} 
				//	}
			}
		System.out.println("__________Done randomString2LtrDmn()");
	}



	public static void main(String[] args){
		//www.SIBExl.com
		//String st = "Bex";
		//String st = "bexl";
		//String st = "tech";
		
		String st = "blog";
		//build1LtrDmn(st);
		//build2LtrDmn(st);
		//build3LtrDmn(st);
		build4LtrDmn(st);
		//randomString2LtrDmn();
	}
}
