package com.app.core.algthm;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class DomainAlg {

	
	public static void build3LtrDmn(){
		File file = new File("domn/33.txt");
		StringBuffer sbf=  new StringBuffer();;
		BufferedReader bfrdr =  null;
		String lineOneText = null;
		
		InetAddress inetAddress =null;
		List<String> notExistDmnList = new ArrayList<String>();
		try {
			bfrdr = new BufferedReader(new FileReader(file));
			while(((lineOneText = bfrdr.readLine())) != null){
				
			String str = lineOneText.substring(0,4);
			sbf.append("www.");
			sbf.append(str.trim()+"job");
			sbf.append(".com");
			String dmn =  sbf.toString();
			System.out.println(dmn);
			sbf = new StringBuffer();
			
			try{
			 inetAddress = InetAddress.getByName(dmn);
			System.out.println(inetAddress.getHostName()+"__"+inetAddress.getHostAddress());
			}catch(UnknownHostException e){
				System.out.println(dmn+"_____This Domain not existed");
				notExistDmnList.add(inetAddress.getHostName());
			} 
		    
			}
			
			writeToFile(notExistDmnList);
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public static void build4LtrDmn(){
		File file = new File("domn/44.txt");
		
		BufferedReader bfrdr =  null;
		String lineOneText = null;
		InetAddress inetAddress =null;
		List<String> notExistDmnList = new ArrayList<String>();
		
		try {
			bfrdr = new BufferedReader(new FileReader(file));
			
			while(((lineOneText = bfrdr.readLine())) != null){
				if(lineOneText.trim().length() >4){
			String str = lineOneText.substring(0,4);
			System.out.println("___line:::"+lineOneText);
		
			StringBuffer sbff= new StringBuffer();
			sbff.append("www.");
			for(char c: lineOneText.toCharArray()){
				if(!Character.isSpace(c)){
					sbff.append(c);
					if(sbff.toString().length() ==8){
						sbff.append("job");
						sbff.append(".com");
						//System.out.println(sbff.toString());
						//stringList.add(sbff.toString());
						String domain = sbff.toString();
						sbff= new StringBuffer();
						sbff.append("www.");
						try{
							 inetAddress = InetAddress.getByName(domain);
							//System.out.println(inetAddress.getHostName()+"__"+inetAddress.getHostAddress());
							}catch(UnknownHostException e){
								System.out.println(domain+"_____This Domain not existed");
								notExistDmnList.add(domain);
							} 
					}
				}
			}

			}
			}
			System.out.println("_______dmns list: " +notExistDmnList.size());
			writeToFile(notExistDmnList);
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	
	
	
	public static void buil5LtrDmn(){
		File file = new File("domn/5.txt");
		
		BufferedReader bfrdr =  null;
		String lineOneText = null;
		InetAddress inetAddress =null;
		List<String> notExistDmnList = new ArrayList<String>();
		
		try {
			bfrdr = new BufferedReader(new FileReader(file));
			
			while(((lineOneText = bfrdr.readLine())) != null){
				if(lineOneText.trim().length() >4){
			String str = lineOneText.substring(0,4);
			System.out.println("___line:::"+lineOneText);
		
			StringBuffer sbff= new StringBuffer();
			sbff.append("www.");
			for(char c: lineOneText.toCharArray()){
				if(!Character.isSpace(c)){
					sbff.append(c);
					if(sbff.toString().length() ==9){
						sbff.append("job");
						sbff.append(".com");
						//System.out.println(sbff.toString());
						//stringList.add(sbff.toString());
						String domain = sbff.toString();
						sbff= new StringBuffer();
						sbff.append("www.");
						try{
							 inetAddress = InetAddress.getByName(domain);
							}catch(UnknownHostException e){
								System.out.println(domain+"_____This Domain not existed");
								notExistDmnList.add(domain);
							} 
					}
				}
			}

			}
			}
			System.out.println("_______dmns list: " +notExistDmnList.size());
			writeToFile(notExistDmnList);
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	
	public static void writeToFile(List<String> dmnList){
		//File dmnFile = new File("domn/dmn3.txt");
		//File dmnFile = new File("domn/dmn4.txt");
		//File dmnFile = new File("domn/dmn.txt");
		File dmnFile = new File("domn/7ltr-dmn.txt");
		//File dmnFile = new File("domn/8ltr-dmn.txt");
		
		try {
			BufferedWriter bfwtr = null;
			bfwtr = new BufferedWriter(new FileWriter(dmnFile));
			StringBuffer sbf = new StringBuffer();
			int i = 0;
			for (String string : dmnList) {
				sbf.append(string);
				sbf.append("        ");
				i++;
				if(i==4){
					bfwtr.write(sbf.append(System.getProperty("line.separator")).toString());
					i =0;
				}
				
			}
			
			bfwtr.flush();
			bfwtr.close();
		}catch(Exception e){
			
		}
		
		System.out.println("____Done written all domains to file");
	}
	
	public static void main(String[] args){
		
		
		//build3LtrDmn();
		//build4LtrDmn();
		buil5LtrDmn();
	}

	
}
