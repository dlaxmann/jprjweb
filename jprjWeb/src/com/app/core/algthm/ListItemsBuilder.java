package com.app.core.algthm;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

public class ListItemsBuilder {
	
	
	public static void formatStateList(){
		File file = new File("files/states.txt");
		StringBuffer sbf=  new StringBuffer();
		BufferedReader bfrdr =  null;
		String lineOneText = null;
		
		InetAddress inetAddress =null;
		List<String> notExistDmnList = new ArrayList<String>();
		try {
			bfrdr = new BufferedReader(new FileReader(file));
			while(((lineOneText = bfrdr.readLine())) != null){
			
			sbf.append("\"");
			sbf.append(lineOneText.trim()+"\",");
			
			String item =  sbf.toString();
			System.out.println(item);
			sbf = new StringBuffer();
			notExistDmnList.add(item);
			}
			
			writeToFile(notExistDmnList);
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void formatCitiesList(){
		File file = new File("files/cities.txt");
		StringBuffer sbf=  new StringBuffer();
		BufferedReader bfrdr =  null;
		String lineOneText = null;
		
		InetAddress inetAddress =null;
		List<String> notExistDmnList = new ArrayList<String>();
		try {
			bfrdr = new BufferedReader(new FileReader(file));
			while(((lineOneText = bfrdr.readLine())) != null){
			
			sbf.append("\"");
			sbf.append(lineOneText.trim()+"\",");
			
			String item =  sbf.toString();
			System.out.println(item);
			sbf = new StringBuffer();
			notExistDmnList.add(item);
			}
			
			writeToFile(notExistDmnList);
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public static void buildDropDownItems(){
		File file = new File("files/Items.txt");
		StringBuffer sbf=  new StringBuffer();
		BufferedReader bfrdr =  null;
		String lineOneText = null;
		
		InetAddress inetAddress =null;
		List<String> notExistDmnList = new ArrayList<String>();
		try {
			bfrdr = new BufferedReader(new FileReader(file));
			while(((lineOneText = bfrdr.readLine())) != null){
			
			sbf.append("<f:selectItem itemValue=\"");
			sbf.append(lineOneText+"\"  ");
			sbf.append("itemLabel=\"");
			sbf.append(lineOneText+"\"/>");
			
			String item =  sbf.toString();
			System.out.println(item);
			sbf = new StringBuffer();
			notExistDmnList.add(item);
			}
			
			writeToFile(notExistDmnList);
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	
	public static void buildcmdlinkItems(){
		File file = new File("files/Items1.txt");
		StringBuffer sbf=  new StringBuffer();
		BufferedReader bfrdr =  null;
		String lineOneText = null;
		
		InetAddress inetAddress =null;
		List<String> notExistDmnList = new ArrayList<String>();
		try {
			bfrdr = new BufferedReader(new FileReader(file));
			while(((lineOneText = bfrdr.readLine())) != null){
				lineOneText = StringUtils.substring(lineOneText.trim(), 0,lineOneText.trim().length()-1);
				sbf.append("<li><h:commandLink action="); 
				sbf.append("\""); 
				sbf.append("#{searchJobsMBean.getJobsByDescipline}"); 
				sbf.append("\" ");
				sbf.append("value=");
				//sbf.append("value=\"");
			    sbf.append(lineOneText+">");
			    //sbf.append(lineOneText+"\">");
			    sbf.append("\n");
			    sbf.append("<f:setPropertyActionListener target=");
				sbf.append("\"");
				sbf.append("#{searchJobsMBean.industryType}");
				sbf.append("\" ");
				sbf.append("value=");
				//sbf.append("value=\"");
			    sbf.append(lineOneText+"/>");
			    //sbf.append(lineOneText+"\"/>");
			    sbf.append("</h:commandLink></li>");
			    
			String item =  sbf.toString();
			System.out.println(item);
			sbf = new StringBuffer();
			notExistDmnList.add(item);
			}
			
			writeToFile(notExistDmnList);
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public static void writeToFile(List<String> dmnList){
		//File dmnFile = new File("domn/dmn3.txt");
		File dmnFile = new File("files/DropDownItmes.txt");
		//File dmnFile = new File("domn/dmn.txt");
		try {
			BufferedWriter bfwtr = null;
			bfwtr = new BufferedWriter(new FileWriter(dmnFile));
			StringBuffer sbf = new StringBuffer();
					bfwtr.write(sbf.append(System.getProperty("line.separator")).toString());
	
			
			bfwtr.flush();
			bfwtr.close();
		}catch(Exception e){
			
		}
		
		//System.out.println("____Done written all domains to file");
	}
	
	public static void main(String[] args){
		
		//buildDropDownItems();
		//buildcmdlinkItems();
		//formatStateList();
		formatCitiesList();
		
	}
	
}
