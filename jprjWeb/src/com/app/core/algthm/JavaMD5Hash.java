package com.app.core.algthm;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class JavaMD5Hash {

	
	
	
	private static String getSalt() throws NoSuchAlgorithmException
	{
	    //Always use a SecureRandom generator
	    SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
	    //Create array for salt
	    byte[] salt = new byte[16];
	    //Get a random salt
	    sr.nextBytes(salt);
	    //return salt
	    System.out.println("Salt: "+salt.toString());
	    return salt.toString();
	}
	
	
	public static String md5Two(String str){
		 //String md5ied = null;
		// String passwordToHash = "password";
	     String generatedPassword = null;
	        try {
	            // Create MessageDigest instance for MD5
	            MessageDigest md = MessageDigest.getInstance("MD5");
	            //Add password bytes to digest
	            md.update(str.getBytes());
	            //Get the hash's bytes 
	            byte[] bytes = md.digest();
	            //This bytes[] has bytes in decimal format;
	            //Convert it to hexadecimal format
	            StringBuilder sb = new StringBuilder();
	            for(int i=0; i< bytes.length ;i++)
	            {
	                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
	            }
	            //Get complete hashed password in hex format
	            generatedPassword = sb.toString();
	        } 
	        catch (NoSuchAlgorithmException e) 
	        {
	            e.printStackTrace();
	        }
	        System.out.println(generatedPassword);
		return generatedPassword;
	}
	public static String md5One(String input) {
	     
	    String md5 = null;
	    if(null == input) return null;
	    try {
	         
	    //Create MessageDigest object for MD5
	    MessageDigest digest = MessageDigest.getInstance("MD5");
	    //Update input string in message digest
	    digest.update(input.getBytes(), 0, input.length());
	    //Converts message digest value in base 16 (hex) 
	    md5 = new BigInteger(1, digest.digest()).toString(16);
	    } catch (NoSuchAlgorithmException e) {
	        e.printStackTrace();
	    }
	    return md5;
	}
	public static void main(String[] args) {
		 
        String password = "123456789";
        //String password = "MyPassword123";
        System.out.println("MD5 in hex1: " + md5One(password));
        System.out.println("MD5 in hex2: " + md5One(null));
        //= d41d8cd98f00b204e9800998ecf8427e
        System.out.println("MD5 in hex3: "+ md5One("The quick brown fox jumps over the lazy dog"));
        //= 9e107d9d372bb6826bd81d3542a419d6
        
        md5Two(password);
        try {
			getSalt();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
        
}
 
 

}
