package com.app.core;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class TestingDatesVerizonVerify {
	
	//static Calendar quaterlyCalander = new GregorianCalendar();
	//static SimpleDateFormat qtrlyDateFormat = new SimpleDateFormat("MMMM");
	//static SimpleDateFormat quaterlyDateFormat = new SimpleDateFormat("MMMMM-yyyy");
	
	public static void testDates(){
		 /**
		 * Gives Invoice dates for QUARTERLY..auditPeriod.equals(QUARTERLY)
		 */
	    final String Jan = "January";
	    final String Feb = "February";
	    final String Mar = "March";
		final String Apr = "April";
		final String May = "May";
		final String Jun = "June";
		final String Jul = "July";
		final String Aug = "August";
		final String Sep = "September";
		final String Oct = "October";
		final String Nov = "November";
		final String Dec = "December";
		
			Calendar quaterlyCalander = new GregorianCalendar();
			SimpleDateFormat qtrlyDateFormat = new SimpleDateFormat("MMMM");
			SimpleDateFormat quaterlyDateFormat = new SimpleDateFormat("MMMMM-yyyy");
			quaterlyCalander.set(Calendar.MONTH,0);
			String invoiceQuater = qtrlyDateFormat.format(quaterlyCalander.getTime());
			System.out.println("---invoiceQuater =  " +invoiceQuater);
				
		if (invoiceQuater != null && invoiceQuater.equals(Jan)) {								// Oct-Dec
			quaterlyCalander.set(Calendar.MONTH, 9);
			String firstMonthOfQuater = quaterlyDateFormat.format(quaterlyCalander.getTime());
			int firstDayOfQuaterly = quaterlyCalander.getActualMinimum(Calendar.DAY_OF_MONTH);
			System.out.println("First Month Of Quater=  "+firstMonthOfQuater);

			quaterlyCalander.set(Calendar.MONTH, 11);
			//quaterlyCalander.set(Calendar.YEAR,-1) ;
			String lastMonthOfQuater = quaterlyDateFormat.format(quaterlyCalander.getTime());
			int lastDayOfQuaterly = quaterlyCalander.getActualMaximum(Calendar.DAY_OF_MONTH);
			System.out.println("Last Month Of Quater=   "+lastMonthOfQuater);
			
			String _firstDayOfQuaterly = Integer.toString(firstDayOfQuaterly);
			String _lastDayOfQuaterly = Integer.toString(lastDayOfQuaterly);
			
			String  startDateOfQuaterly  = _firstDayOfQuaterly.concat("-").concat(firstMonthOfQuater);
			String  endDateOfQuaterly  = _lastDayOfQuaterly.concat("-").concat(lastMonthOfQuater);
			System.out.println("Invoice Start Date Of Quaterly=  " + startDateOfQuaterly);
			System.out.println("Invoice End Date Of Quaterly=  " + endDateOfQuaterly);
			//auditexecution.setInvcEndDt(startDateOfQuaterly);
	    	//auditexecution.setInvcEndDt(endDateOfQuaterly);
		}
		else if(invoiceQuater != null && invoiceQuater.equals(Feb)){     						// Nov-Jan
				
			quaterlyCalander.set(Calendar.MONTH, 0);
			String firstMonthOfQuater = quaterlyDateFormat.format(quaterlyCalander.getTime());
			int firstDayOfQuaterly = quaterlyCalander.getActualMinimum(Calendar.DAY_OF_MONTH);
			System.out.println("First Month Of Quater=  "+firstMonthOfQuater);

			quaterlyCalander.set(Calendar.MONTH, 10);
			//quaterlyCalander.set(Calendar.YEAR,-1) ;
			String lastMonthOfQuater = quaterlyDateFormat.format(quaterlyCalander.getTime());
			int lastDayOfQuaterly = quaterlyCalander.getActualMaximum(Calendar.DAY_OF_MONTH);
			System.out.println("Last Month Of Quater=   "+lastMonthOfQuater);
			
			String _firstDayOfQuaterly = Integer.toString(firstDayOfQuaterly);
			String _lastDayOfQuaterly = Integer.toString(lastDayOfQuaterly);
			
			String  startDateOfQuaterly  = _firstDayOfQuaterly.concat("-").concat(firstMonthOfQuater);
			String  endDateOfQuaterly  = _lastDayOfQuaterly.concat("-").concat(lastMonthOfQuater);
			System.out.println("Invoice Start Date Of Quaterly=  " + startDateOfQuaterly);
			System.out.println("Invoice End Date Of Quaterly=  " + endDateOfQuaterly);
			//auditexecution.setInvcEndDt(startDateOfQuaterly);
	    	//auditexecution.setInvcEndDt(endDateOfQuaterly);
		}
		else if(invoiceQuater != null && invoiceQuater.equals(Mar)){     						// Dec-Feb
			
			quaterlyCalander.set(Calendar.MONTH, 11);
			String firstMonthOfQuater = quaterlyDateFormat.format(quaterlyCalander.getTime());
			int firstDayOfQuaterly = quaterlyCalander.getActualMinimum(Calendar.DAY_OF_MONTH);
			System.out.println("First Month Of Quater=  "+firstMonthOfQuater);

			quaterlyCalander.set(Calendar.MONTH, 1);
			//quaterlyCalander.set(Calendar.YEAR,-1) ;
			String lastMonthOfQuater = quaterlyDateFormat.format(quaterlyCalander.getTime());
			int lastDayOfQuaterly = quaterlyCalander.getActualMaximum(Calendar.DAY_OF_MONTH);
			System.out.println("Last Month Of Quater=   "+lastMonthOfQuater);
			
			String _firstDayOfQuaterly = Integer.toString(firstDayOfQuaterly);
			String _lastDayOfQuaterly = Integer.toString(lastDayOfQuaterly);
			
			String  startDateOfQuaterly  = _firstDayOfQuaterly.concat("-").concat(firstMonthOfQuater);
			String  endDateOfQuaterly  = _lastDayOfQuaterly.concat("-").concat(lastMonthOfQuater);
			System.out.println("Invoice Start Date Of Quaterly=  " + startDateOfQuaterly);
			System.out.println("Invoice End Date Of Quaterly=  " + endDateOfQuaterly);
			//auditexecution.setInvcEndDt(startDateOfQuaterly);
	    	//auditexecution.setInvcEndDt(endDateOfQuaterly);
		}
		else if(invoiceQuater != null && invoiceQuater.equals(Apr)){     						// Jan-March
		
			quaterlyCalander.set(Calendar.MONTH, 0);
			String firstMonthOfQuater = quaterlyDateFormat.format(quaterlyCalander.getTime());
			int firstDayOfQuaterly = quaterlyCalander.getActualMinimum(Calendar.DAY_OF_MONTH);
			System.out.println("First Month Of Quater=  "+firstMonthOfQuater);
	
			quaterlyCalander.set(Calendar.MONTH, 2);
			//quaterlyCalander.set(Calendar.YEAR,-1) ;
			String lastMonthOfQuater = quaterlyDateFormat.format(quaterlyCalander.getTime());
			int lastDayOfQuaterly = quaterlyCalander.getActualMaximum(Calendar.DAY_OF_MONTH);
			System.out.println("Last Month Of Quater=   "+lastMonthOfQuater);
			
			String _firstDayOfQuaterly = Integer.toString(firstDayOfQuaterly);
			String _lastDayOfQuaterly = Integer.toString(lastDayOfQuaterly);
			
			String  startDateOfQuaterly  = _firstDayOfQuaterly.concat("-").concat(firstMonthOfQuater);
			String  endDateOfQuaterly  = _lastDayOfQuaterly.concat("-").concat(lastMonthOfQuater);
			System.out.println("Invoice Start Date Of Quaterly=  " + startDateOfQuaterly);
			System.out.println("Invoice End Date Of Quaterly=  " + endDateOfQuaterly);
			//auditexecution.setInvcEndDt(startDateOfQuaterly);
	    	//auditexecution.setInvcEndDt(endDateOfQuaterly);
		}				
		else if(invoiceQuater != null && invoiceQuater.equals(May)){     						// Feb-April
			
			quaterlyCalander.set(Calendar.MONTH, 1); 
			String firstMonthOfQuater = quaterlyDateFormat.format(quaterlyCalander.getTime());
			int firstDayOfQuaterly = quaterlyCalander.getActualMinimum(Calendar.DAY_OF_MONTH);
			System.out.println("First Month Of Quater=  "+firstMonthOfQuater);

			quaterlyCalander.set(Calendar.MONTH, 3);
			String lastMonthOfQuater = quaterlyDateFormat.format(quaterlyCalander.getTime());
			int lastDayOfQuaterly = quaterlyCalander.getActualMaximum(Calendar.DAY_OF_MONTH);
			System.out.println("Last Month Of Quater=   "+lastMonthOfQuater);
			
			String _firstDayOfQuaterly = Integer.toString(firstDayOfQuaterly);
			String _lastDayOfQuaterly = Integer.toString(lastDayOfQuaterly);
			
			String  startDateOfQuaterly  = _firstDayOfQuaterly.concat("-").concat(firstMonthOfQuater);
			String  endDateOfQuaterly  = _lastDayOfQuaterly.concat("-").concat(lastMonthOfQuater);
			System.out.println("Invoice Start Date Of Quaterly=  " + startDateOfQuaterly);
			System.out.println("Invoice End Date Of Quaterly=  " + endDateOfQuaterly);
			//auditexecution.setInvcEndDt(startDateOfQuaterly);
	    	//auditexecution.setInvcEndDt(endDateOfQuaterly);
		}
		else if(invoiceQuater != null && invoiceQuater.equals(Jun)){							// Mar-May
			
			quaterlyCalander.set(Calendar.MONTH, 2 ); 
			String firstMonthOfQuater = quaterlyDateFormat.format(quaterlyCalander.getTime());
			int firstDayOfQuaterly = quaterlyCalander.getActualMinimum(Calendar.DAY_OF_MONTH);
			System.out.println("First Month Of Quater=  "+firstMonthOfQuater);
			
			quaterlyCalander.set(Calendar.MONTH, 4); 
			String lastMonthOfQuater = quaterlyDateFormat.format(quaterlyCalander.getTime());
			int lastDayOfQuaterly = quaterlyCalander.getActualMaximum(Calendar.DAY_OF_MONTH);
			System.out.println("Last Month Of Quater=   "+lastMonthOfQuater);
			
			String _firstDayOfQuaterly = Integer.toString(firstDayOfQuaterly);
			String _lastDayOfQuaterly = Integer.toString(lastDayOfQuaterly);
			
			String  startDateOfQuaterly  = _firstDayOfQuaterly.concat("-").concat(firstMonthOfQuater);
			String  endDateOfQuaterly  = _lastDayOfQuaterly.concat("-").concat(lastMonthOfQuater);
			System.out.println("Invoice Start Date Of Quaterly=  " + startDateOfQuaterly);
			System.out.println("Invoice End Date Of Quaterly=  " + endDateOfQuaterly);
			//auditexecution.setInvcEndDt(startDateOfQuaterly);
	    	//auditexecution.setInvcEndDt(endDateOfQuaterly);
		}
		else if(invoiceQuater != null && invoiceQuater.equals(Jul)){							// April-June
			quaterlyCalander.set(Calendar.MONTH, 3 ); 
			String firstMonthOfQuater = quaterlyDateFormat.format(quaterlyCalander.getTime());
			int firstDayOfQuaterly = quaterlyCalander.getActualMinimum(Calendar.DAY_OF_MONTH);
			System.out.println("First Month Of Quater=  "+firstMonthOfQuater);
			
			quaterlyCalander.set(Calendar.MONTH, 5); 
			String lastMonthOfQuater = quaterlyDateFormat.format(quaterlyCalander.getTime());
			int lastDayOfQuaterly = quaterlyCalander.getActualMaximum(Calendar.DAY_OF_MONTH);
			System.out.println("Last Month Of Quater=   "+lastMonthOfQuater);
			
			String _firstDayOfQuaterly = Integer.toString(firstDayOfQuaterly);
			String _lastDayOfQuaterly = Integer.toString(lastDayOfQuaterly);
			
			String  startDateOfQuaterly  = _firstDayOfQuaterly.concat("-").concat(firstMonthOfQuater);
			String  endDateOfQuaterly  = _lastDayOfQuaterly.concat("-").concat(lastMonthOfQuater);
			System.out.println("Invoice Start Date Of Quaterly=  " + startDateOfQuaterly);
			System.out.println("Invoice End Date Of Quaterly=  " + endDateOfQuaterly);
			//auditexecution.setInvcEndDt(startDateOfQuaterly);
	    	//auditexecution.setInvcEndDt(endDateOfQuaterly);
		}
		else if(invoiceQuater != null && invoiceQuater.equals(Aug)){							// May-July
			quaterlyCalander.set(Calendar.MONTH, 4 ); 
			String firstMonthOfQuater = quaterlyDateFormat.format(quaterlyCalander.getTime());
			int firstDayOfQuaterly = quaterlyCalander.getActualMinimum(Calendar.DAY_OF_MONTH);
			System.out.println("First Month Of Quater=  "+firstMonthOfQuater);
			
			quaterlyCalander.set(Calendar.MONTH, 6); 
			String lastMonthOfQuater = quaterlyDateFormat.format(quaterlyCalander.getTime());
			int lastDayOfQuaterly = quaterlyCalander.getActualMaximum(Calendar.DAY_OF_MONTH);
			System.out.println("Last Month Of Quater=   "+lastMonthOfQuater);
			
			String _firstDayOfQuaterly = Integer.toString(firstDayOfQuaterly);
			String _lastDayOfQuaterly = Integer.toString(lastDayOfQuaterly);
			
			String  startDateOfQuaterly  = _firstDayOfQuaterly.concat("-").concat(firstMonthOfQuater);
			String  endDateOfQuaterly  = _lastDayOfQuaterly.concat("-").concat(lastMonthOfQuater);
			System.out.println("Invoice Start Date Of Quaterly=  " + startDateOfQuaterly);
			System.out.println("Invoice End Date Of Quaterly=  " + endDateOfQuaterly);
			//auditexecution.setInvcEndDt(startDateOfQuaterly);
	    	//auditexecution.setInvcEndDt(endDateOfQuaterly);
		}
		else if(invoiceQuater != null && invoiceQuater.equals(Sep)){							// June-Aug
			quaterlyCalander.set(Calendar.MONTH, 5 ); 
			String firstMonthOfQuater = quaterlyDateFormat.format(quaterlyCalander.getTime());
			int firstDayOfQuaterly = quaterlyCalander.getActualMinimum(Calendar.DAY_OF_MONTH);
			System.out.println("First Month Of Quater=  "+firstMonthOfQuater);
			
			quaterlyCalander.set(Calendar.MONTH, 7); 
			String lastMonthOfQuater = quaterlyDateFormat.format(quaterlyCalander.getTime());
			int lastDayOfQuaterly = quaterlyCalander.getActualMaximum(Calendar.DAY_OF_MONTH);
			System.out.println("Last Month Of Quater=   "+lastMonthOfQuater);
			
			String _firstDayOfQuaterly = Integer.toString(firstDayOfQuaterly);
			String _lastDayOfQuaterly = Integer.toString(lastDayOfQuaterly);
			
			String  startDateOfQuaterly  = _firstDayOfQuaterly.concat("-").concat(firstMonthOfQuater);
			String  endDateOfQuaterly  = _lastDayOfQuaterly.concat("-").concat(lastMonthOfQuater);
			System.out.println("Invoice Start Date Of Quaterly=  " + startDateOfQuaterly);
			System.out.println("Invoice End Date Of Quaterly=  " + endDateOfQuaterly);
			//auditexecution.setInvcEndDt(startDateOfQuaterly);
	    	//auditexecution.setInvcEndDt(endDateOfQuaterly);
		}
		else if(invoiceQuater != null && invoiceQuater.equals(Oct)){							// July-Sept
			quaterlyCalander.set(Calendar.MONTH, 6 ); 
			String firstMonthOfQuater = quaterlyDateFormat.format(quaterlyCalander.getTime());
			int firstDayOfQuaterly = quaterlyCalander.getActualMinimum(Calendar.DAY_OF_MONTH);
			System.out.println("First Month Of Quater=  "+firstMonthOfQuater);
			
			quaterlyCalander.set(Calendar.MONTH, 8); 
			String lastMonthOfQuater = quaterlyDateFormat.format(quaterlyCalander.getTime());
			int lastDayOfQuaterly = quaterlyCalander.getActualMaximum(Calendar.DAY_OF_MONTH);
			System.out.println("Last Month Of Quater=   "+lastMonthOfQuater);
			
			String _firstDayOfQuaterly = Integer.toString(firstDayOfQuaterly);
			String _lastDayOfQuaterly = Integer.toString(lastDayOfQuaterly);
			
			String  startDateOfQuaterly  = _firstDayOfQuaterly.concat("-").concat(firstMonthOfQuater);
			String  endDateOfQuaterly  = _lastDayOfQuaterly.concat("-").concat(lastMonthOfQuater);
			System.out.println("Invoice Start Date Of Quaterly=  " + startDateOfQuaterly);
			System.out.println("Invoice End Date Of Quaterly=  " + endDateOfQuaterly);
			//auditexecution.setInvcEndDt(startDateOfQuaterly);
	    	//auditexecution.setInvcEndDt(endDateOfQuaterly);
		}
		else if(invoiceQuater != null && invoiceQuater.equals(Nov)){							// Aug-Oct
			quaterlyCalander.set(Calendar.MONTH, 7 ); 
			String firstMonthOfQuater = quaterlyDateFormat.format(quaterlyCalander.getTime());
			int firstDayOfQuaterly = quaterlyCalander.getActualMinimum(Calendar.DAY_OF_MONTH);
			System.out.println("First Month Of Quater=  "+firstMonthOfQuater);
			
			quaterlyCalander.set(Calendar.MONTH, 9); 
			String lastMonthOfQuater = quaterlyDateFormat.format(quaterlyCalander.getTime());
			int lastDayOfQuaterly = quaterlyCalander.getActualMaximum(Calendar.DAY_OF_MONTH);
			System.out.println("Last Month Of Quater=   "+lastMonthOfQuater);
			
			String _firstDayOfQuaterly = Integer.toString(firstDayOfQuaterly);
			String _lastDayOfQuaterly = Integer.toString(lastDayOfQuaterly);
			
			String  startDateOfQuaterly  = _firstDayOfQuaterly.concat("-").concat(firstMonthOfQuater);
			String  endDateOfQuaterly  = _lastDayOfQuaterly.concat("-").concat(lastMonthOfQuater);
			System.out.println("Invoice Start Date Of Quaterly=  " + startDateOfQuaterly);
			System.out.println("Invoice End Date Of Quaterly=  " + endDateOfQuaterly);
			//auditexecution.setInvcEndDt(startDateOfQuaterly);
	    	//auditexecution.setInvcEndDt(endDateOfQuaterly);
		}
		else if(invoiceQuater != null && invoiceQuater.equals(Dec)){							// Sep-Nov
			quaterlyCalander.set(Calendar.MONTH, 8 ); 
			String firstMonthOfQuater = quaterlyDateFormat.format(quaterlyCalander.getTime());
			int firstDayOfQuaterly = quaterlyCalander.getActualMinimum(Calendar.DAY_OF_MONTH);
			System.out.println("First Month Of Quater=  "+firstMonthOfQuater);
			
			quaterlyCalander.set(Calendar.MONTH, 10); 
			String lastMonthOfQuater = quaterlyDateFormat.format(quaterlyCalander.getTime());
			int lastDayOfQuaterly = quaterlyCalander.getActualMaximum(Calendar.DAY_OF_MONTH);
			System.out.println("Last Month Of Quater=   "+lastMonthOfQuater);
			
			String _firstDayOfQuaterly = Integer.toString(firstDayOfQuaterly);
			String _lastDayOfQuaterly = Integer.toString(lastDayOfQuaterly);
			
			String  startDateOfQuaterly  = _firstDayOfQuaterly.concat("-").concat(firstMonthOfQuater);
			String  endDateOfQuaterly  = _lastDayOfQuaterly.concat("-").concat(lastMonthOfQuater);
			System.out.println("Invoice Start Date Of Quaterly=  " + startDateOfQuaterly);
			System.out.println("Invoice End Date Of Quaterly=  " + endDateOfQuaterly);
			//auditexecution.setInvcEndDt(startDateOfQuaterly);
	    	//auditexecution.setInvcEndDt(endDateOfQuaterly);
		}
		
		
	}

	public static void testDates2(){
		/**
    	 * Gives Invoice dates for TWOMONTHTREND..auditPeriod.equals(TWOMONTHTREND)
    	 */ 
    	
		 	final String Jan = "January";
		    final String Feb = "February";
		    final String Mar = "March";
			final String Apr = "April";
			final String May = "May";
			final String Jun = "June";
			final String Jul = "July";
			final String Aug = "August";
			final String Sep = "September";
			final String Oct = "October";
			final String Nov = "November";
			final String Dec = "December";
			
			
			Calendar twoMonthTrendCalander = new GregorianCalendar();
			SimpleDateFormat twoMonthTrendFormat = new SimpleDateFormat("MMMM");
			SimpleDateFormat twoMonthTrendDateFormat = new SimpleDateFormat("MMMMM-yyyy");
			twoMonthTrendCalander.set(Calendar.MONTH,0);
			String invoiceTwoMnthTrend = twoMonthTrendFormat.format(twoMonthTrendCalander.getTime());
			System.out.println("---invoiceQuater =  " +invoiceTwoMnthTrend);
			
			
			if (invoiceTwoMnthTrend != null && invoiceTwoMnthTrend.equals(Jan)) {								// Nov-Dec
				twoMonthTrendCalander.set(Calendar.MONTH, 10);
				String firstMonthOfQuater = twoMonthTrendDateFormat.format(twoMonthTrendCalander.getTime());
				int firstDayOfQuaterly = twoMonthTrendCalander.getActualMinimum(Calendar.DAY_OF_MONTH);
				System.out.println("First Month Of Quater=  "+firstMonthOfQuater);

				twoMonthTrendCalander.set(Calendar.MONTH, 11);
				//twoMonthTrendCalander.set(Calendar.YEAR,-1) ;
				String lastMonthOfQuater = twoMonthTrendDateFormat.format(twoMonthTrendCalander.getTime());
				int lastDayOfQuaterly = twoMonthTrendCalander.getActualMaximum(Calendar.DAY_OF_MONTH);
				System.out.println("Last Month Of Quater=   "+lastMonthOfQuater);
				
				String _firstDayOfQuaterly = Integer.toString(firstDayOfQuaterly);
				String _lastDayOfQuaterly = Integer.toString(lastDayOfQuaterly);
				
				String  startDateOfQuaterly  = _firstDayOfQuaterly.concat("-").concat(firstMonthOfQuater);
				String  endDateOfQuaterly  = _lastDayOfQuaterly.concat("-").concat(lastMonthOfQuater);
				System.out.println("Invoice Start Date Of Quaterly=  " + startDateOfQuaterly);
				System.out.println("Invoice End Date Of Quaterly=  " + endDateOfQuaterly);
				//auditexecution.setInvcEndDt(startDateOfQuaterly);
		    	//auditexecution.setInvcEndDt(endDateOfQuaterly);
			}
			else if(invoiceTwoMnthTrend != null && invoiceTwoMnthTrend.equals(Feb)){     						// Dec-Jan
					
				twoMonthTrendCalander.set(Calendar.MONTH, 11);
				String firstMonthOfQuater = twoMonthTrendDateFormat.format(twoMonthTrendCalander.getTime());
				int firstDayOfQuaterly = twoMonthTrendCalander.getActualMinimum(Calendar.DAY_OF_MONTH);
				System.out.println("First Month Of Quater=  "+firstMonthOfQuater);

				twoMonthTrendCalander.set(Calendar.MONTH, 0);
				//twoMonthTrendCalander.set(Calendar.YEAR,-1) ;
				String lastMonthOfQuater = twoMonthTrendDateFormat.format(twoMonthTrendCalander.getTime());
				int lastDayOfQuaterly = twoMonthTrendCalander.getActualMaximum(Calendar.DAY_OF_MONTH);
				System.out.println("Last Month Of Quater=   "+lastMonthOfQuater);
				
				String _firstDayOfQuaterly = Integer.toString(firstDayOfQuaterly);
				String _lastDayOfQuaterly = Integer.toString(lastDayOfQuaterly);
				
				String  startDateOfQuaterly  = _firstDayOfQuaterly.concat("-").concat(firstMonthOfQuater);
				String  endDateOfQuaterly  = _lastDayOfQuaterly.concat("-").concat(lastMonthOfQuater);
				System.out.println("Invoice Start Date Of Quaterly=  " + startDateOfQuaterly);
				System.out.println("Invoice End Date Of Quaterly=  " + endDateOfQuaterly);
				//auditexecution.setInvcEndDt(startDateOfQuaterly);
		    	//auditexecution.setInvcEndDt(endDateOfQuaterly);
			}
			else if(invoiceTwoMnthTrend != null && invoiceTwoMnthTrend.equals(Mar)){     						// Jan-Feb
				
				twoMonthTrendCalander.set(Calendar.MONTH, 0);
				String firstMonthOfQuater = twoMonthTrendDateFormat.format(twoMonthTrendCalander.getTime());
				int firstDayOfQuaterly = twoMonthTrendCalander.getActualMinimum(Calendar.DAY_OF_MONTH);
				System.out.println("First Month Of Quater=  "+firstMonthOfQuater);

				twoMonthTrendCalander.set(Calendar.MONTH, 1);
				//twoMonthTrendCalander.set(Calendar.YEAR,-1) ;
				String lastMonthOfQuater = twoMonthTrendDateFormat.format(twoMonthTrendCalander.getTime());
				int lastDayOfQuaterly = twoMonthTrendCalander.getActualMaximum(Calendar.DAY_OF_MONTH);
				System.out.println("Last Month Of Quater=   "+lastMonthOfQuater);
				
				String _firstDayOfQuaterly = Integer.toString(firstDayOfQuaterly);
				String _lastDayOfQuaterly = Integer.toString(lastDayOfQuaterly);
				
				String  startDateOfQuaterly  = _firstDayOfQuaterly.concat("-").concat(firstMonthOfQuater);
				String  endDateOfQuaterly  = _lastDayOfQuaterly.concat("-").concat(lastMonthOfQuater);
				System.out.println("Invoice Start Date Of Quaterly=  " + startDateOfQuaterly);
				System.out.println("Invoice End Date Of Quaterly=  " + endDateOfQuaterly);
				//auditexecution.setInvcEndDt(startDateOfQuaterly);
		    	//auditexecution.setInvcEndDt(endDateOfQuaterly);
			}
			else if(invoiceTwoMnthTrend != null && invoiceTwoMnthTrend.equals(Apr)){     						// Feb-March
			
				twoMonthTrendCalander.set(Calendar.MONTH, 1);
				String firstMonthOfQuater = twoMonthTrendDateFormat.format(twoMonthTrendCalander.getTime());
				int firstDayOfQuaterly = twoMonthTrendCalander.getActualMinimum(Calendar.DAY_OF_MONTH);
				System.out.println("First Month Of Quater=  "+firstMonthOfQuater);
		
				twoMonthTrendCalander.set(Calendar.MONTH, 2);
				//twoMonthTrendCalander.set(Calendar.YEAR,-1) ;
				String lastMonthOfQuater = twoMonthTrendDateFormat.format(twoMonthTrendCalander.getTime());
				int lastDayOfQuaterly = twoMonthTrendCalander.getActualMaximum(Calendar.DAY_OF_MONTH);
				System.out.println("Last Month Of Quater=   "+lastMonthOfQuater);
				
				String _firstDayOfQuaterly = Integer.toString(firstDayOfQuaterly);
				String _lastDayOfQuaterly = Integer.toString(lastDayOfQuaterly);
				
				String  startDateOfQuaterly  = _firstDayOfQuaterly.concat("-").concat(firstMonthOfQuater);
				String  endDateOfQuaterly  = _lastDayOfQuaterly.concat("-").concat(lastMonthOfQuater);
				System.out.println("Invoice Start Date Of Quaterly=  " + startDateOfQuaterly);
				System.out.println("Invoice End Date Of Quaterly=  " + endDateOfQuaterly);
				//auditexecution.setInvcEndDt(startDateOfQuaterly);
		    	//auditexecution.setInvcEndDt(endDateOfQuaterly);
			}				
			else if(invoiceTwoMnthTrend != null && invoiceTwoMnthTrend.equals(May)){     						// March-April
				
				twoMonthTrendCalander.set(Calendar.MONTH, 2); 
				String firstMonthOfQuater = twoMonthTrendDateFormat.format(twoMonthTrendCalander.getTime());
				int firstDayOfQuaterly = twoMonthTrendCalander.getActualMinimum(Calendar.DAY_OF_MONTH);
				System.out.println("First Month Of Quater=  "+firstMonthOfQuater);

				twoMonthTrendCalander.set(Calendar.MONTH, 3);
				String lastMonthOfQuater = twoMonthTrendDateFormat.format(twoMonthTrendCalander.getTime());
				int lastDayOfQuaterly = twoMonthTrendCalander.getActualMaximum(Calendar.DAY_OF_MONTH);
				System.out.println("Last Month Of Quater=   "+lastMonthOfQuater);
				
				String _firstDayOfQuaterly = Integer.toString(firstDayOfQuaterly);
				String _lastDayOfQuaterly = Integer.toString(lastDayOfQuaterly);
				
				String  startDateOfQuaterly  = _firstDayOfQuaterly.concat("-").concat(firstMonthOfQuater);
				String  endDateOfQuaterly  = _lastDayOfQuaterly.concat("-").concat(lastMonthOfQuater);
				System.out.println("Invoice Start Date Of Quaterly=  " + startDateOfQuaterly);
				System.out.println("Invoice End Date Of Quaterly=  " + endDateOfQuaterly);
				//auditexecution.setInvcEndDt(startDateOfQuaterly);
		    	//auditexecution.setInvcEndDt(endDateOfQuaterly);
			}
			else if(invoiceTwoMnthTrend != null && invoiceTwoMnthTrend.equals(Jun)){							// April-May
				
				twoMonthTrendCalander.set(Calendar.MONTH, 3 ); 
				String firstMonthOfQuater = twoMonthTrendDateFormat.format(twoMonthTrendCalander.getTime());
				int firstDayOfQuaterly = twoMonthTrendCalander.getActualMinimum(Calendar.DAY_OF_MONTH);
				System.out.println("First Month Of Quater=  "+firstMonthOfQuater);
				
				twoMonthTrendCalander.set(Calendar.MONTH, 4); 
				String lastMonthOfQuater = twoMonthTrendDateFormat.format(twoMonthTrendCalander.getTime());
				int lastDayOfQuaterly = twoMonthTrendCalander.getActualMaximum(Calendar.DAY_OF_MONTH);
				System.out.println("Last Month Of Quater=   "+lastMonthOfQuater);
				
				String _firstDayOfQuaterly = Integer.toString(firstDayOfQuaterly);
				String _lastDayOfQuaterly = Integer.toString(lastDayOfQuaterly);
				
				String  startDateOfQuaterly  = _firstDayOfQuaterly.concat("-").concat(firstMonthOfQuater);
				String  endDateOfQuaterly  = _lastDayOfQuaterly.concat("-").concat(lastMonthOfQuater);
				System.out.println("Invoice Start Date Of Quaterly=  " + startDateOfQuaterly);
				System.out.println("Invoice End Date Of Quaterly=  " + endDateOfQuaterly);
				//auditexecution.setInvcEndDt(startDateOfQuaterly);
		    	//auditexecution.setInvcEndDt(endDateOfQuaterly);
			}
			else if(invoiceTwoMnthTrend != null && invoiceTwoMnthTrend.equals(Jul)){							// May-June
				twoMonthTrendCalander.set(Calendar.MONTH, 4 ); 
				String firstMonthOfQuater = twoMonthTrendDateFormat.format(twoMonthTrendCalander.getTime());
				int firstDayOfQuaterly = twoMonthTrendCalander.getActualMinimum(Calendar.DAY_OF_MONTH);
				System.out.println("First Month Of Quater=  "+firstMonthOfQuater);
				
				twoMonthTrendCalander.set(Calendar.MONTH, 5); 
				String lastMonthOfQuater = twoMonthTrendDateFormat.format(twoMonthTrendCalander.getTime());
				int lastDayOfQuaterly = twoMonthTrendCalander.getActualMaximum(Calendar.DAY_OF_MONTH);
				System.out.println("Last Month Of Quater=   "+lastMonthOfQuater);
				
				String _firstDayOfQuaterly = Integer.toString(firstDayOfQuaterly);
				String _lastDayOfQuaterly = Integer.toString(lastDayOfQuaterly);
				
				String  startDateOfQuaterly  = _firstDayOfQuaterly.concat("-").concat(firstMonthOfQuater);
				String  endDateOfQuaterly  = _lastDayOfQuaterly.concat("-").concat(lastMonthOfQuater);
				System.out.println("Invoice Start Date Of Quaterly=  " + startDateOfQuaterly);
				System.out.println("Invoice End Date Of Quaterly=  " + endDateOfQuaterly);
				//auditexecution.setInvcEndDt(startDateOfQuaterly);
		    	//auditexecution.setInvcEndDt(endDateOfQuaterly);
			}
			else if(invoiceTwoMnthTrend != null && invoiceTwoMnthTrend.equals(Aug)){							// June-July
				twoMonthTrendCalander.set(Calendar.MONTH, 5 ); 
				String firstMonthOfQuater = twoMonthTrendDateFormat.format(twoMonthTrendCalander.getTime());
				int firstDayOfQuaterly = twoMonthTrendCalander.getActualMinimum(Calendar.DAY_OF_MONTH);
				System.out.println("First Month Of Quater=  "+firstMonthOfQuater);
				
				twoMonthTrendCalander.set(Calendar.MONTH, 6); 
				String lastMonthOfQuater = twoMonthTrendDateFormat.format(twoMonthTrendCalander.getTime());
				int lastDayOfQuaterly = twoMonthTrendCalander.getActualMaximum(Calendar.DAY_OF_MONTH);
				System.out.println("Last Month Of Quater=   "+lastMonthOfQuater);
				
				String _firstDayOfQuaterly = Integer.toString(firstDayOfQuaterly);
				String _lastDayOfQuaterly = Integer.toString(lastDayOfQuaterly);
				
				String  startDateOfQuaterly  = _firstDayOfQuaterly.concat("-").concat(firstMonthOfQuater);
				String  endDateOfQuaterly  = _lastDayOfQuaterly.concat("-").concat(lastMonthOfQuater);
				System.out.println("Invoice Start Date Of Quaterly=  " + startDateOfQuaterly);
				System.out.println("Invoice End Date Of Quaterly=  " + endDateOfQuaterly);
				//auditexecution.setInvcEndDt(startDateOfQuaterly);
		    	//auditexecution.setInvcEndDt(endDateOfQuaterly);
			}
			else if(invoiceTwoMnthTrend != null && invoiceTwoMnthTrend.equals(Sep)){							// July-Aug
				twoMonthTrendCalander.set(Calendar.MONTH, 6 ); 
				String firstMonthOfQuater = twoMonthTrendDateFormat.format(twoMonthTrendCalander.getTime());
				int firstDayOfQuaterly = twoMonthTrendCalander.getActualMinimum(Calendar.DAY_OF_MONTH);
				System.out.println("First Month Of Quater=  "+firstMonthOfQuater);
				
				twoMonthTrendCalander.set(Calendar.MONTH, 7); 
				String lastMonthOfQuater = twoMonthTrendDateFormat.format(twoMonthTrendCalander.getTime());
				int lastDayOfQuaterly = twoMonthTrendCalander.getActualMaximum(Calendar.DAY_OF_MONTH);
				System.out.println("Last Month Of Quater=   "+lastMonthOfQuater);
				
				String _firstDayOfQuaterly = Integer.toString(firstDayOfQuaterly);
				String _lastDayOfQuaterly = Integer.toString(lastDayOfQuaterly);
				
				String  startDateOfQuaterly  = _firstDayOfQuaterly.concat("-").concat(firstMonthOfQuater);
				String  endDateOfQuaterly  = _lastDayOfQuaterly.concat("-").concat(lastMonthOfQuater);
				System.out.println("Invoice Start Date Of Quaterly=  " + startDateOfQuaterly);
				System.out.println("Invoice End Date Of Quaterly=  " + endDateOfQuaterly);
				//auditexecution.setInvcEndDt(startDateOfQuaterly);
		    	//auditexecution.setInvcEndDt(endDateOfQuaterly);
			}
			else if(invoiceTwoMnthTrend != null && invoiceTwoMnthTrend.equals(Oct)){							// Aug-Sept
				twoMonthTrendCalander.set(Calendar.MONTH, 7 ); 
				String firstMonthOfQuater = twoMonthTrendDateFormat.format(twoMonthTrendCalander.getTime());
				int firstDayOfQuaterly = twoMonthTrendCalander.getActualMinimum(Calendar.DAY_OF_MONTH);
				System.out.println("First Month Of Quater=  "+firstMonthOfQuater);
				
				twoMonthTrendCalander.set(Calendar.MONTH, 8); 
				String lastMonthOfQuater = twoMonthTrendDateFormat.format(twoMonthTrendCalander.getTime());
				int lastDayOfQuaterly = twoMonthTrendCalander.getActualMaximum(Calendar.DAY_OF_MONTH);
				System.out.println("Last Month Of Quater=   "+lastMonthOfQuater);
				
				String _firstDayOfQuaterly = Integer.toString(firstDayOfQuaterly);
				String _lastDayOfQuaterly = Integer.toString(lastDayOfQuaterly);
				
				String  startDateOfQuaterly  = _firstDayOfQuaterly.concat("-").concat(firstMonthOfQuater);
				String  endDateOfQuaterly  = _lastDayOfQuaterly.concat("-").concat(lastMonthOfQuater);
				System.out.println("Invoice Start Date Of Quaterly=  " + startDateOfQuaterly);
				System.out.println("Invoice End Date Of Quaterly=  " + endDateOfQuaterly);
				//auditexecution.setInvcEndDt(startDateOfQuaterly);
		    	//auditexecution.setInvcEndDt(endDateOfQuaterly);
			}else if(invoiceTwoMnthTrend != null && invoiceTwoMnthTrend.equals(Nov)){							// Sept-Oct
				twoMonthTrendCalander.set(Calendar.MONTH, 8 ); 
				String firstMonthOfQuater = twoMonthTrendDateFormat.format(twoMonthTrendCalander.getTime());
				int firstDayOfQuaterly = twoMonthTrendCalander.getActualMinimum(Calendar.DAY_OF_MONTH);
				System.out.println("First Month Of Quater=  "+firstMonthOfQuater);
				
				twoMonthTrendCalander.set(Calendar.MONTH, 9); 
				String lastMonthOfQuater = twoMonthTrendDateFormat.format(twoMonthTrendCalander.getTime());
				int lastDayOfQuaterly = twoMonthTrendCalander.getActualMaximum(Calendar.DAY_OF_MONTH);
				System.out.println("Last Month Of Quater=   "+lastMonthOfQuater);
				
				String _firstDayOfQuaterly = Integer.toString(firstDayOfQuaterly);
				String _lastDayOfQuaterly = Integer.toString(lastDayOfQuaterly);
				
				String  startDateOfQuaterly  = _firstDayOfQuaterly.concat("-").concat(firstMonthOfQuater);
				String  endDateOfQuaterly  = _lastDayOfQuaterly.concat("-").concat(lastMonthOfQuater);
				System.out.println("Invoice Start Date Of Quaterly=  " + startDateOfQuaterly);
				System.out.println("Invoice End Date Of Quaterly=  " + endDateOfQuaterly);
				//auditexecution.setInvcEndDt(startDateOfQuaterly);
		    	//auditexecution.setInvcEndDt(endDateOfQuaterly);
			}else if(invoiceTwoMnthTrend != null && invoiceTwoMnthTrend.equals(Dec)){							// Oct-Nov
				twoMonthTrendCalander.set(Calendar.MONTH, 9 ); 
				String firstMonthOfQuater = twoMonthTrendDateFormat.format(twoMonthTrendCalander.getTime());
				int firstDayOfQuaterly = twoMonthTrendCalander.getActualMinimum(Calendar.DAY_OF_MONTH);
				System.out.println("First Month Of Quater=  "+firstMonthOfQuater);
				
				twoMonthTrendCalander.set(Calendar.MONTH, 10); 
				String lastMonthOfQuater = twoMonthTrendDateFormat.format(twoMonthTrendCalander.getTime());
				int lastDayOfQuaterly = twoMonthTrendCalander.getActualMaximum(Calendar.DAY_OF_MONTH);
				System.out.println("Last Month Of Quater=   "+lastMonthOfQuater);
				
				String _firstDayOfQuaterly = Integer.toString(firstDayOfQuaterly);
				String _lastDayOfQuaterly = Integer.toString(lastDayOfQuaterly);
				
				String  startDateOfQuaterly  = _firstDayOfQuaterly.concat("-").concat(firstMonthOfQuater);
				String  endDateOfQuaterly  = _lastDayOfQuaterly.concat("-").concat(lastMonthOfQuater);
				System.out.println("Invoice Start Date Of Quaterly=  " + startDateOfQuaterly);
				System.out.println("Invoice End Date Of Quaterly=  " + endDateOfQuaterly);
				//auditexecution.setInvcEndDt(startDateOfQuaterly);
		    	//auditexecution.setInvcEndDt(endDateOfQuaterly);
			}
	
	}
	
	public static void testDates3(){
		
	}
	public static void main(String[] args) {
		testDates();
		testDates2();
		testDates3();
	}

}
