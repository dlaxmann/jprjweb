package com.app.core;

public class Single {

	private static Single _single = null;
	
	private Single(){
		
	}
	
	public static Single getInstance(){
		if(_single == null){
		_single = new Single();
		}
		return _single;
	}
}
