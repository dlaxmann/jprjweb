package com.app.core.logic.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtils2 {
	public static void main(String[] args){
		
		Date d = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MMM dd,yyyy");
		System.out.println(sdf.format(d));
		
		
        System.out.println(getMonthBegin(new Date()).toLocaleString());
        //System.out.println(getMonthEnd(new Date()).toLocaleString());
        //System.out.println(str2Date("2006-11-31 23:59:59").toLocaleString());
        //System.out.println(getYearEnd(new Date()).toLocaleString());
        Date beginTime = str2Date("2006-12-12 00:00:00");
        Date endTime = str2Date("2006-12-13 00:00:00");
        System.out.println(getBetweenDay(beginTime,endTime));
    }
	public final static String DEFAULT_DATE_TIME_FORMAT_STYLE = "yyyy-MM-dd HH:mm:ss";
	public final static String getCurrTime(){
		return date2Str(new Date(),DEFAULT_DATE_TIME_FORMAT_STYLE);
	}
    public final static String getYear(){
        return date2Str(new Date(),"yyyy");
    }
    public final static String getDate(){
        return date2Str(new Date(),"yyyy-MM-dd");
    }
    public final static String getYear(Date date){
        if(null==date)return getYear();
        return date2Str(date,"yyyy");
    }
	
	public final static String date2Str(Date date,String formatString){
		SimpleDateFormat df = new SimpleDateFormat(formatString);
		return df.format(date);
	}
	public final static Date str2Date(String dateStr,String formatString){
		SimpleDateFormat df = new SimpleDateFormat(formatString);
		try {
			return df.parse(dateStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
    public final static Date str2Date(String dateStr){
        return str2Date(dateStr,DEFAULT_DATE_TIME_FORMAT_STYLE);
    }
    public final static Date getNextYear(Date now,int i){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(now);
        calendar.add(Calendar.YEAR,i);
        return calendar.getTime();
    }
    public final static Date getNextMonth(Date now,int i){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(now);
        calendar.add(Calendar.MONTH,i);
        return calendar.getTime();
    }
    public final static Date getNextWeek(Date now,int i){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(now);
        calendar.add(Calendar.DAY_OF_YEAR,i*7);
        return calendar.getTime();
    }
    public final static String getWeekDayName(Date now){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(now);
        switch(calendar.get(Calendar.DAY_OF_WEEK)){
            case Calendar.SUNDAY : return "";
            case Calendar.MONDAY : return "һ";
            case Calendar.TUESDAY : return "ڶ";
            case Calendar.WEDNESDAY : return "";
            case Calendar.THURSDAY : return "";
            case Calendar.FRIDAY : return "";
            case Calendar.SATURDAY : return "";
        }
        return "";
    }
    public final static Date getNextHour(Date now,int i){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(now);
        calendar.add(Calendar.HOUR,i);
        return calendar.getTime();
    }
    public final static Date getNextDay(Date now,int i){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(now);
        calendar.add(Calendar.DAY_OF_YEAR,i);
        return calendar.getTime();
    }
    public final static Date getMonthBegin(Date now){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(now);
        int today = calendar.get(Calendar.DAY_OF_MONTH-1);
        calendar.add(Calendar.DAY_OF_YEAR,1-today);
        return calendar.getTime();
    }
    public final static Date getMonthEnd(Date now){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(now);
        calendar.add(Calendar.MONTH, 1);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.add(Calendar.DAY_OF_YEAR, -1);
        return calendar.getTime();
    }
    public final static Date getWeekBegin(Date now){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(now);
        int today = calendar.get(Calendar.DAY_OF_WEEK);
        calendar.add(Calendar.DAY_OF_YEAR,1-today);
        return calendar.getTime();
    }
    public final static Date getWeekEnd(Date now){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(now);
        int today = calendar.get(Calendar.DAY_OF_WEEK);
        calendar.add(Calendar.DAY_OF_YEAR,7-today);
        return calendar.getTime();
    }
    public final static Date getYearBegin(Date now){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(now);
        calendar.set(Calendar.DAY_OF_YEAR, 1);
        return calendar.getTime();
    }
    public final static Date getYearEnd(Date now){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(now);
        calendar.add(Calendar.YEAR, 1);
        calendar.set(Calendar.DAY_OF_YEAR, 1);
        calendar.add(Calendar.DAY_OF_YEAR, -1);
        return calendar.getTime();
    }
    private final static long MS_DAY_LONG = 1000*60*60*24; 
    public final static long getBetweenDay(Date beginTime,Date endTime){
        return (endTime.getTime() - beginTime.getTime())/MS_DAY_LONG;
    }
}
