package com.app.core.logic.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

import org.apache.poi.hssf.model.Sheet;
import org.apache.poi.hssf.model.Workbook;
import org.apache.poi.hssf.record.formula.functions.Row;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFHeader;
import org.apache.poi.hssf.usermodel.HSSFPalette;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.usermodel.HeaderFooter;
import org.apache.poi.hssf.util.CellRangeAddress;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Footer;
import org.apache.poi.ss.usermodel.Header;
import org.apache.poi.ss.usermodel.IndexedColors;

import com.app.core.app.bean.Customer;

public class ExcelUtils {
	
	//, NewsletterBO newsletterBO ArrayList<Customer> custmrList, 
	//=================================================================================================================
		public static void generateCustomerListExcel(String fileName)   throws Exception {
			
			ArrayList<Customer> custmrList = new ArrayList();
			for(int i=0; i<20; i++){
				Customer custmr = new Customer();
				custmr.setTitle("title"+i);
				custmr.setFirstName("firstName"+i);
				custmr.setLastName("lastName"+i);
				custmr.setCompanyName("companyName"+i);
				custmr.setEmailAddress("fileName"+i);
				custmr.setAddress("address"+i);
				custmr.setCity("city"+i);
				custmrList.add(custmr);
			}
			System.out.println("____"+custmrList.size());
			Collection client = new HashSet();
			
			HSSFWorkbook wb = new HSSFWorkbook();
			HSSFSheet sheet = wb.createSheet("Report");
			//sheet.autoSizeColumn(1);
			
			HSSFCellStyle columnHeaderStyle = wb.createCellStyle();
			columnHeaderStyle.setFillBackgroundColor(
			HSSFColor.BLUE_GREY.index);
			
			HSSFFont font = wb.createFont();        
			font.setColor(HSSFColor.BLACK.index);
			font.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
			
			HSSFFont headerFont = wb.createFont();
			headerFont.setColor(HSSFColor.BLUE.index);
			headerFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			
			// Create the style
			HSSFCellStyle cellStyle= wb.createCellStyle();
			cellStyle.setFont(font);
			cellStyle.setFillBackgroundColor(HSSFColor.AQUA.index);
			
			HSSFCellStyle cellHeaderStyle= wb.createCellStyle();
			cellHeaderStyle.setFont(headerFont);
			cellHeaderStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
			cellHeaderStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
			//cellHeaderStyle.setFillBackgroundColor(HSSFColor.AQUA.index);
			cellHeaderStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND );
			cellHeaderStyle.setFillForegroundColor(new HSSFColor.LEMON_CHIFFON().getIndex());
			 
			cellHeaderStyle.setBorderTop((short)1);
			cellHeaderStyle.setBorderBottom((short)1);
			cellHeaderStyle.setBorderLeft((short)1);
			cellHeaderStyle.setBorderRight((short)1);
			
			cellStyle.setBorderTop((short)1);
			cellStyle.setBorderBottom((short)1);
			cellStyle.setBorderLeft((short)1);
			cellStyle.setBorderRight((short)1);
			
			if(!custmrList.isEmpty())	{
			    int rowNum =0;
			    HSSFRow row0 = sheet.createRow(rowNum);
			    CellRangeAddress ad = new CellRangeAddress(0,0,4,5);
			    sheet.addMergedRegion(ad);
			    
			    CellRangeAddress ad1 = new CellRangeAddress(0,1,1,2);
			    sheet.addMergedRegion(ad1);
			    
			    HSSFCell cell00a=  row0.createCell(0);
			    cell00a.setCellValue("Cell0");
			    cell00a.setCellStyle(cellHeaderStyle);
			    
			    HSSFCell cell000a=  row0.createCell(1);
			    cell000a.setCellValue("Cell1 & Cell2");
			    cell000a.setCellStyle(cellHeaderStyle);
			    
			    HSSFCell cell0000a=  row0.createCell(3);
			    cell0000a.setCellValue("Cell3");
			    cell0000a.setCellStyle(cellHeaderStyle);
			
			    HSSFCell cell10a=  row0.createCell(4);
			    cell10a.setCellValue("Cell4");
			    cell10a.setCellStyle(cellHeaderStyle);
			    
			    HSSFCell cell20a=  row0.createCell(5);
			    cell20a.setCellValue("Cell5");
			    cell20a.setCellStyle(cellHeaderStyle);
			    
			    HSSFCell cell30a=  row0.createCell(6);
			    cell30a.setCellValue("Cell6");
			    cell30a.setCellStyle(cellHeaderStyle);
			    
			    rowNum = 1;
			    HSSFRow rowa = sheet.createRow(rowNum);
			    
			    HSSFCell cell0a=  rowa.createCell(0);
			    cell0a.setCellValue("Title");
			    cell0a.setCellStyle(cellHeaderStyle);
			
			    HSSFCell cell1a=  rowa.createCell(1);
			    cell1a.setCellValue("FirstName");
			    cell1a.setCellStyle(cellHeaderStyle);
			    //row.createCell(2).setCellValue(cust.FirstName);
			
			    HSSFCell cell2a=  rowa.createCell(2);
			    cell2a.setCellValue("LastName");
			    cell2a.setCellStyle(cellHeaderStyle);
			
			    HSSFCell cell3a=  rowa.createCell(3);
			    cell3a.setCellValue("CompanyName");
			    cell3a.setCellStyle(cellHeaderStyle);
			
			    HSSFCell cell4a=  rowa.createCell(4);
			    cell4a.setCellValue("EmailAddress");
			    cell4a.setCellStyle(cellHeaderStyle);
			    
			    HSSFCell cell5a=  rowa.createCell(5);
			    cell5a.setCellValue("Address");
			    cell5a.setCellStyle(cellHeaderStyle);
			    
			    HSSFCell cell6a=  rowa.createCell(6);
			    cell6a.setCellValue("City");
			    cell6a.setCellStyle(cellHeaderStyle);
			
			    sheet.setRowBreak(4);
			    
			    rowNum = 2;
			    Iterator<Customer> itr = custmrList.iterator();
			    while(itr.hasNext())   {
			        Customer cust = (Customer)itr.next();
			       // client.add(new Customer(cust.Title, cust.FirstName, cust.LastName, cust.CompanyName, cust.EmailAddress));
			
			        HSSFRow row = sheet.createRow(rowNum);
			
			        HSSFCell cell1=  row.createCell(0);
			        cell1.setCellValue(cust.getTitle());
			        cell1.setCellStyle(cellStyle);
			
			        HSSFCell cell2=  row.createCell(1);
			        cell2.setCellValue(cust.getFirstName());
			        cell2.setCellStyle(cellStyle);
			        //row.createCell(2).setCellValue(cust.FirstName);
			
			        HSSFCell cell3=  row.createCell(2);
			        cell3.setCellValue(cust.getLastName());
			        cell3.setCellStyle(cellStyle);
			
			        HSSFCell cell4=  row.createCell(3);
			        cell4.setCellValue(cust.getCompanyName());
			        cell4.setCellStyle(cellStyle);
			
			        HSSFCell cell5=  row.createCell(4);
			        cell5.setCellValue(cust.getEmailAddress());
			        cell5.setCellStyle(cellStyle);
			        
			        HSSFCell cell6=  row.createCell(5);
			        cell6.setCellValue(cust.getAddress());
			        cell6.setCellStyle(cellStyle);
			        
			        HSSFCell cell7=  row.createCell(6);
			        cell7.setCellValue(cust.getCity());
			        cell7.setCellStyle(cellStyle);
			        
			        rowNum = rowNum+1;
			        if(rowNum == 5){
			        	 sheet.setRowBreak(2);
			        }
			       
			    }//while end
			}
			/*Map beans = new HashMap();
			beans.put("distributionlist", client);*/
			
			//XLSTransformer transformer = new XLSTransformer();
			//FileOutputStream fileOut = new FileOutputStream(newsletterBO.OutputAttachmentFolderPath+fileName);
			  for (int i = 0; i < 25; i++) {
				  sheet.autoSizeColumn((short)i);
				}
	            FileOutputStream out = new FileOutputStream(fileName);
	            wb.write(out);
	            out.flush();
	            out.close();
	            
	            System.out.println("....Done");
			}
	
	//===============================================================================================================
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	//reading excel
	public static void readExcel(){
		 try {
			 //get inputstream from the file
		        FileInputStream iStream = new FileInputStream("files/H1.xls");
		        HSSFWorkbook workbook = new HSSFWorkbook(iStream);
		        
		        //get sheet to write
		        HSSFSheet worksheet = workbook.getSheetAt(0);
		        //HSSFSheet worksheet = workbook.getSheet("Worksheet");
		        
		        //get first row
		        HSSFRow row = worksheet.getRow(0);
		        
		        //get cell
		        HSSFCell cellA1 = row.getCell(0);
		        System.out.println("A1 "+cellA1.getStringCellValue());
		        HSSFCell cellB1 = row.getCell(1);
		        System.out.println("B1 "+cellB1.getStringCellValue());
		        HSSFCell cellC1 = row.getCell(2);
		        System.out.println("C1 "+cellC1.getStringCellValue());
		        HSSFCell cellD1 = row.getCell(3);
		        System.out.println("D1 "+cellD1.getDateCellValue());
		        //get next row
		        row = worksheet.getRow(1);
		        HSSFCell cellA2 = row.getCell(0);
		        System.out.println("A2 "+cellA2.getStringCellValue());
		        HSSFCell cellB2 = row.getCell(1);
		        System.out.println("B2 "+cellB2.getStringCellValue());
		        HSSFCell cellC2 = row.getCell(2);
		        System.out.println("C2 "+cellC2.getBooleanCellValue());
		        HSSFCell cellC3 = row.getCell(3);
		        System.out.println("D2 "+cellC3.getErrorCellValue());

		      } catch (FileNotFoundException e) {
		        e.printStackTrace();
		      } catch (IOException e) {
		        e.printStackTrace();
		      }
	}
	
	//writing to excel
	public static void writeExcel(){
		try{
			  FileOutputStream fileOut = new FileOutputStream("files/H3.xls");
		      HSSFWorkbook workbook = new HSSFWorkbook();
		      HSSFSheet worksheet = workbook.createSheet("Worksheet1");

		      HSSFRow row = worksheet.createRow((short) 0);

		      HSSFCell cellA1 = row.createCell(0);
		      cellA1.setCellValue("Hello");
		      HSSFCellStyle styleOfCell = workbook.createCellStyle();
		      styleOfCell.setFillForegroundColor(HSSFColor.AQUA.index);
		      styleOfCell.setFillPattern(HSSFCellStyle.BORDER_THIN);
		      cellA1.setCellStyle(styleOfCell);

		      HSSFCell cellB1 = row.createCell(1);
		      cellB1.setCellValue("World");
		      styleOfCell = workbook.createCellStyle();
		      styleOfCell.setFillForegroundColor(HSSFColor.AQUA.index);
		      styleOfCell.setFillPattern(HSSFCellStyle.BORDER_THIN);
		      cellB1.setCellStyle(styleOfCell);

		      HSSFCell cellC1 = row.createCell(2);
		      cellC1.setCellValue("Happy");
		      styleOfCell = workbook.createCellStyle();
		      styleOfCell.setFillForegroundColor(HSSFColor.AQUA.index);
		      styleOfCell.setFillPattern(HSSFCellStyle.BORDER_THIN);
		      cellC1.setCellStyle(styleOfCell);

		      HSSFCell cellD1 = row.createCell(3);
		      cellD1.setCellValue(new Date());
		      styleOfCell = workbook.createCellStyle();
		      styleOfCell.setDataFormat(HSSFDataFormat
		          .getBuiltinFormat("m/d/yy h:mm"));
		      styleOfCell.setFillForegroundColor(HSSFColor.AQUA.index);
		      styleOfCell.setFillPattern(HSSFCellStyle.BORDER_THIN);
		      cellD1.setCellStyle(styleOfCell);
		     
		      row = worksheet.createRow(1);
		        row.createCell(0).setCellValue(Calendar.getInstance().getTime().toString());
		        row.createCell(1).setCellValue("a string");
		        row.createCell(2).setCellValue(true);
		        row.createCell(3).setCellType(Cell.CELL_TYPE_ERROR);

		      workbook.write(fileOut);
		      fileOut.flush();
		      fileOut.close();
		    } catch (FileNotFoundException e) {
		      e.printStackTrace();
		    } catch (IOException e) {
		      e.printStackTrace();
		    }

	}
	//------------------------------------------------------------------------------
	
	 public static void readDataToExcelFile(String fileName){
	        try{
	            FileInputStream fis = new FileInputStream(fileName);
	            HSSFWorkbook workbook = new HSSFWorkbook(fis);
	            HSSFSheet sheet = workbook.getSheetAt(0);

	            for (int rowNum = 0; rowNum < 10; rowNum++) {
	                for (int cellNum = 0; cellNum < 5; cellNum++) {
	                    HSSFCell cell = sheet.getRow(rowNum).getCell(cellNum);
	                    System.out.println(rowNum+":"+cellNum+" = " + cell.getStringCellValue());
	                }
	            }
	            
	            fis.close();
	        }catch(Exception e){
	            e.printStackTrace();
	        }
	    }
	
	 
	 public static void writeDataToExcelFile(String fileName) {
	        try {

	            HSSFWorkbook myWorkBook = new HSSFWorkbook();
	            HSSFSheet mySheet = myWorkBook.createSheet();
	            HSSFRow myRow;
	            HSSFCell myCell;
	            
	            HSSFFont hssfFont = myWorkBook.createFont();
	            hssfFont.setFontName(HSSFFont.FONT_ARIAL);
	            hssfFont.setFontHeightInPoints((short) 12);
	            hssfFont.setBoldweight((short) 18);
	            hssfFont.setColor(HSSFColor.BLUE.index);
	            hssfFont.setFontHeightInPoints((short) 12);
	            hssfFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
	       
	            HSSFCellStyle style = myWorkBook.createCellStyle();
	            style.setFont(hssfFont);
	            style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
	            
	           /* myRow = mySheet.createRow(0);
	            myRow.createCell(0).setCellValue(new HSSFRichTextString("Header1"));
	            myRow.createCell(0).setCellValue(new HSSFRichTextString("Header2"));
	            myRow.createCell(0).setCellValue("Header3");
	            myRow.createCell(0).setCellValue("Header4");
	            myRow.createCell(0).setCellValue("Header5");
	            myRow.createCell(0).setCellStyle(style);        
	            mySheet.autoSizeColumn((short) 1);*/
	            
	            for (int rowNum = 0; rowNum < 10; rowNum++) {
	            	int count = 0;
	                myRow = mySheet.createRow(rowNum);
	                
	                if(rowNum == 0){
	                	
	                	CellRangeAddress ad = new CellRangeAddress(0,0,1,3);
	                	mySheet.addMergedRegion(ad);
	                	
	                	//myRow.createCell(0).setCellStyle(style); 
	                	//myRow.createCell(0).setCellValue(new HSSFRichTextString("Header0"));
	                	myRow.createCell(1).setCellValue(new HSSFRichTextString("Header1  merging col1, col2, col3"));
	                	myRow.createCell(2).setCellValue(new HSSFRichTextString("Header2"));
	                	myRow.createCell(3).setCellValue(new HSSFRichTextString("Header3"));
	                	myRow.createCell(4).setCellValue(new HSSFRichTextString("Header4"));
	                	myRow.createCell(5).setCellValue(new HSSFRichTextString("Header5"));
	                	myRow.createCell(6).setCellValue(new HSSFRichTextString("Header6"));
	                	
	                	HSSFCell cell = myRow.createCell(0);
	         	        cell.setCellValue(new HSSFRichTextString("Header0"));
	         	        cell.setCellStyle(style);        
	                	myRow.setRowStyle(style);
	                	
                    }else{
	                for (int cellNum = 0; cellNum < 10; cellNum++) {
	                	
	                    myCell = myRow.createCell(cellNum+count);
	                    myCell.setCellValue(new HSSFRichTextString("rowNum"+rowNum + "___cellNum"+cellNum));
	                    System.out.println(rowNum+"__"+cellNum);
	                    /*if(rowNum == 0){
	                    	myCell.setCellValue(new HSSFRichTextString("Header1"));
	                    	myCell.setCellValue(new HSSFRichTextString("Header2"));;
	                    }else{
	                    	myCell.setCellValue(new HSSFRichTextString(rowNum + "___________" + cellNum));
		                    System.out.println(rowNum+"__"+cellNum);
	                    }*/
	                    
	                }
                    }
	            }
	            
	            for (int i = 0; i < 10; i++) {
	            	mySheet.autoSizeColumn((short)i);
				}
	            
	            
	            FileOutputStream out = new FileOutputStream(fileName);
	            myWorkBook.write(out);
	            out.flush();
	            out.close();
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	    }
	
	
	 public static void excelCellFormat(){
	        HSSFWorkbook workbook = new HSSFWorkbook();
	        HSSFSheet sheet = workbook.createSheet();
	 
	        //
	        // Create an instance of HSSFCellStyle which will be use to format the
	        // cell. Here we define the cell top and bottom border and we also
	        // define the background color.
	        //
	        HSSFCellStyle style = workbook.createCellStyle();
	        style.setBorderTop((short) 6); // double lines border
	        style.setBorderBottom((short) 1); // single line border
	        style.setFillBackgroundColor(HSSFColor.GREY_25_PERCENT.index);
	 
	        //
	        // We also define the font that we are going to use for displaying the
	        // data of the cell. We set the font to ARIAL with 20pt in size and
	        // make it BOLD and give blue as the color.
	        //
	        HSSFFont font = workbook.createFont();
	        font.setFontName(HSSFFont.FONT_ARIAL);
	        font.setFontHeightInPoints((short) 20);
	        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
	        font.setColor(HSSFColor.BLUE.index);
	        style.setFont(font);
	 
	        //
	        // We create a simple cell, set its value and apply the cell style.
	        //
	        HSSFRow row = sheet.createRow(1);
	        HSSFCell cell = row.createCell(1);
	        cell.setCellValue(new HSSFRichTextString("Hi there... It's me again!"));
	        cell.setCellStyle(style);        
	        sheet.autoSizeColumn((short) 1);
	 
	        //
	        // Finally we write out the workbook into an excel file.
	        //
	        FileOutputStream fos = null;
	        try {
	            fos = new FileOutputStream(new File("files/XLFormat.xls"));
	            workbook.write(fos);
	        } catch (IOException e) {
	            e.printStackTrace();
	        } finally {
	            if (fos != null) {
	                try {
	                    fos.flush();
	                    fos.close();
	                } catch (IOException e) {
	                    e.printStackTrace();
	                }
	            }
	        }
	 }
	 
	 
	 //=================================================================================
	//------------------------------------------------------------------------------------------
		public static void generateCustomerListExcelTest(String fileName)   throws Exception {
			
			ArrayList<Customer> custmrList = new ArrayList();
			for(int i=0; i<20; i++){
				Customer custmr = new Customer();
				custmr.setTitle("title"+i);
				custmr.setFirstName("firstName"+i);
				custmr.setLastName("lastName"+i);
				custmr.setCompanyName("companyName"+i);
				custmr.setEmailAddress("fileName"+i);
				custmr.setAddress("address"+i);
				custmr.setCity("city"+i);
				custmrList.add(custmr);
			}
			System.out.println("____"+custmrList.size());
			Collection client = new HashSet();
			
			HSSFWorkbook wb = new HSSFWorkbook();
			HSSFSheet sheet = wb.createSheet("Report");
			
			HSSFCellStyle columnHeaderStyle = wb.createCellStyle();
			columnHeaderStyle.setFillBackgroundColor(
			HSSFColor.BLUE_GREY.index);
			
			HSSFFont font = wb.createFont();        
			font.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
			
			HSSFFont headerFont = wb.createFont();
			headerFont.setColor(HSSFColor.BLACK.index);
			headerFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			
			HSSFCellStyle cellStyle= wb.createCellStyle();
			cellStyle.setFont(font);
			
			HSSFCellStyle cellHeaderStyle= wb.createCellStyle();
			cellHeaderStyle.setFont(headerFont);
			cellHeaderStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
			cellHeaderStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
			cellHeaderStyle.setFillForegroundColor(new HSSFColor.ORANGE().getIndex());
			cellHeaderStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
			
						
			HSSFCellStyle style = wb.createCellStyle();
			style.setFont(headerFont);
			style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
			style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		    style.setFillForegroundColor(HSSFColor.LIME.index);
		    style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		    style.setBorderTop((short)1);
		    style.setBorderBottom((short)1);
		    style.setBorderLeft((short)1);
		    style.setBorderRight((short)1);
		    
		    
			
			cellHeaderStyle.setBorderTop((short)1);
			cellHeaderStyle.setBorderBottom((short)1);
			cellHeaderStyle.setBorderLeft((short)1);
			cellHeaderStyle.setBorderRight((short)1);
			
			cellStyle.setBorderTop((short)1);
			cellStyle.setBorderBottom((short)1);
			cellStyle.setBorderLeft((short)1);
			cellStyle.setBorderRight((short)1);
			
			if(!custmrList.isEmpty())	{
			    int rowNum =0;
			    
			    HSSFPalette palette = wb.getCustomPalette();
			    //replacing the standard red with freebsd.org red
			    palette.setColorAtIndex(HSSFColor.RED.index,
			            (byte) 153,  //RGB red (0-255)
			            (byte) 0,    //RGB green
			            (byte) 0     //RGB blue
			    );
			    //replacing lime with freebsd.org gold
			    palette.setColorAtIndex(HSSFColor.LIME.index, (byte) 255, (byte) 204, (byte) 102);
			    
			    
			    HSSFRow row0 = sheet.createRow(rowNum);
			    CellRangeAddress ad = new CellRangeAddress(0,0,4,5);
			    sheet.addMergedRegion(ad);
			    
			    CellRangeAddress ad1 = new CellRangeAddress(0,1,1,2);
			    sheet.addMergedRegion(ad1);
			    
			  
			    
			    
			    HSSFCell cell00a=  row0.createCell(0);
			    cell00a.setCellValue("Cell0");
			    cell00a.setCellStyle(style);
			    
			    HSSFCell cell000a=  row0.createCell(1);
			    cell000a.setCellValue("Cell1 & Cell2");
			    cell000a.setCellStyle(style);
			    
			    HSSFCell cell0000a=  row0.createCell(3);
			    cell0000a.setCellValue("Cell3");
			    cell0000a.setCellStyle(style);
			
			    HSSFCell cell10a=  row0.createCell(4);
			    cell10a.setCellValue("Cell4");
			    cell10a.setCellStyle(cellHeaderStyle);
			    
			    HSSFCell cell20a=  row0.createCell(5);
			    cell20a.setCellValue("Cell5");
			    cell20a.setCellStyle(style);
			    
			    HSSFCell cell30a=  row0.createCell(6);
			    cell30a.setCellValue("Cell6");
			    cell30a.setCellStyle(style);
			    
			    rowNum = 1;
			    HSSFRow rowa = sheet.createRow(rowNum);
			    
			    HSSFCell cell0a=  rowa.createCell(0);
			    cell0a.setCellValue("Title");
			    cell0a.setCellStyle(cellHeaderStyle);
			
			    HSSFCell cell1a=  rowa.createCell(1);
			    cell1a.setCellValue("FirstName");
			    cell1a.setCellStyle(cellHeaderStyle);
			    //row.createCell(2).setCellValue(cust.FirstName);
			
			    HSSFCell cell2a=  rowa.createCell(2);
			    cell2a.setCellValue("LastName");
			    cell2a.setCellStyle(cellHeaderStyle);
			
			    HSSFCell cell3a=  rowa.createCell(3);
			    cell3a.setCellValue("CompanyName");
			    cell3a.setCellStyle(cellHeaderStyle);
			
			    HSSFCell cell4a=  rowa.createCell(4);
			    cell4a.setCellValue("EmailAddress");
			    cell4a.setCellStyle(cellHeaderStyle);
			    
			    HSSFCell cell5a=  rowa.createCell(5);
			    cell5a.setCellValue("Address");
			    cell5a.setCellStyle(cellHeaderStyle);
			    
			    HSSFCell cell6a=  rowa.createCell(6);
			    cell6a.setCellValue("City");
			    cell6a.setCellStyle(cellHeaderStyle);
			
			    sheet.setRowBreak(4);
			    
			    rowNum = 2;
			    Iterator<Customer> itr = custmrList.iterator();
			    while(itr.hasNext())   {
			        Customer cust = (Customer)itr.next();
			       // client.add(new Customer(cust.Title, cust.FirstName, cust.LastName, cust.CompanyName, cust.EmailAddress));
			
			        HSSFRow row = sheet.createRow(rowNum);
			
			        HSSFCell cell1=  row.createCell(0);
			        cell1.setCellValue(cust.getTitle());
			        cell1.setCellStyle(cellStyle);
			
			        HSSFCell cell2=  row.createCell(1);
			        cell2.setCellValue(cust.getFirstName());
			        cell2.setCellStyle(cellStyle);
			        //row.createCell(2).setCellValue(cust.FirstName);
			
			        HSSFCell cell3=  row.createCell(2);
			        cell3.setCellValue(cust.getLastName());
			        cell3.setCellStyle(cellStyle);
			
			        HSSFCell cell4=  row.createCell(3);
			        cell4.setCellValue(cust.getCompanyName());
			        cell4.setCellStyle(cellStyle);
			
			        HSSFCell cell5=  row.createCell(4);
			        cell5.setCellValue(cust.getEmailAddress());
			        cell5.setCellStyle(cellStyle);
			        
			        HSSFCell cell6=  row.createCell(5);
			        cell6.setCellValue(cust.getAddress());
			        cell6.setCellStyle(cellStyle);
			        
			        HSSFCell cell7=  row.createCell(6);
			        cell7.setCellValue(cust.getCity());
			        cell7.setCellStyle(cellStyle);
			        
			        rowNum = rowNum+1;
			        if(rowNum == 5){
			        	 sheet.setRowBreak(2);
			        }
			       
			    }//while end
			}
			Map beans = new HashMap();
			beans.put("distributionlist", client);
			
			//XLSTransformer transformer = new XLSTransformer();
			//FileOutputStream fileOut = new FileOutputStream(newsletterBO.OutputAttachmentFolderPath+fileName);
			  for (int i = 0; i < 10; i++) {
				  sheet.autoSizeColumn((short)i);
				}
	            FileOutputStream out = new FileOutputStream(fileName);
	            wb.write(out);
	            out.flush();
	            out.close();
	            System.out.println("---Done");
			}

	 //===================================================================================
		public static void customColors(String fileName,String fileName1) throws IOException{
			HSSFWorkbook wb = new HSSFWorkbook();
		    HSSFSheet sheet = wb.createSheet();
		    HSSFRow row = sheet.createRow((short) 0);
		    HSSFCell cell = row.createCell((short) 0);
		    cell.setCellValue("Default Paletteeeeeee");

		    //apply some colors from the standard palette,
		    // as in the previous examples.
		    //we'll use red text on a lime background

		    HSSFCellStyle style = wb.createCellStyle();
		    style.setFillForegroundColor(HSSFColor.LIME.index);
		    style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

		   /* HSSFFont font = wb.createFont();
		    font.setColor(HSSFColor.RED.index);
		    style.setFont(font);*/

		    cell.setCellStyle(style);

		    //save with the default palette
		    FileOutputStream out = new FileOutputStream(fileName1);
		    //wb.write(out);
		   // out.close();

		    //now, let's replace RED and LIME in the palette
		    // with a more attractive combination
		    // (lovingly borrowed from freebsd.org)

		    cell.setCellValue("Modified Palettexxxxxx");

		    //creating a custom palette for the workbook
		    HSSFPalette palette = wb.getCustomPalette();

		    //replacing the standard red with freebsd.org red
		    palette.setColorAtIndex(HSSFColor.RED.index,
		            (byte) 153,  //RGB red (0-255)
		            (byte) 0,    //RGB green
		            (byte) 0     //RGB blue
		    );
		    //replacing lime with freebsd.org gold
		    palette.setColorAtIndex(HSSFColor.LIME.index, (byte) 255, (byte) 204, (byte) 102);

		    //save with the modified palette
		    // note that wherever we have previously used RED or LIME, the
		    // new colors magically appear
		    //FileOutputStream out = new FileOutputStream(fileName1);
		    wb.write(out);
		    out.close();
		    System.out.println("...Done");

		}
		
		public static void bigExample(String fileName){

				 
		}
		
		
		
		public static void setBorderMerged(String fileName){
			
		}
		
		
		
	public static void main(String[] args) throws Exception {

		//ExcelUtils.readExcel();
		//ExcelUtils.writeExcel();
		String fileName = "files/H22.xls";
		String fileName1 = "files/H222.xls";
		String fileName11 = "files/D544050001.xls";
		
		
       // writeDataToExcelFile(fileName);
       generateCustomerListExcel(fileName1);
        
      // generateCustomerListExcelTest(fileName11);
       //readDataToExcelFile(fileName);
        //excelCellFormat(); 
       // customColors(fileName,fileName1);
        
      // setBorderMerged(fileName);
        
        
	    }
	    
	  }
	


