package com.app.core.logic.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.app.core.app.bean.Customer;
import com.app.core.app.bean.Student;

public class CollectionsUtil {
	private static Logger _logger = Logger.getLogger(CollectionsUtil.class);
	//http://www.techlabs4u.com/2012/04/java-to-find-print-duplicates-using-or.html
	
	//find and remove duplicates in array
	public static void getDuplicatesInArray(){
		String[] ar ={"21","22","23","21","22"};
		Set<String> st = new HashSet<String>();
		for (String string : ar) {
			if(!st.add(string.trim()));
			System.out.println("found dup"+string);
		}
	}
	
	// find and print duplicate words in a Array / ArrayList / Line using Map (HashMap) 
	public static void duplicateInArrayList(){
		final Object dummy = new Object();  // class variable
		String inputLine="Constructs a new set set containing the elements elements in the specified collection";
		
		// split the words in a line and stores in a list
		List<String> arrayList1 = Arrays.asList(inputLine.split("\\s+"));   
		arrayList1.toString();
		//Creating HashMap which does not allows duplicates and Maintains order 
		Map<Object, Object> hm = new LinkedHashMap<Object, Object>(); 
		 for (Object item : arrayList1)
		if (hm.put(item, dummy) !=null) 
		 System.out.println(item ); 
	}
	
	//find and remove duplicates in a Array / ArrayList using ArrayList
	//remove duplicates and cature the number of occurrence of the duplicate values in the arraylist. 
	  public static void removeDup(){
		  String[] ar ={"21","22","23","21","22"};
		  List<String> arrayList1 = Arrays.asList(ar);
		  System.out.println("initial siz: "+arrayList1.size());
		  Map<Object, Integer> hm = new LinkedHashMap<Object, Integer>();
			 for (Object item : arrayList1) {
			              Integer count = hm.get(item);
			              if  (hm.put(item , (count == null ? 1 : count + 1))!=null)
			               System.out.println("Found Duplicate : " +item  );
			 }
	  }
	  
	  
	  
	  
	  public static void getValuesFromMap(){
		  Map mapp = new HashMap();
		  Customer cs = new Customer();
		  cs.setFirstName("fnm");
		  cs.setLastName("lnm");
		  mapp.put(1, cs);
		 
		  System.out.println( mapp.get(1));
		  System.out.println(mapp.get("____"+cs.getFirstName()));
		  
		
	  }
	  
	  
		public static void testList() {
			List lt = new ArrayList();

			lt.add("a");
			lt.add("b");
			lt.add("c");

			lt.get(0);
			lt.get(1);
			lt.get(2);
			_logger.info("list size" + lt.size());

			lt.getClass().getClassLoader();
			System.out.println("lt;" + lt);
			System.out.println("lt.get(0);" + lt.get(0));
			System.out.println("lt.get(1);" + lt.get(1));
			System.out.println("lt.get(2);" + lt.get(2));
			System.out.println("lt.getClass();" + lt.getClass());
			System.out.println("lt.getClass().getClassLoader();"
					+ lt.getClass().getClassLoader());
			_logger.info("list size" + lt.size());
		}

		
		public static void listToArr(){
			List<String> Mylist = new ArrayList<String>();
			  Mylist.add("Java"); 
			  Mylist.add("is"); 
			  Mylist.add("a");
			  Mylist.add("wonderful");
			  Mylist.add("language");
			  String[] s1 = Mylist.toArray(new String[0]); //Collection to array 
			  for(int i = 0; i < s1.length; ++i){
			  String contents = s1[i];
			  System.out.print(contents);
			  }
		}
		
		
		public static void mapTest(){
			Map<Long,String> hashMap = new HashMap<Long,String>();
			
			hashMap.put(1L, "one");
			hashMap.put(2L, "two");
			hashMap.put(3L, "three");
			
			System.out.println(hashMap.get(1L));
			System.out.println(hashMap.get(3L));
			
		}
	  
	  
	  
	  
	  public static void getFromList(){
		  Student std1 = new Student();
		  std1.setRollNo("1");
		  std1.setName("rama");
		  
		  Student std2 = new Student();
		  std2.setRollNo("2");
		  std2.setName("ramaa");
		  
		  Student std3 = new Student();
		  std3.setRollNo("3");
		  std3.setName("ramaaa");
		  List<Student> students = new ArrayList();
		  
		  students.add(std1);
		  students.add(std2);
		  students.add(std3);
		  
		  System.out.println("--" +students.get(0).getName());
		  System.out.println("-----" +students.contains(std2.getName().equalsIgnoreCase("ramaa")));
		  
	  }
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	
//	  (a > b) ? a : b; is an expression which returns one of two values, 
//a or b. The condition, (a > b), is tested. If it is true the first value, a, is returned. 
// If it is false, the second value, b, is returned.
	public static void main(String[] args){
		//getDuplicatesInArray();
		//duplicateInArrayList();
		//removeDup();
		//getValuesFromMap();
		getFromList();
	}
}
