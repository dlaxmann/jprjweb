package com.app.core.logic.utils;

import java.util.Calendar;

public class CalendarSetVal {

	public static void printDates(){
		Calendar c= Calendar.getInstance();
		 //Displays the current calendar
		 System.out.println("Calendar before Setting is "+c.getTime());    
		 //Sets all the calendar field values and the time value of this Calendar
		 //undefined.
		 c.clear();
		 //Sets the values for the calendar fields YEAR, MONTH,and DAY_OF_MONTH.
		 c.set(2007,10,27);
		 //Displays the calendar after setting
		 System.out.println("Calendar after Setting is "+c.getTime());
		 c.clear();
		 System.out.println("---------------");
		 c.set(Calendar.YEAR,Calendar.MONTH,Calendar.DAY_OF_MONTH);
		 System.out.println("Calendar after Setting is "+c.getTime());
		 
		 Calendar today = Calendar.getInstance();
			today.set(Calendar.DAY_OF_MONTH, 1);
			today.set(Calendar.MONTH, Calendar.AUGUST);
			today.set(Calendar.YEAR, 2012);
			System.out.println("Calendar after Setting is "+today.getTime());
			
			today.clear();
			
			today.set(Calendar.YEAR,0);
			today.set(Calendar.MONTH, 0);
			today.set(Calendar.DAY_OF_MONTH, 0);
			System.out.println("Calendar after Setting is "+today.getTime());
			
			System.out.println(Calendar.getInstance().get(Calendar.YEAR));
			System.out.println(Calendar.getInstance().get(Calendar.MONTH));
			System.out.println(Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
			today.clear();
			
			/*today.set(Calendar.YEAR,Calendar.MONTH,Calendar.DAY_OF_MONTH);
			System.out.println("Calendar after Setting is "+today.getTime());*/
			
			 Calendar todayy = Calendar.getInstance();
			 System.out.println("Calendar after Setting is "+todayy.getTime());
	    }
	
	
	
	public static void main(String[] arg){
		printDates();
		
		
	}
}
