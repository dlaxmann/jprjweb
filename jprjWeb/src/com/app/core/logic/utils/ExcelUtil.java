package com.app.core.logic.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

public class ExcelUtil {
	
static String filename = "";

	
	
	/**
	 * This method is used for parsing an excel file
	 * 
	 * @param fileName
	 * @return list containing recodes sets of each sheet of an excel sheet.
	 */

	public static List returnExcelAsList(String fileName) throws Exception {

		final List excelData = new ArrayList();

		InputStream myxls = null;
		try {
			myxls = new FileInputStream(fileName);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			throw new Exception(e.getMessage(),e.getCause());
		}
		HSSFWorkbook wb = null;
		try {
			wb = new HSSFWorkbook(myxls);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			throw new Exception(e.getMessage(),e.getCause());
		}
		int sheetIdx = 0;
		HSSFSheet sheet = wb.getSheetAt(sheetIdx);
		Set dataSheet = new HashSet();
		while (sheet != null) {
			int beginRow = sheet.getFirstRowNum();
			int endRow = sheet.getLastRowNum();
			List headerCols = new ArrayList();
			for (int i = beginRow; i <= endRow; i++) {
				HSSFRow row = sheet.getRow(i);
				Map dataMap = new HashMap();
				if (row != null) {
					int beginCell = row.getFirstCellNum();
					int endCell = row.getLastCellNum();
					dataSheet = new HashSet();
					for (int j = beginCell; j <= endCell; j++) {
						HSSFCell cell = row.getCell((short) j);
						String cellVal = null;
						double cellValDoub;
						if (cell != null) {
							switch (cell.getCellType()) {
							case HSSFCell.CELL_TYPE_NUMERIC:
								HSSFCellStyle style = cell.getCellStyle();
								if (style.getIndex() == 35) {
									cellVal = cell.getDateCellValue()
											.toString();
									String date = null;
									try {
										date = new SimpleDateFormat(
												"MM/dd/yyyy")
												.format(new SimpleDateFormat(
														"EEE MMM dd HH:mm:ss zzz yyyy")
														.parse(cellVal
																.toString()));
									} catch (ParseException e) {
										// TODO Auto-generated catch block
										throw new Exception(e.getMessage(),e.getCause());
									}
									cellVal = date;
								} else {
									cellVal = String.valueOf(cell
											.getNumericCellValue());
								}
								break;
							case HSSFCell.CELL_TYPE_STRING:
								cellVal = cell.getStringCellValue();
								break;
							case HSSFCell.CELL_TYPE_FORMULA:
								cellValDoub = cell.getNumericCellValue();
								DecimalFormat df = new DecimalFormat("##");
								cellVal = df.format(cellValDoub).toString();
								break;
							default:
								break;
							}// switch end

							if (i == beginRow) {
								headerCols.add(j, cellVal);
							} else {								
								dataMap.put(headerCols.get(j).toString() + i,
										cellVal);
							}
						}

					}
					if (i != beginRow) {
						dataSheet.add(dataMap);
					}
				}
			}

			if (dataSheet != null && dataSheet.size() != 0) {
				excelData.add(dataSheet);
			}
			sheetIdx++;
			try {
				sheet = wb.getSheetAt(sheetIdx);
			} catch (IllegalArgumentException IllegalArgException) {
				break;
			}

		}
		return excelData;
	}
	/**
	 * @param filepath - path of the output File.
	 * @param lastTwoKey = true - the Map key will be combination of last 2 elements / false - Key will be combination of first two elements.
	 * @return - Output set Object
	 * @throws Exception
	 */
	public static Set<Map> returnExcelAsMap(String filepath,boolean lastTwoKey) throws Exception   {
		filename = filepath;
		Set<Map> hashSet =  new HashSet<Map>();
		LinkedHashMap mapObject= null;
		LinkedHashMap map= null;
		try {
			
			InputStream input = new FileInputStream(filename);
            POIFSFileSystem fs = new POIFSFileSystem( input );
            HSSFWorkbook wb = new HSSFWorkbook(fs);
            HSSFSheet sheet = wb.getSheetAt(0);
			 
            Iterator rows = sheet.rowIterator(); 
            while( rows.hasNext() ) {           
                HSSFRow row = (HSSFRow) rows.next();
                mapObject= new LinkedHashMap();
				map= new LinkedHashMap();
                // Iterate over each cell in the row and print out the cell's content
                Iterator cells = row.cellIterator();
                int j=0;
                while( cells.hasNext() ) {
                    HSSFCell cell = (HSSFCell) cells.next();
                    switch ( cell.getCellType() ) {
	                    case HSSFCell.CELL_TYPE_NUMERIC:
	                        map.put(j, (int)cell.getNumericCellValue());
	                        break;
	                    case HSSFCell.CELL_TYPE_STRING: 
	                        map.put(j, cell.getStringCellValue());
	                        break;
	                    default:	                        
	                        break;
	                }j++;
                }
                if(lastTwoKey){
                	int k=map.size();
    				mapObject.put(""+map.get(k-2)+map.get(k-1), map);
                }else{
    				mapObject.put(""+map.get(0)+map.get(1), map);
                }
                 
				hashSet.add(mapObject);
            }
            input.close();
		} catch (Exception e) {
			throw new Exception(e.getMessage(),e.getCause());
		} 
		return hashSet;
	}
	
	/**
	 * Find a row based on the keyName
	 * @param keyName
	 * @param filepath
	 * @return
	 * @throws Exception
	 */
	public static Map findaKey(String keyName,String filepath)throws Exception {
		HashSet<Map> hashSet = new HashSet<Map>();
		hashSet = (HashSet)returnExcelAsMap(filepath,true);
		Iterator rows = hashSet.iterator();
		while( rows.hasNext() ) { 
			HashMap row = (HashMap) rows.next();
			if(row.containsKey(keyName))
				return (Map)row.get(keyName);
		}
		return null;
	}
	
	/**
	 *  Search based on the last but one row column. and return the entire results in Map
	 * @param keyName
	 * @param filepath
	 * @return
	 * @throws Exception
	 */
	public static Map getLastButOneClmnRows(String keyName,String filepath)throws Exception {
		LinkedHashMap mapObject= new LinkedHashMap();
		HashSet<Map> hashset = new HashSet<Map>();
		hashset = (HashSet)returnExcelAsMap(filepath,true);
		Iterator rows = hashset.iterator();
		while( rows.hasNext() ) { 
			HashMap row = (HashMap) rows.next();
			Iterator itr = row.values().iterator();
			while(itr.hasNext()){
			      HashMap colMap= (HashMap) itr.next();
			      if(keyName.equalsIgnoreCase(""+colMap.get(colMap.size()-2))){
			    	  mapObject.put(mapObject.size(),colMap );
			      }
			}
		}
		return mapObject;
	}
	
	/**
	 * Copy data from inputObj to other Objects based on the XML Mappings.
	 * @param inputObj
	 * @param filepath
	 * @return
	 * @throws Exception
	 */
	public static Map getExlDataObjMapping(Object inputObj,String filepath)throws Exception {
		LinkedHashMap mapObject= new LinkedHashMap();
		HashSet<Map> hashset = new HashSet<Map>();
		hashset = (HashSet)returnExcelAsMap(filepath,false);
		
		try {
			Field[] fields=inputObj.getClass().getDeclaredFields();
			HashMap eml = (HashMap)BeanUtils.describe(inputObj);
			for(int j=0;j<fields.length;j++){
				Field field = fields[j];
				String keyName=inputObj.getClass().getSimpleName()+field.getName();
				Iterator rows = hashset.iterator();
				while( rows.hasNext() ) {
					HashMap row = (HashMap) rows.next();
					if(row.containsKey(keyName)){
						LinkedHashMap detailMap= (LinkedHashMap)row.get(keyName);
						String cName= (String)detailMap.get(2);
						String cAttrib= (String)detailMap.get(3);
						Object refObj=null;
						if(mapObject.containsKey(cName))
							refObj = mapObject.get(cName);
						else
							refObj = Class.forName("com.farmers.framework.common.testcase."+cName).newInstance();
						
						BeanUtils.setProperty(refObj, cAttrib, eml.get(field.getName()));
						mapObject.put(cName, refObj);
					}
				}
				
			}
			
		} catch (Exception e) {
			throw new Exception(e.getMessage(),e.getCause());
		} 
		
		return mapObject;
	}
	
	public static void main(String[] args) throws Exception{
		ExcelUtil utl = new ExcelUtil();
		
		utl.returnExcelAsList("files/D544050001.xls");
		
	}
}
