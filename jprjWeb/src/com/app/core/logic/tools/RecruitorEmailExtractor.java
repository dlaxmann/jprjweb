package com.app.core.logic.tools;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.poi.hssf.record.Record;
//import org.apache.poi.hssf.record.Record;
import org.xbill.DNS.Lookup;
import org.xbill.DNS.SimpleResolver;
import org.xbill.DNS.Type;


public class RecruitorEmailExtractor {

	public static ArrayList<String> emailList = new ArrayList();
	public static Connection connection = null;

	

	public static void scanDirectoryForEmails(){
		File actual = new File("E:\\EMAIL_DATABASE\\EMAIL-EXTRACT-SOURCE\\");
		//File actual = new File("E:\\emails\\");
        for( File f : actual.listFiles()){
           System.out.println("Processing :"+f.getAbsolutePath());
           readFile(f.getAbsolutePath());
        }
	}
	
	public static void readFile(String filename){
		String filetoRead = (filename.isEmpty()|| filename == null ? "E:\\EMAIL_DATABASE\\EMAIL-SOURCE\\" : filename);
		try{
		    FileInputStream fstream = new FileInputStream(filetoRead);
		    DataInputStream in = new DataInputStream(fstream);
		    BufferedReader br = new BufferedReader(new InputStreamReader(in));
		    String strLine ="";
		    while ((strLine = br.readLine()) != null)   {
		    	//fileContent += strLine+"\n";
		    	getEmail(strLine);
		    }
		    in.close();
		    }catch (Exception e){//Catch exception if any
		      System.err.println("Error: " + e.getMessage());
		    }
	}

	
	public static void getEmail(String line){
		final String RE_MAIL = "([\\w\\-]([\\.\\w])+[\\w]+@([\\w\\-]+\\.)+[A-Za-z]{2,4})";
	    Pattern p = Pattern.compile(RE_MAIL);
	    Matcher m = p.matcher(line);
	    
	    while(m.find()) {
	    	if(checkDomainFromEmails(m.group(1))&& verifyDomainMXRecord(m.group(1))){
	    		System.out.println(m.group(1));
		       // emailList.add(m.group(1));
	    	}
	    }
	   // writeToFile();
	}
	
	
	
	
	
	public static void writeToFile(){
		try {
	        BufferedWriter out = new BufferedWriter(new FileWriter("E:\\EMAIL_DATABASE\\EMAIL_EXT-DB\\Shine-1-EMPLOYER-01AUG"));
	        for(String s : emailList){
	        	System.out.println(s);
	        	out.write(s.toLowerCase()+"\n");
			}
	        out.close();
	        System.out.println("..... Done.");
	    } catch (IOException e) {
	    }
	}
	
	
	
	
	
	public static boolean verifyDomainMXRecord(String email){
		try	{
			String domainName=extractDomainFromEmails(email);
			SimpleResolver resolver = new SimpleResolver();
			resolver.setTimeout(10000);
			Lookup lu = new Lookup(domainName,Type.MX);
			lu.setResolver(resolver);
			org.xbill.DNS.Record[] records = lu.run();
			    if (records == null) {
			       //System.out.println("No MX records were found");
			    	return false;
			    }  
			        StringBuilder sb = new StringBuilder();
			        String seperator = "";
			        for(org.xbill.DNS.Record record : records) {
			          sb.append(seperator);
			          sb.append(record.getName());
			          seperator = ", ";
			        //System.out.println("Found "+records.length+" MX records: "+sb.toString());
			    }
			}catch(Exception e){
				e.printStackTrace();
			}
		
		return true;
	}
	
	
	
	
	public static boolean checkDomainFromEmails(String email){
		String[] tokens = email.split("@");
		List<String> emailDomainsList = new ArrayList<String>();
	    emailDomainsList.add("gmail.com");
	    emailDomainsList.add("yahoo.com");
	    emailDomainsList.add("yahoo.co.in");
	    if(!emailDomainsList.contains(tokens[1]))
	    	return true;
	    	
		return false;
	}
	
	
	
	public static String extractDomainFromEmails(String email){
		//String email = "abc@gmail.con";
		String[] tokens = email.split("@");
		//System.out.println("______"+tokens[0]+"__________"+tokens[1]);
		
		List<String> emailDomainsList = new ArrayList<String>();
	    emailDomainsList.add("gmail.com");
	    emailDomainsList.add("yahoo.com");
	    emailDomainsList.add("yahoo.co.in");
	    
	    if(emailDomainsList.contains(tokens[1]))
	    	System.out.println("true");
	    	
		return tokens[1];
	}
	
	//org.apache.poi.hssf.record.Record[]
	//org.apache.poi.hssf.record.Record
	
	public static void main(String[] args) {
		scanDirectoryForEmails();
		//extractDomainFromEmails();
		// writeToDB();
		
		
			
				
	}
}
