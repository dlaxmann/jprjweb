package com.app.core.logic.tools.trigger;

import java.text.ParseException;

import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;

public class CronTriggerr {

	public static void main (String arg[]){
		
		JobDetail jb = new JobDetail();
		jb.setName("job");
		jb.setJobClass(CronTriggerr.class);
		 
    	CronTrigger trigger = new CronTrigger();
    	trigger.setName("dummyTriggerName");
    	try {
			trigger.setCronExpression("0/30 * * * * ?");
			//schedule it
	    	Scheduler scheduler = new StdSchedulerFactory().getScheduler();
	    	scheduler.start();
	    	scheduler.scheduleJob(jb, trigger);
	    	
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SchedulerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 
    	
	}
}
