package com.app.core.logic.tools;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class LatestModfdFilesInADir {

	int count = -1;
	SimpleDateFormat dtFormat = new SimpleDateFormat("dd-MMMMM-yyyy");
	static List<Date> fileDates = new ArrayList<Date>();
	
	void process(File sfile){
		if(sfile.isFile()){
			count++;
			//add file to a list
			try {
				//get last modified date of the file
				String st= dtFormat.format(sfile.lastModified());
				Date lstMdfdDt = dtFormat.parse(st);
				fileDates.add(lstMdfdDt);
				System.out.println("added Date"+lstMdfdDt+"--"+fileDates.size());
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
		}else if(sfile.isDirectory()){
			File[] flsInaDir = sfile.listFiles();
			if(flsInaDir != null){
				for(int i=0; i<flsInaDir.length; i++){
					process(flsInaDir[i]);
				}
			}else{
				System.out.println("Access Denied");
			}
		}
	}
	
	
	public void searchTextInFile(File file){
		try {
			FileInputStream fs = new FileInputStream(file);
			DataInputStream ds = new DataInputStream(fs);
			BufferedReader bfr = new BufferedReader(new InputStreamReader(ds));
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		String dirNm = "D:/SERVER LOG";
		File fle = new File(dirNm);
		LatestModfdFilesInADir ltr= new LatestModfdFilesInADir();
		ltr.process(fle);
		System.out.println("files"+fileDates.size());
		Collections.sort(fileDates);
		for(Date dates: fileDates){
			System.out.println(dates);
		}
	}

}
