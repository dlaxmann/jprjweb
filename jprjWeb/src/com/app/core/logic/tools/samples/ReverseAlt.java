package com.app.core.logic.tools.samples;
// This program reads in a string and reverses the letters in alternate words.
// by www.neiljohan.com

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.StringTokenizer;


public class ReverseAlt
{
   public static void main(String[] pArgs) throws IOException
   {
      final BufferedReader tKeyboard =  new BufferedReader(new InputStreamReader(System.in));
      System.out.print("Type in a sentance ");
      System.out.flush();
      final String tLine = tKeyboard.readLine();

      final StringTokenizer tTokensOnLine = new StringTokenizer(tLine);
      final int tNoTokens = tTokensOnLine.countTokens();

      int tReverseWord=0;

     for (int tWordNumber = 0; tWordNumber<tNoTokens; tWordNumber++) 
      {

          String tThisToken = tTokensOnLine.nextToken();
          String tThisWord = tThisToken;
          
      if (tReverseWord==0)
      {
           
         System.out.print(" " + tThisToken + " ");
         tReverseWord=1;
      }

      else
      {

          final int tStringLength = tThisWord.length();
          int tCounter=0;
      
          for (int tCharNumber = tStringLength; tCharNumber>0; tCharNumber--) 
          {
              System.out.print(tThisWord.charAt(tCharNumber-1));
              

          }
          

          tReverseWord=0;
          
      }
      
      
      }

     System.out.println();
     
     
    
   }
}

