package com.app.core.logic.tools.samples;
//Interface for Bermuda Triangle Program
//by www.neiljohan.com

public interface Position
{
    public double getLongitude();
    public double getLatitude();
    public void setLongitude(final double newLong);
    public void setLatitude(final double newLat);
    public String toString();
    public int hashCode();
    public boolean equals(final Object pObject);
}
