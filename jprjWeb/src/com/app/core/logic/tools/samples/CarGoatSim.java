package com.app.core.logic.tools.samples;
//Cars & Goats Simulator Program
//There are 3 doors behind one is a car and the other two have goats.
//This program puts the car behind a random door and then guesses which
//door the car is behind. Then one of the remaining doors which has a goat
//behind it is opened. At this stage you can change doors.
//This program is to test whether it is better to change doors or stick
//with your original choice.
//
//It should show that there is a 33% chance of winning if you do not change doors
//and a 66% chance of winning if you do. (Actually tends to show about 61-62%
//chance of winning when you change, I don't know why.
//
//
//by www.neiljohan.com

import java.io.*;

public class CarGoatSim
{
    public static void main(String[] pArgs) throws IOException
        {
            int change=0;
            
            int Wins=0;
            int Lose=0;
            
            int Counter=0;
            int CarDoor=0;

            int [] Door = new int[3];
            
            int Guess=0;
            int OpenedDoor=0;

                //Should we change doors?
            boolean CHANGE_DOOR=false;

            for (int p=0; p<2; p++){
                if (change==1){
                    System.out.println("Changing Door Results Below");
                    CHANGE_DOOR=true;
                    Wins=0;
                    Lose=0;
                    
                }
                change++;
            

                for (int i=0; i<10000; i++)
                {
                    //Initialsise Doors and Generate the guesses 
                    Door[0]=0;
                    Door[1]=0;
                    Door[2]=0;

                    //Some casting to convert Doubles to integers
                    CarDoor = (int)((Math.random()*9)%3);
                    Door[CarDoor]=1;
                    Guess = (int)((Math.random()*9)%3);

                    //Open a Door which contains a goat
                    OpenedDoor = FindGoat(Door,Guess);
                    Door[OpenedDoor]=-1;
                
                    //Change Door if CHANGE_DOOR is set to true
                    if (CHANGE_DOOR){
                        for (int j=0; j<3; j++){
                            if (Door[j] != -1 && Door[j] != Guess){
                                Guess=j;
                            }
                        }
                    }
                
                    //Check to See if we have won a Car
                    if (Door[Guess]==1){
                        Wins++;
                    }
                    else{
                        Lose++;
                    }
                }
                System.out.println(Wins + " Wins and " + Lose + " Loses ");
            }
        }


    public static int FindGoat(int[] Door,int Guess)
        {
            int GoatDoor=0;
            int TryDoor=0;
            boolean Found=false;
            
            while(!Found)
                {
                    TryDoor = (int)((Math.random()*9)%3);
                    if (Door[TryDoor]==0){
                        GoatDoor=TryDoor;
                        Found=true;
                    }
                }
            return GoatDoor;
        }
}

            
