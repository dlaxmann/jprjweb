package com.app.core.logic.tools.samples;
//This extracts the surname from suitable email addresses.
//Try it with mr.blobby@durham.ac.uk
//by www.neiljohan.com

import java.io.*;

public class EmailSurname
{
    public static void main(String[] pArgs) throws IOException
        {
            final InputStreamReader tInputStreamReader = new InputStreamReader(System.in);
            final BufferedReader tKeyboard = new BufferedReader(tInputStreamReader);

            System.out.print("Type in an email address ");

            final String tEmail = tKeyboard.readLine();
            final int tPositionOfAt = tEmail.indexOf("@");
            final int tLastDot = tEmail.lastIndexOf(".",tPositionOfAt);
            final String tSurname = tEmail.substring(tLastDot+1,tPositionOfAt);
            
            System.out.println("The surname is  " + tSurname);
            System.out.println();
            
            
        }
}

            
