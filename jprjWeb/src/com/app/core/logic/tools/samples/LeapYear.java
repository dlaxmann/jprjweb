package com.app.core.logic.tools.samples;
//Leap Year program - prints out which years are leap years.
//by www.neiljohan.com

import java.io.*;


public class LeapYear
{
    public static void main(String[] pArgs) throws IOException
        {
            for (int Year = 2000; Year < 2020; Year++)
            {
                boolean tIsLeap = IsItLeap(Year);
                System.out.println(Year + " " +  tIsLeap);
            }
        }

    public static boolean IsItLeap(int tYear)
        {

            if ((tYear%400==0) || (tYear%4==0 && tYear%100!=0))
            {
                return true;
            }
            else
            {
                return false;
            }

        }
    
}

            
