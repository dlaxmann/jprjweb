package com.app.core.logic.tools.samples;
// This program reads in two words and says how many of the letters are different.
// by www.neiljohan.com

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
public class SimilarWords
{
   public static void main(String[] pArgs) throws IOException
   {
      final BufferedReader tKeyboard =  new BufferedReader(new InputStreamReader(System.in));
      System.out.print("Type in a string: ");
      System.out.flush();
      final String tString1 = tKeyboard.readLine();

      System.out.print("Type in another string: ");
      System.out.flush();
      final String tString2 = tKeyboard.readLine();

      final int tStringLength = tString1.length();
      int tCounter=0;
      
      for (int tCharNumber = 0; tCharNumber<tStringLength; tCharNumber++) 
      {
         final char tChar = tString1.charAt(tCharNumber);
         
         
         if (tString1.charAt(tCharNumber)==tString2.charAt(tCharNumber))
         {
             tCounter++;
         }
      }
      System.out.println(tCounter);
   }
}
