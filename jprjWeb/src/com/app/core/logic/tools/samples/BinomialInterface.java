package com.app.core.logic.tools.samples;
// Interface for the Binomial Program
// By www.neiljohan.com
public interface BinomialInterface
{
    
    public int getBinomial();
    public String toString();
}
