package com.app.core.logic.tools.samples;
//Program to calculate the square root of a number using the Newton-Raphson.
//It stops when the difference between consecutive approximations is less than 0.00005
//by www.neiljohan.com

import java.io.IOException;


public class NewtonRaphsonClass 
{
	private static boolean debugging = true;

	/** @param tFactorial the value from which to find the square root
	 *  precondition tFactorial>0
	 *  @return x1 so that x1>0 */

    public static double iSquareRoot (double tFactorial) throws IOException
        {

            double x1=0;
            double x0=tFactorial/2;
            double a=tFactorial;
            boolean finished=false;

	/* check pre-condition */
	if (debugging && (tFactorial<=0)){
		System.out.println("Pre-condition violoated");
		finished=true;
	};



            while (finished==false)
            {

                x1=(x0+(a/x0))/2;

                if (x1>x0){
                    if ((x1-x0)<0.00005){
                        finished=true;
                    }
                }
                else if (x0>x1){
                    if ((x0-x1)<0.00005){
                        finished=true;
                    }
                }
                x0=x1; 
            }


		/* check post-condition */
		if (debugging && (x1<=0)){
			System.out.println("Post-condition violoated");
		};	


            return x1;
        }
 }

