package com.app.core.logic.tools.samples;
//Bermuda Triangle Program. Prints out a representation
//of the Triangle's Position
//by www.neiljohan.com

import java.io.*;

public class PositionMain
{
    public static void main(String[] pArgs) throws IOException
        {
            final Position tPosition1 = new PositionImpl("64-32");
            final Position tPosition2 = new PositionImpl("66-18");
            final Position tPosition3 = new PositionImpl("80-26");
            System.out.println("Bermuda Triangle");

            String tCoord;

            System.out.print("    ");
            for (int tLong = 85; tLong >=60; tLong--){
                System.out.print(tLong);
                
            }
            System.out.println();
            
            
            for (int tLat = 35; tLat >=15; tLat--){
                System.out.print(tLat + ": ");
                
                for (int tLong = 85; tLong >=60; tLong--){
                    
                    tCoord = tLong + "-" + tLat;
                    Position tPoint = new PositionImpl(tCoord);

                    if (iIsInside(tPosition1,tPosition2,tPosition3,tPoint))
                    {
                        System.out.print("X ");
                    }
                    else
                    {
                        System.out.print(". ");
                    }
                }
                System.out.println("");
            }
        }



    

        private static boolean iIsInside(final Position pPosition1,final Position pPosition2,final Position pPosition3,final Position pMe)
        {
            final double x1 = pPosition1.getLongitude();
            final double y1 = pPosition1.getLatitude();
            final double x2 = pPosition2.getLongitude();
            final double y2 = pPosition2.getLatitude();
            final double x3 = pPosition3.getLongitude();
            final double y3 = pPosition3.getLatitude();
            final double x0 =        pMe.getLongitude();
            final double y0 =        pMe.getLatitude();
            //thanks to Horst Kraemer for the clever stuff from here on
        final double  d = (x2-x1)*(y3-y1) - (y2-y1)*(x3-x1);
        final double  a = (x0-x1)*(y3-y1) - (y0-y1)*(x3-x1);
        final double  b = (x2-x1)*(y0-y1) - (y2-y1)*(x0-x1);
        if (d==0)
        {
            return true;
        }
        return 0<=a/d && 0<=b/d && a/d+b/d<=1;
        } 
    
}

            
    
