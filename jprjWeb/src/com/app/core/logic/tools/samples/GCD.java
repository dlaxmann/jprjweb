package com.app.core.logic.tools.samples;
//Program to calculate the GCD of 2 numbers
//By www.neiljohan.com

import java.io.*;
import java.util.StringTokenizer;

public class GCD
{
    public static void main(String[] pArgs) throws IOException
        {
	    boolean debugging = true;

            final BufferedReader tKeyboard = new BufferedReader(new InputStreamReader(System.in));

            System.out.print("Type in the first  numbers in the form 4,5 ");
            final String tInput = tKeyboard.readLine();
            StringTokenizer tTokensOnLine = new StringTokenizer(tInput, ",");

            String tThisToken = tTokensOnLine.nextToken();
            int N1 = new Integer(tThisToken).intValue();

            tThisToken = tTokensOnLine.nextToken();
            int N2 = new Integer(tThisToken).intValue();	

		/* check pre-condition */
		if (debugging && ((N1<=0) || (N2<=0))){
			System.out.println("Pre-condition violoated");
		};

            int GCD =0;
//GCDClass.GCD(N1,N2);
            System.out.println("The GCD is " + GCD);	


		/* check post-condition */
		if (debugging && (GCD<=0)){
			System.out.println("Post-condition violoated");
		};	

        }
}
