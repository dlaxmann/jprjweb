package com.app.core.logic.tools.samples;
// This program reads a load of Positions into a HashMap
// It then reads a flight plan from a second file and
// retrieves the Positions for each of the destinations
// from the HashMap
// by www.neiljohan.com

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.FileReader;
import java.util.Set;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;

public class TranslateFlight
{
    static Map tPointSet = new HashMap();
    
    public static void main(String[] pArgs) throws IOException
        {
            String tPosition,tPlace;
            int tSpace=0;
            
            Position tPositionObj = new PositionImpl("00-00-00-W:00-00-00-E"); //Initialsise tPositionObj

            BufferedReader tInputHandle = new BufferedReader(new FileReader("Points.dat"));

            String tLine = "NotNull";
            while (tLine!=null)
            {
                tLine = tInputHandle.readLine();
                if (tLine==null)
                {
                    break;
                }
                else{
                    tSpace = tLine.lastIndexOf(" ");
                    tPlace = tLine.substring(0,tSpace);
                    tPlace = tPlace.trim();
                    tPosition = tLine.substring(tSpace+1,tLine.length());
                    tPositionObj = new PositionImpl(tPosition);
                    tPointSet.put(tPlace,tPositionObj);
                }
            }
                
            tInputHandle = new BufferedReader(new FileReader("Flight.dat"));
            Object tGotPosition;

            tLine = "NotNull";
            while (tLine!=null)
            {
                tLine = tInputHandle.readLine();
                
                if (tLine==null)
                {
                    break;
                }
                else{
                    tLine = tLine.trim();
                    tGotPosition = tPointSet.get(tLine);
                    System.out.println(tLine + "  " + tGotPosition);
                }
            }
        }
}













