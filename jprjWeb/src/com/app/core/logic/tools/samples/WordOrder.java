package com.app.core.logic.tools.samples;
//Type in two words and the program will tell in which order they come
//By www.neiljohan.com

import java.io.*;
import java.util.StringTokenizer;

public class WordOrder
{
    public static void main(String[] pArgs) throws IOException
        {
            final BufferedReader tKeyboard = new BufferedReader(new InputStreamReader(System.in));

            System.out.print("Type in two words ");

            final String tLine = tKeyboard.readLine();


            final StringTokenizer tTokensOnLine = new StringTokenizer(tLine);
            final int tNoTokens = tTokensOnLine.countTokens();

            String tThisToken = tTokensOnLine.nextToken();
            final String tFirstWord = tThisToken;
            
            
            
            tThisToken = tTokensOnLine.nextToken();
            final String tSecondWord = tThisToken;

            final int tComp = tFirstWord.compareTo(tSecondWord);


            
            if(tNoTokens==2){


            if(tComp==0){
                System.out.print(tFirstWord + " is the same as " + tSecondWord);
            }else if(tComp>0){
                System.out.print(tFirstWord + " comes after " + tSecondWord);
            }else if (tComp<0){
                System.out.print(tFirstWord + " comes before " + tSecondWord);
            }

                
            }
            else{
                System.out.println("The input is not valid");
                
            }

            System.out.println();
 
        }
}

