package com.app.core.logic.tools.samples;
//Program to calculate the square root of a number using the Newton-Raphson.
//It stops when the difference between consecutive approximations is less than 0.00005
//by www.neiljohan.com

import java.io.IOException;


public class NewtonRaphson
{
   public static void main(String[] pArgs) throws IOException
   {
       
       double tValue = NeilClass.GetInputDouble("Type in a number ");
       double sr = NewtonRaphsonClass.iSquareRoot(tValue);
       System.out.println("The Calculated Square root is    " + sr);
       System.out.println("The value from java.math.lang is " + Math.sqrt(tValue));
       
    }


 }

