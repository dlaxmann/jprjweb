package com.app.core.logic.tools.samples;
//By www.neiljohan.com

import java.io.*;
import java.util.StringTokenizer;


public class BinomialCoefficient
{
    public static void main(String[] pArgs) throws IOException
        {
            final BufferedReader tKeyboard = new BufferedReader(new InputStreamReader(System.in));

            System.out.print("Type in the first  points in the form 4,5 ");
            final String tInput = tKeyboard.readLine();
            StringTokenizer tTokensOnLine = new StringTokenizer(tInput, ",");

            String tThisToken = tTokensOnLine.nextToken();
            int tN1 = new Integer(tThisToken).intValue();

            tThisToken = tTokensOnLine.nextToken();
            int tK1 = new Integer(tThisToken).intValue();

            final double Coef = GetBiClass.GetBiCoefficient(tN1,tK1);
            System.out.println("The answer is " + Coef);

        }
}
