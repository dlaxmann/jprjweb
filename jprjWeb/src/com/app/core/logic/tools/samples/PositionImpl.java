package com.app.core.logic.tools.samples;
//Main class for Bermuda Triangle Program
//by www.neiljohan.com

import java.util.*;

public class PositionImpl implements Position
{
    private double tWest, tEast;
    private char tDirection;

        //Co-ordinate in the form of 64-32
    
    public PositionImpl (final String tInput)
        {
            StringTokenizer tTokensOnLine = new StringTokenizer(tInput, "-");

            String tThisToken = tTokensOnLine.nextToken();
            tWest = new Double(tThisToken).doubleValue();

            tThisToken = tTokensOnLine.nextToken();
            tEast = new Double(tThisToken).doubleValue();
        }

    public double getLongitude()
        {
            return tWest;
        }

    public double getLatitude()
        {
            return tEast;
        }

    public void setLongitude(final double newLat)
        {
            tWest = newLat;
        }

    public void setLatitude(final double newLong)
        {
            tEast = newLong;
        }

    public String toString()
        {
            return "" + tWest + tEast;
        }

    public int hashCode()
        {
            String West = Double.toString(tWest);
            String East = Double.toString(tEast);
            int iWest = new Integer(West).intValue();
            int iEast = new Integer(West).intValue();
            return iWest*360 + iWest*60;
        }

    public boolean equals(final Object pObject)
        {
            if (! (pObject instanceof PositionImpl)){
                return false;
            }
            {
                return tWest==((PositionImpl)pObject).tWest && tEast==((PositionImpl)pObject).tEast;
            }
        }
}
