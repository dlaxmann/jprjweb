package com.app.core.logic.tools;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class JobSeekerEmailExtractor {

	public static ArrayList<String> emailList = new ArrayList();
	public static Connection connection = null;

	

	public static void scanDirectoryForEmails(){
		File directory = new File("D:\\EMAIL_DATABASE\\EMAIL-EXTRACT-SOURCE\\");
		
		
        for( File f : directory.listFiles()){
           System.out.println("Processing :"+f.getAbsolutePath());
           readFile(f.getAbsolutePath());
        }
        
        
        
	}
	
	
	
	
	public static void readFile(String filename){
		String filetoRead = (filename.isEmpty()|| filename == null ? "D:\\EMAIL_DATABASE\\EMAIL-EXTRACT-SOURCE\\" : filename);
		try{
		    FileInputStream fstream = new FileInputStream(filetoRead);
		    DataInputStream in = new DataInputStream(fstream);
		    BufferedReader br = new BufferedReader(new InputStreamReader(in));
		    String strLine ="";
		    while ((strLine = br.readLine()) != null)   {
		    	//fileContent += strLine+"\n";
		    	getEmail(strLine);
		    }
		    in.close();
		    }catch (Exception e){//Catch exception if any
		      System.err.println("Error: " + e.getMessage());
		    }
	}

	
	public static void getEmail(String line){
		final String RE_MAIL = "([\\w\\-]([\\.\\w])+[\\w]+@([\\w\\-]+\\.)+[A-Za-z]{2,4})";
	    Pattern p = Pattern.compile(RE_MAIL);
	    Matcher m = p.matcher(line);

	    List<String> emailDomainsList = new ArrayList<String>();
	    emailDomainsList.add("gmail.com");
	    emailDomainsList.add("yahoo.com");
	    emailDomainsList.add("yahoo.co.in");
	    
	    
	    
	    try {
			BufferedWriter out = new BufferedWriter(new FileWriter("D:\\EMAIL_DATABASE\\EMAIL-EXTRACT-TARGET\\FILTERED-1"));
		
			 while(m.find()) {
			    	if(checkDomainFromEmails(m.group(1))){
			    		
				        //emailList.add(m.group(1));
				        out.write(m.group(1).toLowerCase()+"\n");
				        System.out.println(m.group(1));
			    	}
			    }	 
			 out.close();
	    } catch (IOException e) {
			e.printStackTrace();
		}
	   
	    //writeToFile();
	}
	
	
	
	
	
	public static boolean checkDomainFromEmails(String email){
		String[] tokens = email.split("@");
		List<String> emailDomainsList = new ArrayList<String>();
	    emailDomainsList.add("gmail.com");
	    emailDomainsList.add("yahoo.com");
	    emailDomainsList.add("yahoo.co.in");
	    if(emailDomainsList.contains(tokens[1]))
	    	return true;
	    	
		return false;
	}
	
	
	
	public static String extractDomainFromEmails(String email){
		//String email = "abc@gmail.con";
		String[] tokens = email.split("@");
		System.out.println("______"+tokens[0]+"__________"+tokens[1]);
		
		List<String> emailDomainsList = new ArrayList<String>();
	    emailDomainsList.add("gmail.com");
	    emailDomainsList.add("yahoo.com");
	    emailDomainsList.add("yahoo.co.in");
	    
	    if(emailDomainsList.contains(tokens[1]))
	    	System.out.println("true");
	    	
		return tokens[1];
	}
	
	
	
	public static void writeToDB(){
		getConnection();
		for(String email : emailList){
	        String todo = ("INSERT into emails (email) values ('"+email.toLowerCase()+"')") ;
	        try {
	                java.sql.Statement s = connection.createStatement();
	                s.executeUpdate(todo);
	        }catch (Exception e) {
	        	e.printStackTrace();
	        }
		}
	}





	public static void getConnection(){
	    try {
	        String driverName = "org.gjt.mm.mysql.Driver"; // MySQL MM JDBC driver
	        Class.forName(driverName);

	        String serverName = "localhost";
	        String mydatabase = "emails";
	        String url = "jdbc:mysql://" + serverName +  "/" + mydatabase; // a JDBC url
	        String username = "root";
	        String password = "password";
	        connection = DriverManager.getConnection(url, username, password);
	    } catch (ClassNotFoundException e) {
	    	e.printStackTrace();
	    } catch (SQLException e) {
	    	e.printStackTrace();
	    }
	}
	
	
	
	public static void main(String[] args) {
		scanDirectoryForEmails();
		//extractDomainFromEmails();
		// writeToDB();
	}
}
