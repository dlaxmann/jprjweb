package com.app.core;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;

public class JdbcInsertDate {

	public static void main (String[] args) { 
        try { 
            
        	String driver = "oracle.jdbc.OracleDriver";
        	Class.forName(driver);
        	String url = "jdbc:oracle:thin:@//localhost:1521/XE"; 
            Connection conn = DriverManager.getConnection(url,"verify_app_user","appuser123"); 
            System.out.println("Connection Established" + conn);
            Statement st = conn.createStatement(); 
            st.executeUpdate("INSERT INTO EMPLOYEES (EMPLOYEE_ID,NAME,DOB)values(11,'rama',TO_DATE('08-JUN-10', 'DD-MON-RR'))"); 
           // st.executeUpdate("INSERT INTO EMPLOYEES (EMPLOYEE_ID,NAME,DOB)values(12,'rama',TO_DATE('07-JUN-10', 'DD-MON-RR'))");
            st.executeUpdate("INSERT INTO EMPLOYEES (EMPLOYEE_ID,NAME,DOB)values(11,'rama','08-JUN-10')");
            System.out.println("Records insrted successfully");
            
            
            ResultSet rs;
            rs = st.executeQuery("select DOB from EMPLOYEES where EMPLOYEE_ID =11");
            while ( rs.next() ) {
            	Date dt = rs.getDate("DOB");
            	String str = dt.toString();
                //String dob = rs.getString("DOB");
               // System.out.println(dt);
                System.out.println(str);
            }

            conn.close(); 
        } catch (Exception e) { 
            System.err.println("Got an exception! "); 
            System.err.println(e.getMessage()); 
        } 
	}
}
