package com.app.core.lang;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.regex.Matcher;

import org.apache.commons.collections.map.HashedMap;

public class BuildEmailContentUtil extends org.apache.commons.lang3.StringUtils{

	public static void utilFuncatins(){
		int x = 10;
		if(x ==100)
		System.out.println("x");
		System.out.println("y");
		
		String text = "sometext";
		String t = text.substring(0, 3);
		System.out.println(text+":"+t);
		
		String st = "Hello_World";
        String str[] = st.split("_");
        System.out.println("String after split="+str[0]);
        
        String repText = "sometext";
		String t1 = repText.substring(0, 3);
		String strr = repText.replace(repText, "hi");
		System.out.println(strr);
		
		String atext ="sometext";
		String s = atext.substring(1,atext.length()-1);
		//StringUtil.
	}
	
	public static void someStringFun(){
		 final String __LINE_SEPARATOR=
		        (String)System.getProperty("line.separator","\n");
		    
		    String __ISO_8859_1;
		    String iso=System.getProperty("ISO_8859_1");
		        if (iso!=null)
		            __ISO_8859_1=iso;
		        else
		        {
		            try{
		                new String(new byte[]{(byte)20},"ISO-8859-1");
		                __ISO_8859_1="ISO-8859-1";
		            }
		            catch(java.io.UnsupportedEncodingException e)
		            {
		                __ISO_8859_1="ISO8859_1";
		            }        
		        }
		    }
		    
		    private static char[] lowercases = {
		          '\000','\001','\002','\003','\004','\005','\006','\007',
		          '\010','\011','\012','\013','\014','\015','\016','\017',
		          '\020','\021','\022','\023','\024','\025','\026','\027',
		          '\030','\031','\032','\033','\034','\035','\036','\037',
		          '\040','\041','\042','\043','\044','\045','\046','\047',
		          '\050','\051','\052','\053','\054','\055','\056','\057',
		          '\060','\061','\062','\063','\064','\065','\066','\067',
		          '\070','\071','\072','\073','\074','\075','\076','\077',
		          '\100','\141','\142','\143','\144','\145','\146','\147',
		          '\150','\151','\152','\153','\154','\155','\156','\157',
		          '\160','\161','\162','\163','\164','\165','\166','\167',
		          '\170','\171','\172','\133','\134','\135','\136','\137',
		          '\140','\141','\142','\143','\144','\145','\146','\147',
		          '\150','\151','\152','\153','\154','\155','\156','\157',
		          '\160','\161','\162','\163','\164','\165','\166','\167',
		          '\170','\171','\172','\173','\174','\175','\176','\177' };

		    /* ------------------------------------------------------------ */
		    /**
		     * fast lower case conversion. Only works on ascii (not unicode)
		     * @param s the string to convert
		     * @return a lower case version of s
		     */
		    public static String asciiToLowerCase(String s)
		    {
		        char[] c = null;
		        int i=s.length();

		        // look for first conversion
		        while (i-->0)
		        {
		            char c1=s.charAt(i);
		            if (c1<=127)
		            {
		                char c2=lowercases[c1];
		                if (c1!=c2)
		                {
		                    c=s.toCharArray();
		                    c[i]=c2;
		                    break;
		                }
		            }
		        }

		        while (i-->0)
		        {
		            if(c[i]<=127)
		                c[i] = lowercases[c[i]];
		        }
		        
		        return c==null?s:new String(c);
		    }


		    /* ------------------------------------------------------------ */
		    public static boolean startsWithIgnoreCase(String s,String w)
		    {
		        if (w==null)
		            return true;
		        
		        if (s==null || s.length()<w.length())
		            return false;
		        
		        for (int i=0;i<w.length();i++)
		        {
		            char c1=s.charAt(i);
		            char c2=w.charAt(i);
		            if (c1!=c2)
		            {
		                if (c1<=127)
		                    c1=lowercases[c1];
		                if (c2<=127)
		                    c2=lowercases[c2];
		                if (c1!=c2)
		                    return false;
		            }
		        }
		        return true;
		    }
		    
		    /* ------------------------------------------------------------ */
		    /**
		     * returns the next index of a character from the chars string
		     */
		    public static int indexFrom(String s,String chars)
		    {
		        for (int i=0;i<s.length();i++)
		           if (chars.indexOf(s.charAt(i))>=0)
		              return i;
		        return -1;
		    }
		    
		    /* ------------------------------------------------------------ */
		    /**
		     * replace substrings within string.
		     */
		    public static String replace(String s, String sub, String with) {
		        int c=0;
		        int i=s.indexOf(sub,c);
		        if (i == -1)
		            return s;
		        StringBuffer buf = new StringBuffer(s.length()+with.length());
		        synchronized(buf) {
		            do {
		                buf.append(s.substring(c,i));
		                buf.append(with);
		                c=i+sub.length();
		            } while ((i=s.indexOf(sub,c))!=-1);
		            if (c<s.length())
		                buf.append(s.substring(c,s.length()));
		            return buf.toString();
		        }
		    }

		    /* ------------------------------------------------------------ */
		    /** Remove single or double quotes.
		     */
		    public static String unquote(String s)
		    {
		        if ((s.startsWith("\"") && s.endsWith("\"")) ||
		            (s.startsWith("'") && s.endsWith("'")))
		            s=s.substring(1,s.length()-1);
		        return s;
		    }


		    public static String replaceString(String s, String sMatch, String sReplace)
		    {
		        if (sReplace == null)
		            sReplace = "";

		        if (sMatch == null || "".equals(sMatch) || sMatch.equals(sReplace))
		            return s;

		        if (s == null || s.equals(""))
		        {
		            return "";
		        }

		        int i = 0;
		        int j = s.indexOf(sMatch);

		        if (j < 0)
		        {
		            return s;
		        }

		        StringBuffer sb = new StringBuffer(s.length());

		        while (true)
		        {
		            sb.append(s.substring(i, j));
		            sb.append(sReplace);

		            i = j + sMatch.length();
		            j = s.indexOf(sMatch, i);

		            if (j < 0)
		            {
		                sb.append(s.substring(i));
		                break;
		            }
		        }

		        return sb.toString();
		    }


		    public static String escapeDelimiter(String s, String sEscaper, String sDelimiter, String sBackendDelimiter)
		    {
		        if (s == null || "".equals(s))
		            return "";

		        StringBuffer sbResult = new StringBuffer();
		        for (int i = 0; i < s.length(); i++)
		        {
		            if (s.startsWith("" + sEscaper + sEscaper, i))
		            {
		                sbResult.append(sEscaper);
		                i++;
		            }
		            else if (s.startsWith("" + sEscaper + sDelimiter, i))
		            {
		                sbResult.append(sDelimiter);
		                i++;
		            }
		            else if (s.startsWith("" + sDelimiter, i))
		            {
		                sbResult.append(sBackendDelimiter);
		            }
		            else
		            {
		                sbResult.append(s.charAt(i));
		            }
		        }

		        return sbResult.toString();
		    }


		    public static String escapeForJS(String txt)
		    {
		        return escapeForJS(txt, false);
		    }



		    public static String escapeForJS(String txt, boolean useDosCRLF)
		    {
		        txt = BuildEmailContentUtil.replaceString(txt, "\\", "\\\\");

		        if (useDosCRLF)
		        {
		            txt = BuildEmailContentUtil.replaceString(txt, "\r", "");
		            txt = BuildEmailContentUtil.replaceString(txt, "\n", "\\r\\n");
		        }
		        else
		        {
		            txt = BuildEmailContentUtil.replaceString(txt, "\r", "");
		        }

		        txt = BuildEmailContentUtil.replaceString(txt, "\n", "\\n");
		        txt = BuildEmailContentUtil.replaceString(txt, "'", "\\'");
		        txt = BuildEmailContentUtil.replaceString(txt, "\"", "\\\"");
		        return txt;
		    }




		    public static String escapeForCSV(String sArg)
		    {
		        StringBuffer sb = new StringBuffer();
		        if (sArg != null)
		        {
		            if ((sArg.indexOf(",") >= 0) || (sArg.indexOf("\n") >= 0) || (sArg.indexOf("\r") >= 0)
		                || (sArg.indexOf("\"") >= 0))
		            {
		                sb.append("\"");
		                sb.append(BuildEmailContentUtil.replaceString(sArg, "\"", "\"\""));
		                sb.append("\"");
		            }
		            else
		            {
		                sb.append(sArg);
		            }
		        }

		        return sb.toString();
		    }

		    public static String escapeForHTML(String sArg, boolean isAscii)
		    {
		        String s;
		        if(isAscii)
		        {
		            s = replaceString(sArg, "&", "&");
		            s = replaceString(s, "<", "<");
		            s = replaceString(s, ">", ">");
		            s = replaceString(s, "\"", "");
		            s = replaceString(s, "'", "'");
		        }
		        else
		        {
		            s = replaceString(sArg,"&","&");
		            s = replaceString(s, "<", "<");
		            s = replaceString(s, ">", ">");
		            s = replaceString(s, "\"", "");
		            s = replaceString(s, "'", "'");
		        }
		        return preserveSpaceRuns(s);
		    }

		    public static String preserveSpaceRuns(String s)
		    {
		        if (s.indexOf("  ") < 0)        
		// Quick check for no runs of spaces
		            return s;
		        else
		        {
		            int imax = s.length();
		            StringBuffer sb = new StringBuffer(imax + imax);
		            for (int i = 0; i < imax; i++)
		            {
		                char c = s.charAt(i);
		                sb.append(c);
		                if (c == ' ')
		                {
		                    for (int j = i + 1; j < imax && s.charAt(j) == c; j++)
		                    {
		                        sb.append(" ");
		                        i = j;
		                    }
		                }
		            }
		            return sb.toString();
		        }
		    }



		    public static String escapeWithHTMLEntities(String s, int beg, int end)
		    {
		        StringBuffer sbResult = new StringBuffer();

		        for (int i = 0; i < s.length(); i++)
		        {
		            int ch = (int)s.charAt(i);
		            if (ch < beg || ch > end)
		                sbResult.append("&#" + ch + ";");
		            else
		                sbResult.append(s.charAt(i));
		        }

		        return sbResult.toString();
		    }

		    public static void stringTokenizing(){
		    	String stt ="a,ab,cd,def,efg";
		    	StringTokenizer stk = new StringTokenizer(stt,",");
		    	while(stk.hasMoreElements()){
		    		System.out.println(stk.nextElement());
		    	}

		    	String str = "This is String , split by StringTokenizer, created by mkyong";
				StringTokenizer st = new StringTokenizer(str);
				System.out.println("---- Split by space ------");
				while (st.hasMoreElements()) {
					System.out.println(st.nextElement());
				}
				System.out.println("---- Split by comma ',' ------");
				StringTokenizer st2 = new StringTokenizer(str, ",");
				while (st2.hasMoreElements()) {
					System.out.println(st2.nextElement());
				}
		    	
				StringTokenizer stringTokenizer = new StringTokenizer("You are tokenizing a string");
				  System.out.println("The total no. of tokens  generated :  " + stringTokenizer.countTokens());
				  while(stringTokenizer.hasMoreTokens()){
				  System.out.println(stringTokenizer.nextToken());
				  }
		    }
		    
		   public static void matchString(){
			  String str = "johnn";
		    	if (str.matches("john|mary|peter"))
		    	{
		    		System.out.println("_________matched: "+str);
		    	}
		    	String strr ="image/giff";
		    if(!strr.matches("image/gif|image/jpeg|image/tiff|image/png")){
		    	System.out.println("_________matched: "+strr);	
		    }
		    }
		    
		   public static void stringSplits(){
			   String s ="a,b,c,d,ee,fff,gg";
			   String[] sarray = s.split(",");
			   for (int i = sarray.length-1;  i>= 0; i--) {
				  // String str = "data.add(new Tag("+i+", \"" +sarray[i].trim()+ "\"));";
				
			}
			  // List<String> slist = Arrays.asList(sarray);
			  // System.out.println(slist.size());
		   }
		    
	
		   public  static void printSkills(){
		   
		   String s ="Networking Jobs Software Testing Jobs SAP abap Jobs Call Center Jobs Mainframe Jobs SAP FICO Jobs SAP Jobs SEO Jobs .NET Jobs Oracle Jobs PHP Jobs Back office Jobs Java Jobs Online Marketing Jobs SAP Basis Jobs SAS Jobs CISCO Jobs Manual Testing Jobs SAP HR Jobs SAP MM Jobs SAP SD Jobs SAP Security Jobs VLSI Jobs Web Designing Jobs CCIE Jobs Informatica Jobs Linux Jobs PeopleSoft Jobs Plc Jobs SQL Jobs ASP.NET Jobs Datastage Jobs Finacle Jobs SAP CRM Jobs Tally Jobs Tibco Jobs CATIA Jobs Cognos Jobs ITIL Jobs Multimedia Jobs Photoshop Jobs PRO E Jobs ABAP Jobs Backend Jobs C Jobs DTP Jobs ERP Jobs Internet Marketing Jobs Siebel Jobs Video Editing Jobs AbInitio Jobs CAD/CAM Jobs Digital Marketing Jobs Editing Jobs HTML Jobs Oracle Apps Jobs Perl Jobs Python Jobs SAP PP Jobs SAP PS Jobs SQL Server Jobs Database Jobs MCSE Jobs PPC Jobs QTP Jobs SAP PM Jobs Teradata Jobs CRM Jobs data warehousing Jobs Excel Jobs Flexcube Jobs MATLAB Jobs PowerBuilder Jobs SAP BW Jobs Solaris Jobs AIX Jobs ArchiCAD Jobs Business Objects Jobs Documentum Jobs FACT Jobs Flex Jobs Hyperion Jobs SCADA Jobs UniGraphics Jobs Unix Jobs Vision Plus Jobs VSAT Jobs WebMethods Jobs Auto CAD Jobs Datawarehousing Jobs Flash Jobs Lotus Notes Jobs Maximo Jobs Primavera Jobs SAP IS-Retail Jobs Autosys Jobs COBOL Jobs ColdFusion Jobs Maya Jobs Programming Jobs RIM Jobs SAP MDM Jobs XML Jobs 3D Max Jobs Automation Testing Jobs BAAN Jobs C# Jobs Delphi Jobs J2EE Jobs MySQL Jobs RedHat Jobs SAP EP Jobs SAP SRM Jobs TPF Jobs VB.NET Jobs VOIP Jobs WebLogic Jobs WebSphere Jobs Active Directory Jobs Ansys Jobs Data Analysis Jobs Documentation Jobs DSP Jobs IMS Jobs Remedy Jobs SAP QM Jobs SPSS Jobs Sybase Jobs TLM Jobs ASIC Jobs Blackberry Jobs Corel Draw Jobs CSS Jobs DB2 Jobs Embedded C Jobs FileNet Jobs FoxPro Jobs FPGA Jobs J2ME Jobs JavaScript Jobs MCSA Jobs Murex Jobs SAP XI Jobs WPF Jobs Adobe Photoshop Jobs DCA Jobs Distribution Jobs Jquery Jobs Natural Adabas Jobs ORCAD Jobs Progress 4GL Jobs SAP SCM Jobs Silverlight Jobs Switching Jobs Android Development Jobs ASP Jobs Calypso Jobs GSM Jobs Load Runner Jobs MCP Jobs MicroStation Jobs MSCIT Jobs Savvion Jobs Shell Scripting Jobs Spring Jobs Visual Basic Jobs Affiliate Marketing Jobs Eclipse Jobs Focus Jobs ForTran Jobs HP UNIX Jobs JSF Jobs MFC Jobs SAP IS-Utilities Jobs Verilog Jobs Visual Foxpro Jobs WCF Jobs Website Development Jobs Dreamweaver Jobs IIS Jobs JSP Jobs Microsoft Excel Jobs Nortel Jobs RDO Jobs ActiveX Jobs ADO Jobs Adobe Illustrator Jobs Adobe Pagemaker Jobs AJAX Jobs ALE Jobs Ant Jobs Apache Commons Jobs Apache Tomcat Jobs Apache Web Server Jobs AS 400 Jobs Assembly Language Jobs ATL Jobs AutoLISP Jobs AWT Jobs BASIC Jobs Bluetooth Jobs BPCS Jobs BPEL Jobs BREW Jobs C++ Jobs CDMA Jobs Chordiant Jobs CICS Jobs CLDC Jobs Clipper Jobs COM/COM+/DCOM Jobs CORBA Jobs CoreJava Jobs Crystal Report Jobs Csharp Jobs Data Structures Jobs Database Administration Jobs Db4o Jobs dBase Jobs DCOM Jobs Derby Jobs Developer/D2K Jobs DHCP Jobs DHTML Jobs DNS Jobs DOS Jobs Downstream Jobs EJB Jobs Ethernet Jobs Expeditor Jobs Finone Jobs Firewall Jobs Fireworks Jobs FlashLite Jobs FreeBSD Jobs FTP Jobs GLOSS Jobs Hibernate Jobs Humming Bird Jobs iBatis Jobs Ideas Jobs ImageReady Jobs Impromptu Jobs Informix Jobs Ingres Jobs Installshield Jobs ISDN Jobs J++ Jobs J2SE Jobs Java2D Jobs JavaCard Jobs JavaFX Jobs JavaSE Jobs JBoss Jobs JBoss Seam Jobs JCL Jobs JDBC Jobs JDOM Jobs JFace Jobs Jini Jobs JIRA Jobs JMock Jobs JMS Jobs JNI Jobs JPA Jobs JSE Jobs JUnit Jobs Kickboxing Jobs KSH Jobs LAN Jobs LINQ Jobs Log4j Jobs Mac OS Jobs Maven Jobs Microcontrollers Jobs Microprocessors Jobs Microsoft Access Jobs Microsoft Exchange Jobs Microsoft Office Jobs MIDP Jobs MIS Reports Jobs MOSS Jobs Motif Jobs MS DOS Jobs MS Project Jobs MS SQL Server Jobs MS Visio Jobs Multithreading Jobs MVS Jobs NetWeaver Jobs Novell Jobs ODBC Jobs Office Operations Jobs OOPS Jobs OpenGL ES Jobs Operating Systems Jobs Oracle WareHouse Builder Jobs PASCAL Jobs PL/SQL Jobs PL/1 Jobs PostgreSQL Jobs PowerPlay Jobs Pro*C Jobs PVCS Jobs QBasic Jobs Quality Assurance/QA Jobs Remoting Jobs REXX Jobs RichFaces Jobs RMI Jobs Routing Jobs RTOS Jobs S60 Jobs SAP Bl Jobs SAP COPA Jobs SAP idocs Jobs SAP IS-GAS/OIL Jobs SAP Practice Jobs SAP SEM Jobs SAP WMS Jobs Seam Jobs Sharepoint Server Jobs SMARTY Jobs SMTP Jobs SoundForge Jobs STAAD Jobs Struts Jobs SunOS Jobs SWT Jobs Symbian C++ Jobs Symbian Jobs SyncML Jobs T SQL Jobs TCP/IP Jobs TELNET Jobs TOAD Jobs Turbo Pascal Jobs Tuxedo Jobs UIQ Jobs UML Jobs Upstream Jobs VBScript Jobs Vi Jobs Vignette Jobs Visual C++ Jobs Visual Interdev Jobs VMS Jobs VPN Jobs VSS/Clearcase Jobs VxWorks Jobs WAN Jobs WAP Jobs Winform Jobs Winrunner Jobs WML Jobs XDoclet Jobs Xenix Jobs XHTML Jobs XStream Jobs Yantra Jobs Designing Jobs SMO Jobs VAX/VMS Jobs TCL/TK Jobs Back office Operations Jobs OS/2 Jobs SAP BI Jobs Sharepoint MOSS Jobs"; 
		   String[] skillsarray = s.split("Jobs");
		   for (int i = 0; i < skillsarray.length; i++) {
		//System.out.println("data.add(new Tag(1, ".NET"));	
			   System.out.println("\""+skillsarray[i].trim()+"\",");
		}
		   }
		   
		   

		   
		   
		   
		   public static void buildEmailContent() {
				File file = new File("files/emailcontent.txt");
				File wrtFile = new File("files/writeText.txt");
				StringBuffer contents = new StringBuffer();
				BufferedReader reader = null;
				BufferedWriter writer = null;
				Map<String, String> map = new HashedMap();
				try {
					reader = new BufferedReader(new FileReader(file));
					writer = new BufferedWriter(new FileWriter(wrtFile));
					String text = null;
					int i=1;
					
					while ((text = reader.readLine()) != null) {
						if(BuildEmailContentUtil.isNotEmpty(text))
							text = text.replaceAll("\"", Matcher.quoteReplacement("\\\"")).trim();
						    System.out.println("sbf.append(\""+text+"\");");
						    contents.append(text).append(System.getProperty("line.separator"));
						    map.put(text, text);
					}
					writer.write(contents.toString());
					writer.flush();
					writer.close();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					try {
						if (reader != null) {
							reader.close();
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				// show file contents here
				for (Iterator iterator = map.keySet().iterator(); iterator.hasNext();) {
					String  str = (String) iterator.next();
					//System.out.println(str);
					//urlsList.add(e)
				//	if(str.length()>0)
						//System.out.println(str.length());
						//str = str.replaceAll("(?m)^\\s*$[\n\r]{1,}", "");
					//	System.out.println(str);
					//System.out.println("urlsList.add(\""+str+"\");");
				}
				//System.out.println(contents.toString());
			}
		   
		   
		   
		/* //String st = replace("a", "b", "c");
			//System.out.println("..."+st);
			//stringTokenizing();
			//matchString();
			//stringSplits();
			//printSkills();
			String str ="seo,sem,smo,digital marketing";
			 str = str.replaceAll(",", " ").trim();
			// System.out.println(str);
			 String[] skills = str.split(" ");
			 for (int i = 0; i < skills.length; i++) {
			System.out.println(skills[i]);	
			}*/
			 	   
		   
	public static void main(String[] args){
		buildEmailContent();
		
	}
}
