package com.app.core.lang;

import java.math.BigDecimal;

public class BigDecAdd {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		
		BigDecimal b3 = new BigDecimal("0");
		
		for(int i=0; i<4; i++){
			BigDecimal b1 = new BigDecimal(i);
			b3 = b3.add(b1);
			System.out.println("b1="+b1+"  b3="+b3);
		}
	}

}
