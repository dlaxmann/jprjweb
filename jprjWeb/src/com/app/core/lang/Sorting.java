package com.app.core.lang;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Sorting {
	
	
	public static void sortArray(){
		String[] fields ={"a","a1","b","b1","c","c1","c2","a3"};
		Arrays.sort(fields);
		for (String str: fields) {
			System.out.println(str);
		}
	}
	
	public static void sortArrayList(){
		List<String> fields = new ArrayList<String>();
		fields.add("a");
		fields.add("a1");
		fields.add("b");
		fields.add("b1");
		fields.add("c");
		fields.add("c1");
		fields.add("c2");
		fields.add("a3");
		//Collections.sort(fields);
		
		for (String string : fields) {
			System.out.println(string);
		}
	}

	public static void main(String[] args){
		Sorting.sortArray();
		Sorting.sortArrayList();
	}
}
