package com.app.core.lang;

import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.Locale;

public class DateUtl implements Comparable<DateUtl> {
    private static final int[] DAYS = { 0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

    private final int month;   // month (between 1 and 12)
    private final int day;     // day   (between 1 and DAYS[month]
    private final int year;    // year

    // do bounds-checking to ensure object represents a valid DateUtl
    public DateUtl(int month, int day, int year) {
        if (!isValid(month, day, year)) throw new RuntimeException("Invalid date");
        this.month = month;
        this.day   = day;
        this.year  = year;
    }

    // create new data by parsing from string of the form mm/dd/yy
    public DateUtl(String date) {
        String[] fields = date.split("/");
        if (fields.length != 3) {
            throw new RuntimeException("Date parse error");
        }
        month = Integer.parseInt(fields[0]);
        day   = Integer.parseInt(fields[1]);
        year  = Integer.parseInt(fields[2]);
        if (!isValid(month, day, year)) throw new RuntimeException("Invalid date");
    }

    public int month() { return month; }
    public int day()   { return day;   }
    public int year()  { return year;  }


    // is the given date valid?
    private static boolean isValid(int m, int d, int y) {
        if (m < 1 || m > 12)      return false;
        if (d < 1 || d > DAYS[m]) return false;
        if (m == 2 && d == 29 && !isLeapYear(y)) return false;
        return true;
    }

    // is y a leap year?
    private static boolean isLeapYear(int y) {
        if (y % 400 == 0) return true;
        if (y % 100 == 0) return false;
        return y % 4 == 0;
    }

    // return the next Date
    public DateUtl next() {
        if (isValid(month, day + 1, year))    return new DateUtl(month, day + 1, year);
        else if (isValid(month + 1, 1, year)) return new DateUtl(month + 1, 1, year);
        else                                  return new DateUtl(1, 1, year + 1);
    }


    // is this Date after b?
    public boolean isAfter(DateUtl b) {
        return compareTo(b) > 0;
    }

    // is this Date a before b?
    public boolean isBefore(DateUtl b) {
    	boolean flg = false;
    	flg = compareTo(b) < 0;
    	return flg;
        //return compareTo(b) < 0;
    }

    // compare this Date to that one
    public int compareTo(DateUtl that) {
        if (this.year  < that.year)  return -1;
        if (this.year  > that.year)  return +1;
        if (this.month < that.month) return -1;
        if (this.month > that.month) return +1;
        if (this.day   < that.day)   return -1;
        if (this.day   > that.day)   return +1;
        return 0;
    }

    // return a string representation of this date
    public String toString() {
        return month + "/" + day + "/" + year;
    }

    // is this Date equal to x?
    public boolean equals(Object x) {
        if (x == this) return true;
        if (x == null) return false;
        if (x.getClass() != this.getClass()) return false;
        DateUtl that = (DateUtl) x;
        return (this.month == that.month) && (this.day == that.day) && (this.year == that.year);
    }

    public int hashCode() {
        int hash = 17;
        hash = 31*hash + month;
        hash = 31*hash + day;
        hash = 31*hash + year;
        return hash;
    }

    
   public boolean isHoliday(DateUtl d){
	 boolean flg = false;
	   return flg;
   }

    // sample client for testing
    public static void main(String[] args) {
    	
    	
    	
    	// get today and clear time of day
    	Calendar cal = Calendar.getInstance();
    	cal.clear(Calendar.HOUR_OF_DAY);
    	cal.clear(Calendar.MINUTE);
    	cal.clear(Calendar.SECOND);
    	cal.clear(Calendar.MILLISECOND);

    	// get start of this week in milliseconds
    	cal.set(Calendar.DAY_OF_WEEK, cal.getFirstDayOfWeek());
    	System.out.println("Start of this week:       " + cal.getTime());
    	System.out.println("... in milliseconds:      " + cal.getTimeInMillis());

    	// start of the next week
    	cal.add(Calendar.WEEK_OF_YEAR, 1);
    	System.out.println("Start of the next week:   " + cal.getTime());
    	System.out.println("... in milliseconds:      " + cal.getTimeInMillis());
    	
    	
    	
    	
    	
    	
    	Calendar cl = Calendar.getInstance();
    	Calendar c2 = Calendar.getInstance();
    	cl.add(cl.YEAR,1 ); //adds one year to the current year
    	cl.add(cl.MONTH,0); // adds one month to the current month
    	cl.setFirstDayOfWeek(3);
    	System.out.println(cl.getFirstDayOfWeek());
    	System.out.println(cl.getTime());
    	
    	System.out.println("------");
    	System.out.println(cl.YEAR);
    	System.out.println(cl.MONTH);
    	System.out.println(cl.WEEK_OF_YEAR);
    	System.out.println(cl.WEEK_OF_MONTH);
    	System.out.println(cl.DAY_OF_MONTH);
    	System.out.println(cl.DAY_OF_WEEK);
    	System.out.println(c2.getTime());
    	
    	
    	
    	DateUtl today = new DateUtl(8, 20, 2012);
        System.out.println(today);
        int y = 12;
        System.out.println("..."+ isLeapYear(y));
        System.out.println();
        
       /* for (int i = 0; i < 10; i++) {
            today = today.next();
            System.out.println(today);
        }*/

        System.out.println(today.isAfter(today.next()));
        System.out.println(today.isAfter(today));
        System.out.println(today.next().isAfter(today));


        DateUtl birthday = new DateUtl(10, 16, 1971);
        System.out.println(birthday);
        /*for (int i = 0; i < 10; i++) {
            birthday = birthday.next();
            System.out.println(birthday);
        }*/
    }


}
