package com.app.core.app.bean;

import java.lang.reflect.InvocationTargetException;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class BeanUtilsCopyPropertiesTest {

	public static void main(String[] args) {

		FromBean fromBean = new FromBean("fromBean", "fromBeanAProp", "fromBeanBProp");
		ToBean toBean = new ToBean("toBean", "toBeanBProp", "toBeanCProp");
		System.out.println(ToStringBuilder.reflectionToString(fromBean));
		System.out.println(ToStringBuilder.reflectionToString(toBean));
		try {
			System.out.println("Copying properties from fromBean to toBean");
			BeanUtils.copyProperties(toBean, fromBean);
			toBean.setBean1("abcd");
			System.out.println("......."+toBean.getBProp());
			System.out.println("......."+toBean.getBean1());
			
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		System.out.println(ToStringBuilder.reflectionToString(fromBean));
		System.out.println(ToStringBuilder.reflectionToString(toBean));
	}
}
