package com.app.core.app.bean;

import java.util.Date;
import java.util.List;

public class Certificate {

	private int registrationNo;
	private String fullName;
	private String fatherName;
	private String permanentAddres;
	private int citizenshipNo;
	private int passportNo;
	private String creditHours;
	private String system;
	private Date fromDate;
	private Date toDate;
	private String course;
	private String imagePath;
	private List<String> headers;
	private String workSystem;

	public int getRegistrationNo() {
	return registrationNo;
	}
	public void setRegistrationNo(int registrationNo) {
	this.registrationNo = registrationNo;
	}
	public String getFullName() {
	return fullName;
	}
	public void setFullName(String fullName) {
	this.fullName = fullName;
	}
	public String getFatherName() {
	return fatherName;
	}
	public void setFatherName(String fatherName) {
	this.fatherName = fatherName;
	}
	public String getPermanentAddres() {
	return permanentAddres;
	}
	public void setPermanentAddres(String permanentAddres) {
	this.permanentAddres = permanentAddres;
	}
	public int getPassportNo() {
	return passportNo;
	}
	public void setPassportNo(int passportNo) {
	this.passportNo = passportNo;
	}
	public int getCitizenshipNo() {
	return citizenshipNo;
	}
	public void setCitizenshipNo(int citizenshipNo) {
	this.citizenshipNo = citizenshipNo;
	}
	public String getCreditHours() {
	return creditHours;
	}
	public void setCreditHours(String creditHours) {
	this.creditHours = creditHours;
	}
	public String getSystem() {
	return system;
	}
	public void setSystem(String system) {
	this.system = system;
	}
	public Date getFromDate() {
	return fromDate;
	}
	public void setFromDate(Date fromDate) {
	this.fromDate = fromDate;
	}
	public Date getToDate() {
	return toDate;
	}
	public void setToDate(Date toDate) {
	this.toDate = toDate;
	}
	public String getCourse() {
	return course;
	}
	public void setCourse(String course) {
	this.course = course;
	}

	public String getImagePath() {
	return imagePath;
	}
	public void setImagePath(String imagePath) {
	this.imagePath = imagePath;
	}
	public List<String> getHeaders() {
	return headers;
	}
	public void setHeaders(List<String> headers) {
	this.headers = headers;
	}
	public String getWorkSystem() {
	return workSystem;
	}
	public void setWorkSystem(String workSystem) {
	this.workSystem = workSystem;
	}
}
