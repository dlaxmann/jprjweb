package com.app.core.app.hibernate;

import java.io.DataInputStream;
import java.util.Iterator;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.frame.util.HibernateUtil;

public class SaveStudentControl {

	

	public void addNewStudent() throws Exception {
		try {
			Session s = HibernateUtil.getSession();
			Transaction tx = s.beginTransaction();
			Student stu = new Student();
			DataInputStream ds = new DataInputStream(System.in);
			/*System.out.println("*********************\n");
			System.out.println("Enter the Student Id");
			String stuid = ds.readLine();
			System.out.println("Enter the Student RegNo");
			String sturegno = ds.readLine();
			System.out.println("Enter the Student name");
			String stuname = ds.readLine();
			System.out.println("Enter the Student mark1");
			String stumark1 = ds.readLine();
			System.out.println("Enter the Student mark2");
			String stumark2 = ds.readLine();
			System.out.println("Enter the Degree");
			String degree = ds.readLine();
			System.out.println("Enter the mobile No");
			String mobileno = ds.readLine();
			System.out.println("Enter the Student mail Id");
			String mailid = ds.readLine();*/
			
			for (int i = 0; i < 5; i++) {
				stu.setId(1);
				stu.setStuid("STD_ID"+i);
				stu.setSturegno("REGNO"+i);
				stu.setStuname("NAME"+i);
				stu.setStumark1("1000"+i);
				stu.setStumark2("5000"+i);
				stu.setDegree("BE");
				stu.setMobileno("9885098850");
				stu.setMailid("STD@gmail.com");
				s.save(stu);
				tx.commit();
			}
			
			
			// Query q =
			// s.createQuery("select stu.stuid, avg(stu.stuid) from Student stu group by stu.stuid having avg(stu.stuid) > 10 ");
			// List li=q.list();
			// System.out.println(li);
			// Student ss=new Student();
			
			System.out.println("\n\n Details Added \n");
		} catch (HibernateException e) {
			System.out.println(e.getMessage());
			System.out.println("error");
		}

	}
}
