package com.app.core.dp.iterator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MenuIterator implements Iterator<Item> {
	int currentIndex = 0;
	 List<Item> itemList = new ArrayList<Item>();
	
	@Override
	public boolean hasNext() {
		if (currentIndex >= itemList.size()) {
			return false;
		} else {
			return true;
		}
	}
	@Override
	public Item next() {
		return itemList.get(currentIndex++);
	}
	@Override
	public void remove() {
		itemList.remove(--currentIndex);
	}
}
