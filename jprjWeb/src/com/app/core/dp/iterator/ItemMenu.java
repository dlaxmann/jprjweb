package com.app.core.dp.iterator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ItemMenu {
		 List<Item> itemList;

		public ItemMenu() {
			itemList = new ArrayList<Item>();
		}

		public void addItems(Item item) {
			itemList.add(item);
		}

		public Iterator<Item> iterator() {
			return new MenuIterator();

		}
}

