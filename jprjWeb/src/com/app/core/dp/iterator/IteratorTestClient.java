package com.app.core.dp.iterator;

public class IteratorTestClient {

	public static void main(String[] args){
		Item item1 = new Item("item1", (float) 200.00);
		Item item2 = new Item("item1",  300.00f);
		Item item3 = new Item("item1",  400.00f);
		
		ItemMenu  itmMenu = new ItemMenu();
		itmMenu.addItems(item1);
		itmMenu.addItems(item2);
		itmMenu.addItems(item3);
		System.out.println("Done----");
	}
}

 
	


class Item {
	String itemName;
	float itemPrice;
	
	public Item(String itemName, float itemPrice) {
		super();
		this.itemName = itemName;
		this.itemPrice = itemPrice;
	}
	@Override
	public String toString() {
		return itemName+"_"+itemPrice ;
	}
}
