package com.app.core.dp.decorator;

 abstract class IceCreamDecorator implements IceCream{

	protected IceCream specialIcecream;
	 
	  public IceCreamDecorator(IceCream specialIcecream) {
	    this.specialIcecream = specialIcecream;
	  }
	 
	  public String makeIcecream() {
	    return specialIcecream.makeIceCream();
	  }
}
