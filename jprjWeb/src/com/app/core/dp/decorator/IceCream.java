package com.app.core.dp.decorator;

public interface IceCream {
	public String makeIceCream();
}
