package com.app.core.dp.str.adapter;


/**
 * Adapter design pattern can be implemented in two ways. 
 * One using the inheritance method and second using the composition method. 
 * Just the implementation methodology is different but the purpose and solution is same
 * @author ldevasani
 *
 */
public class CylindricalSocket {

	public String supply(String cylinStem1, String cylinStem2) {
	    System.out.println("Power power power...");
		return cylinStem2;
	  }

}
