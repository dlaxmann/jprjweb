/**
 * 
 */
package com.app.core.dp.str.bridgePattern;

/**
 * The Bridge Pattern is used to separate out the interface from its implementation. 
 * Doing this gives the flexibility so that both can vary independently.
 *
 */
public interface Switch {

	// Two positions of switch.
	public void switchOn();
	public void switchOff();
}
