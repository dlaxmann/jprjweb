package com.app.core.dp.creational.factoryPattern;

public class FactoryClientOne {

	public static void main(String[] args)
	  {
	    // create a small dog
	    Dog dog = DogFactory.getDog("small");
	    dog.speak();
	    System.out.println(".......Created small Dog");
	    
	    
	    // create a big dog
	    dog = DogFactory.getDog("big");
	    dog.speak();
	    System.out.println(".......Created Big Dog");
	    
	    // create a working dog
	    dog = DogFactory.getDog("working");
	    dog.speak();
	    System.out.println(".......Created Working Dog");
	  }
}
