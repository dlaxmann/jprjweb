package com.app.core.dp.creational.factoryPattern;

public class Poodle implements Dog {

	public void speak()
	  {
	    System.out.println("The poodle says \"arf\"");
	  }

}
