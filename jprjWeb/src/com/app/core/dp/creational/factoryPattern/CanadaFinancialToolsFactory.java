package com.app.core.dp.creational.factoryPattern;

public class CanadaFinancialToolsFactory {

	public TaxProcessor createTaxProcessor() {
		return new CanadaTaxProcessor();
	}
	public ShipFeeProcessor createShipFeeProcessor() {
		return new CanadaShipFeeProcessor();
		}
}
