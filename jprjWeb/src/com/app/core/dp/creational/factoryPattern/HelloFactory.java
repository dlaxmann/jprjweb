package com.app.core.dp.creational.factoryPattern;

/*
 * if we have a super class and n sub-classes, and based on data provided, we have to 
 * return the object of one of the sub-classes, we use a factory pattern.
 */
public class HelloFactory {

	public static void main(String args[]) {
		
		HelloFactory factory = new HelloFactory();
		factory.getPerson("abc", "M.");
		}
		public Person getPerson(String name, String gender) {
		if (gender.equals("M"))
		return new Male(name);
		else if(gender.equals("F"))
		return new Female(name);
		else
		return null;
		}
}
