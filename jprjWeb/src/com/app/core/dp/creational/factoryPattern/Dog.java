package com.app.core.dp.creational.factoryPattern;

public interface Dog {
	public void speak ();
}
