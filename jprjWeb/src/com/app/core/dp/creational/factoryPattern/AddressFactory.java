package com.app.core.dp.creational.factoryPattern;

public interface AddressFactory {

	public Address createAddress();
	 public PhoneNumber createPhoneNumber();
}
