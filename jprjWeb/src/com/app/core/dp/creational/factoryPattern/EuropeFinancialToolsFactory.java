package com.app.core.dp.creational.factoryPattern;

public class EuropeFinancialToolsFactory {

	public TaxProcessor createTaxProcessor() {
		return new EuropeTaxProcessor();
	}
	public ShipFeeProcessor createShipFeeProcessor() {
		return new EuropeShipFeeProcessor();
	}
}
