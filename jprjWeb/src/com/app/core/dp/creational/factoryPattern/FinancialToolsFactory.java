package com.app.core.dp.creational.factoryPattern;

public abstract class FinancialToolsFactory {

	public abstract TaxProcessor createTaxProcessor();
	public abstract ShipFeeProcessor createShipFeeProcessor();


}
