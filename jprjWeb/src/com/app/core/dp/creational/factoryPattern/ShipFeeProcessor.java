package com.app.core.dp.creational.factoryPattern;

public abstract class ShipFeeProcessor {

	abstract void calculateShipFee(Order order);
}

