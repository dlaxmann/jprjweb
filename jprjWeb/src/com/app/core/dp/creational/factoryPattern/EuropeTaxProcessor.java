package com.app.core.dp.creational.factoryPattern;

public class EuropeTaxProcessor extends TaxProcessor {
	public void calculateTaxes(Order order) {
		// insert here Europe specific taxt calculation
	}

}
