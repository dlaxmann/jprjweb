package com.app.core.dp.creational.factoryPattern;

public class Male extends Person {

	public Male(String fullName) {
		System.out.println("Hello Mr. "+fullName);
		}
}
