package com.app.core.dp.creational.factoryPattern;

public class EuropeShipFeeProcessor extends ShipFeeProcessor {
	public void calculateShipFee(Order order) {
		// insert here Europe specific ship fee calculation
		}
}
