package com.app.core.dp.creational.factoryPattern;

public class CanadaShipFeeProcessor extends ShipFeeProcessor {
	public void calculateShipFee(Order order) {
		// insert here Canada specific ship fee calculation
		}
}
