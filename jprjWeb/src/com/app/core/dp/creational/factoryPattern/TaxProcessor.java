package com.app.core.dp.creational.factoryPattern;

public abstract class TaxProcessor {
	abstract void calculateTaxes(Order order);
}
