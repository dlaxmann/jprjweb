package com.app.core.dp.creational.factoryPattern;

public class AbstractFactoryPattern {
	public static void main(String[] args) {
		  Address fd=new FrenchAddressFactory().createAddress();
		  System.out.println("French Address:" + fd.getFullAddress());
		 }
}






class FrenchPhoneNumber extends PhoneNumber{
	 private static final String COUNTRY_CODE = "33";
	 private static final int NUMBER_LENGTH = 9;
	 
	 public String getCountryCode(){ return COUNTRY_CODE; }
	 
	 public void setPhoneNumber(String newNumber){
	  if (newNumber.length() == NUMBER_LENGTH){
	   super.setPhoneNumber(newNumber);
	  }
	 }
	}

class USAddressFactory implements AddressFactory{
	 public Address createAddress(){
	  return new USAddress();
	 }
	 
	 public PhoneNumber createPhoneNumber(){
	  return new USPhoneNumber();
	 }
	}
	 
	class USAddress extends Address{
	 private static final String COUNTRY = "UNITED STATES";
	 private static final String COMMA = ",";
	 
	 public String getCountry(){ return COUNTRY; }
	 
	 public String getFullAddress(){
	  return getStreet() + EOL_STRING +
	  getCity() + COMMA + SPACE + getRegion() +
	  SPACE + getPostalCode() + EOL_STRING +
	  COUNTRY + EOL_STRING;
	 }
	}
	 
	class USPhoneNumber extends PhoneNumber{
	 private static final String COUNTRY_CODE = "01";
	 private static final int NUMBER_LENGTH = 10;
	 
	 public String getCountryCode(){ return COUNTRY_CODE; }
	 
	 public void setPhoneNumber(String newNumber){
	  if (newNumber.length() == NUMBER_LENGTH){
	   super.setPhoneNumber(newNumber);
	  }
	 }
	}
	 
	class FrenchAddressFactory implements AddressFactory{
	 public Address createAddress(){
	  return new FrenchAddress();
	 }
	 
	 public PhoneNumber createPhoneNumber(){
	  return new FrenchPhoneNumber();
	 }
	}
	 
	class FrenchAddress extends Address{
	 private static final String COUNTRY = "FRANCE";
	 
	 public String getCountry(){ return COUNTRY; }
	 
	 public String getFullAddress(){
	  return getStreet() + EOL_STRING + getPostalCode() + SPACE + getCity() + EOL_STRING + COUNTRY + EOL_STRING;
	 }
	}
