package com.app.core.dp.creational.factoryPattern;

public class Person {

	// name string
	public String name;
	// gender : M or F
	private String gender;
	
	
	public String getName() {
	return name;
	}

	public String getGender() {
	return gender;
	}
}
