package com.app.core.dp.creational.factoryPattern;

public abstract class PhoneNumber {
	private String phoneNumber;
	 abstract String getCountryCode();
	 
	 public String getPhoneNumber() {
	  return phoneNumber;
	 }
	 public void setPhoneNumber(String phoneNumber) {
	  this.phoneNumber = phoneNumber;
	 }
}
