package com.app.core.dp.creational.prototype;

/*
 * The prototype means making a clone. This implies cloning of an object to avoid creation. 
 * If the cost of creating a new object is large and creation is resource intensive, 
 * we clone the object. We use the interface Cloneable and call its method clone() to clone the object.
 * 
 * One thing is you cannot use the clone as it is. You need to instantiate the clone before using it. 
 * This can be a performance drawback. This also gives sufficient access to the data and methods of the class. 
 * This means the data access methods have to be added to the prototype once it has been cloned.
 */
public class PlantCell implements Cloneable{

	public Object split() {
 	 	try {
 	 	 	return super.clone();
}catch(Exception e) {
System.out.println("Exception occured: "+e.getMessage());
return null;
}
 	 	}
}
