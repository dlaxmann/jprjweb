package com.app.core.dp.creational.singleton;

public class SingletonExm {

	//create static class variable
	private static SingletonExm singleton = null;
	
	//create private constructor
	private SingletonExm(){
			}
	
	//create getinstance, if instance null create one using con
	public static SingletonExm getInstance(){
		if(singleton == null){
			singleton = new SingletonExm();
		}
		return singleton;
	}
	
	public void sayHi(){
		System.out.println("--");
	}
}
