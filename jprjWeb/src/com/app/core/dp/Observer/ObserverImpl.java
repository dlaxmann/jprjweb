package com.app.core.dp.Observer;

public class ObserverImpl implements Observer {

	private String state = "";

	 public void update(Subject o) {
	   state = o.getState();
	   System.out.println("Update received from Subject, state changed to : " + state);
	 }

}
