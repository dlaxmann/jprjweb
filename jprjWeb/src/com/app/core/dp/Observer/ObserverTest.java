package com.app.core.dp.Observer;

public class ObserverTest {

	public static void main(String[] args) {
		   Observer o = new ObserverImpl();
		   Subject s = new SubjectImpl();
		   s.addObserver(o);
		   s.setState("New State");

		 }
}
