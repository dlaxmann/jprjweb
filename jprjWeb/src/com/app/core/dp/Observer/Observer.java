package com.app.core.dp.Observer;

public interface Observer {
	public void update(Subject o);
}
