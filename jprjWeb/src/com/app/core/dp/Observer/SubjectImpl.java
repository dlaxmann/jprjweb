package com.app.core.dp.Observer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SubjectImpl implements Subject {

	private List observers = new ArrayList();

	 private String state = "";

	 public String getState() {
	   return state;
	 }

	 public void setState(String state) {
	   this.state = state;
	   notifyObservers();
	 }

	 public void addObserver(Observer o) {
	   observers.add(o);
	 }

	 public void removeObserver(Observer o) {
	   observers.remove(o);
	 }

	 public void notifyObservers() {
	   Iterator i = observers.iterator();
	   while (i.hasNext()) {
	     Observer o = (Observer) i.next();
	     o.update(this);
	   }
	 }
}
