package com.app.core.dp.façadePattern;

public class TestFacadeSample {
	
	public static void main(String[] args) {
		Computer facadePC = new MyPC();
        //after buying one, he starts it up.
        facadePC.boot();
	}
}

class CPU{
	private String name;
	 
    public CPU(String name) {
        this.name = name;
    }
    public void freeze() {
        System.out.println("CPU is freezing the process");
    }
    public void resume() {
        System.out.println("CPU is resuming the process");
    }
    public void jumpAndExecute(long index) {
        System.out.println("CPU has jumped to the position: " + index
                + " and continue the process");
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
}


 class RAM {
    public void load(long index) {
        System.out.println("RAM memory is loading at the position: " + index);
    }
}
 class HardDisk{
	public void read(long position, long size){
		System.out.println("HDD is reading data at position: " + position
                + " with a length of " + size + " bytes ");
	}
 }
 
class MyPC implements Computer{
	
	private CPU cpu;
	private RAM ram;
	private HardDisk hdd;
	
	public MyPC() {
		this.cpu = new CPU("Core2Duo");
		this.ram = new RAM();
		this.hdd = new HardDisk();
	}

	// After create MyPC, CPU, RAM and HDD are integrated and ready to work.
	public void boot() {
		this.cpu.freeze();
		this.ram.load(120);
		this.hdd.read(1500, 512);
		this.cpu.resume();
	}
		
	public void shutdown() {
	// ehhh I will run all day long at the moment.
	}
}
