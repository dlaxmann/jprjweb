package com.app.core.dp.façadePattern;

public interface Goods {
	public Goods getGoods();
}
