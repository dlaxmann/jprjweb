package com.app.core.dp.façadePattern;

public interface Computer {
		//do some operations
		public void boot();
	    public void shutdown();
}
