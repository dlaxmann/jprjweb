package com.app.core.dp.proxyPattern;

public interface Animal {

	public void getSound();

}