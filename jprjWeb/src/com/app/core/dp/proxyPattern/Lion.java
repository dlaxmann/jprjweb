package com.app.core.dp.proxyPattern;

public class Lion implements Animal {

	public void getSound() {
		System.out.println("Roar");
	}

}
