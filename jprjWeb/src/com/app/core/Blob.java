package com.app.core;


/*CREATE TABLE blobing (
		fid number(4),
		file blob,
		filetype verchar2(12) 
		);*/

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Blob {

	public static void main(String args[]) throws ClassNotFoundException, IOException {
    
	    Connection conn = null;
	    Statement stmt = null;
	    ResultSet rs = null;
	    try {
	    	Class.forName("oracle.jdbc.OracleDriver");
	    	System.out.println("Loaded driver class.. oracle.jdbc.driver.OracleDriver");
	    	conn = DriverManager.getConnection("jdbc:oracle:thin:@//localhost:1521/xe", "SYSTEM", "ldevasani");
	    	PreparedStatement pstmt = conn.prepareStatement("INSERT INTO savefile (id,myfile)"+ " VALUES(?,?)");
	           	
	        File file = new File("C:/BMC.txt");
	        FileInputStream fs = new FileInputStream(file);
	        pstmt.setInt(1, 1);
	        pstmt.setBinaryStream(2, (InputStream)fs, (int)(file.length()));
	         
	        int i = pstmt.executeUpdate();
	         if(i!=0){
	           System.out.println("file inserted successfully");
	         }
	         else{
	        	 System.out.println("problem in file insertion");
	         }  
	       
	         PreparedStatement stmnt = conn.prepareStatement("select myfile from savefile");
	         rs = stmnt.executeQuery();
	         if(!rs.next()){
	        	 System.out.println("No such myfile stored.");
	         }else{
	         try
	            {
	               // Get as a BLOB
	               Blob aBlob = (Blob) rs.getBlob(1);
	               BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
	               bos.write(0);
	               //bos.write(aBlob.getBytes(0,(int)aBlob.length()),0,(int)aBlob.length());
	            }
	            catch(Exception ex)
	            {
	            	ex.printStackTrace();
	                
	            }
	         }

	         
	         
	         String sql = "SELECT name, description, image FROM pictures ";
	         PreparedStatement stmt1 = conn.prepareStatement(sql);
	         ResultSet resultSet = stmt1.executeQuery();
	         while (resultSet.next()) {
	           String name = resultSet.getString(1);
	           String description = resultSet.getString(2);
	           File image = new File("D:\\java.gif");
	           FileOutputStream fos = new FileOutputStream(image);

	           byte[] buffer = new byte[1];
	           InputStream is = resultSet.getBinaryStream(3);
	           while (is.read(buffer) > 0) {
	             fos.write(buffer);
	           }
	           fos.close();
	         }
	         conn.close();

	         
	         
	         
	         

	         	         
	         
	         
	         
	         
	      rs.close();
	      rs = null;
	      stmt1.close();
	      stmt1 = null;
	      conn.close();
	      conn = null;
	    } catch (SQLException e) {
	      System.out.println(" SQL error: " + e.getMessage());
	    } finally {
	      if (rs != null)
	        try {
	          rs.close();
	        } catch (SQLException ignore) {
	        }
	      if (stmt != null)
	        try {
	          stmt.close();
	        } catch (SQLException ignore) {
	        }
	      if (conn != null)
	        try {
	          conn.close();
	        } catch (SQLException ignore) {
	        }
	    }
	  }

	public InputStream getBinaryStream() {
		// TODO Auto-generated method stub
		return null;
	}

}
