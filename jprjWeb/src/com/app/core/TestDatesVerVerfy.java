package com.app.core;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class TestDatesVerVerfy {

	static String inputdate = "02-01-2010";
	public static Calendar inputtCalander = new GregorianCalendar();
	static final String jan = "January";
	static final String feb = "February";
	static final String mar = "March";
    
    
	static boolean validateInputDate(String inputdate){
	if(inputdate.equals(null) || inputdate.length() != 10){
		Calendar auditParamValCalender = new GregorianCalendar();
		SimpleDateFormat auditParamFormat = new SimpleDateFormat("MM-DD-yyyy");
		inputdate = auditParamFormat.format(auditParamValCalender.getTime()).toString();
		System.out.println("length not equal, new Date()= " +inputdate);
	}
		return true;
	}


    static Calendar monthForInputDate(String auditInputParameter){
		if(auditInputParameter.equals(null) || auditInputParameter.length() != 10){
    		return inputtCalander;
    	}
		else{	
	    	String _month = auditInputParameter.substring(0,2);
	        String _date = auditInputParameter.substring(3,5);
	        String _year = auditInputParameter.substring(6,10);
	        int date = Integer.parseInt(_date);
	        int month = Integer.parseInt(_month);
	        int year = Integer.parseInt(_year);
	        inputtCalander.set(year, month-1,date);
	        System.out.println("The Entered Param Date is " +date);
	        System.out.println("The Entered Param Month is " +month);
	        System.out.println("The Entered Param Year is " +year);
	    	return inputtCalander;
    		}
		}
	
    
    static Calendar priorMonthForInputDate(String auditInputParameter){
		if(auditInputParameter.equals(null) || auditInputParameter.length() != 10){
    		return inputtCalander;
    	}
		else{
	    	String _month = auditInputParameter.substring(0,2);
	        String _date = auditInputParameter.substring(3,5);
	        String _year = auditInputParameter.substring(6,10);
	        int date = Integer.parseInt(_date);
	        int month = Integer.parseInt(_month);
	        int year = Integer.parseInt(_year);
	        inputtCalander.set(year, month-2,date);
	        System.out.println("The Entered Param Date is " +date);
	        System.out.println("The Entered Param Month is " +month);
	        System.out.println("The Entered Param Year is " +year);
	    	return inputtCalander;
	    	}
	 	}
    
    
    static Calendar PrevYrForInputDate(String auditInputParameter){
		if(auditInputParameter.equals(null) || auditInputParameter.length() != 10){
    		return inputtCalander;
    	}
		else{
	    	String _month = auditInputParameter.substring(0,2);
	        String _date = auditInputParameter.substring(3,5);
	        String _year = auditInputParameter.substring(6,10);
	        int date = Integer.parseInt(_date);
	        int month = Integer.parseInt(_month);
	        int year = Integer.parseInt(_year);
	        inputtCalander.set(year-1, month-1,date);
	        System.out.println("The Entered Param Date is " +date);
	        System.out.println("The Entered Param Month is " +month);
	        System.out.println("The Entered Param Year is " +year);
	    	return inputtCalander;
	    	}
	 	}
    
    
	public static void main(String[] args) {
		
		Calendar currentMonthCal = monthForInputDate(inputdate);
		SimpleDateFormat sdf1 = new SimpleDateFormat("MMMMM-yyyy");
		
		String currentMonth = sdf1.format(currentMonthCal.getTime());
		System.out.println("The Given Input Month is   "+currentMonth);
		
		int startDateOfMonth = currentMonthCal.getActualMinimum(Calendar.DAY_OF_MONTH);
	    int endDateOfMonth = currentMonthCal.getActualMaximum(Calendar.DAY_OF_MONTH);
	    
	    String startDate = Integer.toString(startDateOfMonth);
	    String endDate = Integer.toString(endDateOfMonth);
	    
	    String  startDateOfMonthStr = startDate.concat("-").concat(currentMonth);
	    String  endDateOfMonthStr = endDate.concat("-").concat(currentMonth);
	    System.out.println("start date of Current Month is   "+startDateOfMonthStr);
	    System.out.println("end date of Current Month is   "+endDateOfMonthStr);

	    
	    
	    System.out.println("----------------------MONTHLYPRIOR--------------------------");
	   
	    /**
		 * Gives Invoice dates for prior month..auditPeriod.equals(MONTHLYPRIOR)
		 */
	   
	    Calendar priorMonthCalander = priorMonthForInputDate(inputdate);
		SimpleDateFormat sdf11 = new SimpleDateFormat("MMMMM-yyyy");
	    String priorMonth = sdf11.format(priorMonthCalander.getTime());
	    System.out.println("Prior Month is   "+priorMonth);
	    
		int _startDateOfPriorMonth = priorMonthCalander.getActualMinimum(Calendar.DAY_OF_MONTH);
		System.out.println("first day of MONTHLYPRIOR is   "+_startDateOfPriorMonth);
		int _lastDateOfPriorMonth = priorMonthCalander.getActualMaximum(Calendar.DAY_OF_MONTH);
		System.out.println("last day of MONTHLYPRIOR is   "+_lastDateOfPriorMonth);
		
		String startOfPriorMonth = Integer.toString(_startDateOfPriorMonth);
	    String lastOfPriorMonth = Integer.toString(_lastDateOfPriorMonth);
	    
	    String  startDateOfPriorMonth = startOfPriorMonth.concat("-").concat(priorMonth);
	    String  lastDateOfPriorMonth = lastOfPriorMonth.concat("-").concat(priorMonth);
	    System.out.println("start date of Prior Month is   "+startDateOfPriorMonth);
	    System.out.println("end date of Prior Month is   "+lastDateOfPriorMonth);
	    
		//auditexecution.setInvcStrtDt(startDateOfPriorMonth);
		//auditexecution.setInvcEndDt(lastDateOfPriorMonth);
	    
	
	    
	    System.out.println("---------------------QUARTERLY----------------------------");
	   
	    Calendar quaterlyCalander = monthForInputDate(inputdate);
		SimpleDateFormat qtrlyDateFormat = new SimpleDateFormat("MMMM");
		SimpleDateFormat quaterlyDateFormat = new SimpleDateFormat("MMMMM-yyyy");
		String invoiceQuater = qtrlyDateFormat.format(quaterlyCalander.getTime());
		System.out.println("---invoiceQuater =  " +invoiceQuater);
		
		
		SimpleDateFormat threeMonthDateFormat = new SimpleDateFormat("MMMM");
		SimpleDateFormat threeMnthTrendFormat = new SimpleDateFormat("MMMMM-yyyy");
		
		//Calendar threeMonthTrendCalendar = monthForInputDate(inputdate);
		
		//System.out.println("---threeMonthTrendMonth =  " +threeMonthTrendMonth);
		
		if (invoiceQuater != null && invoiceQuater.equals(jan)) {								// Oct-Dec
			
			Calendar quterlyJan = PrevYrForInputDate(inputdate);
			quterlyJan.set(Calendar.MONTH, 9);
			String firstMonthOfQuater = quaterlyDateFormat.format(quterlyJan.getTime());
			int firstDayOfQuaterly = quterlyJan.getActualMinimum(Calendar.DAY_OF_MONTH);
			System.out.println("First Month Of Quater=  "+firstMonthOfQuater);

			
			quterlyJan.set(Calendar.MONTH, 11);
			String lastMonthOfQuater = quaterlyDateFormat.format(quterlyJan.getTime());
			int lastDayOfQuaterly = quterlyJan.getActualMaximum(Calendar.DAY_OF_MONTH);
			System.out.println("Last Month Of Quater=   "+lastMonthOfQuater);
			
			String _firstDayOfQuaterly = Integer.toString(firstDayOfQuaterly);
			String _lastDayOfQuaterly = Integer.toString(lastDayOfQuaterly);
			
			String  startDateOfQuaterly  = _firstDayOfQuaterly.concat("-").concat(firstMonthOfQuater);
			String  endDateOfQuaterly  = _lastDayOfQuaterly.concat("-").concat(lastMonthOfQuater);
			System.out.println("Invoice Start Date Of Quaterly=  " + startDateOfQuaterly);
			System.out.println("Invoice End Date Of Quaterly=  " + endDateOfQuaterly);
		}
		else if(invoiceQuater != null && invoiceQuater.equals(feb)){     						// Nov-Jan
			
			Calendar quterlyFeb = PrevYrForInputDate(inputdate);
			quterlyFeb.set(Calendar.MONTH, 10);
			String firstMonthOfQuater = quaterlyDateFormat.format(quterlyFeb.getTime());
			int firstDayOfQuaterly = quterlyFeb.getActualMinimum(Calendar.DAY_OF_MONTH);
			System.out.println("First Month Of Quater=  "+firstMonthOfQuater);
			
			Calendar quterlySecMonthFeb = monthForInputDate(inputdate);
			quterlySecMonthFeb.set(Calendar.MONTH, 0);
			String lastMonthOfQuater = threeMnthTrendFormat.format(quterlySecMonthFeb.getTime());
			int lastDayOfQuaterly = quterlySecMonthFeb.getActualMaximum(Calendar.DAY_OF_MONTH);
			System.out.println("Last Month Of Quater=   "+lastMonthOfQuater);
			
			String _firstDayOfQuaterly = Integer.toString(firstDayOfQuaterly);
			String _lastDayOfQuaterly = Integer.toString(lastDayOfQuaterly);
			
			String  startDateOfQuaterly  = _firstDayOfQuaterly.concat("-").concat(firstMonthOfQuater);
			String  endDateOfQuaterly  = _lastDayOfQuaterly.concat("-").concat(lastMonthOfQuater);
			System.out.println("Invoice Start Date Of Quaterly=  " + startDateOfQuaterly);
			System.out.println("Invoice End Date Of Quaterly=  " + endDateOfQuaterly);
			
		}
		else if(invoiceQuater != null && invoiceQuater.equals(mar)){     						// Dec-Feb
			
			Calendar quterlyMar = PrevYrForInputDate(inputdate);
			quterlyMar.set(Calendar.MONTH, 11);
			String firstMonthOfQuater = threeMnthTrendFormat.format(quterlyMar.getTime());
			int firstDayOfQuaterly = quterlyMar.getActualMinimum(Calendar.DAY_OF_MONTH);
			System.out.println("First Month Of Quater=  "+firstMonthOfQuater);

			Calendar quterlySecMonthMar = monthForInputDate(inputdate);
			quterlySecMonthMar.set(Calendar.MONTH, 1);
			//threeMonthTrendCalendar.set(Calendar.YEAR,-1) ;
			String lastMonthOfQuater = threeMnthTrendFormat.format(quterlySecMonthMar.getTime());
			int lastDayOfQuaterly = quterlySecMonthMar.getActualMaximum(Calendar.DAY_OF_MONTH);
			System.out.println("Last Month Of Quater=   "+lastMonthOfQuater);
			
			String _firstDayOfQuaterly = Integer.toString(firstDayOfQuaterly);
			String _lastDayOfQuaterly = Integer.toString(lastDayOfQuaterly);
			
			String  startDateOfQuaterly  = _firstDayOfQuaterly.concat("-").concat(firstMonthOfQuater);
			String  endDateOfQuaterly  = _lastDayOfQuaterly.concat("-").concat(lastMonthOfQuater);
			System.out.println("Invoice Start Date Of Quaterly=  " + startDateOfQuaterly);
			System.out.println("Invoice End Date Of Quaterly=  " + endDateOfQuaterly);
		
		}
	    System.out.println("-----------------------SEMIANNUAL--------------------------");
	    
	    Calendar semiAnualCalander = monthForInputDate(inputdate);
	    SimpleDateFormat semiDateFormat = new SimpleDateFormat("MMMMM-yyyy");
	    SimpleDateFormat formatForSemiMonth = new SimpleDateFormat("MMMMM");
	    String monthForSemiAnn = formatForSemiMonth.format(semiAnualCalander.getTime()).trim();
		System.out.println("monthForSemiAnn=   "+monthForSemiAnn);
		
		
		 System.out.println("-----------------------ANNUAL--------------------------");
		
		 	Calendar yearCalendar = monthForInputDate(inputdate);
			SimpleDateFormat simpleDateYearFormat = new SimpleDateFormat("MMMMM-yyyy");
			//yearCalendar.get(Calendar.YEAR);
			String annualAuditPeriod = simpleDateYearFormat.format(yearCalendar.getTime());
			System.out.println("currentAnnual=  " + annualAuditPeriod);
			
			
			System.out.println("-----------------------TWOMONTHTREND--------------------------");
			
		 	Calendar twoMonthTrendCalander = monthForInputDate(inputdate);
			SimpleDateFormat twoMonthTrendDateFormat = new SimpleDateFormat("MMMMM-yyyy");
			SimpleDateFormat twoMonthFormat = new SimpleDateFormat("MMMM");
			String invoiceTwoMnthTrend = twoMonthFormat.format(twoMonthTrendCalander.getTime());
			System.out.println("Month for invoiceTwoMnthTrend=  " + invoiceTwoMnthTrend);
			
			
			
			if (invoiceTwoMnthTrend != null && invoiceTwoMnthTrend.equals(jan)) {								// Nov-Dec
				Calendar twoMonthjanCalander = PrevYrForInputDate(inputdate);
				twoMonthjanCalander.set(Calendar.MONTH, 10);
				String firstMonthOfTwoMnthTrend = twoMonthTrendDateFormat.format(twoMonthjanCalander.getTime());
				int firstDayOfQuaterly = twoMonthjanCalander.getActualMinimum(Calendar.DAY_OF_MONTH);
				System.out.println("First Month Of TwoMnthTrend=  "+firstMonthOfTwoMnthTrend);

				twoMonthjanCalander.set(Calendar.MONTH, 11);
				//twoMonthTrendCalander.set(Calendar.YEAR,-1) ;
				String lastMonthOfTwoMnthTrend = twoMonthTrendDateFormat.format(twoMonthjanCalander.getTime());
				int lastDayOfQuaterly = twoMonthjanCalander.getActualMaximum(Calendar.DAY_OF_MONTH);
				System.out.println("Last Month Of TwoMnthTrend=   "+lastMonthOfTwoMnthTrend);
				
				String _firstDayOfQuaterly = Integer.toString(firstDayOfQuaterly);
				String _lastDayOfQuaterly = Integer.toString(lastDayOfQuaterly);
				
				String  startDateOfQuaterly  = _firstDayOfQuaterly.concat("-").concat(firstMonthOfTwoMnthTrend);
				String  endDateOfQuaterly  = _lastDayOfQuaterly.concat("-").concat(lastMonthOfTwoMnthTrend);
				System.out.println("Invoice Start Date Of TwoMnthTrend=  " + startDateOfQuaterly);
				System.out.println("Invoice End Date Of TwoMnthTrend=  " + endDateOfQuaterly);
			
			
			}else if(invoiceTwoMnthTrend != null && invoiceTwoMnthTrend.equals(feb)){     						// Dec-Jan
				
				Calendar twoMonthFebCalander = PrevYrForInputDate(inputdate);
				twoMonthFebCalander.set(Calendar.MONTH, 11);
				String firstMonthOfTwoMnthTrend = twoMonthTrendDateFormat.format(twoMonthFebCalander.getTime());
				int firstDayOfQuaterly = twoMonthFebCalander.getActualMinimum(Calendar.DAY_OF_MONTH);
				System.out.println("First Month Of TwoMnthTrend=  "+firstMonthOfTwoMnthTrend);

				
				Calendar twoMonthFebCal = monthForInputDate(inputdate);
				twoMonthFebCal.set(Calendar.MONTH, 0);
				//twoMonthTrendCalander.set(Calendar.YEAR,-1) ;
				String lastMonthOfTwoMnthTrend = twoMonthTrendDateFormat.format(twoMonthFebCal.getTime());
				int lastDayOfQuaterly = twoMonthFebCal.getActualMaximum(Calendar.DAY_OF_MONTH);
				System.out.println("Last Month Of TwoMnthTrend=   "+lastMonthOfTwoMnthTrend);
				
				String _firstDayOfQuaterly = Integer.toString(firstDayOfQuaterly);
				String _lastDayOfQuaterly = Integer.toString(lastDayOfQuaterly);
				
				String  startDateOfQuaterly  = _firstDayOfQuaterly.concat("-").concat(firstMonthOfTwoMnthTrend);
				String  endDateOfQuaterly  = _lastDayOfQuaterly.concat("-").concat(lastMonthOfTwoMnthTrend);
				System.out.println("Invoice Start Date Of TwoMnthTrend=  " + startDateOfQuaterly);
				System.out.println("Invoice End Date Of TwoMnthTrend=  " + endDateOfQuaterly);
			
			
			}
			
			
			System.out.println("---------------------THREEMONTHTREND----------------------------");
			   
		    Calendar threeMonthTrendCalendar = monthForInputDate(inputdate);
			//SimpleDateFormat qtrlyDateFormat = new SimpleDateFormat("MMMM");
			//SimpleDateFormat threeMnthTrendFormat = new SimpleDateFormat("MMMMM-yyyy");
			String threeMonthTrendMonth = threeMnthTrendFormat.format(threeMonthTrendCalendar.getTime());
			System.out.println("---invoiceThreeMonthTrend =  " +threeMonthTrendMonth);
			
			
			
			
			System.out.println("---------------------THREEMONTHTREND----------------------------");
			   
		    Calendar sixMonthTrendCalendar = monthForInputDate(inputdate);
			//SimpleDateFormat qtrlyDateFormat = new SimpleDateFormat("MMMM");
			SimpleDateFormat sixMnthTrendFormat = new SimpleDateFormat("MMMMM-yyyy");
			String sixMonthTrendMonth = threeMnthTrendFormat.format(threeMonthTrendCalendar.getTime());
			System.out.println("---invoiceThreeMonthTrend =  " +threeMonthTrendMonth);
			
			
			System.out.println("---------------------Testing----------------------------");
			System.out.println("--Testing-"+PrevYrForInputDate(inputdate));
			
			
	}

	
}


/*
else{
	System.out.println("length is equal= ");
	return true;
	
	
	
	static Calendar monthForInputDate(String inputdate){
    	
    	if(inputdate.equals(null) || inputdate.length() != 10){
    		Calendar auditParamValCalender = new GregorianCalendar();
    		/*SimpleDateFormat auditParamFormat = new SimpleDateFormat("dd-mm-yyyy");
    		inputdate = auditParamFormat.format(auditParamValCalender.getTime()).toString();
    		System.out.println("length not equal, new Date()= " +inputdate);
    		return inputtCalander;
    	}else{
    	
	    	String _date = inputdate.substring(0,2);
	        String _month = inputdate.substring(3,5);
	        String _year = inputdate.substring(6,10);
	        
	        int date = Integer.parseInt(_date);
	        int month = Integer.parseInt(_month);
	        int year = Integer.parseInt(_year);
	        inputtCalander.set(year, month-1,date);
	        
	        System.out.println("The Entered Param Date is " +date);
	        System.out.println("The Entered Param Month is " +month);
	        System.out.println("The Entered Param Year is " +year);
	    	
	    	return inputtCalander;
    	}
    }
}

*/
//Calendar currentMonthcalander = new GregorianCalendar();

//int maxDay = inputtCalander(inputtMonthcalander.DAY_OF_MONTH);
//int minDay = inputtCalander(inputtMonthcalander.DAY_OF_MONTH);
 
//System.out.println("maxDay" +maxDay);
// System.out.println("minDay" +minDay);
//SimpleDateFormat sdf1 = new SimpleDateFormat("MMMMM-yyyy");
//String currentMonth = sdf1.format(inputtCalander.getTime());
//System.out.println("The Entered Param Year is " +currentMonth);