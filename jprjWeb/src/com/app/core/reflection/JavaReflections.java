package com.app.core.reflection;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class JavaReflections {
	public static void main(String args[]) {
		try {
			/*
			 * Step 1 Define a class loader
			 */
			ClassLoader myClassLoader = ClassLoader.getSystemClassLoader();
			/*
			 * Step 2: Define a class to be loaded.
			 */
			String classNameToBeLoaded = "com.app.core.app.reflection.MethodList";

			/*
			 * Step 3: Load the class
			 */
			Class myClass = myClassLoader.loadClass(classNameToBeLoaded);
			java.lang.reflect.Field[] fields =  myClass.getFields();
			
			

			/*
			 *Step 4: create a new instance of that class
			 */
			Object whatInstance = myClass.newInstance();

			String methodParameter = "ABC";
			/*
			 * Step 5: get the method, with proper parameter signature.
			 * The second parameter is the parameter type.
			 * There can be multiple parameters for the method we are trying to call, 
			 * hence the use of array.
			 */

			Method myMethod = myClass.getMethod("myMethod123",
					new Class[] { String.class });

			/*
			 *Step 6:
			 *Calling the real method. Passing methodParameter as
			 *parameter. You can pass multiple paramters based on 
			 *the signature of the method you are calling. Hence
			 *there is an array. 
			 */
			String returnValue = (String) myMethod.invoke(whatInstance,
					new Object[] { methodParameter });

			System.out.println("The value returned from the method is:"
					+ returnValue);

			/*
			 * Catching Probable Exceptions
			 * You can simply catch the Generic 'Exception' to reduce your coding.		 
			 */
		} catch (ClassNotFoundException e1) {
			System.out.println("EXCEPTION:CLASS NOT FOUND");
		} catch (IllegalAccessException e2) {
			System.out.println("EXCEPTION:ILLEGAL ACCESS");
		} catch (InstantiationException e3) {
			System.out.println("EXCEPTION:INSTANTIATION EXCEPTION");
		} catch (NoSuchMethodException e4) {
			System.out.println("EXCEPTION:NO SUCH METHOD");
		} catch (InvocationTargetException e5) {
			System.out.println("EXCEPTION: INVOCATION TARGET");

		}
	}

}

/**
 * @author Kushal Paudyal
 * 08/05/2008
 * www.sanjaal.com/java
 */

/*
 * This is the demo class that we will use to load and invoke method
 * using Java Reflection.
 */
class DemoClassToBeLoaded {
	public static String myMethod123(String someParam) {
		System.out.println("You Are Calling myMethod()");
		System.out.println("You Passed " + someParam + " as Parameter");

		/*
		 * Just returning the current class name
		 * This is just an example method. 
		 */
		return DemoClassToBeLoaded.class.getName();
	}

}
