package com.app.core.reflection;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import com.app.core.app.bean.Student;

public class MethodList {

	public void listMethods(){
	Class 	studentClass = Student.class; 
	Method[] listOfMethods = studentClass.getMethods();
	Field[] listOfFields = studentClass.getFields();
	
	for (Method method : listOfMethods) {
		System.out.println(method.getName());
	}
	
	for (Field field : listOfFields) {
		System.out.println(".."+field.getName());
	}
	
	
	}
	public static void main(String args[]){
		MethodList li = new MethodList();
		li.listMethods();
		
	}
}
