package com.app.core.reflection;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class DynamicClassLoader {

	public static void main(String args[]){
		
		ClassLoader cld = ClassLoader.getSystemClassLoader();
		String classToLoad = "com.app.core.app.reflection.InvokeMyMethods";
		
		try {
			Class clsser = cld.loadClass(classToLoad);
			Object objInstance = clsser.newInstance();
			String parameter ="param1";
			Method myMethod = clsser.getMethod("invokeMyMethod", new Class[]{String.class});
			String methodRetrnVal = (String) myMethod.invoke(objInstance, new Object[]{parameter});
			System.out.println("***"+methodRetrnVal);
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
