package com.app.core;

import java.util.Scanner;


public class Main {

   public static void main(String[] args) {

	   Scanner scinput=new Scanner(System.in);  
	   System.out.println("How many electronic devices does the company produce?"); 
	   int electronicDeviceNumber=scinput.nextInt();  
	   System.out.println("How many different component types are used?"); 
	   int componentsForDevice=scinput.nextInt();  
	 
	   
      //double[] componentCosts = {10.95, 6.30, 14.74, 11.25, 5.00};
	   double[] componentCosts = new double[5];
	   for (int i=0;i<componentCosts.length;i++) {
           System.out.println("Enter cost of each component");
           componentCosts[i]=scinput.nextDouble();
       }
			   
	   System.out.println("Cost Per Component:");
	   for (int i = 0; i < componentCosts.length; i++) {
		   System.out.print(componentCosts[i]+" ");
	}
	   System.out.println("\n");
	
	   
	   
	   int[][] componentsPerDevice = new int[electronicDeviceNumber][componentsForDevice];
	   
	   for (int i = 0; i < electronicDeviceNumber; i++) {
	       for (int j = 0; j < componentsForDevice; j++) {
	         System.out.println("Row [" + i + "]:  Column " + j + " :");
	         componentsPerDevice[i][j] = scinput.nextInt();
	        }
	    }
	   
	   
     /* int[][] componentsPerDevice = {
            {
                  10, 4, 5, 6, 7
            },
            {
                  7, 0, 12, 1, 3
            },
            {
                  4, 9, 5, 0, 8
            },
            {
                  3, 2, 1, 5, 6
            }
      };*/

      
      // show componentsPerDevice table
      for (int[] n : componentsPerDevice) {
         for (int x : n) {
            System.out.print(x + " ");
         }
         System.out.println("");
      }
      System.out.println("");

      double[] costPerDevice = {0, 0, 0, 0, 0};

     
      // calculate costs per device
      int device = 1;
      for (int[] n : componentsPerDevice) {
         int c = 0;
         double cost = 0;
         for (int x : n) {
            cost = componentCosts[c] * x;
            costPerDevice[c] = costPerDevice[c] + cost;
         }
         System.out.println("Cost for device " + device + " is " + costPerDevice[c]);
         System.out.println("Cost for producing the device " + device + " is " + (costPerDevice[c] * 1.1));
         device++;
      }
   }

}