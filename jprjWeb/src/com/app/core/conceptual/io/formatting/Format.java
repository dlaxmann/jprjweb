package com.app.core.conceptual.io.formatting;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import net.sf.JRecord.Common.RecordException;

public interface Format {

	public void init(String outputFile) throws IOException;
	public List<byte[]> write(String layout, Object object) throws IOException, 
			IllegalAccessException, InvocationTargetException, NoSuchMethodException, RecordException;
	public void close() throws IOException;
}
