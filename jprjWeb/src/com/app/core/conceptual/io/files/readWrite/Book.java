package com.app.core.conceptual.io.files.readWrite;

import java.io.Serializable;

public class Book implements Serializable {

	private String isbn;
    private String title;
    private String author;
 
    public Book(String isbn, String title, String author) {
        this.isbn = isbn;
        this.title = title;
        this.author = author;
    }
 
    public String toString() {
        return "[Book: " + isbn + ", " + title + ", " + author + "]";
    }
}
