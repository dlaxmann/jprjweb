package com.app.core.conceptual.io.files.readWrite;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.app.core.app.bean.Student;

public class FlatFileToExcel {
 
	public static void main(String[] args) {
        try {
                /*Class.forName("com.mysql.jdbc.Driver").newInstance();
                Connection connection = DriverManager.getConnection(
                                "jdbc:mysql://localhost:3306/test", "root", "root");
                PreparedStatement psmnt = null;
                Statement st = connection.createStatement();
                ResultSet rs = st.executeQuery("Select * from student");*/

                
                List<Student> li = new ArrayList<Student>();
                
                for (int i = 0; i < 30; i++) {
                	Student std = new Student();
                	std.setRollNo("rollNo"+i);
                	std.setName("name"+i);
                	std.setMarks("marks"+i);
                	std.setGrade("grade"+i);
                	std.setClasss("classs"+i);
                	li.add(std);
				}
                
                
                
                HSSFWorkbook wb = new HSSFWorkbook();
                HSSFSheet sheet = wb.createSheet("Excel Sheet");
                HSSFRow rowhead = sheet.createRow((short) 0);
                rowhead.createCell((short) 0).setCellValue("Roll No");
                rowhead.createCell((short) 1).setCellValue("Name");
                rowhead.createCell((short) 2).setCellValue("Class");
                rowhead.createCell((short) 3).setCellValue("Markssssssssss");
                rowhead.createCell((short) 4).setCellValue("Grade.....");

                int index = 1;
               
                System.out.println("list size: "+li.size());
               for (Student stt : li) {
				
			
              //  while (rs.next()) {

                        HSSFRow row = sheet.createRow((short) index);
                        row.createCell((short) 0).setCellValue(stt.getRollNo());
                        row.createCell((short) 1).setCellValue(stt.getName());
                        row.createCell((short) 2).setCellValue(stt.getMarks());
                        row.createCell((short) 3).setCellValue(stt.getGrade());
                        row.createCell((short) 4).setCellValue(stt.getClasss());
                        index++;
                }
                FileOutputStream fileOut = new FileOutputStream("files/R1.xls");
                wb.write(fileOut);
                fileOut.close();
                System.out.println("done...");
        } catch (Exception e) {
        }
}
}
