package com.app.core.conceptual.io.files.xml;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.StringTokenizer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.xerces.parsers.DOMParser;
import org.w3c.dom.Attr;
import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.Attributes;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


/**
 * 
 Get a document builder using document builder factory and parse the xml file
 to create a DOM object
 Get a list of employee elements from the DOM
 For each employee element get the id, name, age and type. Create an employee
 value object and add it to the list. At the end iterate through the list and
 print the employees to verify we parsed it right.
 * 
 */
public class ParsingXML {

	
	
	//dom parsing
	public static void domParsing(){
		try {
			File fl = new File("files/parsing.xml");
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder dbl = dbf.newDocumentBuilder();
			Document dc = (Document) dbl.parse(fl);
			dc.getDocumentElement().normalize();
System.out.println(
			dc.getAttributes()+
		/*	dc.getBaseURI()+
			dc.getDocumentURI()+*/
			dc.getLocalName()
			
);
			/*System.out.println("Root El: "
					+ dc.getDocumentElement().getNodeName());
			NodeList nl = dc.getElementsByTagName("staff");
			for (int tmp = 0; tmp < nl.getLength(); tmp++) {
				Node ndd = nl.item(tmp);
				System.out.println("ndd " + ndd);

			}*/
			/*System.out.println(dc.getChildNodes());
			System.out.println(dc.getFirstChild());
			System.out.println(dc.getLastChild());
			System.out.println(dc.getXmlVersion());*/

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	public static void saxParsing(){
		try {
			File fl = new File("files/parsing.xml");
			SAXParserFactory spf = SAXParserFactory.newInstance();
			SAXParser saxp = spf.newSAXParser();
		//	saxp.parse(fl);
			/*public void startElement(String uri, String localName, String qName,Attributes att){
				
			}*/
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		}
	}
	
	
	//SAX Parser
	public static void saxparsing1(){
		 try {
				SAXParserFactory factory = SAXParserFactory.newInstance();
				SAXParser saxParser = factory.newSAXParser();

				DefaultHandler handler = new DefaultHandler() {
				boolean bfname = false;
				boolean blname = false;
				boolean bnname = false;
				boolean bsalary = false;

				public void startElement(String uri, String localName,
											String qName, Attributes attributes)
						throws SAXException {
					
					System.out.println("Start Element :" + qName);
					if (qName.equalsIgnoreCase("FIRSTNAME")) {
						bfname = true;
					}
					if (qName.equalsIgnoreCase("LASTNAME")) {
						blname = true;
					}
					if (qName.equalsIgnoreCase("NICKNAME")) {
						bnname = true;
					}
					if (qName.equalsIgnoreCase("SALARY")) {
						bsalary = true;
					}
				}
				public void endElement(String uri, String localName,
					String qName) throws SAXException {
					System.out.println("End Element :" + qName);
				}
				public void characters(char ch[], int start, int length)
						throws SAXException {
					if (bfname) {
						System.out.println("First Name : "
								+ new String(ch, start, length));
						bfname = false;
					}
					if (blname) {
						System.out.println("Last Name : "
								+ new String(ch, start, length));
						blname = false;
					}
					if (bnname) {
						System.out.println("Nick Name : "
								+ new String(ch, start, length));
						bnname = false;
					}
					if (bsalary) {
						System.out.println("Salary : "
								+ new String(ch, start, length));
						bsalary = false;
					}
				}
			     };
			       saxParser.parse("files/parsing.xml", handler);
			     } catch (Exception e) {
			       e.printStackTrace();
			     }
	}
	
	public static void saxParsing2(){
		
	}
	
	public static void getAllElementsInDomDoc() throws SAXException, IOException, ParserConfigurationException{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	    factory.setValidating(true);
	    factory.setExpandEntityReferences(false);
	    Document doc = factory.newDocumentBuilder().parse(new File("filename"));
	    NodeList list = doc.getElementsByTagName("*");
	    for (int i = 0; i < list.getLength(); i++) {
	      Element element = (Element) list.item(i);
	    }
	}
	
	
	
	 public static class NullResolver implements EntityResolver {
		    public InputSource resolveEntity (String publicId, String systemId)
		        throws SAXException, IOException    {
		        return new InputSource(new StringReader(""));
		    }
		}

		  /** Read XML as DOM.
		   */
		  public static Document readXml(InputStream is)
		      throws SAXException, IOException, ParserConfigurationException {
		      DocumentBuilderFactory dbf =
		          DocumentBuilderFactory.newInstance();

		      dbf.setValidating(false);
		      dbf.setIgnoringComments(false);
		      dbf.setIgnoringElementContentWhitespace(true);
		      //dbf.setCoalescing(true);
		      //dbf.setExpandEntityReferences(true);

		      DocumentBuilder db = null;
		      db = dbf.newDocumentBuilder();
		      db.setEntityResolver( new NullResolver() );

		      // db.setErrorHandler( new MyErrorHandler());

		      Document doc = db.parse(is);
		      return doc;
		  }
	
		  
public static void domTesting(){
	 try {
	      DOMParser p = new DOMParser();
	      //File fl = new File("files/myfile.xml");
	      String str = "files/myfile.xml";
	      p.parse(str);
	      Document doc = p.getDocument();
	      Node n = doc.getDocumentElement().getFirstChild();
	      while (n!=null && !n.getNodeName().equals("recipe")) 
	        n = n.getNextSibling();
	      PrintStream out = System.out;
	      out.println("<?xml version=\"1.0\"?>");
	      out.println("<collection>");
	      if (n!=null)
	        print(n, out);
	      out.println("</collection>");
	    } catch (Exception e) {e.printStackTrace();}
	  }

	  static void print(Node node, PrintStream out) {
	    int type = node.getNodeType();
	    switch (type) {
	      case Node.ELEMENT_NODE:
	        out.print("<" + node.getNodeName());
	        NamedNodeMap attrs = node.getAttributes();
	        int len = attrs.getLength();
	        for (int i=0; i<len; i++) {
	            Attr attr = (Attr)attrs.item(i);
	            out.print(" " + attr.getNodeName() + "=\"" +
	                      escapeXML(attr.getNodeValue()) + "\"");
	        }
	        out.print('>');
	        NodeList children = node.getChildNodes();
	        len = children.getLength();
	        for (int i=0; i<len; i++)
	          print(children.item(i), out);
	        out.print("</" + node.getNodeName() + ">");
	        break;
	      case Node.ENTITY_REFERENCE_NODE:
	        out.print("&" + node.getNodeName() + ";");
	        break;
	      case Node.CDATA_SECTION_NODE:
	        out.print("<![CDATA[" + node.getNodeValue() + "]]>");
	        break;
	      case Node.TEXT_NODE:
	        out.print(escapeXML(node.getNodeValue()));
	        break;
	      case Node.PROCESSING_INSTRUCTION_NODE:
	        out.print("<?" + node.getNodeName());
	        String data = node.getNodeValue();
	        if (data!=null && data.length()>0)
	           out.print(" " + data);
	        out.println("?>");
	        break;
	    }
	  }

	  static String escapeXML(String s) {
	    StringBuffer str = new StringBuffer();
	    int len = (s != null) ? s.length() : 0;
	    for (int i=0; i<len; i++) {
	       char ch = s.charAt(i);
	       switch (ch) {
	       case '<': str.append("&lt;"); break;
	       case '>': str.append("&gt;"); break;
	       case '&': str.append("&amp;"); break;
	       case '"': str.append("&quot;"); break;
	       case '\'': str.append("&apos;"); break;
	       default: str.append(ch);
	     }
	    }
	    return str.toString();
	  }

		  public static void createXMLUsingDOM(){
			  DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
			    DocumentBuilder domBuilder = null;
				try {
					domBuilder = domFactory.newDocumentBuilder();
					 Document newDoc = domBuilder.newDocument();
					    Element rootElement = newDoc.createElement("CSV2XML");
					    newDoc.appendChild(rootElement);

					    BufferedReader csvReader = new BufferedReader(new FileReader("files/WriteTextAppend.txt"));
					    String curLine = csvReader.readLine();
					    String[] csvFields = curLine.split(",");
					    Element rowElement = newDoc.createElement("row");
					    for(String value: csvFields){
					    	System.out.println(value);
					      Element curElement = newDoc.createElement(value);
					      curElement.appendChild(newDoc.createTextNode(value));
					      rowElement.appendChild(curElement);
					      rootElement.appendChild(rowElement);
					    }
					    csvReader.close();
					    TransformerFactory tranFactory = TransformerFactory.newInstance();
					    Transformer aTransformer = tranFactory.newTransformer();
					    Source src = new DOMSource(newDoc);
					    Result dest = new StreamResult(new File("xmlFileName"));
					    aTransformer.transform(src, dest);
				} catch (ParserConfigurationException e) {
					e.printStackTrace();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (TransformerConfigurationException e) {
					e.printStackTrace();
				} catch (TransformerException e) {
					e.printStackTrace();
				}

			   
			    
		  }
		  
	public static void csvToXml(){
		DocumentBuilderFactory dbf = null;
		DocumentBuilder docBuilder = null;
		try{
		dbf = DocumentBuilderFactory.newInstance();
		docBuilder = dbf.newDocumentBuilder();
		
		
		}catch (Exception e) {

		}
		
		
	}
	
	public static int convertFile(){
		/*File fl = new File("files/my.csv");
		String csvFileName,String xmlFileName
		"files/my.csv", "files/my.xml"*/
		String csvFileName = null;
		String xmlFileName;
		DocumentBuilderFactory dbf = null;
		DocumentBuilder docBuilder = null;
		try{
		dbf = DocumentBuilderFactory.newInstance();
		docBuilder = dbf.newDocumentBuilder();
		
		int rowCount = -1;
		//Root Element
		Document doc = docBuilder.newDocument();
		Element rootEl = doc.createElement("ROOT");
		doc.appendChild(rootEl);
		//Read comma seperated CSV File
		BufferedReader csvReader;
		csvReader = new BufferedReader(new FileReader(csvFileName));
		int fieldCount = 0;
		String[] csvFields = null;
		StringTokenizer token = null;
		
		// Assumption: first line in CSV file is column/field names 
        // As the column names are used to name the elements in the XML file, 
        // avoid using spaces/any other characters not suitable for XML element naming 
		String currentLine = csvReader.readLine();
		if(currentLine != null){
			token = new StringTokenizer(currentLine,",");
			fieldCount = token.countTokens();
			if(fieldCount >0){
				csvFields = new String[fieldCount];
				int i = 0;
				while(token.hasMoreElements()){
					csvFields[i++]=String.valueOf(token.nextElement());
				}
			}
		}
		
		//now we know columns, now read data row lines
		while((currentLine = csvReader.readLine()) != null){
			token = new StringTokenizer(currentLine,",");
			fieldCount =  token.countTokens();
			if(fieldCount > 0){
				Element rowElemnt = doc.createElement("row");
				int i = 0;
				while(token.hasMoreElements()){
				try{
				String currentValue = String.valueOf(token.nextElement());
				Element el  = doc.createElement(csvFields[i++]);
				el.appendChild(doc.createTextNode(currentValue));
				rowElemnt.appendChild(el);
				} catch (Exception e) {
				}
			}rootEl.appendChild(rowElemnt); 
				rowCount++; 
			}
		}
		} catch(Exception e){
		}
		return 0;
	}
	
	
	
	public static void CreateDomXml(){
	        try {
	        	//to create document to put DOM tree.
	            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	            DocumentBuilder db = dbf.newDocumentBuilder();
	            Document doc = db.newDocument();

	            //to create root element email
	            Element root = doc.createElement("email");
	            doc.appendChild(root);

	            //to create comment
	            Comment comment = doc.createComment("Sample XML format of Email ");
	            root.appendChild(comment);

	            Element subject = doc.createElement("subject");
	            root.appendChild(subject);

	            Text subText = doc.createTextNode("Message from TechLabs");
	            subject.appendChild(subText);

	            //to create child element from and text node of that element
	            Element from = doc.createElement("from");
	            root.appendChild(from);
	            Text fromText = doc.createTextNode("j2eeforum@rediffmail.com");
	            from.appendChild(fromText);

	            Element fromdisplay = doc.createElement("fromdisplay");
	            root.appendChild(fromdisplay);
	           Text fromdisplayText = doc.createTextNode("TechLabs");
	            fromdisplay.appendChild(fromdisplayText);

	            Element receipients = doc.createElement("receipients");
	            root.appendChild(receipients);
	           
	          Element receipient = doc.createElement("receipient");
	            receipients.appendChild(receipient);
	           receipient.setAttribute("role", "to"); // to create attribute with value

	           Element email_id = doc.createElement("email-id");
	            receipient.appendChild(email_id);
	            Text emailidText= doc.createTextNode("vkj@xyz.com");
	            email_id.appendChild(emailidText);

	           Element disp = doc.createElement("display");
	            receipient.appendChild(disp);
	            Text dispText= doc.createTextNode("VKJ");
	            disp.appendChild(dispText);

	           Element bodytext = doc.createElement("bodytext");
	            root.appendChild(bodytext);
	           Text bodytextText = doc.createTextNode("Hope this URL will help you to learn about  how to create XML using DOM.");
	            bodytext.appendChild(bodytextText);

	          Element  sign= doc.createElement("signature");
	            root.appendChild(sign);
	           Text signText = doc.createTextNode("Administrator");
	            sign.appendChild(signText);

	            //to create transformer
	            TransformerFactory tf = TransformerFactory.newInstance();
	            Transformer transformer = tf.newTransformer();
	            transformer.setOutputProperty(OutputKeys.ENCODING, "utf-8");  

	            //to include the  xml declaration line  <?xml version="1.0" encoding="utf-8" standalone="no"?>
	             transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");

	             //for indent of XML
	            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
	             transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "3");

	             // to write into a file
	          // String fileName= "D://as2/jf6/email1.xml";  
	          // StreamResult result = new StreamResult(new File(fileName));

	            DOMSource source = new DOMSource(doc);

	            //to write as  a string 
	           StringWriter sw = new StringWriter();
	           StreamResult result = new StreamResult(sw);
	            transformer.transform(source, result);
	            //System.out.println("XML file saved " + fileName);

	           String xmlString = sw.toString();
	            System.out.println("Email XML \n\n " + xmlString);

	        } catch (Exception e) {
	            System.out.println(e);
	        }
	}
	
	public static void getAllDomElements() {
			 DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			 factory.setValidating(true);
			 factory.setExpandEntityReferences(false);
		     Document doc = null;
			try {
				doc = factory.newDocumentBuilder().parse(new File("files/one.xml"));
			} catch (SAXException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ParserConfigurationException e) {
				e.printStackTrace();
			}
		    NodeList list = doc.getElementsByTagName("*");
		    for (int i = 0; i < list.getLength(); i++) {
		      Element element = (Element) list.item(i);
		    }
	}
	public static void main(String args[]) throws FileNotFoundException {

		//saxparsing1();
		//domTesting();
		//createXMLUsingDOM();
		//csvToXml();
		//convertFile();
		//CreateDomXml();
		//getAllDomElements();
		domParsing();
	}
}
