package com.app.core.conceptual.io.files.readWrite;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.Writer;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;

import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.mysql.jdbc.DatabaseMetaData;

/**
 * 1. get file location 2. Using BufferReader read the file 3. read all lines of
 * file, line by line append to String buffer, add line seperator at the end
 * after adding each line 4.convert StringBuffer to String
 * 
 */
public class ReadWriteFiles {

		public static void readwriteSomeTextFromFile() {
			File file = new File("files/ReadText.txt");
			File wrtFile = new File("files/writeText.txt");
			StringBuffer contents = new StringBuffer();
			BufferedReader reader = null;
			BufferedWriter writer = null;
			try {
				reader = new BufferedReader(new FileReader(file));
				writer = new BufferedWriter(new FileWriter(wrtFile));
				String text = null;
				// repeat until all lines is read
				while ((text = reader.readLine()) != null) {
					// add all lines to StringBuffer
					contents.append(text).append(
							System.getProperty("line.separator"));
					
				}
				writer.write(contents.toString());
				writer.flush();
				writer.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					if (reader != null) {
						reader.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			// show file contents here
			
			System.out.println(contents.toString());
		}

		public static void writeSomeTextToFile() {
			Writer writer = null;
			try {
				// the text to be written
				String text = "some text";
				// get file path
				File file = new File("files/WriteText.txt");
				// use BufferedWriter to write
				writer = new BufferedWriter(new FileWriter(file));
				writer.write(text);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					if (writer != null) {
						writer.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		public static void readPositionsOfFile(){
			BufferedReader rdr = null;
			BufferedWriter wtr = null;
			String strPsitions = "";
			String lineString = "";
			StringBuffer sbf = new StringBuffer();
			try {
				File fil = new File("files/ReadText.txt");
				rdr = new BufferedReader(new FileReader(fil));
				while ((strPsitions = rdr.readLine()) != null) {
					lineString = strPsitions.substring(10, 20);
					System.out.println("....." + lineString);
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		public static void readLineNumbers() {
			File file = null;
			FileReader fr = null;
			LineNumberReader lnr = null;
			try {
				// file = new File("student.csv");
				file = new File("files/ReadText.txt");
				try {
					fr = new FileReader(file);
					lnr = new LineNumberReader(fr);
					String line = "";
					while ((line = lnr.readLine()) != null) {
						System.out.println("Line Number " + lnr.getLineNumber()+ ": " + line);
					}
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} finally {
				if (fr != null) {
					try {
						fr.close();
						if (lnr != null) {
							lnr.close();
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
		
		public static void readFlatFiles(){
			File file = new File("files/ReadText.txt");
	        StringBuffer contents = new StringBuffer();
	        BufferedReader reader = null;
	        BufferedWriter writer = null;
	        try {
	        	//writer = new BufferedWriter(out);
	            reader = new BufferedReader(new FileReader(file));
	            String text = null;
	            // repeat until all lines is read
	            while ((text = reader.readLine()) != null) {
	            	//add all lines to StringBuffer
	                contents.append(text)
	                        .append(System.getProperty("line.separator"));
	            }
	        } catch (FileNotFoundException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	        } finally {
	            try {
	                if (reader != null) {
	                    reader.close();
	                }
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	        }
	        System.out.println(contents.toString());
		}
		
		public static void readFileByteArray(){
			File fl = new File("files/items.txt");
			 byte[] fileData = null;
			try {
				FileInputStream fs = new FileInputStream(fl);
				 fileData = new byte[(int) (fl.length())];
					fs.read(fileData);
					String str = new String(fileData);
					System.out.println(str);
				} catch (IOException e) {
					e.printStackTrace();
			}
		}
		
		public static void propertiesToExcel(){
			Properties properties = new Properties();
	        properties.setProperty("database.type", "mysql");
	        properties.setProperty("database.url", "jdbc:mysql://localhost/mydb");
	        properties.setProperty("database.username", "root");
	        properties.setProperty("database.password", "root");
	        FileOutputStream fos;
			try {
				fos = new FileOutputStream("files/database-configuration.xls");
				 properties.storeToXML(fos, "Database Configuration", "UTF-8");
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		public static void chekFileProperties(){
			try{
			File file = new File("files/Writable.txt");
	        // Create a file only if it doesn't exist.
	        file.createNewFile();
	        // Set file attribute to read only so that it cannot be written
	        file.setReadOnly();
	        // We are using the canWrite() method to check whether we can
	        // modified file content.
	        if (file.canWrite()) {
	            System.out.println("File is writable!");
	        } else {
	            System.out.println("File is in read only mode!");
	        }
	        // Now make our file writable
	       // file.setWritable(true);
	        // re-check the read-write status of file 
	        if (file.canWrite()) {
	            System.out.println("File is writable!");
	        } else {
	            System.out.println("File is in read only mode!");
	        }
			}catch (Exception e) {
			}
		}

		public static void readPropertiesFile(){
			Properties prop = new Properties();
	    	try {
	            //load a properties file
	    		prop.load(new FileInputStream("files/MyProps.properties"));
	               //get the property value and print it out
	                System.out.println(prop.getProperty("database"));
	    		System.out.println(prop.getProperty("dbuser"));
	    		System.out.println(prop.getProperty("dbpassword"));
	    	} catch (IOException ex) {
	    		ex.printStackTrace();
	        }
		}
		
		public static void writePropertiesFile(){
			Properties prop = new Properties();
	    	try {
	    		//set the properties value
	    		prop.setProperty("database", "localhost");
	    		prop.setProperty("dbuser", "mkyong");
	    		prop.setProperty("dbpassword", "password");
	    		//save properties to project root folder
	    		prop.store(new FileOutputStream("files/MyProps.properties"), null);
	    	} catch (IOException ex) {
	    		ex.printStackTrace();
	        }
		}
		
		
		public static void readFileWriteSelectedToFile(){
			
			File dir = new File("D:/emails");
			File flToWrte = new File("files/fileUtilWrt.txt"); 
			String fileName = "files/tables.xls";
			
			BufferedReader reder = null;
			StringBuffer sbf = new StringBuffer();
			
			try {
				System.out.println("Getting all files in " + dir.getCanonicalPath() + " including those in subdirectories");
				List<File> files = (List<File>) FileUtils.listFiles(dir, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
				int cont=0;
				String contnt = null;
				Writer wrtr = new BufferedWriter(new FileWriter(flToWrte));
				
				HSSFWorkbook wrkBook = new HSSFWorkbook();
				HSSFSheet wrkSheet = wrkBook.createSheet("Tables");
				HSSFRow myRow;
	            FileOutputStream out = new FileOutputStream(new File(fileName));
	            int rowNum = 1;
				for (File file : files) {
					cont++;
					
					reder = new BufferedReader(new FileReader(file));
					
						while((contnt = reder.readLine()) != null){
							if(contnt.contains("table=")){
								
								String stt = StringUtils.substringAfter(contnt, "\"");
								String str11 = StringUtils.substringAfter(stt, "\"");
								
								String str12 = StringUtils.substringAfter(str11, "\"");
								
								String str13 = StringUtils.substringBefore(str12, "\"");
								System.out.println(str13+"_"+rowNum);
								
								myRow = wrkSheet.createRow(0);
								myRow.createCell(0).setCellValue("Table Name");
							
								myRow = wrkSheet.createRow(rowNum);
								myRow.createCell(0).setCellValue(str13);
								
								rowNum ++;
							}
						}
				           
				}
				
				for(int i=0; i<rowNum; i++){
					wrkSheet.autoSizeColumn((short)i);
				}
				
				 wrkBook.write(out);
				 out.flush();
				
		            out.close();
		            System.out.println("...done rowNum"+rowNum);
				
				wrtr.write(sbf.toString());
				wrtr.flush();
				wrtr.close();
				
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
		}
		
		
		private static void createTblStructure(){
			  try{
				  Connection con = getConnection();
				  Statement st = con.createStatement();
			 
				  // select * insert into emp from employee
			  /*int rows = st.executeUpdate("INSERT INTO Copyemployee SELECT * FROM employee");
			    if (rows == 0) {
			      System.out.println("Don't add any row!");
			    } else {
			      System.out.println(rows + " row(s)affected.");
			     // con.close();
			    }*/
			  
			  
			  ResultSet rs = st.executeQuery("SELECT * FROM employee");
			  ResultSetMetaData md = rs.getMetaData();
			  java.sql.DatabaseMetaData dbm = con.getMetaData();
			  ResultSet rs1 = dbm.getPrimaryKeys("", "dbone", "employee");
			  ResultSetMetaData md1 = rs1.getMetaData();
			  
			  System.out.println(md1.getSchemaName(1));
			  System.out.println(dbm.getPrimaryKeys("", "dbone", "employee"));
			  
			  int col = md.getColumnCount();
			  System.out.println("Number of Column : "+ col);
			  System.out.println("create table "+md.getTableName(col)+" (");
			  //System.out.println("Columns Name: ");
			  for (int i = 1; i <= col; i++){
			 
			  String col_name = md.getColumnName(i);
			  String colType = md.getColumnTypeName(i);
			  int s = md.getPrecision(i);
			  System.out.println(col_name+" "+colType+"("+s+"),");
			  
			  String cLbl = md.getColumnLabel(i);
			  String cTpNm = md.getCatalogName(i);
			  
			  int k = md.getColumnDisplaySize(i);
			  System.out.println(cLbl+"_"+cTpNm+"_"+k);
			  }
			  System.out.println(")");
			  }
			  catch (SQLException s){
			  System.out.println("SQL statement is not executed!");
			  }
	}
		
		
		private static Connection getConnection(){
			  Connection con = null;
			  String url = "jdbc:mysql://localhost:3306/";
			  //DriverManager.getConnection("jdbc:oracle:thin:@//localhost:1521/xe", "SYSTEM", "ldevasani");
			  String db = "dbone";
			  String driver = "com.mysql.jdbc.Driver";
			  String user = "root";
			  String pass = "ldevasani";
			  try{
			  Class.forName(driver);
			  con = DriverManager.getConnection(url+db, user, pass);
			  }catch(SQLException e){
			  } catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			  return con;
		}
		
		
		public static void readFlatFilesPrepareEmailContent(){
			File file = new File("files/emailcontent.txt");
	        StringBuffer contents = new StringBuffer();
	        BufferedReader reader = null;
	        BufferedWriter writer = null;
	        try {
	        	//writer = new BufferedWriter(out);
	            reader = new BufferedReader(new FileReader(file));
	            String text = null;
	            // repeat until all lines is read
	            while ((text = reader.readLine()) != null) {
	            	//add all lines to StringBuffer
	            	System.out.println(text);
	                contents.append(text)
	                        .append(System.getProperty("line.separator"));
	            }
	        } catch (FileNotFoundException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	        } finally {
	            try {
	                if (reader != null) {
	                    reader.close();
	                }
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	        }
	        System.out.println(contents.toString());
		}
		
		
		public static void buildEmailContent() {
			File file = new File("files/emailcontent.txt");
			File wrtFile = new File("files/writeText.txt");
			StringBuffer contents = new StringBuffer();
			BufferedReader reader = null;
			BufferedWriter writer = null;
			Map<String, String> map = new HashedMap();
			try {
				reader = new BufferedReader(new FileReader(file));
				writer = new BufferedWriter(new FileWriter(wrtFile));
				String text = null;
				int i=1;
				
				while ((text = reader.readLine()) != null) {
					if(StringUtils.isNotEmpty(text))
						text = text.replaceAll("\"", Matcher.quoteReplacement("\\\"")).trim();
					    System.out.println("sbf.append(\""+text+"\");");
					    contents.append(text).append(System.getProperty("line.separator"));
					    map.put(text, text);
				}
				writer.write(contents.toString());
				writer.flush();
				writer.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					if (reader != null) {
						reader.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			// show file contents here
			for (Iterator iterator = map.keySet().iterator(); iterator.hasNext();) {
				String  str = (String) iterator.next();
				//System.out.println(str);
				//urlsList.add(e)
			//	if(str.length()>0)
					//System.out.println(str.length());
					//str = str.replaceAll("(?m)^\\s*$[\n\r]{1,}", "");
				//	System.out.println(str);
				//System.out.println("urlsList.add(\""+str+"\");");
			}
			//System.out.println(contents.toString());
		}


		
	public static void main(String[] args) {

		//readFileWriteSelectedToFile();
		
		//writeSomeTextToFile();
		//readwriteSomeTextFromFile();
		//readwriteSomeTextFromFile();
		
		//createTblStructure();
		//readFileByteArray();
		//readFlatFilesPrepareEmailContent();
		buildEmailContent();
	}

}











/*import java.io.*;
public class Test {
    public static void main(String[] args) {
        try {
            StringBuffer data = new StringBuffer("test data in StringBuffer");
            // write it to file
            writeToFile("test.txt",data);
            // read it back from file
            data = readFromFile("test.txt");
            System.out.println("data = " + data.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void writeToFile(String pFilename, StringBuffer pData) throws IOException {
        BufferedWriter out = new BufferedWriter(new FileWriter(pFilename));
        out.write(pData.toString());
        out.flush();
        out.close();
    }
    public static StringBuffer readFromFile(String pFilename) throws IOException {
        BufferedReader in = new BufferedReader(new FileReader(pFilename));
        StringBuffer data = new StringBuffer();
        int c = 0;
        while ((c = in.read()) != -1) {
            data.append((char)c);
        }
        in.close();
        return data;
    }
}
*/