package com.app.core.conceptual.io.files.pdf;
/*
 * **************************************************************************<BR>
 * This program contains trade secrets and confidential information which<BR>
 * are proprietary to CSC Financial Services Group�.  The use,<BR>
 * reproduction, distribution or disclosure of this program, in whole or in<BR>
 * part, without the express written permission of CSC Financial Services<BR>
 * Group is prohibited.  This program is also an unpublished work protected<BR>
 * under the copyright laws of the United States of America and other<BR>
 * countries.  If this program becomes published, the following notice shall<BR>
 * apply:
 *     Property of Computer Sciences Corporation.<BR>
 *     Confidential. Not for publication.<BR>
 *     Copyright (c) 2005-2007 Computer Sciences Corporation. All Rights Reserved.<BR>
 * **************************************************************************<BR>
 

package com.frame.pdf.nbp;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;
import java.util.PropertyResourceBundle;

import org.apache.log4j.Logger;
import org.apache.log4j.spi.LocationInfo;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.csc.fs.vpms.VPMSAttributesCollection;
import com.csc.fs.vpms.dom.VDOMDocument;
import com.csc.fs.vpms.dom.VDOMException;


*//**
 * <p>
 *  Generates the PDF 
 * <p>
 * <b>Modifications:</b><br>
 * <table border=0 cellspacing=5 cellpadding=5>
 * <thead>
 * <th align=left>Task</th><th align=left>Release</th><th align=left>Description</th>
 * </thead>
 * <tr><td>PDFUtility</td><td>Version 3</td><td>PDF Utiity to generate the PDF </td></tr>
 * </table>
 * <p>
 * @author CSC FSG Developer
 * @since 3.0.0
 *//*
public class PDFGenerator{
    
    //Constant
    public static String ACTION_PDF_LINK = "pdflinkspage";
    
    
	public static final String PROPERTY_FILE = "pdf";
	public static PropertyResourceBundle bundle = (PropertyResourceBundle)PropertyResourceBundle.getBundle(PROPERTY_FILE);		

	//input parameters
    public static final String inputDataFolder = bundle.getString("inputDataFolder");
    public static final String inputPDFFolder = bundle.getString("inputPDFFolder");
    public static final String outputPDFFolder = bundle.getString("outputPDFFolder");
	
	
	static {
	    init();
	}
	
	private static Logger log = Logger.getLogger(PDFGenerator.class.getName());
	
	private static void  init(){
	    Properties p = System.getProperties();
	    String logFile = bundle.getString("logDir");
		File file = new File(logFile);
		if(!file.exists()){
		    file.mkdirs();
		} 
	    
		p.put("logFile", logFile);	  
		
		//PDF writer API that should be used
		System.setProperty(CSCConstants.SYSTEM_PDFWRITER , "itext");
		
		//Signature capture API that should be used		
		System.setProperty(CSCConstants.SYSTEM_SIG_CAPTURE , "integrisign");
		
	}
    
    *//**
     * Validates the input parameters and calls processPDF()
     * to generate the PDF forms.
     *//*
    private static void generatePDF() {
        File file = new File(inputDataFolder);
        if(!file.exists() || !file.isDirectory()){
            String err = "Input XML directory not found at location " + inputDataFolder;
            log.error(err);
            throw new RuntimeException(err);
        }
        
        file = new File(inputPDFFolder);
        if(!file.exists() || !file.isDirectory()){
            String err = "Input PDF directory not found at location " + inputPDFFolder;
            log.error(err);
            throw new RuntimeException(err);
        }
        
        //Create output folder
        file = new File(outputPDFFolder);
        if(!file.exists()){
            file.mkdirs();
        }
        
        log.info("");
        log.info("");
        log.info("");
        log.info("************************************");
        log.info("Input XML folder " + inputDataFolder);
        log.info("Input PDF folder  " + inputPDFFolder);
        log.info("Output PDF folder  " + outputPDFFolder);
        log.info("************************************");
        
        processPDF();
    }
    

    *//**
     *  Iterates all the input scenario XMLs and generates PDF forms
     *  for each scenario in different sub-directory
     *
     *//*
    private static void processPDF() {
       
        //iterate PDFs
        File file = new File(inputDataFolder);
        File[] files = file.listFiles();
        for (int i = 0; i < files.length; i++) {
            File xmlFile = files[i];
            if(validXML(xmlFile)){
                //target directory where PDF forms would be generated
                String targetDir = new StringBuffer(outputPDFFolder).append("/").append(xmlFile.getName()).toString();
                log.info("");
                log.info("");
                log.info("Processing " + xmlFile.getName() + "...");
                log.info("Output directory " + targetDir);
                
                //Create attribute collection
                VPMSAttributesCollection attrs = getAttributeCollection(xmlFile.getAbsolutePath());
                
                //Generate PDf form
                generatePDFForms(attrs, targetDir);
            }
        }//end for        
    }
    
    
    *//**
     * Generates PDF forms in the <code>outputDir</code> directory
     * @param attrs VPMS attribute collection
     * @param outputDir directory where PDf forms would be generated
     *//*
    public static void generatePDFForms(VPMSAttributesCollection attrs, String outputDir) {
        String repeatNodes[] = PDFHelper.getRepeatNodes(attrs);
        PDFProperties pdfProp = new PDFProperties();

        //Get the list of pdf forms associated with the application
        ArrayList pdfIdList = PDFHelper.getForms(attrs); //econ32
        int size = pdfIdList.size();
        
        if(size == 0){
            log.info("There is no PDF required to be generated.");
        }
        
	    for (int i = 0; i < size; i++) {
	         String id = (String) pdfIdList.get(i);
	         String destPath = new StringBuffer(outputDir).append("/").append(id).append(".pdf").toString();
	         log.info("");
	         log.info("**Generating " + id + ".pdf ...");
	       
	         //generate the PDF
	         byte[] bytes = generatePDF(attrs,repeatNodes, id);
	         
	         if(bytes != null){
	             //write PDf form to fileSystem
	             PDFHelper.writeFile(destPath, bytes);
	         }
	    }//end for
    }    
    

    *//**
     * Generates the PDF form
     * @param attrs VPMS attribute collection
     * @param repeatNodes repeatnodes in the application
     * @param pdfName Name of PDF
     *//*
    public static byte[] generatePDF(VPMSAttributesCollection attrs, String[] repeatNodes, String pdfName) {
        PDFProperties pdfProp = new  PDFProperties();

        try {
            PDFRepeatInfo repeatInfo = PDFHelper.getFormRepeatDetails(pdfName, repeatNodes);

            //get actual PDF name
            LocationInfo locInfo = getPDFLocation(repeatInfo.getActualFormName());
            if (locInfo.getPdfLocation() == null) {
                return null;
            }

            //Create PDFProperties to be processes by PDF Processor
            pdfProp.setPath(locInfo.getPdfLocation());
            pdfProp.setName(pdfName);
            
            //set dummy values
            pdfProp.setAppId("appId");
            pdfProp.setSno("1");

            if (repeatInfo.isPartySpecific()) {
                pdfProp.setParty(repeatInfo.getParty());
            }

            pdfProp.setShowDraftLabel(false);

            PDFProcessor pdfProcessor = new PDFProcessor(attrs, new HashMap());
            byte[] bytes = pdfProcessor.process(pdfProp, locInfo.getMetaXMLLocation());
            
            return bytes;
        } 
        catch (Exception ex) {
            log.error("Error while generating PDf form -" + pdfName, ex);
        } 
        
        return null;
    }//func end



    
    *//**
     * Creates the VPMS attribute collection from the XML
     * @param path path of xml
     *//*
    protected static VPMSAttributesCollection getAttributeCollection(String path) {
        VPMSAttributesCollection attrs = new VPMSAttributesCollection();
        Document doc = null;
        try {
            doc = VDOMDocument.getDOM(VDOMDocument.vFILE, path);
        }
        catch (VDOMException e) {
            log.error("Error while reading XML file " + path, e);
            return null;
        }
        
        //Iterate through elements and create attributes
        Element root = doc.getDocumentElement();
        NodeList childList = root.getChildNodes();
        int len = childList.getLength();
        for (int i = 0; i < len; i++) {
            Node currentNode = childList.item(i);
            if(currentNode.getNodeName().equals("arg")){
                Element element = (Element)currentNode;
                String name = element.getAttribute("name");
                
				NodeList valueList = element.getElementsByTagName("value");
				String nodeValue = StringFormat.getNodeValue(valueList.item(0));
				attrs.setAttribute(name, nodeValue);
            }
        }//end for
        
        //System.out.println("attrs = " + attrs.toString());
        
        return attrs;
    }

    
    *//**
     *  Generates the POJO class that contains information about where PDF forms and meta XML
     *  files could be located
     * @param pdfName Name of PDf form
     * @return
     *//*
    private static LocationInfo getPDFLocation(String pdfName) {
        
        LocationInfo locInfo = new LocationInfo();
        
        String pdfLocation = new StringBuffer(inputPDFFolder).append("/").append(pdfName).append(".pdf").toString(); 
        String metaXMLPath = new StringBuffer(inputPDFFolder).append("/").append(pdfName).append(".xml").toString();;

        File file = new File(pdfLocation);
        if (!file.exists()) {
            log.error("PDF not found " + pdfLocation);
            pdfLocation = null;
        }
           
        file = new File(metaXMLPath);
        if (!file.exists()) {
            metaXMLPath = null;
        }

        locInfo.setPdfLocation(pdfLocation);
        locInfo.setMetaXMLLocation(metaXMLPath);
        return locInfo;
    }
    
    
    
    
    *//**
     * Validates whether the input file is a XML file
     * @param file
     *//*
    private static boolean validXML(File file){
        boolean result = false;
        
        if(!file.isFile()){
            return result;
        }
        
        String fileName = file.getName();
        int pos = fileName.lastIndexOf(".");
        String ext = "";
        if(pos != -1){
            ext = fileName.substring(pos+1, fileName.length()) ;
        }
        
        if(ext.toLowerCase().equals("xml")){
             result = true;
        }        
        
        return result;
    }    
    
    public static void main(String[] args) {
        try{
            generatePDF();
        }
        catch(Exception e){
            log.error("Error while generating PDF" , e);
        }
        log.info("Exit.");
    }
    
    


}//end class
*/