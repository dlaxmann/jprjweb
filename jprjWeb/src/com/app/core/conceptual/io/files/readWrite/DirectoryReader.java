package com.app.core.conceptual.io.files.readWrite;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class DirectoryReader {
static List filesList = new ArrayList();
static List dirfilesList = new ArrayList();
static List latestFilesList = new ArrayList();
static long lastMod = Long.MIN_VALUE;
static Date leastModDt = new Date(Long.MIN_VALUE);
static List linkedlist = new LinkedList();

private static SimpleDateFormat sdf = new SimpleDateFormat("dd-MMMMM-yyyy");


	static int spc_count=-1;

	  static void Process(File aFile) throws ParseException {
	    spc_count++;
	    String spcs = "";
	    for (int i = 0; i < spc_count; i++)
	      spcs += " ";
	    
	    if(aFile.isFile()){
	    	filesList.add(aFile);
	    	//aFile.
	    	String st = sdf.format(aFile.lastModified());
	    	Date modfddt = sdf.parse(st);
	    	  	 System.out.println("last mod " +"==="+st);
	    	 if(modfddt.after(leastModDt))  	{
	    		 leastModDt = modfddt;
	    		latestFilesList.add(aFile);
	    	}
	    	
	      System.out.println(spcs + "[FILE] " + aFile.getName());
	    }
	    else if (aFile.isDirectory()) {
	    	dirfilesList.add(aFile);
	      System.out.println(spcs + "[DIR] " + aFile.getName());
	      File[] listOfFiles = aFile.listFiles();
	      if(listOfFiles!=null) {
	        for (int i = 0; i < listOfFiles.length; i++)
	          Process(listOfFiles[i]);
	      } else {
	        System.out.println(spcs + " [ACCESS DENIED]");
	      }
	    }
	    spc_count--;
	  }

	  public static void main(String[] args) throws ParseException {
	    String nam = "D:/SERVER LOG";
	    File aFile = new File(nam);
	    Process(aFile);
	    System.out.println("files list   " + filesList.size());
	    System.out.println("dirs list   " + dirfilesList.size());
	    System.out.println("latest files list   " + latestFilesList.size());
	    
	
    /*    Arrays.sort(latestFilesList. new Comparator(){
            public int compare(Object o1, Object o2) {
                return compare( (File)o1, (File)o2);
            }
            private int compare( File f1, File f2){
                long result = f2.lastModified() - f1.lastModified();
                if( result > 0 ){
                    return 1;
                } else if( result < 0 ){
                    return -1;
                } else {
                    return 0;
                }
            }
        }
        );*/
	    
	    
	    /*for (int i = 0; i < latestFilesList.size(); i++) {
			System.out.println("latest file"+latestFilesList.get(i));
		}*/
	    
	    for(int i=0, length=Math.min(latestFilesList.size(), 20); i<length; i++) {
            System.out.println(latestFilesList.get(i));
        }
	  }
}
