package com.app.core.conceptual.io.files.pdf;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XmlUtils {

	/**
	 * Gets Node Value List from given XML document with file Path
	 * 
	 * @param parentTag
	 * @param tagName
	 * @param layoutFile
	 * @return
	 */
	public static List<String> getNodeValue(String parentTag, String tagName,
			String layoutFile) {
		DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory
				.newInstance();
		DocumentBuilder docBuilder = null;
		Document doc = null;
		List<String> valueList = new ArrayList<String>();
		try {
			docBuilder = docBuilderFactory.newDocumentBuilder();
			doc = docBuilder.parse(new File(layoutFile));
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Could not load file");
		}
		NodeList layoutList = doc.getElementsByTagName(parentTag);

		for (int s = 0; s < layoutList.getLength(); s++) {
			Node firstPersonNode = layoutList.item(s);
			if (firstPersonNode.getNodeType() == Node.ELEMENT_NODE) {
				Element firstPersonElement = (Element) firstPersonNode;
				NodeList firstNameList = (firstPersonElement)
						.getElementsByTagName(tagName);
				Element firstNameElement = (Element) firstNameList.item(0);
				NodeList textFNList = (firstNameElement).getChildNodes();
				System.out.println("-----element list " +textFNList.item(0).getNodeValue().trim());
				valueList.add(textFNList.item(0).getNodeValue().trim());
			}
		}
		return valueList;
	}

	/**
	 * Unit test for XmlUtils class
	 * 
	 * @param args
	 */
	public static void main(String args[]) {
		// System.out.println(XmlUtils.getNodeValue("DataElement",
		// "Value","./templates/Contents.xml"));
		System.out.println(XmlUtils.getNodeValue("DataElement", "Value",
				"files/Contents.xml"));
	}
}
