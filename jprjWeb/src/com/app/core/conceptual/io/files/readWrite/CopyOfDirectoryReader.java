package com.app.core.conceptual.io.files.readWrite;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class CopyOfDirectoryReader {
static List filesList = new ArrayList();
static List dirfilesList = new ArrayList();


	static int spc_count=-1;

	  static void Process(File aFile) {
	    spc_count++;
	    String spcs = "";
	    for (int i = 0; i < spc_count; i++)
	      spcs += " ";
	    if(aFile.isFile()){
	    	filesList.add(aFile);
	      System.out.println(spcs + "[FILE] " + aFile.getName());
	    }
	    else if (aFile.isDirectory()) {
	    	dirfilesList.add(aFile);
	      System.out.println(spcs + "[DIR] " + aFile.getName());
	      File[] listOfFiles = aFile.listFiles();
	      if(listOfFiles!=null) {
	        for (int i = 0; i < listOfFiles.length; i++)
	          Process(listOfFiles[i]);
	      } else {
	        System.out.println(spcs + " [ACCESS DENIED]");
	      }
	    }
	    spc_count--;
	  }

	  public static void main(String[] args) {
	    String nam = "D:/SERVER LOG";
	    File aFile = new File(nam);
	    Process(aFile);
	    System.out.println("files list   " + filesList.size());
	    System.out.println("dirs list   " + dirfilesList.size());
	  }
}
