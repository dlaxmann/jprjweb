package com.app.core.conceptual.io.files.readWrite;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class CountFilesInDirectory {
	static int spc_count=-1;
	/*File[] roots = File.listRoots();
	for (int i=0; i<roots.length; i++) {
	    process(roots[i]);
	}*/
	// Process all files and directories under dir
	public static void visitAllDirsAndFiles(File dir) {
	    process(dir);

	    if (dir.isDirectory()) {
	        String[] children = dir.list();
	        for (int i=0; i<children.length; i++) {
	            visitAllDirsAndFiles(new File(dir, children[i]));
	            
	        }
	    }
	}

	// Process only directories under dir
	public static void visitAllDirs(File dir) {
	    if (dir.isDirectory()) {
	        process(dir);

	        String[] children = dir.list();
	        for (int i=0; i<children.length; i++) {
	            visitAllDirs(new File(dir, children[i]));
	        }
	    }
	}

	// Process only files under dir
	public static void visitAllFiles(File dir) {
	    if (dir.isDirectory()) {
	        String[] children = dir.list();
	        for (int i=0; i<children.length; i++) {
	            visitAllFiles(new File(dir, children[i]));
	        }
	    } else {
	        process(dir);
	    }
	}
	
	public static void process(File dirc){
		 int count = 0;
		 for (File file : dirc.listFiles()) {
             if (file.isFile()) {
                     count++;
             }
     }
     System.out.println("Number of files: " + count);
	}
	
	
	static void Process(File aFile) {
	    spc_count++;
	    String spcs = "";
	    for (int i = 0; i < spc_count; i++)
	      spcs += " ";
	    if(aFile.isFile())
	      System.out.println(spcs + "[FILE] " + aFile.getName());
	    else if (aFile.isDirectory()) {
	      System.out.println(spcs + "[DIR] " + aFile.getName());
	      File[] listOfFiles = aFile.listFiles();
	      if(listOfFiles!=null) {
	        for (int i = 0; i < listOfFiles.length; i++)
	          Process(listOfFiles[i]);
	      } else {
	        System.out.println(spcs + " [ACCESS DENIED]");
	      }
	    }
	    spc_count--;
	  }

	public static void main(String[] args) {
       /* File f = new File("D:/SERVER LOG");
        int count = 0;
        for (File file : f.listFiles()) {
                if (file.isFile()) {
                        count++;
                }
        }
        System.out.println("Number of files: " + count);*/
		File f = new File("D:/emails");
		CountFilesInDirectory cf = new CountFilesInDirectory();
		process(f);
		
		 
        
}
}
