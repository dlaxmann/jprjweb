/*
 * **************************************************************************<BR>
 * This program contains trade secrets and confidential information which<BR>
 * are proprietary to CSC Financial Services Group�.  The use,<BR>
 * reproduction, distribution or disclosure of this program, in whole or in<BR>
 * part, without the express written permission of CSC Financial Services<BR>
 * Group is prohibited.  This program is also an unpublished work protected<BR>
 * under the copyright laws of the United States of America and other<BR>
 * countries.  If this program becomes published, the following notice shall<BR>
 * apply:
 *     Property of Computer Sciences Corporation.<BR>
 *     Confidential. Not for publication.<BR>
 *     Copyright (c) 2005-2007 Computer Sciences Corporation. All Rights Reserved.<BR>
 * **************************************************************************<BR>
 */

package com.app.core.conceptual.io.files.pdf;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.PropertyResourceBundle;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.csc.fs.vpms.dom.VDOMDocument;
import com.csc.fs.vpms.dom.VDOMElement;
import com.csc.fs.vpms.dom.VDOMException;
import com.csc.fs.vpms.util.FileHelper;



/**
 * <p>
 * This class generates meta XML for each PDF form
 * <p>
 * <b>Modifications:</b><br>
 * <table border=0 cellspacing=5 cellpadding=5>
 * <thead>
 * <th align=left>Task</th><th align=left>Release</th><th align=left>Description</th>
 * </thead>
 * <tr><td>PDFUtility</td><td>Version 3</td><td>PDF Utility for AXA to generate meta xml for PDF </td></tr>
 * </table>
 * <p>
 * @author CSC FSG Developer
 * @since 3.0.0
 */
public class GenerateMetaXML{
    
    //Member variables
    private static Set centralOutputSet; //Set of output variables from central XML
    private static Map centralOutputMap; //Map of output variables from central XML;key=output element ,value = formatting type
    private static Map centralOINKMap; //Map of output variables from central XML; key=output element ,value=oink value
    
    //Constants
    public static final String ELEMENT_FIELD = "field";
    public static final String ELEMENT_OUTPUT = "output";
    public static final String ELEMENT_ATTR = "attr";
    
    public static final String ATTRIBUTE_INPUT = "input";
    public static final String ATTRIBUTE_OUTPUT = "output";
    public static final String ATTRIBUTE_FORMATTING = "formatting-type";
    public static final String ATTRIBUTE_SOURCE = "source";
    
    public static final String FORMAT_MAPPING = "field-map";
    
	public static final String PROPERTY_FILE = "pdf";
	public static PropertyResourceBundle bundle = (PropertyResourceBundle)PropertyResourceBundle.getBundle(PROPERTY_FILE);		
	
	static {
	    init();
	}
	
	private static Logger log = Logger.getLogger(GenerateMetaXML.class.getName());
	
	private static void  init(){
	    Properties p = System.getProperties();
	    String logFile = bundle.getString("logDir");
		File file = new File(logFile);
		if(!file.exists()){
		    file.mkdirs();
		} 
	    
		p.put("logFile", logFile);	    
	}
	
    
	/**
	 * Validates the input parameters and calls another function
	 * to processMetaXMLFiles to generate meta XML files
	 */
    private static void generateMetaXML() {

        String centralXMLPath = bundle.getString("inputCentalXMLPath");
        String pdfInputFolder = bundle.getString("inputMetaPDFFolder");
        String outputMetaXMLFolder = bundle.getString("outputMetaXMLFolder");
        
        File file = new File(centralXMLPath);
        if(!file.exists()){
            String err = "Central XML file not found at location " + centralXMLPath;
            log.error(err);
            throw new RuntimeException(err);
        }
        
        
        file = new File(pdfInputFolder);
        if(!file.exists() || !file.isDirectory()){
            String err = "PDF directory not found at location " + pdfInputFolder;
            log.error(err);
            throw new RuntimeException(err);
        }
        
        //Create output folder
        file = new File(outputMetaXMLFolder);
        if(!file.exists()){
            file.mkdirs();
        }
        
        log.info("************************************");
        log.info("Central XML " + centralXMLPath);
        log.info("Input PDF folder  " + pdfInputFolder);
        log.info("Output Meta xml folder  " + outputMetaXMLFolder);
        log.info("************************************");
        
        processMetaXMLFiles(centralXMLPath,pdfInputFolder,outputMetaXMLFolder);
    }//end func

    
    /**
     * Generates the meta xml files in output folder
     * @param centralXMLPath path of central XML
     * @param pdfInputFolder path of PDF input folder
     * @param outputMetaXMLFolder path of output folder
     */
    private static void processMetaXMLFiles(String centralXMLPath, String pdfInputFolder, String outputMetaXMLFolder) {
        
        //Read output elements of the central xml and sets the class variables centralOutputSet and centralOutputMap
        Element centralRoot = getCentralXMLFields(centralXMLPath);
        if(centralOutputSet == null || centralOutputSet.size() == 0){
            return;
        }
        
        
        //iterate PDFs
        File file = new File(pdfInputFolder);
        File[] files = file.listFiles();
        for (int i = 0; i < files.length; i++) {

            
            File pdfFile = files[i];
            if(validPDF(pdfFile)){
                Set pdfFieldsSet = iTextHelper.getPDFFields(pdfFile.getAbsolutePath());
                if(pdfFieldsSet == null){
                    continue;
                }
                
                String fileName = pdfFile.getName();
                log.info("Processing " + fileName + " ...");
                
                fileName = fileName.substring(0, fileName.lastIndexOf("."));
                String outputFileName = new StringBuffer(outputMetaXMLFolder).append("/").append(fileName).append(".xml").toString();
                
                //Generate the meta xml file for PDF (with same name as PDF file)
                processPDF(outputFileName, pdfFieldsSet, centralRoot);
            }
        }//end for pdflist
        
    }//end func
    
    
    /**
     * Generates the meta xml file for PDF
     * @param outputFileName name of output xml file
     * @param pdfFieldsSet Set of PDF fields inside PDF
     * @param centralRoot Root Element object of central XML
     */
    private static void processPDF(String outputFileName, Set pdfFieldsSet, Element centralRoot) {
        Element root = null;
        try{
            root = createEmptyMetaXML();
        
		   // Iterate through all pdf fields        
		   Iterator itr = pdfFieldsSet.iterator();
		   while (itr.hasNext()) {
		      String field = (String) itr.next();
		      String formatType = (String)centralOutputMap.get(field);
		      
		      //System.out.println("Field =" + field);
	
		      if(formatType == null){
		          //Create a empty entry for field that could not be found
		          createMappingEntry(root, field, "");
		      }
		      else{
			      if (FORMAT_MAPPING.equals(formatType)){
			          //Create mapping entry
			          processMappingEntry(centralRoot, root, field);
			      }
			      else{
			          //create mapping entry
			          processFormatEntry(centralRoot, root, field);
			          
			      }//end else
		      }
		   } // End of while loop
		   
		   		   
	        //if root has child <fields> elments then write meta xml file to file system
	        NodeList nodeList = ((Element)root).getElementsByTagName(ELEMENT_FIELD);
	        if(nodeList.getLength() > 0){
	            FileHelper.writeFile(outputFileName, VDOMDocument.DOM2String(((Element)root).getOwnerDocument()));
	            log.info("Generated " + outputFileName);
	        }
		   
		   
        }
        catch(ParserConfigurationException e){
            log.error("Error creating empty meta xml file " + e, e);
        }
        catch (IOException e) {
           log.error("Error while writing to file " + outputFileName, e);
        }
    }//end func



    /**
     * Creates a mapping entry in output meta xml
     * @param centralRoot Root element of central XML
     * @param root root element of output meta xml
     * @param field name of PDF field
     */
    private static void processMappingEntry(Element centralRoot, Node root, String field) {

//          String xpath = new StringBuffer("/pdf-info/field[@formatting-type='field-map']/attr[@output='")
//        					.append(field).append("']/@input").toString();
//			String oinkName = VDOMElement.getValue(centralRoot, xpath);
        
            String oinkName = (String)centralOINKMap.get(field);
            if(oinkName == null){
                log.warn("OINK name not found for field" + field);
                return;
            }
            createMappingEntry(root, field, oinkName);
    }//end func

    
    /**
     * Creates a Format mapping entry in output meta xml
     * @param centralRoot Root element of central XML
     * @param root root element of output meta xml
     * @param field name of PDF field
     */
    private static void processFormatEntry(Element centralRoot, Element root, String field) {
        String xpath = new StringBuffer("/pdf-info/field/output[@source='")
		.append(field).append("']").toString();
        
        log.debug("Copy format entry for " + field);
        
        try {
            Element outputElement = (Element)VDOMElement.getNode(centralRoot, xpath);
            if(outputElement != null){
                Element fieldElement = (Element)outputElement.getParentNode();
                
                //impor the format entry from central xml to output meta xml
                Node node = root.getOwnerDocument().importNode(fieldElement, true);
                root.appendChild(node);
            }
        }
        catch (VDOMException e) {
            log.error("Error while handling entry " + xpath, e);
        }        
    }    
    

    /**
     * Creates entry e.g. <attr input="A_InitPaymentAmt" output="TOTAL_PREMIUMS"/>
     * @param root root element of central XML
     * @param field PDF field name
     * @param oinkName oink name corresponding to <pre>field</pre>
     * @throws VDOMException
     */
    private static void createMappingEntry(Node root, String field, String oinkName){
        
        Document doc = ((Element)root).getOwnerDocument();
        String fieldXpath = "/pdf-info/field[@formatting-type='field-map']";
        
        //get field Element (parent of element to be created)
        Element fieldElement;
        try {
            fieldElement = (Element)VDOMElement.getNode(root, fieldXpath);
        
	        if(fieldElement == null){
	            fieldElement = doc.createElement("field");
	            fieldElement.setAttribute(ATTRIBUTE_FORMATTING, FORMAT_MAPPING);
	            root.appendChild(fieldElement);
	        }
	
	        Element attrElement = doc.createElement("attr");
	        attrElement.setAttribute(ATTRIBUTE_INPUT, oinkName);
	        attrElement.setAttribute(ATTRIBUTE_OUTPUT, field);
	        
	        fieldElement.appendChild(attrElement);
        }
        catch (VDOMException e) {
            log.error("Error while creating mapping entry for " + field, e);
        }        
        
    }



    /**
     * Reads the central xml and creates the set of all output elements
     * It also logs the duplicate entries. 
     * @param path path of central XML
     */
    private static Element getCentralXMLFields(String path){
        Set outputSet = new HashSet(1000);
        Map outputMap = new HashMap(1000);
        Map oinkMap = new HashMap(1000);
        
        Document doc = null;
        
        try {
            doc = VDOMDocument.getDOM(VDOMDocument.vFILE, path);
        }
        catch (VDOMException e) {
            log.error("Error while reading central XML " + path, e);
            return null;
        }
        log.info("Analyzing Central XML ...");
        
        Element root = doc.getDocumentElement();
        NodeList metaList = root.getElementsByTagName(ELEMENT_FIELD);
        
        //Iterate <field> elements
        for (int i = 0; i < metaList.getLength(); i++) {
            if (metaList.item(i).getNodeType() == Node.ELEMENT_NODE) {

                Element  field = (Element)metaList.item(i);
                String formattingType = field.getAttribute(ATTRIBUTE_FORMATTING);
                    
                if(FORMAT_MAPPING.equalsIgnoreCase(formattingType)){
                    //handle mapping attributes
                    getMappingFields(field, outputSet, outputMap, oinkMap);
                }
                else{
                    //handle formatting information
                    getFormattingFields(field, outputSet, formattingType, outputMap);
                }
            }//end if
        }//end for metaList.getLength()
        
        //Set class member variables
        GenerateMetaXML.centralOutputSet = outputSet;
        GenerateMetaXML.centralOutputMap = outputMap;
        GenerateMetaXML.centralOINKMap = oinkMap;
        
        return root;
    }
    
    
 


    /**
     * Reads mapping elements i.e.
     * <attr  input="A_InitPaymentAmt" output="TOTAL_PREMIUMS"  />
     * @param field Element corresponding to  <field formatting-type="field-map">
     * @param outputSet   Set of output elements
     * @param outputMap   Map of output elements and their format type
     * @param oinkMap     Map of output elements and their oink map
     */
    private static void getMappingFields(Element field, Set outputSet, Map outputMap, Map oinkMap) {
        NodeList attrList = field.getElementsByTagName(ELEMENT_ATTR);
        for (int i = 0; i < attrList.getLength(); i++) {
            if (attrList.item(i).getNodeType() == Node.ELEMENT_NODE) {
                Element attrElement = (Element)attrList.item(i);
                String output = attrElement.getAttribute(ATTRIBUTE_OUTPUT);
                if("".equals(output.trim())){
                    return;
                }

                String oink = attrElement.getAttribute(ATTRIBUTE_INPUT);
                
                boolean add = outputSet.add(output);
                if(add == false){
                    log.warn("DUPLICATE ENTRY in central XML - Name =" + output + " ,format type=field-map");
                }
                else{
                    //log.debug("Added - format type=field-map, Name =" + output);
                    outputMap.put(output, FORMAT_MAPPING);
                    
                    if(!"".equals(oink.trim())){
                        oinkMap.put(output, oink);
                    }
                }
            }
        }//end for
    }//end func


    /**
     * Reads the format entries from meta xml.
     * <output  source="PRIMARY_OWNER_FULL_NAME"  />
     * @param field Element corresponding to  <field formatting-type="concat">
     * @param outputSet   Set of output elements
     * @param formattingType type of formatting
     * @param outputMap   Map of output elements and their format type
     */
    private static void getFormattingFields(Element field, Set outputSet, String formattingType, Map outputMap) {
        
        //Read the source attribute from <output> 
        NodeList outList = field.getElementsByTagName(ELEMENT_OUTPUT);
        for (int i = 0; i < outList.getLength(); i++) {
            if (outList.item(i).getNodeType() == Node.ELEMENT_NODE) {
                Element outEle = (Element)outList.item(i);
                String output = outEle.getAttribute(ATTRIBUTE_SOURCE);
                if("".equals(output.trim())){
                    return;
                }
                
                boolean add = outputSet.add(output);
                if(add == false){
                    log.warn("DUPLICATE ENTRY in central XML- Name =" + output + " ,format type = " + formattingType);
                }else{
                    log.debug("Added - format type="+ formattingType + ", Name =" + output);
                    outputMap.put(output, formattingType);
                }
            }
        }//end for
    }    
    
    /**
     * Creates a empty meta xml and returns the root element.
     * @throws ParserConfigurationException
     */
    private static Element createEmptyMetaXML() throws ParserConfigurationException{
        DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
        Document doc = docBuilder.newDocument();

        //create the root element and add it to the document
        Element root = doc.createElement("pdf-info");
        doc.appendChild(root);
        
        return root;
    }
    
    
    /**
     * Validates whether the input file is a PDF file
     * @param pdffile File object of pdf file
     * @return
     */
    private static boolean validPDF(File pdffile){
        boolean result = false;
        
        if(!pdffile.isFile()){
            return result;
        }
        
        String fileName = pdffile.getName();
        int pos = fileName.lastIndexOf(".");
        String ext = "";
        if(pos != -1){
            ext = fileName.substring(pos+1, fileName.length()) ;
        }
        
        if(ext.toLowerCase().equals("pdf")){
             result = true;
        }        
        
        return result;
    }
    
    
    
    
    public static void main(String[] args) {
        try{
            generateMetaXML();
        }
        catch(Exception e){
            log.error("Error occured : " , e);
        }
        log.info("Exit.");
    }
    
}//end class
