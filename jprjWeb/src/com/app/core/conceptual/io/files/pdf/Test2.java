package com.app.core.conceptual.io.files.pdf;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.util.PDFMergerUtility;

/**
 * This is an example that creates a simple document
 * with a ttf-font.
 *
 * @author <a href="mailto:m.g.n@gmx.de">Michael Niedermair</a>
 * @version $Revision: 1.2 $
 */
public class Test2
{

    /**
     * create the second sample document from the PDF file format specification.
     *
     * @param file      The file to write the PDF to.
     * @param message   The message to write in the file.
     * @param fontfile  The ttf-font file.
     *
     * @throws IOException If there is an error writing the data.
     * @throws COSVisitorException If there is an error writing the PDF.
     */
           public void doIt(final String file, final String message
            ) throws IOException, COSVisitorException
    {

 // the document
 PDDocument doc = null;
 try
 {
     doc = new PDDocument();

     PDPage page = new PDPage();
     doc.addPage(page);
     PDFont font = PDType1Font.HELVETICA_BOLD;


     PDPageContentStream contentStream = new PDPageContentStream(doc,
             page);
     contentStream.beginText();
     contentStream.setFont(font, 12);
     contentStream.moveTextPositionByAmount(100, 700);
     contentStream.drawString(message);
     contentStream.endText();
     contentStream.close();

     doc.save(file);

     System.out.println(file + " created!");
 }
 finally
 {
     if (doc != null)
     {
         doc.close();
     }
 }
}

/**
* This will create a hello world PDF document
* with a ttf-font.
* <br />
* see usage() for commandline
*
* @param args Command line arguments.
*/
public static void main(String[] args)
{

 Test2 app = new Test2();
 Test2 app2 = new Test2();
 try {
     app.doIt("C:/here.pdf", "hello");
     app2.doIt("C:/here2.pdf", "helloagain");
     PDFMergerUtility merger = new PDFMergerUtility();
     merger.addSource("C:/here.pdf");
     merger.addSource("C:/here2.pdf");
     OutputStream bout2 = new BufferedOutputStream(new FileOutputStream("C:/hereisthefinal.pdf"));

     merger.setDestinationStream(bout2);
     merger.mergeDocuments();

 } catch (COSVisitorException e) {
     // TODO Auto-generated catch block
     e.printStackTrace();
 } catch (IOException e) {
     // TODO Auto-generated catch block
     e.printStackTrace();
 }

}


}