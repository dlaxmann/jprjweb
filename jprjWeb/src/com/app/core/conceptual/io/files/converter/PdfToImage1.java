package com.app.core.conceptual.io.files.converter;

public class PdfToImage1 {
/*
				private static final String PASSWORD = "-password";
	   39       private static final String START_PAGE = "-startPage";
	   40       private static final String END_PAGE = "-endPage";
	   41       private static final String IMAGE_FORMAT = "-imageType";
	   42       private static final String OUTPUT_PREFIX = "-outputPrefix";
	   43       private static final String COLOR = "-color";
	   44       private static final String RESOLUTION = "-resolution";
	   45   
	   46       *//**
	   47        * private constructor.
	   48       *//*
	   49       private PDFToImage()
	   50       {
	   51           //static class
	   52       }
	   53   
	   54       *//**
	   55        * Infamous main method.
	   56        *
	   57        * @param args Command line arguments, should be one and a reference to a file.
	   58        *
	   59        * @throws Exception If there is an error parsing the document.
	   60        *//*
	   61       public static void main( String[] args ) throws Exception
	   62       {
	   63           String password = "";
	   64           String pdfFile = null;
	   65           String outputPrefix = null;
	   66           String imageFormat = "jpg";
	   67           int startPage = 1;
	   68           int endPage = Integer.MAX_VALUE;
	   69           String color = "rgb";
	   70           int resolution;
	   71           try
	   72           {
	   73               resolution = Toolkit.getDefaultToolkit().getScreenResolution();
	   74           }
	   75           catch( HeadlessException e )
	   76           {
	   77               resolution = 96;
	   78           }
	   79           for( int i = 0; i < args.length; i++ )
	   80           {
	   81               if( args[i].equals( PASSWORD ) )
	   82               {
	   83                   i++;
	   84                   if( i >= args.length )
	   85                   {
	   86                       usage();
	   87                   }
	   88                   password = args[i];
	   89               }
	   90               else if( args[i].equals( START_PAGE ) )
	   91               {
	   92                   i++;
	   93                   if( i >= args.length )
	   94                   {
	   95                       usage();
	   96                   }
	   97                   startPage = Integer.parseInt( args[i] );
	   98               }
	   99               else if( args[i].equals( END_PAGE ) )
	  100               {
	  101                   i++;
	  102                   if( i >= args.length )
	  103                   {
	  104                       usage();
	  105                   }
	  106                   endPage = Integer.parseInt( args[i] );
	  107               }
	  108               else if( args[i].equals( IMAGE_FORMAT ) )
	  109               {
	  110                   i++;
	  111                   imageFormat = args[i];
	  112               }
	  113               else if( args[i].equals( OUTPUT_PREFIX ) )
	  114               {
	  115                   i++;
	  116                   outputPrefix = args[i];
	  117               }
	  118               else if( args[i].equals( COLOR ) )
	  119               {
	  120                   i++;
	  121                   color = args[i];
	  122               }
	  123               else if( args[i].equals( RESOLUTION ) )
	  124               {
	  125                   i++;
	  126                   resolution = Integer.parseInt(args[i]);
	  127               }
	  128               else
	  129               {
	  130                   if( pdfFile == null )
	  131                   {
	  132                       pdfFile = args[i];
	  133                   }
	  134               }
	  135           }
	  136           if( pdfFile == null )
	  137           {
	  138               usage();
	  139           }
	  140           else
	  141           {
	  142               if(outputPrefix == null)
	  143               {
	  144                   outputPrefix = pdfFile.substring( 0, pdfFile.lastIndexOf( '.' ));
	  145               }
	  146   
	  147               PDDocument document = null;
	  148               try
	  149               {
	  150                   document = PDDocument.load( pdfFile );
	  151   
	  152   
	  153                   //document.print();
	  154                   if( document.isEncrypted() )
	  155                   {
	  156                       try
	  157                       {
	  158                           document.decrypt( password );
	  159                       }
	  160                       catch( InvalidPasswordException e )
	  161                       {
	  162                           if( args.length == 4 )//they supplied the wrong password
	  163                           {
	  164                               System.err.println( "Error: The supplied password is incorrect." );
	  165                               System.exit( 2 );
	  166                           }
	  167                           else
	  168                           {
	  169                               //they didn't supply a password and the default of "" was wrong.
	  170                               System.err.println( "Error: The document is encrypted." );
	  171                               usage();
	  172                           }
	  173                       }
	  174                   }
	  175                   int imageType = 24;
	  176                   if ("bilevel".equalsIgnoreCase(color))
	  177                   {
	  178                       imageType = BufferedImage.TYPE_BYTE_BINARY;
	  179                   }
	  180                   else if ("indexed".equalsIgnoreCase(color))
	  181                   {
	  182                       imageType = BufferedImage.TYPE_BYTE_INDEXED;
	  183                   }
	  184                   else if ("gray".equalsIgnoreCase(color))
	  185                   {
	  186                       imageType = BufferedImage.TYPE_BYTE_GRAY;
	  187                   }
	  188                   else if ("rgb".equalsIgnoreCase(color))
	  189                   {
	  190                       imageType = BufferedImage.TYPE_INT_RGB;
	  191                   }
	  192                   else if ("rgba".equalsIgnoreCase(color))
	  193                   {
	  194                       imageType = BufferedImage.TYPE_INT_ARGB;
	  195                   }
	  196                   else
	  197                   {
	  198                       System.err.println( "Error: the number of bits per pixel must be 1, 8 or 24." );
	  199                       System.exit( 2 );
	  200                   }
	  201   
	  202                   //Make the call
	  203                   PDFImageWriter imageWriter = new PDFImageWriter();
	  204                   boolean success = imageWriter.writeImage(document, imageFormat, password,
	  205                           startPage, endPage, outputPrefix, imageType, resolution);
	  206                   if (!success)
	  207                   {
	  208                       System.err.println( "Error: no writer found for image format '"
	  209                               + imageFormat + "'" );
	  210                       System.exit(1);
	  211                   }
	  212               }
	  213               catch (Exception e)
	  214               {
	  215                   System.err.println(e);
	  216               }
	  217               finally
	  218               {
	  219                   if( document != null )
	  220                   {
	  221                       document.close();
	  222                   }
	  223               }
	  224           }
	  225       }
	  226   
	  227       *//**
	  228        * This will print the usage requirements and exit.
	  229        *//*
	  230       private static void usage()
	  231       {
	  232           System.err.println( "Usage: java org.apache.pdfbox.PDFToImage [OPTIONS] <PDF file>\n" +
	  233               "  -password  <password>          Password to decrypt document\n" +
	  234               "  -imageType <image type>        (" + getImageFormats() + ")\n" +
	  235               "  -outputPrefix <output prefix>  Filename prefix for image files\n" +
	  236               "  -startPage <number>            The first page to start extraction(1 based)\n" +
	  237               "  -endPage <number>              The last page to extract(inclusive)\n" +
	  238               "  -color <string>                The color depth (valid: bilevel, indexed, gray, rgb, rgba)\n" +
	  239               "  -resolution <number>           The bitmap resolution in dpi\n" +
	  240               "  <PDF file>                     The PDF document to use\n"
	  241               );
	  242           System.exit( 1 );
	  243       }
	  244   
	  245       private static String getImageFormats()
	  246       {
	  247           StringBuffer retval = new StringBuffer();
	  248           String[] formats = ImageIO.getReaderFormatNames();
	  249           for( int i = 0; i < formats.length; i++ )
	  250           {
	  251               retval.append( formats[i] );
	  252               if( i + 1 < formats.length )
	  253               {
	  254                   retval.append( "," );
	  255               }
	  256           }
	  257           return retval.toString();
	  258       }
*/
	
}
