/*
 * **************************************************************************<BR>
 * This program contains trade secrets and confidential information which<BR>
 * are proprietary to CSC Financial Services Group�.  The use,<BR>
 * reproduction, distribution or disclosure of this program, in whole or in<BR>
 * part, without the express written permission of CSC Financial Services<BR>
 * Group is prohibited.  This program is also an unpublished work protected<BR>
 * under the copyright laws of the United States of America and other<BR>
 * countries.  If this program becomes published, the following notice shall<BR>
 * apply:
 *     Property of Computer Sciences Corporation.<BR>
 *     Confidential. Not for publication.<BR>
 *     Copyright (c) 2005-2007 Computer Sciences Corporation. All Rights Reserved.<BR>
 * **************************************************************************<BR>
 */

package com.app.core.conceptual.io.files.pdf;

import java.io.IOException;
import java.util.Set;

import org.apache.log4j.Logger;

import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.PdfReader;

/**
 * <p>
 * Utility class for iText related functions
 * <p>
 * <b>Modifications:</b><br>
 * <table border=0 cellspacing=5 cellpadding=5>
 * <thead>
 * <th align=left>Task</th><th align=left>Release</th><th align=left>Description</th>
 * </thead>
 * <tr><td>taskID</td><td>Version 3</td><td> </td></tr>
 * </table>
 * <p>
 * @author CSC FSG Developer
 * @since 3.0.0
 */
public class iTextHelper{
    
    private static Logger log = Logger.getLogger(iTextHelper.class.getName());
    
    /**
     * Returns Set of fields of PDF
     * @param path path of PDF file
     * @return
     */
    public static Set getPDFFields(String path){
        
        PdfReader reader;
        Set fieldsSet = null;
        try {
            reader = new PdfReader(path);
        }
        catch (IOException e) {
            log.error("Exception while reading file " + path, e);
            return fieldsSet;
        }
        AcroFields fields = reader.getAcroFields();
        fieldsSet = fields.getFields().keySet();
        
        return fieldsSet;
    }
    
    
}//end class
