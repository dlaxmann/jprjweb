package com.app.core.conceptual.io.files.readWrite;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;

public class ReadTextFileWriteToExcel {

	
	/**
	 * @param args
	 */
	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		//open file
		 BufferedReader bufferedReader = new BufferedReader(new FileReader("files/fileUtilWrt.txt"));
		 
		//Create new excel file
		FileOutputStream out = new FileOutputStream("files/R1.xls");
		//Create new workbook
		HSSFWorkbook wb = new HSSFWorkbook();
		//create new sheet
		HSSFSheet sheet1 = wb.createSheet("SHT1");
		// Create a row and put some cells in it. Rows are 0 based.
		Row row = null;
		// Create a cell and put a value in it.
		Cell cell = null;
		 
		String line = null;
		String[] temp;
		//delimiter 
		String delimiter = ",";
		 
        while ((line = bufferedReader.readLine()) != null) {
        	// given string will be split by the argument delimiter provided. 
        	temp = line.split(delimiter);
        	//print substrings 
        	for(int i =0; i < temp.length ; i++){
        		//System.out.print(temp[i]);
        		//System.out.print(" ");
        		String str = " ";
        		str = temp[i] + str;
        		
        		
        		System.out.print(str);
        		
        		// create a sheet with 97 rows (0-96)
        		int rownum;
        		for (rownum = (short) 0; rownum < 97; rownum++)
        		{
        			// create a row
        			row = sheet1.createRow(rownum);
        			// create 55 cells (0-54)
        			for (short cellnum = (short) 0; cellnum <temp.length; cellnum ++){
        			      // create a numeric cell
        			      cell = row.createCell(cellnum);
        			      //int j = Integer.parseInt(str,2);
        			      cell.setCellValue(temp[i]);         
        			}
        	    }  
        		
        	}
        	System.out.println();
        }
		
        wb.write(out);
        out.flush();
 		out.close();
	}
}
