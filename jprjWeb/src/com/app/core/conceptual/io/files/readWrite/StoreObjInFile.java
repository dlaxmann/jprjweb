package com.app.core.conceptual.io.files.readWrite;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class StoreObjInFile {

	/*boolean success = file.createNewFile();
    if (success) {
        // File did not exist and was created
    } else {
        // File already exists
    }*/
	
	public static void main(String[] args) {
        try {
            //
            // Create instances of FileOutputStream and ObjectOutputStream.
            //
            FileOutputStream fos = new FileOutputStream("files/books.dat");
            if(fos.equals(null)){
            	//fos = fos.c
            }
            ObjectOutputStream oos = new ObjectOutputStream(fos);
 
            
            //
            // Create a Book instance. This book object then will be stored in
            // the file.
            //
            Book book = new Book("0-07-222565-3", "Hacking Exposed J2EE & Java",
                "Art Taylor, Brian Buege, Randy Layman");
 
            //
            // By using writeObject() method of the ObjectOutputStream we can
            // make the book object persistent on the books.dat file.
            //
            oos.writeObject(book);
 
            //
            // Flush and close the ObjectOutputStream.
            //
            oos.flush();
            oos.close();
 
            //
            // We have the book saved. Now it is time to read it back and display
            // its detail information.
            //
            FileInputStream fis = new FileInputStream("files/books.dat");
            ObjectInputStream ois = new ObjectInputStream(fis);
 
            //
            // To read the Book object use the ObjectInputStream.readObject() method.
            // This method return Object type data so we need to cast it back the its
            // origin class, the Book class.
            //
            book = (Book) ois.readObject();
            System.out.println(book.toString());
 
            ois.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
