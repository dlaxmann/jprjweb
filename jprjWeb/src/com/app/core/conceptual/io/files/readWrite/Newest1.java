package com.app.core.conceptual.io.files.readWrite;

import java.io.File;
import java.util.Arrays;
import java.util.Comparator;

public class Newest1 {

	public static File[] getTopFiles() {
	    File dir = new File("D:/SERVER LOG");
	    SortFilter filter = new SortFilter(10);
	    dir.listFiles(filter);      
	    File[] topFiles = new File[10];
	    for(int i=0, length=Math.min(topFiles.length, 10); i<length; i++) {
            System.out.println(topFiles[i]);
        }
	    return filter.topFiles.toArray(topFiles);
	}
	
	public static void main(String[] args) {
        File dir = new File("D:/SERVER LOG");
        getTopFiles();
        
        
        File [] files  = dir.listFiles();
        
        Arrays.sort(files, new Comparator(){
            public int compare(Object o1, Object o2) {
                return compare( (File)o1, (File)o2);
            }
            
            private int compare( File f1, File f2){
                long result = f2.lastModified() - f1.lastModified();
                if( result > 0 ){
                    return 1;
                } else if( result < 0 ){
                    return -1;
                } else {
                    return 0;
                }
            }
        });
        for(int i=0, length=Math.min(files.length, 10); i<length; i++) {
            System.out.println(files[i]);
        }
    }
	
}


//System.out.println( Arrays.asList(files ));