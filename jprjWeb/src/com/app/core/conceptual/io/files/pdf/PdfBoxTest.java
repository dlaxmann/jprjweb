package com.app.core.conceptual.io.files.pdf;

import java.io.IOException;
import java.util.GregorianCalendar;

import javax.xml.transform.TransformerException;

import org.apache.jempbox.xmp.XMPMetadata;
import org.apache.jempbox.xmp.XMPSchemaBasic;
import org.apache.jempbox.xmp.XMPSchemaDublinCore;
import org.apache.jempbox.xmp.XMPSchemaPDF;
import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDMetadata;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

public class PdfBoxTest {

	
	// Hold the report font size. 
	private final static float FONT_SIZE = 10f; 

	// Hold the initial x position. 
	private final static float X0 = 5f; 

	// Hold the padding bottom of the document. 
	private final static float PADDING_BOTTOM_OF_DOCUMENT = 30f; 
	
	

	          public void doIt( String file, String message)     {

	        	  PDDocument document = null;
	              try {
	                  
	            	  document = new PDDocument();
	                  PDPage page = new PDPage();
	                  
	                  document.addPage( page );
	                  PDFont font = PDType1Font.HELVETICA_BOLD;
	      
	                  PDPageContentStream contentStream = new PDPageContentStream(document, page);
	                  contentStream.beginText();
	                  contentStream.setFont( font, 12 );
	                  contentStream.moveTextPositionByAmount( 100, 700 );
	                  contentStream.drawString( "some text into pdf..........." );
	                  
	                  contentStream.moveTextPositionByAmount(5, 800); 
	                  contentStream.drawString("Test page one");  
	                  contentStream.endText();
	                  contentStream.close();
	                  
	                  
	                  PDDocumentCatalog catalog = document.getDocumentCatalog();
					PDDocumentInformation info = document.getDocumentInformation();
		
					info.setCreator("Rama");
					info.setCreationDate(new GregorianCalendar());
					
					XMPMetadata metadata = new XMPMetadata();
		
					XMPSchemaPDF pdfSchema = metadata.addPDFSchema();
					pdfSchema.setKeywords(info.getKeywords());
					pdfSchema.setProducer(info.getProducer());
		
					XMPSchemaBasic basicSchema = metadata.addBasicSchema();
					basicSchema.setModifyDate(new GregorianCalendar());
					basicSchema.setCreateDate(info.getCreationDate());
					basicSchema.setCreatorTool(info.getCreator());
					basicSchema.setMetadataDate(new GregorianCalendar());
		
					XMPSchemaDublinCore dcSchema = metadata.addDublinCoreSchema();
					dcSchema.setTitle(info.getTitle());
					dcSchema.addCreator("PDFBox");
					dcSchema.setDescription(info.getSubject());
		
					PDMetadata metadataStream = new PDMetadata(document);
					metadataStream.importXMPMetadata(metadata);
					catalog.setMetadata(metadataStream);
					
					document.save( file );
	                  System.out.println("..........Done pdf created.");
	              }catch(IOException e){
	            	  e.printStackTrace();
	              }catch(COSVisitorException e1){
	            	  e1.printStackTrace();
	              } catch (TransformerException e) {
					e.printStackTrace();
				}
	              finally {
	                  if( document != null )
	                  {
	                      try {
	                    	  document.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
	                  }
	              }
	          }
	      
	          
	          
	          
	          public void createDocumentWithMultiplePage(String filePath1) throws IOException { 

	        	  PDPageContentStream contentStream = null; 
	        	  PDPageContentStream contentStream1 = null; 
	        	  PDDocument doc = null; 

	        	  try { 
	        	  // create empty document. 
	        	  doc = new PDDocument(); 
	        	  // create fist page. 
	        	  PDPage page = new PDPage(); 
	        	  page.setMediaBox(PDPage.PAGE_SIZE_A4); 
	        	  doc.addPage(page); 

	        	  contentStream = new PDPageContentStream(doc, page); 
	        	  contentStream.beginText(); 
	        	  contentStream.setFont(PDType1Font.HELVETICA_BOLD, FONT_SIZE); 
	        	  contentStream.moveTextPositionByAmount(5, 800); 

	        	  contentStream.drawString("Test page one row1"); 
	        	  contentStream.drawString("Test page one row2"); 
	        	  contentStream.drawString("Test page one row2"); 
	        	//  contentStream.
	        	  contentStream.endText(); 
	        	  contentStream.close(); 

	        	  // Create next page. 
	        	  PDPage page1 = new PDPage(); 
	        	  page1.setMediaBox(PDPage.PAGE_SIZE_A4); 
	        	  doc.addPage(page1); 

	        	  contentStream1 = new PDPageContentStream(doc, page1); 
	        	  contentStream1.setFont(PDType1Font.HELVETICA_BOLD, FONT_SIZE); 
	        	  contentStream1.beginText(); 
	        	  contentStream1.moveTextPositionByAmount(5, 800); 

	        	  contentStream1.drawString("Test page Two"); 
	        	  contentStream1.endText(); 
	        	  contentStream1.close(); 
	        	  } catch (final IOException exception) { 
	        	  throw new RuntimeException(exception); 
	        	  } finally { 

	        	  if (contentStream != null) { 
	        	  try { 
	        	  contentStream.close(); 
	        	  } catch (final IOException exception) { 
	        	  throw new RuntimeException(exception); 
	        	  } 

	        	  } 
	        	  if (contentStream1 != null) { 
	        	  try { 
	        	  contentStream1.close(); 
	        	  } catch (IOException exception) { 
	        	  exception.printStackTrace();
	        	  } 

	        	  } 
	        	  } 

	        	  try {
					doc.save(filePath1);
					System.out.println("multi page pdf done...");
				} catch (COSVisitorException e) {
					e.printStackTrace();
				}
	        	 
	        	  } 
	          
	          public static void main(String[] args) throws IOException         {
	        	  PdfBoxTest app = new PdfBoxTest();
	        	  String filePath = "files/pdfBox1.pdf";
	        	  String filePath1 = "files/pdfBox2.pdf";
	        	  String message = "message";
	        	  
	        	  app.doIt(filePath, message);
	        	  app.createDocumentWithMultiplePage(filePath1);
	              
	          }
}
