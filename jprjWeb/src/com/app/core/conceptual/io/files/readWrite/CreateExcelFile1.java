package com.app.core.conceptual.io.files.readWrite;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 * create hsssfworkbook instance
 * create sheets in workbook and name them
 * manipulate the sheets by creating hsssfrow
 * 
 *
 */
public class CreateExcelFile1 {

	public static void main(String args[]){
		
		HSSFWorkbook xl = new HSSFWorkbook();
		
		//create sheets
		HSSFSheet sheet1 = xl.createSheet("SHT2");
		HSSFSheet sheet2 = xl.createSheet("SHT3");
		
		//create row for the sheet1 
		HSSFRow rowA = sheet1.createRow(0);
		HSSFCell cellA = rowA.createCell(0);
		cellA.setCellValue(new HSSFRichTextString("FIRST SHEET SHEET"));
		
		//create row for the sheet2
		HSSFRow rowB = sheet2.createRow(0);
		HSSFCell cellB = rowB.createCell(0);
		cellB.setCellValue(new HSSFRichTextString("SHT"));
		
		//set autoSize to both the sheets
		//sheet1.autoSizeColumn((short)1);
		//sheet2.autoSizeColumn((short)1);
		
		FileOutputStream fos = null;
		try{
			fos = new FileOutputStream(new File("files/H1.xls"));
			xl.write(fos);
			System.out.println("..done..");
		}catch(IOException e){
			e.printStackTrace();
		}finally{
			if(fos != null){
				try {
					fos.flush();
					fos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
