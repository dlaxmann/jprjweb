package com.app.core.conceptual.io.files.pdf;

import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.app.core.app.bean.Certificate;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.Font;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfWriter;

public class CertificateReport {

	public CertificateReport(Certificate certificate) throws Exception {
		SimpleDateFormat formatter = new SimpleDateFormat("EEE, d MMM yyyy");
		Document document = new Document(PageSize.A4.rotate());
		document.setPageSize(PageSize.A4);
		PdfWriter.getInstance(document,
		// new FileOutputStream("./templates/" + certificate.getFullName() + "-"
		// + certificate.getRegistrationNo() + ".pdf"));
				new FileOutputStream("files/pdff.pdf"));
		document.open();
		Paragraph p1 = new Paragraph(30);
		p1.add(new Chunk(certificate.getHeaders().get(0), new Font(
				Font.TIMES_ROMAN, 8)));
		p1.setAlignment(1);
		Paragraph p2 = new Paragraph();
		p2.add(new Chunk(certificate.getHeaders().get(1), new Font(
				Font.TIMES_ROMAN, 9, Font.BOLD)));
		p2.setAlignment(1);
		Paragraph p3 = new Paragraph();
		p3.add(new Chunk(certificate.getHeaders().get(2), new Font(
				Font.TIMES_ROMAN, 14, Font.BOLD)));
		p3.setAlignment(1);
		Paragraph p4 = new Paragraph();
		p4.add(new Chunk(certificate.getHeaders().get(3), new Font(
				Font.TIMES_ROMAN, 14)));
		p4.setAlignment(1);
		Paragraph p5 = new Paragraph(60);
		p5.add(new Chunk("Reg. No. (DVSDT) :- "
				+ certificate.getRegistrationNo(),
				new Font(Font.TIMES_ROMAN, 9)));
		p5.setAlignment(0);
		p5.setIndentationLeft(80);
		Paragraph p6 = new Paragraph(45);
		p6.add(new Chunk("Certificate", new Font(Font.TIMES_ROMAN, 17,
				Font.BOLD)));
		p6.setAlignment(1);
		Paragraph p7 = new Paragraph(30);
		p7.add(new Chunk("This certificate is awarded to " + "  ", new Font(
				Font.TIMES_ROMAN, 9)));
		p7.add(new Chunk(certificate.getFullName() + "  ", new Font(
				Font.TIMES_ROMAN, 9, Font.BOLD)));
		p7.add(new Chunk("son of  ", new Font(Font.TIMES_ROMAN, 9)));
		p7.add(new Chunk("Mr.  " + certificate.getFatherName(), new Font(
				Font.TIMES_ROMAN, 9, Font.BOLD)));
		p7.setAlignment(1);
		Paragraph p8 = new Paragraph(18);
		p8.add(new Chunk("Permanent resident of" + "  ", new Font(
				Font.TIMES_ROMAN, 9)));

		p8.add(new Chunk(certificate.getPermanentAddres() + "  ", new Font(
				Font.TIMES_ROMAN, 9, Font.BOLD)));
		p8.add(new Chunk("holding citizenship No : ", new Font(
				Font.TIMES_ROMAN, 9)));
		p8.add(new Chunk(certificate.getCitizenshipNo() + " ", new Font(
				Font.TIMES_ROMAN, 9, Font.BOLD)));
		p8.setAlignment(1);
		Paragraph p9 = new Paragraph(18);
		p9.add(new Chunk("& passport No. ", new Font(Font.TIMES_ROMAN, 9)));
		p9.add(new Chunk(certificate.getPassportNo() + " ", new Font(
				Font.TIMES_ROMAN, 9, Font.BOLD)));
		p9.add(new Chunk("for successful completion of "
				+ certificate.getCreditHours()
				+ " Credit Hours course on Preliminary", new Font(
				Font.TIMES_ROMAN, 9)));
		p9.setAlignment(1);
		Paragraph p10 = new Paragraph(18);
		p10.add(new Chunk(
				"education for the workers going to Republic of Korea under",
				new Font(Font.TIMES_ROMAN, 9)));
		p10.add(new Chunk("  " + certificate.getWorkSystem(), new Font(
				Font.TIMES_ROMAN, 9, Font.BOLD)));
		p10.setAlignment(1);

		Paragraph p11 = new Paragraph(18);
		p11.add(new Chunk("from  "
				+ formatter.format(certificate.getFromDate()) + "  " + "to  "
				+ formatter.format(certificate.getToDate()), new Font(
				Font.TIMES_ROMAN, 9)));
		p11.setAlignment(1);

		Paragraph p12 = new Paragraph(45);
		p12.add(new Chunk(
				"---------------------"
						+ "                                                                "
						+ "                                                          "
						+ "  ---------------------------", new Font(
						Font.TIMES_ROMAN, 8)));

		p12.setAlignment(1);
		Paragraph p13 = new Paragraph(10);
		p13.add(new Chunk(
				"   Coordinator"
						+ "                                                           "
						+ "                                                                        "
						+ "Executive Director", new Font(Font.TIMES_ROMAN, 8)));
		p13.setAlignment(1);
		Paragraph p14 = new Paragraph(20);
		p14.setAlignment(1);
		p14.add(new Chunk(formatter.format(new Date()), new Font(
				Font.TIMES_ROMAN, 7, Font.BOLD)));

		document.add(p1);
		document.add(p2);
		document.add(p3);
		document.add(p4);
		document.add(p5);
		document.add(p6);
		document.add(p7);
		document.add(p8);
		document.add(p9);
		document.add(p10);
		document.add(p11);
		document.add(p12);
		document.add(p13);
		document.add(p14);
		com.lowagie.text.Image image = com.lowagie.text.Image
				.getInstance("files/index2.jpg");
		image.setBorder(1);
		image.scaleAbsolute(100, 100);
		image.setAbsolutePosition(450, 730);
		document.add(image);
		document.close();

	}

	public static void main(String[] args) {
		String filePath = "files/pdf.pdf";
		String filePath1 = "files/Contents.xml";

		List<String> headerList = XmlUtils.getNodeValue("DataElement", "Value",
				filePath1);
		try {
			Certificate c = new Certificate();
			c.setFullName("Bishal Acharya");
			c.setFatherName("Manoj Acharya");
			c.setRegistrationNo(15236);
			c.setCitizenshipNo(102545);
			c.setPassportNo(3518161);
			c.setFromDate(new Date());
			c.setToDate(new Date());
			c.setCreditHours("Fourty Five (45)");
			c.setPermanentAddres("Shahid Marga Biratnagar");
			c.setWorkSystem("Employment Permit System");
			c.setHeaders(headerList);
			CertificateReport cR = new CertificateReport(c);

		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
