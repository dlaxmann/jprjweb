package com.app.core.conceptual.io.files.readWrite;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class AppendDataToFile {
	//https://www.jboss.org/richfaces/sourcecode
	
	public static void main(String[] args) 
    {
        File file = new File("files/WriteTextAppend.txt");
 
        try
        {            
            FileWriter writer = new FileWriter(file, true);
            writer.write("username=kodejava;password=secret"+ System.getProperty("line.separator"));
            writer.flush();
            writer.close();
        } catch (IOException e) 
        {
            e.printStackTrace();
        }
    }
}
