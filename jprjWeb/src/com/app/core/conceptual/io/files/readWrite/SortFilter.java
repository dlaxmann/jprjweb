package com.app.core.conceptual.io.files.readWrite;

import java.io.File;
import java.io.FileFilter;
import java.util.LinkedList;

public class SortFilter implements FileFilter  {
	
        final LinkedList<File> topFiles;
        private final int n;

        public SortFilter(int n) {
            this.n = n;
            topFiles = new LinkedList<File>();
        }

        public boolean accept(File newF) {
            long newT = newF.lastModified();

            if(topFiles.size()==0){
                //list is empty, so we can add this one for sure
                topFiles.add(newF);
            } else {
                int limit = topFiles.size()<n?topFiles.size():n;
                //find a place to insert
                int i=0;
                while(i<limit && newT <= topFiles.get(i).lastModified())
                    i++;

                if(i<limit){    //found
                    topFiles.add(i, newF);
                    if(topFiles.size()>n) //more than limit, so discard the last one. Maintain list at size n
                        topFiles.removeLast(); 
                }
            }
            return false;
        }

    }

