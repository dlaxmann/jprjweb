package com.app.core.conceptual.io.sftp;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

public class sftpex {

	public static void main(String[] args) {
	       // TODO code application logic here

	       String username = "testuser";
	       String host = "testserver.example.com";
	       String pass = "testpass";
	       String khfile = "/home/testuser/.ssh/known_hosts";
	       String identityfile = "/home/testuser/.ssh/id_rsa";

	       JSch jsch = null;
	       Session session = null;
	       Channel channel = null;
	       ChannelSftp c = null;
	       try {
	           jsch = new JSch();
	           session = jsch.getSession(username, host, 22);
	           session.setPassword(pass);
	           jsch.setKnownHosts(khfile);
	           jsch.addIdentity(identityfile);
	           session.connect();

	           channel = session.openChannel("sftp");
	           channel.connect();
	           c = (ChannelSftp) channel;

	       } catch (Exception e) { 	e.printStackTrace();	}

	       try {
	           System.out.println("Starting File Upload:");
	           String fsrc = "/folderA/abc.txt", fdest = "/folderA/cde.txt";
	           c.put(fsrc, fdest);
	       
	           c.get(fdest, "/folderA/testfile.bin");
	       } catch (Exception e) {	e.printStackTrace();	}
	       
	       c.disconnect();
	       session.disconnect();

	   }
}
