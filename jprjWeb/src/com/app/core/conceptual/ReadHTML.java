package com.app.core.conceptual;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

public class ReadHTML implements Runnable {

	private String url;

	public ReadHTML(String url) {
		this.url = url;
	}

	public String getUrl() {
		return url;
	}

	@Override
	public void run() {
		try {
			URL oracle = new URL(url);
	        BufferedReader in = new BufferedReader(
	        new InputStreamReader(oracle.openStream()));

	        String inputLine="";
	        int i=0;
	        while ((inputLine = in.readLine()) != null){
	        	// start from table tag wherever it starts
	        	if(inputLine.equalsIgnoreCase("<table>")||inputLine.contains("<table>")){
	        		//System.out.println(inputLine);
	        		i=1;
	        	}
	        	if(i==1)
	        		System.out.println(inputLine);
	        	
	        	// end at table tag wherever it ends
	        	if(inputLine.equalsIgnoreCase("</table>")||inputLine.contains("</table>")){
	        		i=0;
	        	}
	            
	            
	        }
	        in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
