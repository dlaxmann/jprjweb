package com.app.core.conceptual;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class ThreadTaskClient {
	
	
	public void readSubmissions(String[] urls) {
		//Used executor service for poolable threads 
		 ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newCachedThreadPool();
		for (int i = 0; i < urls.length; i++) {
			//only when you have a reasonable number of threads 
	       if(urls[i]==null || urls[i].equals(""))
	    	    continue;
	            ReadHTML task = new ReadHTML(urls[i]);
	            System.out.println("A new thread has been added for url:  " + task.getUrl());
	            executor.execute(task);
	        }
		
			//clears threads after tasks completion
	        executor.shutdown();
	}

	public static void main(String[] args)     {
		ThreadTaskClient threadTaskClient = new ThreadTaskClient();
		String[] urls = {"file:///C:/Users/BhavyaSri/Desktop/1.html","http://oracle.com/","file:///C:/Users/BhavyaSri/Desktop/2.html"};
		threadTaskClient.readSubmissions(urls);
        
    }
}
