package com.app.core.conceptual;

public class MyRunnableThread implements Runnable {

	Thread runner;
	public MyRunnableThread() {
	}
	
	
	public MyRunnableThread(String threadName) {
		runner = new Thread(this, threadName); // (1) Create a new thread.
		System.out.println(runner.getName());
		runner.start(); // (2) Start the thread.
	}
	
	
	//overriding run()
	public void run() {
		System.out.println(Thread.currentThread());
	}

	public void startThread(String thrdName){
		runner =  new Thread(this,thrdName);
		System.out.println(runner.getName());
		runner.start();
		try{
			runner.sleep(20000);
			System.out.println("after sleep");
		}catch(InterruptedException e){
			
		}
	}
	
	
	public static void main(String args[]){
		MyRunnableThread mrt = new MyRunnableThread();
		String th ="th1";
		mrt.startThread(th);
		
	}
	
	
}
