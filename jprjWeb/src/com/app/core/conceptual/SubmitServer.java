package com.app.core.conceptual;

import java.util.HashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class SubmitServer {

	// projNum Name submissionNum Score
		private HashMap<Integer, HashMap<String, HashMap<Integer, Integer>>> projects = new HashMap<Integer, HashMap<String, HashMap<Integer, Integer>>>();
		private int numProjects;
	 
	private boolean projInRange(int x) {
		return (x >= 1 && x <= numProjects);
	}
 
	private boolean inRange(int x) {
		return (x >= 0 && x <= 100);
	}
 
	
	public SubmitServer(int numProjects) {
		for (int i = 1; i <= numProjects; ++i)
			projects.put(i, new HashMap<String, HashMap<Integer, Integer>>());
		System.out.println("______PRJEC NUM ADDED##########");
		this.numProjects = numProjects;
	}
 
	public void readSubmissions(String[] urls) {
		//Used executor service for poolable threads 
		 ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newCachedThreadPool();
		for (int i = 0; i < urls.length; i++) {
			//only when you have a reasonable number of threads 
	       if(urls[i]==null || urls[i].equals(""))
	    	    continue;
	            ReadHTML task = new ReadHTML(urls[i]);
	            System.out.println("A new thread has been added for url:  " + task.getUrl());
	            
	            addSubmission("Laura Llama",i+1,i+10);
	            executor.execute(task);
	            
	            
	            
	        }
		
			//clears threads after tasks completion
	        executor.shutdown();
		//throw new UnsupportedOperationException("You must write this method.");
	}
 
	public SubmitServer(SubmitServer otherSubmitServer) {
		for (Integer i : otherSubmitServer.getProjects().keySet()) {
			HashMap<String, HashMap<Integer, Integer>> hM1 = new HashMap<String, HashMap<Integer, Integer>>();
			for (String s : otherSubmitServer.getProjects().get(i).keySet()) {
				HashMap<Integer, Integer> hM2 = new HashMap<Integer, Integer>();
				for (Integer j : otherSubmitServer.getProjects().get(i).get(s)
						.keySet()) {
					hM2.put(j, otherSubmitServer.getProjects().get(i).get(s)
							.get(j));
				}
				hM1.put(s, hM2);
			}
			projects.put(i, hM1);
		}
		this.numProjects = otherSubmitServer.getNumProjects();
	}
 
	public HashMap<Integer, HashMap<String, HashMap<Integer, Integer>>> getProjects() {
		return this.projects;
	}
 
	public int getNumProjects() {
		return this.numProjects;
	}
 
	public SubmitServer addSubmission(String name, int projectNumber, int score) {
		if (name.equals(null))
			throw new NullPointerException();
		if (name.equals("") || !inRange(score))
			return this;
		if (!projInRange(projectNumber))
			return this;
 
		if (!projects.get(projectNumber).containsKey(name)) {
			projects.get(projectNumber).put(name,
					new HashMap<Integer, Integer>());
		}
		int size = projects.get(projectNumber).get(name).size();
		projects.get(projectNumber).get(name).put(size + 1, score);
 
		return this;
	}
 
	public int numSubmissions(String name, int projectNumber) {
		System.out.println("____name: "+name);
		System.out.println("____projectNumber: "+projectNumber);
		if (name.equals(null))
			throw new NullPointerException();
		if (name.equals("") || !projInRange(projectNumber)){
			System.out.println("____name: "+name);
			return 0;
		}
		System.out.println("{}________"+projects.get(projectNumber));
		System.out.println("________"+projects.get(projectNumber).get(name));
		if (projects.get(projectNumber).get(name) != null)
			return projects.get(projectNumber).get(name).size();
 
		return 0;
	}
 
	public int numSubmissions(int projectNumber) {
		if (!projInRange(projectNumber))
			return 0;
 
		int count = 0;
 
		for (String name : projects.get(projectNumber).keySet())
			count += numSubmissions(name, projectNumber);
 
		return count;
	}
 
	public int numSubmissions(String name) {
		if (name.equals(null))
			throw new NullPointerException();
 
		int count = 0;
		for (Integer i : projects.keySet()) {
			for (String s : projects.get(i).keySet()) {
				if (name.equals(s))
					count += numSubmissions(name, i);
			}
		}
		return count;
	}
 
	public int numSubmissions() {
		int count = 0;
 
		for (Integer i : projects.keySet()) {
			for (String name : projects.get(i).keySet())
				count += numSubmissions(name, i);
		}
 
		return count;
	}
 
	public int bestSubmissionScore(String name, int projectNumber) {
		if (name.equals(null))
			throw new NullPointerException();
		if (name.equals("") || !projInRange(projectNumber)) {
			System.out.println(name + " " + projectNumber);
			return -1;
		}
		if (numSubmissions(name, projectNumber) == 0)
			return 0;
 
		int bestScore = 0;
 
		for (Integer i : projects.get(projectNumber).get(name).keySet()) {
			int score = projects.get(projectNumber).get(name).get(i);
			if (score > bestScore) {
				bestScore = score;
			}
		}
		return bestScore;
	}
 
	public boolean satisfactory(String name, int projectNumber) {
		if (name.equals(null))
			throw new NullPointerException();
 
		return numSubmissions(name, projectNumber) > 0
				&& bestSubmissionScore(name, projectNumber) > 0;
 
	}
 
	public boolean gotExtraCredit(String name, int projectNumber) {
		if (name.equals(null))
			throw new NullPointerException();
 
		return numSubmissions(name, projectNumber) == 1
				&& bestSubmissionScore(name, projectNumber) == 100;
	}
}
