package com.app.core.conceptual;

import com.app.core.dp.proxyPattern.Animal;

public class InstanceOfInterfaceExample {
		  public static void main(String[] args)
		  {
		    Lion lion = new Lion();

		    // basic "instanceof class" test:
		    // the dog reference is clearly an instance of the Lion class,
		    // so this line will print.
		    if (lion instanceof Lion) System.out.println("lion is an instanceof Lion");

		    // instanceof interface test:
		    // because of java inheritance, the lion reference is also an 
		    // instance of the Animal interface, so this line
		    // will also print.
		    if (lion instanceof Animal) System.out.println("lion is an instanceof Animal");
		  }
}
