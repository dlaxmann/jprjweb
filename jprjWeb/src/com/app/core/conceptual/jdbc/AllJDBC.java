package com.app.core.conceptual.jdbc;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import com.app.core.Blob;

public class AllJDBC {

	private static Connection getConnection(){
		  Connection con = null;
		  String url = "jdbc:mysql://localhost:3306/";
		  //DriverManager.getConnection("jdbc:oracle:thin:@//localhost:1521/xe", "SYSTEM", "ldevasani");
		  String db = "dbone";
		  String driver = "com.mysql.jdbc.Driver";
		  String user = "root";
		  String pass = "ldevasani";
		  try{
		  Class.forName(driver);
		  con = DriverManager.getConnection(url+db, user, pass);
		  }catch(SQLException e){
		  } catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		  return con;
	}
	
			  
	public static void createTables() {
		Connection con = getConnection();
		String sql = "create table save_hugee55555555555(ID int(4) auto_increment, " +
				"NAME varchar(32), City varchar(32), IMAGE Blob, " +
				"PHONE varchar(10),PRIMARY KEY(ID))";
		try {
			Statement stmt = con.createStatement();
			stmt.execute(sql);
			System.out.println("...Done");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	public static void createTablesFromFile(){
		Connection con = getConnection();
		
		//String sqlFilePath ="com/frame/sql/batchSchema/schema-mysql.sql";
		//String sqlFilePath = "conf/mysql_db.properties";
		String sqlFilePath ="";
		 URL f = AllJDBC.class.getClassLoader().getResource("com/frame/sql/batchSchema/schema-mysql.sql");
	        System.out.println(f);
	        sqlFilePath = f.getPath();
		String str="";
		try{
			
			BufferedReader rdr = new BufferedReader(new FileReader(sqlFilePath));
			StringBuffer sbf = new StringBuffer();
			while((str = rdr.readLine()) != null){
				sbf.append(str+"\n");
			}
			Statement stmt = con.createStatement();
			rdr.close();
			System.out.println("__"+sbf);
			stmt.executeUpdate(sbf.toString());
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	private static void getTblColNms(){
			  try{
				  Connection con = getConnection();
			  Statement st = con.createStatement();
			 // select * insert into emp from employee
			  int rows = st.executeUpdate("INSERT INTO Copyemployee SELECT * FROM employee");
			    if (rows == 0) {
			      System.out.println("Don't add any row!");
			    } else {
			      System.out.println(rows + " row(s)affected.");
			     // con.close();
			    }
			  
			  
			  ResultSet rs = st.executeQuery("SELECT * FROM employee");
			  ResultSetMetaData md = rs.getMetaData();
			  int col = md.getColumnCount();
			  System.out.println("Number of Column : "+ col);
			  System.out.println("create table "+md.getTableName(col));
			  //System.out.println("Columns Name: ");
			  for (int i = 1; i <= col; i++){
			  String tableName = md.getTableName(i);
			  String col_name = md.getColumnName(i);
			  
			  System.out.println(col_name);
			  }
			  }
			  catch (SQLException s){
			  System.out.println("SQL statement is not executed!");
			  }
	}
		public static void insertImage(){
			Connection conn = null;
			try{
			conn = getConnection();
		    String qry = "insert into savefile(name, myfile) values (?, ?)";
		    PreparedStatement stmt1 = conn.prepareStatement(qry);
		    System.out.println("file inserted successfully");
		    String sql = "SELECT name, myfile FROM savefile ";
		    PreparedStatement stmt = conn.prepareStatement(sql);
		    ResultSet resultSet = stmt.executeQuery();
		    while (resultSet.next()) {
		      String name = resultSet.getString(1);
		      //String description = resultSet.getString(2);
		      File image = new File("c:\\BMC.txt");
		      FileOutputStream fos = new FileOutputStream(image);
		     
		      byte[] buffer = new byte[1];
		      InputStream is = resultSet.getBinaryStream(2);
		      while (is.read(buffer) > 0) {
		        fos.write(buffer);
		        System.out.println();
		      }
		      fos.close();
		    }
			}catch(Exception e){
				
			}finally{
				if(conn != null ){
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
				
			}
		    
		}

			
			public static void saveFile(){
				  Connection connection = null;
				  ResultSet rs = null;
				  PreparedStatement psmnt = null;
				  PreparedStatement psmnt1 = null;
				  FileInputStream fis;
				  try {
				connection = getConnection();
				File image = new File("files/image1.gif");
				psmnt = connection.prepareStatement("insert into save_huge(name, city, image, Phone) "+ "values(?,?,?,?)");
				psmnt.setString(1,"mahendra");
				psmnt.setString(2,"Delhi");
				psmnt.setString(4,"123456");
				fis = new FileInputStream(image);
				psmnt.setBinaryStream(3, (InputStream)fis, (int)(image.length()));
				
				int s = psmnt.executeUpdate();
				if(s>0) {
				  System.out.println("image saved successfully !");
				 }
				else {
				System.out.println("unsucessfull to save image.");
				  }
				
				File textfile = new File("files/fileUtilWrt.txt");
				psmnt1 = connection.prepareStatement("insert into save_huge(name, city, image, Phone) "+ "values(?,?,?,?)");
				psmnt1.setString(1,"mahe");
				psmnt1.setString(2,"D");
				psmnt1.setString(4,"123");
				fis = new FileInputStream(textfile);
				psmnt1.setBinaryStream(3, (InputStream)fis, (int)(image.length()));
				int t = psmnt1.executeUpdate();
				if(t>0) {
				  System.out.println("file saved successfully !");
				 }
				else {
				System.out.println("unsucessfull to save image.");
				  }
				 
				  InputStream sImage;
				  psmnt = connection.prepareStatement("SELECT image FROM save_huge WHERE id = ?");
				  psmnt.setString(1, "1"); // here integer number '11' is image id from the table
				  rs = psmnt.executeQuery();
				  InputStream x = rs.getBlob("image").getBinaryStream();
				 
				  int size = x.available();
				  System.out.println("file size: "+size);
				  } catch (Exception ex) {
				ex.printStackTrace();
				}
				finally {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				try {
					psmnt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				  }
			}
			
	public static void getBlob() {
		FileOutputStream image;
		Connection con = null;
		PreparedStatement pstmt = null;
		Statement stmt = null;
		ResultSet res = null;
		StringBuffer query = null;
		try {
			con = getConnection();
			stmt = con.createStatement();
			ResultSet rs = stmt
					.executeQuery("select * from picture where image_id='3'");
			if (rs.next()) {
				Blob test = (Blob) rs.getBlob("image");
				InputStream x = test.getBinaryStream();
				int size = x.available();
				OutputStream out = new FileOutputStream("C:\\anu.jpg");
				byte b[] = new byte[size];
				x.read(b);
				out.write(b);
			}
		} catch (Exception e) {
			System.out.println("Exception :" + e);
		} finally {
			try {
				stmt.close();
				con.close();
			} catch (Exception e) {
				System.out.println(e);
			}
		}

	}
	
	
	public static void validateEmployerEmails(){
		Connection con = getConnection();
	}
	
	public static void saveObjInBlob(){
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	public static void main(String[] args) {
		createTables();
		//getTblColNms();
		//insertImage();
		//saveFile();
		//getBlob();
		//createTablesFromFile();
		//saveObjInBlob();
		
	}

}
