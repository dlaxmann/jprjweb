package com.app.core.conceptual.jdbc;

	import java.sql.DriverManager;
	import java.sql.Connection;
	import java.sql.Statement;
	import java.sql.ResultSet;
	import java.sql.SQLException;

	/**
	 * -----------------------------------------------------------------------------
	 * The following class provides an example of using JDBC to perform a simple
	 * query from an Oracle database.
	 * 
	 * @version 1.0
	 * @author  Jeffrey M. Hunter  (jhunter@idevelopment.info)
	 * @author  http://www.idevelopment.info
	 * -----------------------------------------------------------------------------
	 */

	public class QuerySample {

	    final static String driverClass    = "oracle.jdbc.driver.OracleDriver";
	    final static String connectionURL  = "jdbc:oracle:thin:@localhost:1521:O920NT";
	    final static String userID         = "scott";
	    final static String userPassword   = "tiger";
	    Connection   con                   = null;


	    /**
	     * Construct a QueryExample object. This constructor will create an Oracle
	     * database connection.
	     */
	    public QuerySample() {

	        try {

	            System.out.print("  Loading JDBC Driver  -> " + driverClass + "\n");
	            Class.forName(driverClass).newInstance();

	            System.out.print("  Connecting to        -> " + connectionURL + "\n");
	            this.con = DriverManager.getConnection(connectionURL, userID, userPassword);
	            System.out.print("  Connected as         -> " + userID + "\n");

	        } catch (ClassNotFoundException e) {
	            e.printStackTrace();
	        } catch (InstantiationException e) {
	            e.printStackTrace();
	        } catch (IllegalAccessException e) {
	            e.printStackTrace();
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }

	    }


	    /**
	     * Method to perform a simply query from the "emp" table.
	     */
	    public void performQuery() {

	        Statement stmt      = null;
	        ResultSet rset      = null;
	        String queryString  = "SELECT name, date_of_hire, monthly_salary " +
	                              "FROM   emp " +
	                              "ORDER BY name";

	        try {

	            System.out.print("  Creating Statement...\n");
	            stmt = con.createStatement ();

	            System.out.print("  Opening ResultsSet...\n");
	            rset = stmt.executeQuery(queryString);

	            int counter = 0;
	            
	            while (rset.next()) {
	                System.out.println();
	                System.out.println("  Row [" + ++counter + "]");
	                System.out.println("  ---------------------");
	                System.out.println("      Name             -> " + rset.getString(1));
	                System.out.println("      Date of Hire     -> " + rset.getString(2));
	                System.out.println("      Monthly Salary   -> " + rset.getFloat(3));
	            }

	            System.out.println();
	            System.out.print("  Closing ResultSet...\n");
	            rset.close();

	            System.out.print("  Closing Statement...\n");
	            stmt.close();

	        } catch (SQLException e) {

	            e.printStackTrace();

	        }

	    }


	    /**
	     * Close down Oracle connection.
	     */
	    public void closeConnection() {

	        try {
	            System.out.print("  Closing Connection...\n");
	            con.close();
	            
	        } catch (SQLException e) {
	        
	            e.printStackTrace();
	            
	        }

	    }


	    /**
	     * Sole entry point to the class and application.
	     * @param args Array of String arguments.
	     * @exception java.lang.InterruptedException
	     *            Thrown from the Thread class.
	     */
	    public static void main(String[] args)
	            throws java.lang.InterruptedException {

	        QuerySample qe = new QuerySample();
	        qe.performQuery();
	        qe.closeConnection();

	    }

}
