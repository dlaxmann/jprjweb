package com.app.core.conceptual.jdbc;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.commons.lang3.StringUtils;

public class DbUtil {
	
	
	public void getJskrInCompleteRegistrations(){
		ResultSet rs = null;
		Statement stmt = null;
		Connection con = null;
		long jskrId=0;
		String email="";
		try {
			con = getDevEnvConnection();
			stmt = con.createStatement();
			String query ="select jsk_id,email from jsk_login where jsk_id not in (select jsk_id from jsk_files)";
			rs = stmt.executeQuery(query);
			writeTextToFile(rs);
			/*while(rs.next()) {
				jskrId = rs.getLong("jsk_id");
				email = rs.getString("email");
				System.out.println("jskrId: "+jskrId);
				System.out.println("Email: "+email);
				writeTextToFile(email);
			}*/
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void writeTextToFile(ResultSet rs){
		Writer writer = null;
		File file = new File("dist/incomplemails.txt");
		try {
			writer = new BufferedWriter(new FileWriter(file));
			try {
				while(rs.next()) {
					System.out.println("jskrId: "+rs.getLong("jsk_id"));
					System.out.println("Email: "+rs.getString("email"));
					writer.write(rs.getString("email"));
					writer.write(System.getProperty("line.separator"));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (writer != null) {
					writer.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void deleteMultipleRecFromJskSkillSetExpTable(){
		ResultSet rs = null;
		Statement stmt = null;
		Statement stmt1 = null;
		Connection con = null;
		long jskrId=0;
		long jsk_skillset_experience_id=0;
		String email="";
		try {
			con = getPrdEnvConnection();
			stmt = con.createStatement();
			String query ="select jsk_skillset_experience_id,jsk_id from jsk_skillset_experience group by jsk_id having count(jsk_id)>1";
			rs = stmt.executeQuery(query);
			//writeTextToFile(rs);
			int i=0;
			while(rs.next()) {
				i++;
				jskrId = rs.getLong("jsk_id");
				jsk_skillset_experience_id = rs.getLong("jsk_skillset_experience_id");
				//if(i<5){
				System.out.println("jsk_skillset_experience_id: "+jsk_skillset_experience_id);
				System.out.println("jskrId: "+jskrId+" I Value "+i);
				con = getPrdEnvConnection();
				stmt1 = con.createStatement();
				String query1 ="delete from jsk_skillset_experience where jsk_skillset_experience_id="+jsk_skillset_experience_id;
				stmt1.executeUpdate(query1);
				//email = rs.getString("email");
				//writeTextToFile(email);
				//}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void deleteIncorrectJskrWorkHistoryExpTable(){
		ResultSet rs = null;
		Statement stmt = null;
		Statement stmt1 = null;
		Connection con = null;
		long jskrId=0;
		long jskr_work_hstry_id=0;
		try {
			con = getPrdEnvConnection();
			stmt = con.createStatement();
			String query ="select jskr_work_hstry_id,jsk_id from jskr_work_hstry group by jsk_id having count(jsk_id)>7";
			rs = stmt.executeQuery(query);
			int i=0;
			StringBuffer sbf = new StringBuffer();
			while(rs.next()) {
				i++;
				jskrId = rs.getLong("jsk_id");
				jskr_work_hstry_id = rs.getLong("jskr_work_hstry_id");
				System.out.println("jskr_work_hstry_id: "+jskr_work_hstry_id);
				System.out.println("jskrId: "+jskrId);
				sbf.append(jskr_work_hstry_id+",");
				i=0;
			}
			System.out.println(sbf.toString());
			String ids=StringUtils.substring(sbf.toString(), 0, sbf.toString().length()-1);
			if(!ids.equals("")){
			con = getPrdEnvConnection();
			stmt1 = con.createStatement();
			String query1 ="delete from jskr_work_hstry where jskr_work_hstry_id in ("+ids+");";
			System.out.println(query1);
			stmt1.executeUpdate(query1);
			//con.commit();
			con.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
	
	
	
	
	
	private Connection getDevEnvConnection() throws Exception {
		String username = "root";
		String password = "ldevasani";
		String url = "jdbc:mysql://localhost:3306/localenv1";  //localenv1  //localtest
		Class.forName("com.mysql.jdbc.Driver");
		Connection con = DriverManager.getConnection(url, username, password);
		return con;
	}
	
	private Connection getQAEnvConnection() throws Exception {
		String username = "jumbojob_dldusd";
		String password = "mysql@$#$";
		String url = "jdbc:mysql://103.241.144.191:3306/jumbojob_jeikqad";
		Class.forName("com.mysql.jdbc.Driver");
		Connection con = DriverManager.getConnection(url, username, password);
		return con;
	}
	
	public Connection getPrdEnvConnection() throws Exception {
		String username = "jumbojob-LaXmAN";
		String password = "L1M2hgkUG1n6";
		String url = "jdbc:mysql://182.18.160.167:3306/jumbojob_qzwxzprd";
		Class.forName("com.mysql.jdbc.Driver");
		Connection con = DriverManager.getConnection(url, username, password);
		return con;
	}
	
	
	public void validateEmployerEmails(){
		try {
			String email="";
			Connection con = getPrdEnvConnection();
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("select email from emplr_login_accounts"); 
			while(rs.next()){
				email = rs.getString(1);
				System.out.println(email);
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	
	
	public void insertSitemapURLs(){
		File file = new File("files/sitemap-urls.txt");
        StringBuffer contents = new StringBuffer();
        BufferedReader reader = null;
        BufferedWriter writer = null;
        Connection conn = null;
        
        try {
        	conn = getDevEnvConnection();
        	 String qry = "insert into sitemap_urls (SITEMAP_URL_ID, SITEMAP_URL) values (?, ?)";
        	 PreparedStatement preparedStatement = conn.prepareStatement(qry);
        	
            reader = new BufferedReader(new FileReader(file));
            String text = null;
            int i = 0;
            
            while ((text = reader.readLine()) != null) {
            	if(text.length()>0){
            	//System.out.println(text.length());
            	 preparedStatement.setInt(1, i++);
            	 preparedStatement.setString(2, text.trim());
            	 preparedStatement.executeUpdate();
            	} 
            }
            System.out.println("Done inserted...");
            conn.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
			e.printStackTrace();
		} finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        System.out.println(contents.toString());
	}
	
	
	
	
	
	
	public static void main(String args[]){
		DbUtil util = new DbUtil();
		util.insertSitemapURLs();
		
		//util.validateEmployerEmails();
		//util.getJskrInCompleteRegistrations();
		/*for (int i = 0; i < 20; i++) {
			util.deleteMultipleRecFromJskSkillSetExpTable();
			util.deleteIncorrectJskrWorkHistoryExpTable();
		}*/
		
	}
}

