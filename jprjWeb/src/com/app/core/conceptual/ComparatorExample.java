package com.app.core.conceptual;

import java.util.Arrays;

import com.app.core.app.bean.Person;

public class ComparatorExample {

	public static void main(String args[]){
        Person person[] = new Person[3];

        person[0] = new Person();
        person[0].setAge(35);
        person[0].setName("A");

        person[1] = new Person();
        person[1].setAge(30);
        person[1].setName("B");

        person[2] = new Person();
        person[2].setAge(32);
        person[2].setName("C");

        System.out.println("Order of person before sorting is");
        
        for(int i=0; i < person.length; i++){
            System.out.println( person[i].getName() + "\t" + person[i].getAge());
        }

        Arrays.sort(person, new AgeComparator());
        System.out.println("\n\nOrder of person after sorting by person age is");

        for(int i=0; i < person.length; i++){
            System.out.println( person[i].getName() + "\t" + person[i].getAge());
        }
       }
}
