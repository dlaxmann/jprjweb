package com.app.core.conceptual.collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

public class Enumerate {

	public static void main(String args[]){
		
	List<String> ls = new ArrayList<String>();
	   
    //populate the Vector
	
	ls.add("One");
	ls.add("Two");
	ls.add("six");
	ls.add("five");
	ls.add("Three");
	ls.add("Four");
   
    //Get Enumeration of Vector's elements using elements() method
    Enumeration<String> e =  Collections.enumeration(ls);
   
    /*
      Enumeration provides two methods to enumerate through the elements.
      It's hasMoreElements method returns true if there are more elements to
      enumerate through otherwise it returns false. Its nextElement method returns
      the next element in enumeration.
    */
   
    System.out.println("Elements of the List are : ");
   
    while(e.hasMoreElements())
      System.out.println(e.nextElement());
  }

	

}

