package com.app.frame.springBatch;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.batch.core.JobInstance;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameter.ParameterType;
import org.springframework.batch.core.JobParameters;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * @author Kulasekara Pandiyan S
 *
 */
public class SpringBatchDAOImpl implements SpringBatchDAO {
	private static final Logger logger = LoggerFactory.getLog(SpringBatchDAOImpl.class.getName());
	private DataSource dataSource;
	private String prefix = "";
	
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	
	public List<JobInstance> getJobInstancesByRunDate(String jobName, Date runDate) {
		List<JobInstance> jobInstances = new ArrayList<JobInstance>();
		runDate = com.app.frame.springBatch.DateUtils.addTime(runDate);
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		try {
			StringBuffer query = new StringBuffer();
			query.append(" select bji.JOB_INSTANCE_ID, bji.JOB_NAME, bjp.JOB_INSTANCE_ID, ");
			query.append(" bjp.DATE_VAL from ");
			query.append(prefix);
			query.append("JOB_INSTANCE bji, ");
			query.append(prefix);
			query.append("JOB_PARAMS bjp ");
			query.append(" WHERE bji.JOB_INSTANCE_ID = bjp.JOB_INSTANCE_ID and UPPER(bjp.KEY_NAME) = UPPER(CAST (? AS VARCHAR(20)))");
			query.append("  and bjp.DATE_VAL = CAST (? AS TIMESTAMP) and ");
			query.append(" bji.JOB_NAME = CAST (? AS VARCHAR(20)) order by bji.JOB_INSTANCE_ID desc");

//			String dateStr = StringUtils.getTimestampStr(runDate);
			con = dataSource.getConnection();
			pstmt = con.prepareStatement(query.toString());
			pstmt.setString(1, "runDate");
			pstmt.setTimestamp(2, new Timestamp(runDate.getTime()));
			pstmt.setString(3, jobName);

			rs = pstmt.executeQuery();
			JobInstance instance;;
			List<JobInstance> tmpInstances = new ArrayList<JobInstance>();
			while (rs.next()) {
				instance = new JobInstance(rs.getLong(1), null, rs.getString(2));
				tmpInstances.add(instance);
			}
			
			if (rs != null) rs.close();
			if (pstmt != null) pstmt.close();
			
			query = new StringBuffer();
			
			query.append("SELECT bjp.JOB_INSTANCE_ID, bjp.KEY_NAME, bjp.TYPE_CD, ");
			query.append(" bjp.STRING_VAL, bjp.DATE_VAL, bjp.LONG_VAL, bjp.DOUBLE_VAL from ");
			query.append(prefix);
			query.append("JOB_PARAMS bjp WHERE bjp.JOB_INSTANCE_ID = ?");
			
			pstmt = con.prepareStatement(query.toString());
			for (JobInstance ji : tmpInstances) {
				pstmt.setLong(1, ji.getId());
				rs = pstmt.executeQuery();
				Map<String, JobParameter> map = new HashMap<String, JobParameter>();
				
				while (rs.next()) {
					ParameterType type = ParameterType.valueOf(rs.getString(3));
					JobParameter value = null;
					if (type == ParameterType.STRING) {
						value = new JobParameter(rs.getString(4));
					} else if (type == ParameterType.LONG) {
						value = new JobParameter(rs.getLong(6));
					} else if (type == ParameterType.DOUBLE) {
						value = new JobParameter(rs.getDouble(7));
					} else if (type == ParameterType.DATE) {
						value = new JobParameter(rs.getTimestamp(5));
					}
					map.put(rs.getString(2), value);
				}
				JobParameters jobParameters = new JobParameters(map);
				instance = new JobInstance(ji.getId(), jobParameters, ji.getJobName());
				jobInstances.add(instance);
				
				if (rs != null) rs.close();
				rs = null;
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) rs.close();
				if (pstmt != null) pstmt.close();
				if (con != null) con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return jobInstances;
	}
	
	public List<JobInstance> getGate1JobInstancesByRunDate(String jobName, Date runDate) {
		List<JobInstance> jobInstances = new ArrayList<JobInstance>();
		runDate = com.app.frame.springBatch.DateUtils.addTime(runDate);
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		updateJobExecutionStatus();
		try {
			StringBuffer query = new StringBuffer();
			query.append(" select bji.JOB_INSTANCE_ID, bji.JOB_NAME, bjp.JOB_INSTANCE_ID, ");
			query.append(" bjp.DATE_VAL from ");
			query.append(prefix);
			query.append("JOB_INSTANCE bji, ");
			query.append(prefix);
			query.append("JOB_PARAMS bjp ");
			query.append(" WHERE bji.JOB_INSTANCE_ID = bjp.JOB_INSTANCE_ID and UPPER(bjp.KEY_NAME) = UPPER(CAST (? AS VARCHAR(20)))");
			query.append("  and DATE(bjp.DATE_VAL) = DATE(CAST (? AS TIMESTAMP)) and ");
			query.append(" bji.JOB_NAME = CAST (? AS VARCHAR(20)) order by bji.JOB_INSTANCE_ID desc");

//			String dateStr = StringUtils.getTimestampStr(runDate);
			con = dataSource.getConnection();
			pstmt = con.prepareStatement(query.toString());
			pstmt.setString(1, "runDate");
			pstmt.setTimestamp(2, new Timestamp(runDate.getTime()));
			pstmt.setString(3, jobName);

			rs = pstmt.executeQuery();
			JobInstance instance;;
			List<JobInstance> tmpInstances = new ArrayList<JobInstance>();
			while (rs.next()) {
				instance = new JobInstance(rs.getLong(1), null, rs.getString(2));
				tmpInstances.add(instance);
			}
			
			if (rs != null) rs.close();
			if (pstmt != null) pstmt.close();
			
			query = new StringBuffer();
			
			query.append("SELECT bjp.JOB_INSTANCE_ID, bjp.KEY_NAME, bjp.TYPE_CD, ");
			query.append(" bjp.STRING_VAL, bjp.DATE_VAL, bjp.LONG_VAL, bjp.DOUBLE_VAL from ");
			query.append(prefix);
			query.append("JOB_PARAMS bjp WHERE bjp.JOB_INSTANCE_ID = ?");
			
			pstmt = con.prepareStatement(query.toString());
			for (JobInstance ji : tmpInstances) {
				pstmt.setLong(1, ji.getId());
				rs = pstmt.executeQuery();
				Map<String, JobParameter> map = new HashMap<String, JobParameter>();
				
				while (rs.next()) {
					ParameterType type = ParameterType.valueOf(rs.getString(3));
					JobParameter value = null;
					if (type == ParameterType.STRING) {
						value = new JobParameter(rs.getString(4));
					} else if (type == ParameterType.LONG) {
						value = new JobParameter(rs.getLong(6));
					} else if (type == ParameterType.DOUBLE) {
						value = new JobParameter(rs.getDouble(7));
					} else if (type == ParameterType.DATE) {
						value = new JobParameter(rs.getTimestamp(5));
					}
					map.put(rs.getString(2), value);
				}
				JobParameters jobParameters = new JobParameters(map);
				instance = new JobInstance(ji.getId(), jobParameters, ji.getJobName());
				jobInstances.add(instance);
				
				if (rs != null) rs.close();
				rs = null;
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) rs.close();
				if (pstmt != null) pstmt.close();
				if (con != null) con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return jobInstances;
	}
	
	public List<JobInstance> getJobInstancesByCount(String jobName, int start) {
		List<JobInstance> jobInstances = new ArrayList<JobInstance>();
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		try {
			StringBuffer query = new StringBuffer();
			query.append(" select bji.JOB_INSTANCE_ID, bji.JOB_NAME, bjp.JOB_INSTANCE_ID,");
			query.append(" bjp.DATE_VAL from ");
			query.append(prefix);
			query.append("JOB_INSTANCE bji, ");
			query.append(prefix);
			query.append("JOB_PARAMS bjp ");
			query.append(" WHERE bji.JOB_INSTANCE_ID = bjp.JOB_INSTANCE_ID and bjp.KEY_NAME = CAST (? AS VARCHAR(20))");
			query.append(" and bji.JOB_NAME = CAST (? AS VARCHAR(20)) order by bji.JOB_INSTANCE_ID desc");

			con = dataSource.getConnection();
			pstmt = con.prepareStatement(query.toString());
			pstmt.setString(1, "runDate");
			pstmt.setString(2, jobName);

			rs = pstmt.executeQuery();
			JobInstance instance;
			List<JobInstance> tmpInstances = new ArrayList<JobInstance>();
			int i=0;
			while (rs.next()) {
					if(i==start){
						instance = new JobInstance(rs.getLong(1), null, rs.getString(2));
						tmpInstances.add(instance);
						break;
					}
					i++;
				}
	
			if (rs != null){ 
				rs.close();
			}
			if (pstmt != null) pstmt.close();
			
			query = new StringBuffer();
			
			query.append("SELECT bjp.JOB_INSTANCE_ID, bjp.KEY_NAME, bjp.TYPE_CD, ");
			query.append(" bjp.STRING_VAL, bjp.DATE_VAL, bjp.LONG_VAL, bjp.DOUBLE_VAL from ");
			query.append(prefix);
			query.append("JOB_PARAMS bjp WHERE bjp.JOB_INSTANCE_ID = ?");
			
			pstmt = con.prepareStatement(query.toString());
			for (JobInstance ji : tmpInstances) {
				pstmt.setLong(1, ji.getId());
				rs = pstmt.executeQuery();
				Map<String, JobParameter> map = new HashMap<String, JobParameter>();
				
				while (rs.next()) {
					ParameterType type = ParameterType.valueOf(rs.getString(3));
					JobParameter value = null;
					if (type == ParameterType.STRING) {
						value = new JobParameter(rs.getString(4));
					} else if (type == ParameterType.LONG) {
						value = new JobParameter(rs.getLong(6));
					} else if (type == ParameterType.DOUBLE) {
						value = new JobParameter(rs.getDouble(7));
					} else if (type == ParameterType.DATE) {
						value = new JobParameter(rs.getTimestamp(5));
					}
					map.put(rs.getString(2), value);
				}
				JobParameters jobParameters = new JobParameters(map);
				instance = new JobInstance(ji.getId(), jobParameters, ji.getJobName());
				jobInstances.add(instance);
				
				if (rs != null) rs.close();
				rs = null;
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) rs.close();
				if (pstmt != null) pstmt.close();
				if (con != null) con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return jobInstances;
	}

	/**
	 * This method clears the spring batch tables records 
	 * whose date less than or equal to given date 
	 */
	@Transactional
	public int deleteAllSpringTable(List<Date> dates)
			throws DataAccesException {
		logger.info("Entering the deleteAllSpringTable method");
		
		String BATCH_STEP_EXECUTION_CXT = "delete from "+prefix+"STEP_EXECUTION_CONTEXT sec where sec.STEP_EXECUTION_ID in (select s.STEP_EXECUTION_ID from "+prefix+"STEP_EXECUTION s where s.START_TIME <= CAST (? AS TIMESTAMP))";

		String BATCH_STEP_EXECUTION = "delete from "+prefix+"STEP_EXECUTION s where s.START_TIME <= CAST (? AS TIMESTAMP)";

		String BATCH_JOB_EXECUTION_CONTEXT = "delete from "+prefix+"JOB_EXECUTION_CONTEXT bj where bj.JOB_EXECUTION_ID in (select j.JOB_EXECUTION_ID from "+prefix+"JOB_EXECUTION j where j.START_TIME <= CAST (? AS TIMESTAMP))";

		String BATCH_JOB_PARAMS = "delete from "+prefix+"JOB_PARAMS p where p.JOB_INSTANCE_ID in (select j.JOB_INSTANCE_ID from " +prefix+ "JOB_EXECUTION j where j.START_TIME <= CAST (? AS TIMESTAMP))";

		String BATCH_JOB_EXECUTION = "delete from "+prefix+"JOB_EXECUTION j where j.START_TIME <= CAST (? AS TIMESTAMP)";

		String BATCH_JOB_INSTANCE = "delete from "+prefix+"JOB_INSTANCE bj where bj.JOB_INSTANCE_ID not in (select j.JOB_INSTANCE_ID from "+ prefix+"JOB_EXECUTION j)";
		
		String [] queryArray={BATCH_STEP_EXECUTION_CXT,BATCH_STEP_EXECUTION,BATCH_JOB_EXECUTION_CONTEXT,
				BATCH_JOB_PARAMS,BATCH_JOB_EXECUTION,BATCH_JOB_INSTANCE};

		Connection con = null;
		PreparedStatement pstmt = null;
		Date runDate = com.app.frame.springBatch.DateUtils.addTime(dates.get(0));
		logger.info("The Range of Date selected to delete <:"+runDate);
		int result = 0;
		try {
			con = dataSource.getConnection();
//			con.setAutoCommit(false);
			for (int i = 0; i < queryArray.length; i++) {
				if (i == 5) {
					pstmt = con.prepareStatement(queryArray[i]);
				} else {
					pstmt = con.prepareStatement(queryArray[i]);
					pstmt.setTimestamp(1, new Timestamp(runDate.getTime()));
				}
				result = pstmt.executeUpdate();
				logger.info("queryArray[i]" + queryArray[i] + " :result :"
						+ result);
				if (pstmt != null)
					pstmt.close();
			}
//			con.commit();

		} catch (Exception e) {
			if (con != null) {
//				try {
//					con.rollback();
//				} catch (SQLException e1) {
//					// TODO Auto-generated catch block
//					e1.printStackTrace();
//				}
			}
			e.printStackTrace();
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
				if (con != null)
					con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		logger.info("Exit the deleteAllSpringTable method");
		return result;
	}
	
	/**
	 * Used to updated the Gate1 Job status,exit_code and exit_message 
	 * details by trimming the extra spaces,as it cause so problem in displaying 
	 * job details in the JOB tab screen 
	 */
	
	@Override
	public int updateJobExecutionStatus() {
		// TODO Auto-generated method stub
		Connection con = null;
		PreparedStatement pstmt = null;
		int result = 0;
		try{
			StringBuffer query = new StringBuffer();
			query.append("UPDATE ");
			query.append(prefix);
			query.append("JOB_EXECUTION je SET je.STATUS=LTRIM(RTRIM(je.STATUS)),");
			query.append("je.EXIT_CODE=LTRIM(RTRIM(je.EXIT_CODE)),");
			query.append("je.EXIT_MESSAGE=LTRIM(RTRIM(je.EXIT_MESSAGE)) ");
			query.append("WHERE je.JOB_INSTANCE_ID IN (");
			query.append("SELECT sje.JOB_INSTANCE_ID FROM ");
			query.append(prefix);
			query.append("JOB_EXECUTION sje WHERE sje.STATUS LIKE '% %' ");
			query.append("AND sje.JOB_INSTANCE_ID IN (");
			query.append("SELECT DISTINCT ji.JOB_INSTANCE_ID FROM ");
			query.append(prefix);
			query.append("JOB_INSTANCE ji WHERE ji.JOB_NAME='COS2COSSTJOB'))");

			con = dataSource.getConnection();
			pstmt = con.prepareStatement(query.toString());
			result = pstmt.executeUpdate();
				if (pstmt != null)
					pstmt.close();
		}catch(Exception ex){
			try{
				ex.printStackTrace();
				if (con != null) {
					con.close();
				}	
			}catch (SQLException e){
				e.printStackTrace();
			}
		}finally {
			try {
				if (pstmt != null)
					pstmt.close();
				if (con != null)
					con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}
	
}
