package com.app.frame.springBatch;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;



public class DateUtils {
	public static Date truncateTime(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.set(Calendar.MILLISECOND, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.HOUR_OF_DAY, 0);
		
		return c.getTime();
	}
	
	public static Calendar truncateTime(Calendar c) {
		c.set(Calendar.MILLISECOND, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.HOUR_OF_DAY, 0);
		
		return c;
	}
	
	public static Date addTime(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.set(Calendar.MILLISECOND, c.getActualMaximum(Calendar.MILLISECOND));
		c.set(Calendar.SECOND, c.getActualMaximum(Calendar.SECOND));
		c.set(Calendar.MINUTE, c.getActualMaximum(Calendar.MINUTE));
		c.set(Calendar.HOUR_OF_DAY, c.getActualMaximum(Calendar.HOUR_OF_DAY));
		
		return c.getTime();
	}
	
	public static long diffDays(Calendar c1, Calendar c2) {
		long milliseconds1 = c1.getTimeInMillis();
		long milliseconds2 = c2.getTimeInMillis();
		long diff = milliseconds2 - milliseconds1;
		long diffDays = diff / (24 * 60 * 60 * 1000);
		
		return diffDays;
	}
	
	public static long daysBetweenDates(Date date1, Date date2) {
		long milliseconds1 = truncateTime(date1).getTime();
		long milliseconds2 = truncateTime(date2).getTime();
		long diff = milliseconds2 - milliseconds1;
		long diffDays = diff / (24 * 60 * 60 * 1000);
		
		return diffDays;
	}
	
	public static void timings(){
		Date d = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(d);
		c.add(Calendar.MONTH,0);
		Date d1;
		Date d2;
		d1 = c.getTime();
		
		System.out.println("***___"+d1);
		
		Calendar c1 = Calendar.getInstance();
		c1.setTime(d);
		c1.add(Calendar.YEAR,-1);
		
		d2 = c1.getTime();
		System.out.println("***___"+d2);
	}
	
	public static void getCR213Dates(){
		Date runDate = new Date();
		
		    Calendar c2=Calendar.getInstance();
		    int fstDate = c2.getActualMinimum(Calendar.DATE);
		    c2.add(c2.YEAR, -1);
		    c2.set(Calendar.MONTH, 6);
		    c2.set(Calendar.DATE, fstDate);
		    SimpleDateFormat sdf2=new SimpleDateFormat("dd-MMMMM-yyyy");
		    System.out.println(sdf2.format(c2.getTime()));
		    
		 	Calendar c1=Calendar.getInstance();
		    int lastDate = c1.getActualMaximum(Calendar.DATE);
		    c1.add(c1.YEAR, -1);
		    c1.set(Calendar.DATE, lastDate);
		    SimpleDateFormat sdf1=new SimpleDateFormat("dd-MMMMM-yyyy");
		    System.out.println(sdf1.format(c1.getTime()));
		    
		    Calendar cc = Calendar.getInstance();
		   // int lastDate0 = cc.getActualMaximum(Calendar.DATE);
			cc.add(cc.YEAR, -1);
			cc.add(cc.MONTH, 6);
			cc.set(Calendar.DATE, 30);
			System.out.println(cc.getTime());
			 SimpleDateFormat sdf100=new SimpleDateFormat("dd-MMMMM-yyyy");
			 System.out.println("..."+sdf100.format(cc.getTime()));
			 
		/*    Calendar c12=Calendar.getInstance();
		    int lastDate0 = c12.getActualMaximum(Calendar.DATE);
		    c12.add(Calendar.YEAR,0);
		    c12.set(Calendar.JUNE,0);
		    c12.set(Calendar.DATE, lastDate0);
		    SimpleDateFormat sdf100=new SimpleDateFormat("dd-MMMMM-yyyy");
		    System.out.println("..."+sdf100.format(c12.getTime()));*/
		
		    Calendar c3=Calendar.getInstance();
		    int fstDate3 = c3.getActualMinimum(Calendar.DATE);
		    c3.set(Calendar.MONTH, 0);
		    c3.set(Calendar.DATE, fstDate3);
		    SimpleDateFormat sdf3=new SimpleDateFormat("dd-MMMMM-yyyy");
		    System.out.println(sdf3.format(c3.getTime()));
		    
		    Calendar c4=Calendar.getInstance();
		    int lastDate4 = c4.getActualMaximum(Calendar.DATE);
		    c4.set(Calendar.MONTH, 5);
		    c4.set(Calendar.DATE, lastDate4);
		    SimpleDateFormat sdf4=new SimpleDateFormat("dd-MMMMM-yyyy");
		    System.out.println(sdf4.format(c4.getTime()));
		
	}
	
	public static void getCR213Dates1(){
		
		Calendar c2=Calendar.getInstance();
	    int fstDate = c2.getActualMinimum(Calendar.DATE);
	    c2.add(c2.YEAR, -1);
	    c2.set(Calendar.MONTH, 6);
	    c2.set(Calendar.DATE, fstDate);
	    System.out.println(c2.getTime());
	    //fromDate = DownstreamUtils.getCommonCloseDate(c2.getTime());
	    
	 	Calendar c1=Calendar.getInstance();
	    int lastDate = c1.getActualMaximum(Calendar.DATE);
	    c1.add(c1.YEAR, -1);
	    c1.set(Calendar.DATE, lastDate);
	    System.out.println(c1.getTime());
	    //endDate = DownstreamUtils.getCommonCloseDate(c1.getTime());
	}
	
	
	
	
	public static void main(String[] args) {
		/*Calendar c1 = Calendar.getInstance();
		Calendar c2 = Calendar.getInstance();
		c2.add(Calendar.DAY_OF_MONTH, 3);
		
		System.out.println("Diff Days: " + DateUtils.diffDays(c1, c1));
		System.out.println("Diff Days: " + DateUtils.diffDays(c1, c2));
		System.out.println("Diff Days: " + DateUtils.diffDays(c2, c1));
		
		Calendar c3 = Calendar.getInstance();
		Date d = c3.getTime();
		System.out.println("Before: " + d);
		d = DateUtils.truncateTime(d);
		System.out.println("After : " + d);
		
		d = DateUtils.addTime(d);
		System.out.println("After adding time: " + d);
		
		
		Date runDate = new Date();
		Calendar cq = Calendar.getInstance();
		cq.setTime(runDate);
		cq.add(Calendar.MONTH, -3);
		cq.add(Calendar.DATE, +1);
		Date prevCloseDate = cq.getTime();
		System.out.println("After adding time: " + prevCloseDate);
	
			Date runDate1 = new Date();
		System.out.println(runDate1);
		Calendar c = Calendar.getInstance();
		c.setTime(runDate1);
		c.add(Calendar.MONTH, -1);
		c.add(Calendar.DATE, +1);
		System.out.println(c.getTime());
		
		timings();*/
		
		getCR213Dates();
		
	}
}
