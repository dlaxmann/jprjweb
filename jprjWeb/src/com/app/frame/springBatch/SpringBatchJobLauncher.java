package com.app.frame.springBatch;

import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class SpringBatchJobLauncher extends QuartzJobBean {
	private static final Logger log = LoggerFactory.getLog(SpringBatchJobLauncher.class.getName());
	private SpringBatchService service;
	
	protected void executeInternal(JobExecutionContext context)
		throws JobExecutionException {
		Map<String, Object> jobDataMap = context.getMergedJobDataMap();
		String jobName = (String) jobDataMap.get("jobName");
		//Boolean dayAfterClose = (Boolean) jobDataMap.get("dayAfterClose");
		String dayAfterClose = (String)jobDataMap.get("dayAfterClose");
		String dayClose = (String)jobDataMap.get("dayClose");
		
		boolean triggerJob = false;
		Calendar now = Calendar.getInstance();
		Date runDate = DateUtils.truncateTime(now.getTime());
		Date closeDate = null;
		if(dayAfterClose != null && dayAfterClose.trim().equals("true")){
		
		if(jobName != null && jobName.trim().equals("CMW090FinalJob")){
			int dayOfWeek = now.get(Calendar.DAY_OF_WEEK);
			int dayOfMonth = now.get(Calendar.DAY_OF_MONTH);
			if ((dayOfWeek == Calendar.MONDAY || dayOfWeek == Calendar.TUESDAY
					|| dayOfWeek == Calendar.WEDNESDAY
					|| dayOfWeek == Calendar.THURSDAY || dayOfWeek == Calendar.FRIDAY)
					&& (dayOfMonth == 1 || dayOfMonth == 15)) {
				triggerJob = true;
			} else if (dayOfWeek == Calendar.MONDAY
					&& (dayOfMonth == 2 || dayOfMonth == 16 || dayOfMonth == 3 || dayOfMonth == 17)) {
				triggerJob = true;
			} else {
				triggerJob = false;
			}
		}
		
		if (triggerJob) {
			try {
				
				String region="";   // = StringUtils.getRegion(runDate);
				Map<String, Object> parameters = new LinkedHashMap<String, Object>();
				
				if(jobName != null && jobName.trim().equals("CMW921Job")){
					Calendar c = Calendar.getInstance();
					c.setTime(runDate);
					c.add(Calendar.MONTH, -1);
					c.add(Calendar.DATE, +1);
					Date prevCloseDate=null; // = DownstreamUtils.getCommonCloseDate(c.getTime());
					parameters.put("fromDate", prevCloseDate);
					parameters.put("toDate", closeDate);
				}
				parameters.put("runDate", com.app.frame.springBatch.DateUtils.addTime(runDate));
				parameters.put("region", region);
				service.startJob(jobName, parameters, true);
			} catch (Exception e) {
				log.error("An error occured while invoking job: " + jobName);
				e.printStackTrace();
			}
		}}
	}

	public SpringBatchService getService() {
		return service;
	}

	public void setService(SpringBatchService service) {
		this.service = service;
	}
}
