package com.app.frame.springBatch;

import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.app.frame.springBatch.bean.DynamicJobParameters;

public class Main {
 private static Logger log = Logger.getLogger(Main.class);
 
	
	    public static void main(String[] args) throws JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException, JobParametersInvalidException {
	        
	    	ApplicationContext context = new ClassPathXmlApplicationContext("/applicationContext.xml");
	    	
	    	JobLauncher jobLauncher = (SimpleJobLauncher) context.getBean("jobLauncher");
			log.info("************* jobLauncher created" +jobLauncher);
	    	
	    	Job compositeJob = (Job) context.getBean("compositeItemReaderJob");
	    	System.out.println("************* Job Started...");
	    	
	    	Calendar c1 = Calendar.getInstance();
	    	JobParameters jps = new JobParametersBuilder().addDate("", new Date()).toJobParameters();
	    	log.info("************* job Launching with the JobParameters = " +jps);
			JobExecution jobExecution = jobLauncher.run(compositeJob, jps);
			
			
			Calendar c2 = Calendar.getInstance();
			System.out.println("###### Job Finished Time taken to Run: " + (c2.getTimeInMillis() - c1.getTimeInMillis()));

	        
	    }

}
