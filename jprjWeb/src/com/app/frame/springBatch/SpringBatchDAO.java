package com.app.frame.springBatch;

import java.util.Date;
import java.util.List;

import org.springframework.batch.core.JobInstance;

public interface SpringBatchDAO {
	public List<JobInstance> getJobInstancesByRunDate(String jobName, Date runDate);
	public List<JobInstance> getGate1JobInstancesByRunDate(String jobName, Date runDate);
	public int deleteAllSpringTable(List<Date> dates) throws DataAccesException;
	public List<JobInstance> getJobInstancesByCount(String jobName, int start);
	public int updateJobExecutionStatus();
}
