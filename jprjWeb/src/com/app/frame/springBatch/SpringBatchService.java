package com.app.frame.springBatch;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobInstance;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.JobOperator;
import org.springframework.batch.core.launch.NoSuchJobException;
import org.springframework.batch.core.launch.NoSuchJobExecutionException;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.batch.core.repository.dao.JobExecutionDao;
import org.springframework.batch.core.repository.dao.JobInstanceDao;

import com.frame.util.AppUtils;


/**
 * This service class provides methods to start/restart and
 * get the status of the job. Any clients that need information about the job
 * or to trigger job should use this service.
 * 
 * @author Sai Krishna Vijay Kumar Pitchika
 * @version 1.0
 */

public class SpringBatchService {
	private static final Logger log = LoggerFactory.getLog(SpringBatchService.class.getName());
	private JobInstanceDao jobInstanceDao;
	private JobExecutionDao jobExecutionDao;
	private JobLauncher jobLauncher;
	private JobOperator jobOperator;
	private SpringBatchDAO springBatchDAO;
	
	/**
	 * A valid reference to JobInstanceDao is required.
	 * @param jobInstanceDao
	 */
	public void setJobInstanceDao(JobInstanceDao jobInstanceDao) {
		this.jobInstanceDao = jobInstanceDao;
	}

	/**
	 * A valid reference to JobExecutionDao is required.
	 * @param jobExecutionDao
	 */
	public void setJobExecutionDao(JobExecutionDao jobExecutionDao) {
		this.jobExecutionDao = jobExecutionDao;
	}

	/**
	 * A valid reference to JobLauncher is required.
	 * @param jobLauncher
	 */
	public void setJobLauncher(JobLauncher jobLauncher) {
		this.jobLauncher = jobLauncher;
	}

	/**
	 * A valid reference to JobOperator is required.
	 * @param jobOperator
	 */
	public void setJobOperator(JobOperator jobOperator) {
		this.jobOperator = jobOperator;
	}
	
	/**
	 * A valid reference to SpringBatchDAO is required.
	 * @param springBatchDAO
	 */
	public void setSpringBatchDAO(SpringBatchDAO springBatchDAO) {
		this.springBatchDAO = springBatchDAO;
	}
	
	/**
	 * Utility method to convert map of parameters to Spring batch
	 * parameters. 
	 * @param parameters
	 * @return
	 */
	private JobParameters getJobParameters(Map<String, Object> parameters) {
		JobParametersBuilder builder = new JobParametersBuilder();
		
		for(String key: parameters.keySet()) {
			Object parameter = parameters.get(key);
			if (parameter instanceof String) {
				builder.addString(key, (String) parameter);
			} else if (parameter instanceof Integer || parameter instanceof Long) {
				builder.addLong(key, ((Number) parameter).longValue());
			} else if (parameter instanceof Float || parameter instanceof Double) {
				builder.addDouble(key, ((Number) parameter).doubleValue());
			} else if (parameter instanceof Date) {
				builder.addDate(key, (Date) parameter);
			} else if (parameter instanceof Boolean) {
				builder.addString(key, parameter.toString());
			}
		}
		
		return builder.toJobParameters();
	}
	
	/**
	 * Utility method to start a job.
	 * @param jobName name of the job that is configured in spring application context.
	 * @param parameters list of parameters as map
	 * @param newInstance true forces to create new job instance 
	 * @return
	 * @throws JobExecutionAlreadyRunningException
	 * @throws JobRestartException
	 * @throws JobInstanceAlreadyCompleteException
	 * @throws JobParametersInvalidException
	 */
	public JobExecution startJob(String jobName, Map<String, Object> parameters,
				boolean newInstance) throws 
							JobExecutionAlreadyRunningException, 
							JobRestartException, 
							JobInstanceAlreadyCompleteException, 
							JobParametersInvalidException {
		
		Job job = (Job) AppUtils.getBean(jobName);
		
		if (newInstance) {
			parameters.put("run.time", Calendar.getInstance().getTimeInMillis());
		}
		
		JobParameters jobParameters = getJobParameters(parameters);
		JobExecution jobExecution = jobLauncher.run(job, jobParameters);
		
		return jobExecution;
	}
	
	/**
	 * Returns job execution for a given job name.
	 * @param jobName name of the job.
	 * @return
	 */
	public JobExecution getRecentJobExecution(String jobName) {
		List<JobInstance> jobInstances = jobInstanceDao.getJobInstances(jobName, 0, 1);
		JobExecution jobExecution = null;
		if (jobName.trim().equalsIgnoreCase(SpringBatchConstants.JOBNAME)) {
			springBatchDAO.updateJobExecutionStatus();
		}
		if (jobInstances.size() > 0) {
			jobExecution = jobExecutionDao.getLastJobExecution(jobInstances.get(0));
		}
	
		return jobExecution;
	}
	
	/**
	 * Returns requested number of recent job instances if any 
	 * @param jobName name of the job
	 * @param count number of job instances in descending order
	 * @return empty list if no job instances found.
	 */
	public List<JobInstance> getRecentJobInstances(String jobName, int count) {
		List<JobInstance> jobInstances = jobInstanceDao.getJobInstances(jobName, 0, count);
		return jobInstances;
	}
	
	/**
	 * Returns job execution by run date.
	 * @param jobName name of the job
	 * @param runDate 
	 * @return
	 */
	public JobExecution getJobExecutionByRunDate(String jobName, Date runDate) {
		//runDate = com.farmers.comm.cds.utils.DateUtils.truncateTime(runDate);
		runDate = com.app.frame.springBatch.DateUtils.addTime(runDate);
		List<JobInstance> jobInstances = springBatchDAO.getJobInstancesByRunDate(jobName, runDate);
		JobExecution jobExecution = null;

		for (JobInstance jobInstance: jobInstances) {
			jobExecution = jobExecutionDao.getLastJobExecution(jobInstance);
			break;
		}
		
		return jobExecution;
	}
	
	public boolean getGate1JobExecutionByRunDate(String jobName, Date runDate) {
		//runDate = com.farmers.comm.cds.utils.DateUtils.truncateTime(runDate);
		runDate = com.app.frame.springBatch.DateUtils.addTime(runDate);
		List<JobInstance> jobInstances = springBatchDAO.getGate1JobInstancesByRunDate(jobName, runDate);
		if (jobInstances.size() > 0) {
			JobExecution je = getJobExecution(jobInstances.get(0));
			if (je != null && je.getExitStatus() != null && ExitStatus.COMPLETED.getExitCode().equalsIgnoreCase(je.getExitStatus().getExitCode())) {
					return true;
			} else if (je != null) {
				if(je.getExitStatus() != null) 
					log.error("Gate 1 job exit status: " + je.getExitStatus().getExitCode() + " for run date: " + runDate);
				else
					log.error("Gate 1 job exit status: Job Execution Exit Status is null for run date: " + runDate);
			}
		}
		
		return false;
	}
	public JobExecution getJobExecution(JobInstance jobInstance) {
		return jobExecutionDao.getLastJobExecution(jobInstance);
	}
	
	public JobExecution getJobExecution(long jobExecutionId) {
		return jobExecutionDao.getJobExecution(jobExecutionId);
	}
	
	public JobExecution restartRecentJob(String jobName) throws JobInstanceAlreadyCompleteException, 
							NoSuchJobExecutionException, 
							NoSuchJobException, JobRestartException, 
							JobParametersInvalidException {
		
		JobExecution jobExecution = getRecentJobExecution(jobName);
		jobOperator.restart(jobExecution.getId());
		
		return jobExecution;
	}
	
	public long restartJob(JobExecution jobExecution) throws JobInstanceAlreadyCompleteException, 
							NoSuchJobExecutionException, 
							NoSuchJobException, JobRestartException, 
							JobParametersInvalidException {
		
		return jobOperator.restart(jobExecution.getId());
	}
	
	public JobExecution startOrRestartJob(String jobName, Map<String, Object> parameters) throws 
							JobExecutionAlreadyRunningException, 
							JobRestartException, 
							JobInstanceAlreadyCompleteException, 
							JobParametersInvalidException, 
							NoSuchJobExecutionException, NoSuchJobException {
		
		JobExecution jobExecution = getRecentJobExecution(jobName); 
		if (jobExecution == null || (jobExecution.getStatus() == BatchStatus.COMPLETED)) {
			jobExecution = startJob(jobName, parameters, false);
		} else if (jobExecution.getStatus() == BatchStatus.STOPPED ||
				jobExecution.getStatus() == BatchStatus.FAILED) {
			System.out.println("Previous Job run: " + jobExecution.getStatus());
			jobOperator.restart(jobExecution.getId());
		}
		
		return jobExecution;
	}
}
