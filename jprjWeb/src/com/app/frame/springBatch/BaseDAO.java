package com.app.frame.springBatch;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.hibernate.SessionFactory;

public interface BaseDAO {
	public void setSessionFactory(SessionFactory sessionFactory);
	public SessionFactory getSessionFactory();
	public void save(Object object) throws DataAccesException;
	public void save(List items) throws DataAccesException;
	public void saveOrUpdate(Object object) throws DataAccesException;
	public void delete(Object object) throws DataAccesException;
	public Object getObject(Class clazz, Serializable id) throws DataAccesException;
	public List getList(String hqlQuery, List<Object> parameters) throws DataAccesException;
	public List getList(String hqlQuery, Map<String, Object> parameters) throws DataAccesException;
	public List getListByNamedQuery(String queryName, List<Object> parameters) throws DataAccesException;
	public List getListByNamedQuery(String queryName, Map<String, Object> parameters) throws DataAccesException;
}
