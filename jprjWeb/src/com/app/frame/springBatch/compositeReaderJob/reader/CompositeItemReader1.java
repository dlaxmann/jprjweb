package com.app.frame.springBatch.compositeReaderJob.reader;

import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.support.CompositeItemStream;



public class CompositeItemReader1 extends CompositeItemStream{
	
	private static Logger log = Logger.getLogger(CompositeItemReader1.class);
	private StepExecution stepExecution;
 
	
	private List delegates;
	 
	public void setDelegates(ItemReader[] delegates) {
	this.delegates = Arrays.asList(delegates);
	log.info("***** In CompositeItemReader Created readers" +Arrays.asList(delegates).size());
	}


	public Object read() throws Exception, UnexpectedInputException, ParseException {
		
		JdbcCursorItemReader  read1 = (JdbcCursorItemReader) delegates.get(0);
		JdbcCursorItemReader  read2 = (JdbcCursorItemReader) delegates.get(1);
		JdbcCursorItemReader  read3 = (JdbcCursorItemReader) delegates.get(2);
		
		log.info("***** In CompositeItemReader Created readers" +read1);
		log.info("***** In CompositeItemReader Created readers" +read2);
		log.info("***** In CompositeItemReader Created readers" +read3);
		
		
		if( true ) // let's say batch process has to call reader1		
		{
			log.info("***** In CompositeItemReader read1");
			log.info("reading line "+(stepExecution.getReadCount()+1));
			return read1.read();
		}
		//let's say batch process has to call reader2
		else if(true) 
		{
			log.info("***** In CompositeItemReader read2");
			return read2.read();
		}
		else  //let's say batch process has to call reader3
		{
			log.info("***** In CompositeItemReader read3");
			return read3.read();
		}
		
	}
	
	
	
	
}
