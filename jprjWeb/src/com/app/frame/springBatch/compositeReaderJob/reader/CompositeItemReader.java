package com.app.frame.springBatch.compositeReaderJob.reader;

import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.batch.item.database.HibernateCursorItemReader;
import org.springframework.batch.item.support.CompositeItemStream;

import com.app.frame.springBatch.bean.Address;
import com.app.frame.springBatch.bean.Location;
import com.app.frame.springBatch.bean.PLDomain;
import com.app.frame.springBatch.bean.Policy;



//public class CompositeItemReader extends AbstractItemStreamItemReader{

public class CompositeItemReader extends CompositeItemStream implements ItemReader{
	
//	public class CompositeItemReader extends AbstractItemStreamItemReader {
	
	private static Logger log = Logger.getLogger(CompositeItemReader.class);
	private StepExecution stepExecution;
 
	
	private List delegates;
	 
	public void setDelegates(ItemReader[] delegates) {
	this.delegates = Arrays.asList(delegates);
	log.info("***** In CompositeItemReader Created number of readers =  " +Arrays.asList(delegates).size());
	}


	@SuppressWarnings("unused")
	public Object read() throws Exception, UnexpectedInputException, ParseException {
		
		HibernateCursorItemReader  read1 = (HibernateCursorItemReader) delegates.get(0);
		HibernateCursorItemReader  read2 = (HibernateCursorItemReader) delegates.get(1);
		HibernateCursorItemReader  read3 = (HibernateCursorItemReader) delegates.get(2);
		
		PLDomain pdmn = new PLDomain();
		log.info("***** In CompositeItemReader Created readers read1 = " +read1);
		log.info("***** In CompositeItemReader Created readers read2 = " +read2);
		log.info("***** In CompositeItemReader Created readers read3 = " +read3);
		
		
		
		Policy pl = (Policy) read1.read();
		pdmn.setPolicy(pl);

		Location lo = (Location) read2.read();
		pdmn.setLocation(lo);

		Address add = (Address) read3.read();
		pdmn.setAddress(add);
	
	
	return pdmn;
	}
}



/*PLDomain pdmn = null;
//Policy pl =		 (Policy) read1.read();
//log.info("************ In CompositeItemReader Created readers read3 = " +stepExecution.getReadCount());
//Location lo = (Location)read2.read();
//log.info("************ In CompositeItemReader Created readers read3 = " +stepExecution.getReadCount());
//Address add = (Address)read3.read();
//log.info("************ In CompositeItemReader Created readers read3 = " +stepExecution.getReadCount());

//pdmn.setPolicy(pl);
//pdmn.setLocation(lo);
//pdmn.setAddress(add);
//read3.read();

log.info("***** In CompositeItemReader read1");
Policy pl =		 (Policy) read1.read();
Location lo = (Location)read2.read();
if( true ) // let's say batch process has to call reader1		
{
	log.info("***** In CompositeItemReader read1");
	Policy pl =		 (Policy) read1.read();
	Location lo = (Location)read2.read();
	Address add = (Address)read3.read();
	
	//pdmn.setPolicy(pl);
	//pdmn.setLocation(lo);
	//pdmn.setAddress(add);
	
	//log.info("reading line "+(stepExecution.getReadCount()+1));
}
//let's say batch process has to call reader2
else if(true) 	{
	
	log.info("***** In CompositeItemReader read2");
	return read2.read();
}
else  //let's say batch process has to call reader3
{
	log.info("***** In CompositeItemReader read3");
	//return read3.read();
}

pdmn.setPolicy(pl);
pdmn.setLocation(lo);

return pdmn;*/

