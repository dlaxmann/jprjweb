package com.app.frame.springBatch.compositeReaderJob.processor;

import org.apache.log4j.Logger;
import org.springframework.batch.item.ItemProcessor;

import com.app.frame.springBatch.bean.Address;
import com.app.frame.springBatch.bean.Location;
import com.app.frame.springBatch.bean.PLDomain;
import com.app.frame.springBatch.bean.Policy;


public class PolicyProcessor implements ItemProcessor<PLDomain, PLDomain> {

	private static Logger log = Logger.getLogger(PolicyProcessor.class);
	
	
		public PLDomain process(PLDomain policy) throws Exception {
			
			log.info("***** In PolicyProcessor process() " +policy);
				if(policy == null ){ 
					return null;
					}
				
				Policy pls= policy.getPolicy();
				Location ls = policy.getLocation();
				Address ads = policy.getAddress();
				
				log.info("***** In PolicyProcessor process() policyNumber =  " +pls.getPolicyNumber());
				log.info("***** In PolicyProcessor process() locatinId = " +ls.getLocatinId());
				log.info("***** In PolicyProcessor process() addressId = " +ads.getAddressId());
				
				PLDomain polsy = new PLDomain();
				polsy.setPolicyNumber(pls.getPolicyNumber());
				polsy.setPolicyEffDate(pls.getPolicyEffDate());
				polsy.setAppliedDateStamp(pls.getAppliedDateStamp());

				polsy.setLocatinId(ls.getLocatinId());
				polsy.setLocationEffTimeStamp(ls.getLocationEffTimeStamp());
				polsy.setLocationExpTimeStamp(ls.getLocationExpTimeStamp());
				
				
				polsy.setAddressId(ads.getAddressId());
				polsy.setAddressIdentifier(ads.getAddressIdentifier());
				
				
				log.info("***** In PolicyProcessor process() " +policy);
				return polsy;
				
		}

}

