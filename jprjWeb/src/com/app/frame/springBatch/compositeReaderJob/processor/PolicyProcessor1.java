package com.app.frame.springBatch.compositeReaderJob.processor;

import org.apache.log4j.Logger;
import org.springframework.batch.item.ItemProcessor;

import com.app.frame.springBatch.bean.Policy;


public class PolicyProcessor1 implements ItemProcessor<Policy, Policy> {

	private static Logger log = Logger.getLogger(PolicyProcessor1.class);
	
	
		public Policy process(Policy policy) throws Exception {
			
			log.info("***** In PolicyProcessor process() " +policy);
				if(policy == null ){ 
					return null;
					}
				
				Policy polsy = new Policy();
				polsy.setPolicyNumber(policy.getPolicyNumber());
				polsy.setAppliedDateStamp(policy.getAppliedDateStamp());
				polsy.setPolicyEffDate(policy.getPolicyEffDate());
				log.info("***** In PolicyProcessor process() " +policy);
				return polsy;
				
		}

}
