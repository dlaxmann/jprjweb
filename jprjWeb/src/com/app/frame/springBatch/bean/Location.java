package com.app.frame.springBatch.bean;

import java.util.Date;

public class Location {

	
	private int locatinId; 
	private int policyNumber; 
	private String locationIdentifier;
	private Date locationEffTimeStamp; 
	private Date locationExpTimeStamp;
	private Date locationAppliedTimeStamp;
	
	
	public int getLocatinId() {
		return locatinId;
	}
	public void setLocatinId(int locatinId) {
		this.locatinId = locatinId;
	}
	public int getPolicyNumber() {
		return policyNumber;
	}
	public void setPolicyNumber(int policyNumber) {
		this.policyNumber = policyNumber;
	}
	public String getLocationIdentifier() {
		return locationIdentifier;
	}
	public void setLocationIdentifier(String locationIdentifier) {
		this.locationIdentifier = locationIdentifier;
	}
	public Date getLocationEffTimeStamp() {
		return locationEffTimeStamp;
	}
	public void setLocationEffTimeStamp(Date locationEffTimeStamp) {
		this.locationEffTimeStamp = locationEffTimeStamp;
	}
	public Date getLocationExpTimeStamp() {
		return locationExpTimeStamp;
	}
	public void setLocationExpTimeStamp(Date locationExpTimeStamp) {
		this.locationExpTimeStamp = locationExpTimeStamp;
	}
	public Date getLocationAppliedTimeStamp() {
		return locationAppliedTimeStamp;
	}
	public void setLocationAppliedTimeStamp(Date locationAppliedTimeStamp) {
		this.locationAppliedTimeStamp = locationAppliedTimeStamp;
	}
	
	
	
	
	
	
	

}
