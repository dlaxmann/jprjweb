package com.app.frame.springBatch.bean;

import java.util.Date;
import java.util.List;

public class PLDomain {

	
	private int policyNumber; 
	private Date policyEffDate; 
	private Date appliedDateStamp;
	private int locatinId; 
	private String locationIdentifier;
	private Date locationEffTimeStamp; 
	private Date locationExpTimeStamp;
	private Date locationAppliedTimeStamp;
	private int addressId; 
	private String addressIdentifier;
	private Date addressEffTimeStamp; 
	private Date addressExpTimeStamp;
	private Date addressAppliedTimeStamp;
	
	List<Policy> policyList;
	List<Location> locationList;
	List<Address> addressList;
	
	Policy policy;
	Location location;
	Address address;
	
	public Policy getPolicy() {
		return policy;
	}
	public void setPolicy(Policy policy) {
		this.policy = policy;
	}
	public Location getLocation() {
		return location;
	}
	public void setLocation(Location location) {
		this.location = location;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	
	
	public int getPolicyNumber() {
		return policyNumber;
	}
	public void setPolicyNumber(int policyNumber) {
		this.policyNumber = policyNumber;
	}
	public Date getPolicyEffDate() {
		return policyEffDate;
	}
	public void setPolicyEffDate(Date policyEffDate) {
		this.policyEffDate = policyEffDate;
	}
	public Date getAppliedDateStamp() {
		return appliedDateStamp;
	}
	public void setAppliedDateStamp(Date appliedDateStamp) {
		this.appliedDateStamp = appliedDateStamp;
	}
	public int getLocatinId() {
		return locatinId;
	}
	public void setLocatinId(int locatinId) {
		this.locatinId = locatinId;
	}
	public String getLocationIdentifier() {
		return locationIdentifier;
	}
	public void setLocationIdentifier(String locationIdentifier) {
		this.locationIdentifier = locationIdentifier;
	}
	public Date getLocationEffTimeStamp() {
		return locationEffTimeStamp;
	}
	public void setLocationEffTimeStamp(Date locationEffTimeStamp) {
		this.locationEffTimeStamp = locationEffTimeStamp;
	}
	public Date getLocationExpTimeStamp() {
		return locationExpTimeStamp;
	}
	public void setLocationExpTimeStamp(Date locationExpTimeStamp) {
		this.locationExpTimeStamp = locationExpTimeStamp;
	}
	public Date getLocationAppliedTimeStamp() {
		return locationAppliedTimeStamp;
	}
	public void setLocationAppliedTimeStamp(Date locationAppliedTimeStamp) {
		this.locationAppliedTimeStamp = locationAppliedTimeStamp;
	}
	public int getAddressId() {
		return addressId;
	}
	public void setAddressId(int addressId) {
		this.addressId = addressId;
	}
	public String getAddressIdentifier() {
		return addressIdentifier;
	}
	public void setAddressIdentifier(String addressIdentifier) {
		this.addressIdentifier = addressIdentifier;
	}
	public Date getAddressEffTimeStamp() {
		return addressEffTimeStamp;
	}
	public void setAddressEffTimeStamp(Date addressEffTimeStamp) {
		this.addressEffTimeStamp = addressEffTimeStamp;
	}
	public Date getAddressExpTimeStamp() {
		return addressExpTimeStamp;
	}
	public void setAddressExpTimeStamp(Date addressExpTimeStamp) {
		this.addressExpTimeStamp = addressExpTimeStamp;
	}
	public Date getAddressAppliedTimeStamp() {
		return addressAppliedTimeStamp;
	}
	public void setAddressAppliedTimeStamp(Date addressAppliedTimeStamp) {
		this.addressAppliedTimeStamp = addressAppliedTimeStamp;
	}
	public List<Policy> getPolicyList() {
		return policyList;
	}
	public void setPolicyList(List<Policy> policyList) {
		this.policyList = policyList;
	}
	public List<Location> getLocationList() {
		return locationList;
	}
	public void setLocationList(List<Location> locationList) {
		this.locationList = locationList;
	}
	public List<Address> getAddressList() {
		return addressList;
	}
	public void setAddressList(List<Address> addressList) {
		this.addressList = addressList;
	}
	
	
	
}
