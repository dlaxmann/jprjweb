package com.app.frame.springBatch.bean;

import java.io.Serializable;
import java.util.Date;

public class Policy implements Serializable 
{
	private int policyNumber; 
	private Date policyEffDate; 
	private Date appliedDateStamp;
	
	
	public int getPolicyNumber() {
		return policyNumber;
	}
	public void setPolicyNumber(int policyNumber) {
		this.policyNumber = policyNumber;
	}
	public Date getPolicyEffDate() {
		return policyEffDate;
	}
	public void setPolicyEffDate(Date policyEffDate) {
		this.policyEffDate = policyEffDate;
	}
	public Date getAppliedDateStamp() {
		return appliedDateStamp;
	}
	public void setAppliedDateStamp(Date appliedDateStamp) {
		this.appliedDateStamp = appliedDateStamp;
	} 
	
	
	

}
