package com.app.frame.springBatch.bean;

import java.util.Calendar;
import java.util.Date;

import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersIncrementer;

public class DynamicJobParameters implements JobParametersIncrementer 
{		
    public JobParameters getNext(JobParameters parameters) 
    {     	
        if (parameters==null || parameters.isEmpty()) 
        {
            return new JobParametersBuilder().addLong("run.id", 1L).toJobParameters();
        }
        long id = parameters.getLong("run.id",1L) + 1;
    	parameters = new JobParametersBuilder().addLong("run.id", id).toJobParameters();
        return parameters;
        /*Date date = Calendar.getInstance().getTime();    
    	 My job parameter name is schedule.time, you can add any no.of parameters using add*** methods 
    	parameters = new JobParametersBuilder().addDate("schedule.time", date).toJobParameters();
        return parameters;*/
    }
}
