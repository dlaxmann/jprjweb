package com.app.frame.springBatch.bean;

import java.util.Date;

public class Address {
	
	private int addressId; 
	private int policyNumber; 
	private String addressIdentifier;
	private Date addressEffTimeStamp; 
	private Date addressExpTimeStamp;
	private int addressAppliedTimeStamp;
	
	
	public int getAddressId() {
		return addressId;
	}
	public void setAddressId(int addressId) {
		this.addressId = addressId;
	}
	public int getPolicyNumber() {
		return policyNumber;
	}
	public void setPolicyNumber(int policyNumber) {
		this.policyNumber = policyNumber;
	}
	public String getAddressIdentifier() {
		return addressIdentifier;
	}
	public void setAddressIdentifier(String addressIdentifier) {
		this.addressIdentifier = addressIdentifier;
	}
	public Date getAddressEffTimeStamp() {
		return addressEffTimeStamp;
	}
	public void setAddressEffTimeStamp(Date addressEffTimeStamp) {
		this.addressEffTimeStamp = addressEffTimeStamp;
	}
	public Date getAddressExpTimeStamp() {
		return addressExpTimeStamp;
	}
	public void setAddressExpTimeStamp(Date addressExpTimeStamp) {
		this.addressExpTimeStamp = addressExpTimeStamp;
	}
	public int getAddressAppliedTimeStamp() {
		return addressAppliedTimeStamp;
	}
	public void setAddressAppliedTimeStamp(int addressAppliedTimeStamp) {
		this.addressAppliedTimeStamp = addressAppliedTimeStamp;
	}
	
	
	
	
}
