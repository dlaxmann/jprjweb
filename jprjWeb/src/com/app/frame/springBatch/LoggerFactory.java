package com.app.frame.springBatch;

import java.util.Hashtable;

/**
 * This the Logger Factory class used to create the Instance of the LoggerImpl class and wrap it into
 * Logger and return the Logger instance.
 * So the client accessing this factory won't be aware of the class that's processing it request.
 * 
 */
public class LoggerFactory {
	// All instances of loggers, one for each className (from getLogger)
	private static Hashtable loggers = new Hashtable();

	public LoggerFactory() {

	}

	/**
	* This method creates the Instance of the LoggerImpl class and Instantiate the class.
	* The constructor of the LoggerImpl will get the Log4J instance for the implementation object. 
	*
	* @param className
	* @return SimpleLog
	* 
	*/

	public static Logger getLog(String className) {

		// Check if there already is an instance
		synchronized (loggers) {
			if (!loggers.containsKey(className)) {
				LoggerImpl mylogger = new LoggerImpl(className);
				loggers.put(className, mylogger);
				return mylogger;
			} else {
				return (LoggerImpl) loggers.get(className);
			}
		}
	}

	/**
	 * @param classs
	 * @return SimpleLog
	 * 
	 */
	@SuppressWarnings("unchecked")
	public static Logger getLog(Class classs) {
		return getLog(classs.getName());
	}
}
