package com.app.frame.springBatch;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.impl.Log4JLogger;

/**
 * This is the Logger Implementation class,which implement the Logger interface and 
 * provide implementation codes for the log methods.
 * 
 * @author ksivagnanam
 *
 */
public class LoggerImpl implements Logger {
	/**
	 * @param log
	 * 
	 */
	Log logger;
	public static final String NULL_MESSAGE = "Message is null";
	
	/**
	 * Default Constructor
	 */
	public LoggerImpl() {
		logger = new Log4JLogger();
	}
	
	/**
	 * The argumented constructor,This creates the log4j  instance and assign it to the log reference variable. 
	 * @param log
	 */
	public LoggerImpl(String log) {
		logger = new Log4JLogger(log);
	}

	/**
	 * This method returns true, if the isTrace option is enabled for the log.
	 * @return boolean
	 * 
	 */
	
	public boolean isTraceEnabled() {
		return true;
	}

	/**
	 * This method returns true, if the isDebug option is enabled for the log.
	 * @return boolean
	 * 
	 */
	public boolean isDebugEnabled() {
		return logger.isDebugEnabled();
	}

	/**
	 * This method returns true, if the isInfo option is enabled for the log.
	 * @return boolean
	 * 
	 */
	public boolean isInfoEnabled() {
		return logger.isInfoEnabled();
	}

	/**
	 * This method returns true, if the isWarning option is enabled for the log.
	 * @return boolean
	 * 
	 */
	public boolean isWarningEnabled() {
		return logger.isWarnEnabled();
	}

	/**
	 * This method returns true, if the isError option is enabled for the log.
	 * @return boolean
	 */
	public boolean isErrorEnabled() {
		return logger.isErrorEnabled();
	}

	/**
	 * This method returns true, if the isFatal option is enabled for the log.
	 * @return boolean
	 * 
	 */
	public boolean isFatalEnabled() {
		return logger.isFatalEnabled();
	}

	/**
	 * This method get the log object and log the message details into the log file
	 * @param message
	 * @return java.lang.String
	 * 
	 */
	public void trace(Object message) {
		logger.trace(message);
	}

	/**
	 * @param message
	 * @param t
	 * @return
	 * 
	 */
	public void trace(Object message, Throwable t) {
		logger.trace(message, t);
	}

	/**
	 * This method get the log object and log the message details into the log file
	 * @param message
	 * @param t
	 * @return java.lang.String
	 * 
	 */
	public void debug(Object message) {
		if (logger.isDebugEnabled()) {
			logger.debug((message == null ? NULL_MESSAGE : message.toString()));
		}
	}

	/**
	 * @param message
	 * @param t
	 * @return
	 * 
	 */
	public void debug(Object message, Throwable t) {
		if (logger.isDebugEnabled()) {
			logger.debug((message == null ? NULL_MESSAGE : message.toString()),
					t);
		}
	}

	/**
	 * @param message
	 * @return
	 * 
	 */
	public void info(Object message) {
		if (logger.isInfoEnabled()) {
			logger.info((message == null ? NULL_MESSAGE : message.toString()));
		}
	}

	/**
	 * @param message
	 * @param t
	 * @return
	 * 
	 */
	public void info(Object message, Throwable t) {
		if (logger.isInfoEnabled()) {
			logger.info((message == null ? NULL_MESSAGE : message.toString()),
					t);
		}
	}

	/**
	 * @param message
	 * @return
	 * 
	 */
	public void warn(Object message) {
		if (logger.isWarnEnabled()) {
			logger.warn((message == null ? NULL_MESSAGE : message.toString()));
		}
	}

	/**
	 * @param message
	 * @param t
	 * @return
	 * 
	 */
	public void warn(Object message, Throwable t) {
		if (logger.isWarnEnabled()) {
			logger.warn((message == null ? NULL_MESSAGE : message.toString()),
					t);
		}
	}

	/**
	 * @param message
	 * @return
	 * 
	 */
	public void error(Object message) {
		if (logger.isErrorEnabled()) {
			logger.error((message == null ? NULL_MESSAGE : message.toString()));
		}
	}

	/**
	 * @param message
	 * @param t
	 * @return
	 * 
	 */
	public void error(Object message, Throwable t) {
		if (logger.isErrorEnabled()) {
			logger.error((message == null ? NULL_MESSAGE : message.toString()),
					t);
		}
	}

	/**
	 * @param message
	 * @return
	 * 
	 */
	public void fatal(Object message) {
		if (logger.isFatalEnabled()) {
			logger.fatal((message == null ? NULL_MESSAGE : message.toString()));
		}
	}

	/**
	 * @param message
	 * @param t
	 * @param sessionid
	 * @return
	 * 
	 */
	public void fatal(Object message, Throwable t) {
		if (logger.isFatalEnabled()) {
			logger.fatal((message == null ? NULL_MESSAGE : message.toString()),
					t);
		}
	}
}
