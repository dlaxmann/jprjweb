package com.app.frame.springBatch;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DatesTester {

	
	public static String getMonth(Calendar dt){
		SimpleDateFormat sdf11 = new SimpleDateFormat("MMMM");
	    String formtdDt = sdf11.format(dt.getTime());
	    System.out.println("jobRun Month: "+formtdDt);
		return formtdDt;
	}
	
	public static Date fstSemJobFromDate(Calendar c2){
	    int fstDate = c2.getActualMinimum(Calendar.DAY_OF_MONTH);
	    c2.add(c2.YEAR, -1);
	    c2.set(Calendar.MONTH, 6);
	    c2.set(Calendar.DATE, fstDate);
	    Date fromDate = c2.getTime();
	    System.out.println(fromDate);
	    return fromDate;
	}
	
	public static Date fstSemJobToDate(Calendar c1){
	    int lastDate = c1.getActualMaximum(Calendar.DAY_OF_MONTH);
	    c1.add(c1.YEAR, 0);
	    c1.set(Calendar.MONTH, 11);
	    c1.set(Calendar.DATE, lastDate);
	    Date toDate = c1.getTime();
	    System.out.println(toDate);
	    return toDate;
	}
	
	
	public static Date secSemJobFromDate(Calendar c3){
		    int fstDate3 = c3.getActualMinimum(Calendar.DAY_OF_MONTH);
		    c3.set(Calendar.MONTH, 0);
		    c3.set(Calendar.DATE, fstDate3);
		    
		    c3.set(Calendar.MILLISECOND, 0);
			c3.set(Calendar.SECOND, 0);
			c3.set(Calendar.MINUTE, 0);
			c3.set(Calendar.HOUR_OF_DAY, 0);
		    
		    Date fromDate =  c3.getTime();
		    System.out.println(fromDate);
		    
		    
		    return fromDate;
	}
	public static Date secSemJobToDate( Calendar cc){
		    cc.add(cc.YEAR, 0);
		    cc.add(cc.MONTH, 5);
		    int lastDate1 = cc.getActualMaximum(Calendar.DAY_OF_MONTH);
			cc.set(Calendar.DATE, lastDate1);
			
			cc.set(Calendar.MILLISECOND, cc.getActualMaximum(Calendar.MILLISECOND));
			cc.set(Calendar.SECOND, cc.getActualMaximum(Calendar.SECOND));
			cc.set(Calendar.MINUTE, cc.getActualMaximum(Calendar.MINUTE));
			cc.set(Calendar.HOUR_OF_DAY, cc.getActualMaximum(Calendar.HOUR_OF_DAY));
			
			Date toDate =cc.getTime();
			System.out.println(toDate);
			 return toDate;
	}
	
	public static Date max(List<Date> dates) {
        long max = Long.MIN_VALUE;
        Date maxDate = null;
        for (Date value : dates) {
            long v = value.getTime();
            if (v > max) {
                max = v;
                maxDate = value;
            }
        }
        return maxDate;
    }
	
	
	public static int getYear(Date date){
		// Date date = new Date();
		 /*  SimpleDateFormat sdf=new SimpleDateFormat("yyyy");
		  System.out.println("Year: " + sdf.format(date));
		  return sdf.format(date).i;*/
		 Calendar cal = Calendar.getInstance();
		 cal.setTime(date);
		int yr = cal.get(Calendar.YEAR);
		System.out.println(yr);
		return yr;
		  
	}
	
	public static void getLastDay(){
		  Calendar calendar = Calendar.getInstance();
		  int year = 2013;
		  int month = Calendar.JUNE;
		  int date = 1;

		  calendar.set(year, month, date);

		  int maxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
		  System.out.println("Max Day: " + maxDay);

		  calendar.set(2004, Calendar.FEBRUARY, 1);
		  maxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
		  System.out.println("Max Day: " + maxDay);

		  }
	
	
	public static void main(String[] args) {
			
		Date runDate = new Date(2010,0,11);
			Calendar cl = Calendar.getInstance();
			//cl.setTime(runDate);
			//cl.set(2010, Calendar.JANUARY, 1);
			cl.set(2010, Calendar.JULY, 1);
			System.out.println(cl.getTime());
			String jobRun = getMonth(cl);
			
			if(jobRun.equalsIgnoreCase("January")){
				Date fromDate = fstSemJobFromDate(cl);
				Date toDate = fstSemJobToDate(cl);
			}else if (jobRun.equalsIgnoreCase("July")){
				Date fromDate = secSemJobFromDate(cl);
				Date toDate = secSemJobToDate(cl);
			}
		//getLastDay();
		//getYear();
	}

}
