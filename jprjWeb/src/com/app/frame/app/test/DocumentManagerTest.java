package com.app.frame.app.test;

import java.sql.Blob;
import java.util.Date;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.app.domain.bean.Document;
import com.frame.dao.DocumentDAO;
import com.frame.util.AppUtils;
import com.frame.util.LoadContext;

public class DocumentManagerTest {

	static ApplicationContext context;
	
	@Autowired
	//private static DocumentDAO documentDAO;
	
	public static void main(String args[]){
		LoadContext lc = new LoadContext();
		lc.laodContext();
		context = AppUtils.getContext();
		DocumentDAO documentDAO = (DocumentDAO) context.getBean("documentDAO");
		Document dc = new Document();
		String str ="Hi DocumentDAO";
		Blob blb = Hibernate.createBlob(str.getBytes());
		dc.setContent(blb);
		dc.setContentType("Content_typ");
		dc.setCreated(new Date());
		dc.setDescription("Description");
		dc.setFilename("file1");
		dc.setName("Name1");
		
		documentDAO.save(dc);
	}
}
