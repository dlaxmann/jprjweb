package com.app.frame.ftp;

import javax.net.ssl.KeyManager;
import javax.net.ssl.TrustManager;

public class FTPConfig {
	private String hostName;
	private int  port;
	private String userName;
	private String password;
	private String transferMode;
	private String fileType;
	private int sendBufferSize = 1024 * 1024;
	
	//Secure FTP properties
	private boolean useClientMode;
	private String cipherSuites;
	private KeyManager keyManager;
	private String protocol;
	private TrustManager trustManager;
	private boolean clientAuthentication;
	private String authenticationValue;
	private boolean newSession;
	private String protocols;
	private boolean implicit;
	
	//SFTP properties
	private boolean strictHostKeyChecking;
	
	public String getHostName() {
		return hostName;
	}
	
	public void setHostName(String hostName) {
		this.hostName = hostName;
	}
	
	public int getPort() {
		return port;
	}
	
	public void setPort(int port) {
		this.port = port;
	}
	
	public String getUserName() {
		return userName;
	}
	
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getTransferMode() {
		return transferMode;
	}
	
	public void setTransferMode(String transferMode) {
		this.transferMode = transferMode;
	}
	
	public String getFileType() {
		return fileType;
	}
	
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	
	public int getSendBufferSize() {
		return sendBufferSize;
	}
	
	public void setSendBufferSize(int sendBufferSize) {
		this.sendBufferSize = sendBufferSize;
	}

	public boolean isUseClientMode() {
		return useClientMode;
	}

	public void setUseClientMode(boolean useClientMode) {
		this.useClientMode = useClientMode;
	}

	public String getCipherSuites() {
		return cipherSuites;
	}

	public void setCipherSuites(String cipherSuites) {
		this.cipherSuites = cipherSuites;
	}

	public KeyManager getKeyManager() {
		return keyManager;
	}

	public void setKeyManager(KeyManager keyManager) {
		this.keyManager = keyManager;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public TrustManager getTrustManager() {
		return trustManager;
	}

	public void setTrustManager(TrustManager trustManager) {
		this.trustManager = trustManager;
	}

	public boolean isClientAuthentication() {
		return clientAuthentication;
	}

	public void setClientAuthentication(boolean clientAuthentication) {
		this.clientAuthentication = clientAuthentication;
	}

	public String getAuthenticationValue() {
		return authenticationValue;
	}

	public void setAuthenticationValue(String authenticationValue) {
		this.authenticationValue = authenticationValue;
	}

	public boolean isNewSession() {
		return newSession;
	}

	public void setNewSession(boolean newSession) {
		this.newSession = newSession;
	}

	public String getProtocols() {
		return protocols;
	}

	public void setProtocols(String protocols) {
		this.protocols = protocols;
	}

	public boolean isImplicit() {
		return implicit;
	}

	public void setImplicit(boolean implicit) {
		this.implicit = implicit;
	}

	public void setStrictHostKeyChecking(boolean strictHostKeyChecking) {
		this.strictHostKeyChecking = strictHostKeyChecking;
	}

	public boolean isStrictHostKeyChecking() {
		return strictHostKeyChecking;
	}
}
