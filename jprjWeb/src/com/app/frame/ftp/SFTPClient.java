package com.app.frame.ftp;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

import com.frame.exception.FileTransferException;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

public class SFTPClient extends AbstractFTPClient {
	private JSch jsch;
	private Properties config;
	
	public SFTPClient(FTPConfig ftpConfig) throws FileTransferException {
		super(ftpConfig);
		jsch = new JSch();
		try {
			if (!ftpConfig.isStrictHostKeyChecking()) {
				jsch.setKnownHosts("known_hosts");
				config = new Properties(); 
				config.put("StrictHostKeyChecking", "no");
			}
		} catch(JSchException e) {
			throw new FileTransferException(e);
		}
	}
	
	public boolean putFile(InputStream in, String destPath) throws FileTransferException {
		boolean success = false;
		ChannelSftp sftp = null;
		Session session = null;
		
		try {
			session = jsch.getSession(ftpConfig.getUserName(), ftpConfig.getHostName(), ftpConfig.getPort());
			session.setPassword(ftpConfig.getPassword());
			if (config != null) {
				session.setConfig(config);
			}
			session.connect();
			Channel channel = session.openChannel("sftp");
			channel.connect();
			sftp = (ChannelSftp)channel;
			sftp.put(in, destPath);

			success = true;
		} catch(JSchException e) {
			throw new FileTransferException(e);
		} catch (SftpException e) {
			throw new FileTransferException(e);
		} finally {
			if (sftp != null) sftp.quit();
			if (session != null) session.disconnect();
		}
		return success;
	}
	
	public boolean getFile(String srcPath, OutputStream out) throws FileTransferException{
		boolean success = false;
		ChannelSftp sftp = null;
		Session session = null;
		try {
			session = jsch.getSession(ftpConfig.getUserName(), ftpConfig.getHostName(), ftpConfig.getPort());
			session.setPassword(ftpConfig.getPassword());
			if (config != null) {
				session.setConfig(config);
			}
			session.connect();
			Channel channel = session.openChannel("sftp");
			channel.connect();
			sftp = (ChannelSftp)channel;
			sftp.get(srcPath, out);

			success = true;
		} catch(JSchException e) {
			throw new FileTransferException(e);
		} catch (SftpException e) {
			throw new FileTransferException(e);
		} finally {
			if (sftp != null) sftp.quit();
			if (session != null) session.disconnect();
		}
		return success;
	}
	
	public boolean deleteFile(String srcPath)throws FileTransferException{
		boolean success = false;
		ChannelSftp sftp = null;
		Session session = null;
		try {
			session = jsch.getSession(ftpConfig.getUserName(), ftpConfig.getHostName(), ftpConfig.getPort());
			session.setPassword(ftpConfig.getPassword());
			if (config != null) {
				session.setConfig(config);
			}
			session.connect();
			Channel channel = session.openChannel("sftp");
			channel.connect();
			sftp = (ChannelSftp)channel;
			sftp.rm(srcPath);

			success = true;
		} catch(JSchException e) {
			throw new FileTransferException(e);
		} catch (SftpException e) {
			throw new FileTransferException(e);
		} finally {
			if (sftp != null) sftp.quit();
			if (session != null) session.disconnect();
		}

		return success;
	}
}
