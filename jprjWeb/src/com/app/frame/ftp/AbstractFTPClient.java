package com.app.frame.ftp;

import java.io.InputStream;
import java.io.OutputStream;

import com.frame.exception.FileTransferException;

public abstract class AbstractFTPClient {
	protected FTPConfig ftpConfig;

	public AbstractFTPClient(FTPConfig ftpConfig) {
		this.ftpConfig = ftpConfig;
	}
	
	public abstract boolean putFile(InputStream in, String destPath) throws FileTransferException;
	public abstract boolean getFile(String srcPath, OutputStream out) throws FileTransferException;
	public abstract boolean deleteFile(String srcPath) throws FileTransferException;
}
