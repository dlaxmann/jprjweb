package com.app.frame.ftp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import com.frame.exception.FileTransferException;

public class FTPService {
	private AbstractFTPClient client;
	
	public boolean putFile(String filePath, String destPath) throws FileTransferException {
		boolean success = false;
		InputStream in = null;
		try {
			File file = new File(filePath);
			in = new FileInputStream(file.getAbsolutePath());
			if (destPath == null) {
				destPath = file.getName();
			} else {
				destPath = destPath + "/" + file.getName();
			}
			success = client.putFile(in, destPath);
		} catch(Exception e) {
			throw new FileTransferException(e);
		} finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch(Exception e) {
			}
		}
		
		return success;
	}
	
	public boolean getFile(String filePath, String destPath) throws FileTransferException {
		boolean success = false;
		OutputStream out= null;
		//InputStream in = null;
		try {
			File file = new File(destPath);
			out = new FileOutputStream(file.getAbsolutePath());
			if (filePath == null) {
				filePath = file.getName();
			} else {
				filePath = filePath + "/" + file.getName();
			}
			success = client.getFile(filePath, out);
		} catch(Exception e) {
			throw new FileTransferException(e);
		} finally {
			try {
				if (out != null) {
					out.close();
				}
			} catch(Exception e) {
			}
		}
		
		return success;
	}
	public boolean deleteFile(String filePath, String destPath) throws FileTransferException {
		boolean success = false;
		try {
			File file = new File(destPath);
			if (filePath == null) {
				filePath = file.getName();
			} else {
				filePath = filePath + "/" + file.getName();
			}
			success = client.deleteFile(filePath);
		} catch(Exception e) {
			throw new FileTransferException(e);
		} 
		
		return success;
	}
	
	public void setClient(AbstractFTPClient client) {
		this.client = client;
	}
}
