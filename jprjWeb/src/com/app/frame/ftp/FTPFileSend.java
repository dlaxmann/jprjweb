package com.app.frame.ftp;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Arrays;

public class FTPFileSend {

	
	public static void main(String[] args) {

		//keep trying until client connects
		try {
			ServerSocket serverSocket =new ServerSocket(5000);
			
			//Accept incoming client request
			Socket socket = serverSocket.accept();
			System.out.println("Client connected....");
			
			//Read write on socket
			PrintWriter pw = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
			BufferedReader bfr = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			
			//send file list
			String dirname="";
			File file = new File(dirname);
			File files[]=file.listFiles();
			
			
			//Sort Alphabetically
			Arrays.sort(files);
			
			//Count for requred files
			int count=0;
			
			for(int i=0; i<files.length;i++){
				if(files[i].canRead() && (files[i].toString().endsWith(".txt")))
					count++;
			}
			pw.println( count+" Text files found...");
			
			for(int i=0; i<files.length;i++){
				if(files[i].toString().endsWith(".txt"))
					pw.println(files[i].getName()+"__"+files[i].length()+" bytes");
			}
			
			//output String delimeter
			pw.println();
			pw.flush();
			
			
			
			//Convert ASCII to Decimal
			String str = bfr.readLine();
			int temp = Integer.parseInt(str);
			
			
			
			
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
