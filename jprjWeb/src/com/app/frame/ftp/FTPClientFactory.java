package com.app.frame.ftp;

import java.io.IOException;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.commons.net.ftp.FTPSClient;

public class FTPClientFactory {
	private static FTPConfig config;
	
	public void setConfig(FTPConfig config) {
		FTPClientFactory.config = config;
	}
	
	public static FTPClient getInstance() throws IOException {
		FTPClient client;
		
		if (config.getProtocol() != null) {
			client = new FTPSClient(config.getProtocol(), config.isImplicit());
		} else {
			client = new FTPClient();
		}
		
		client.setBufferSize(config.getSendBufferSize());
		
		//Secure FTP properties
		if (config.getProtocol() != null) {
			FTPSClient secureClient = (FTPSClient) client;
			secureClient.setUseClientMode(config.isUseClientMode());
			if (config.getCipherSuites() != null) {
				secureClient.setEnabledCipherSuites(config.getCipherSuites().split("[,]"));
			}
			secureClient.setKeyManager(config.getKeyManager());
			secureClient.setTrustManager(config.getTrustManager());
			
			if (config.isClientAuthentication()) {
				secureClient.setAuthValue(config.getAuthenticationValue());
				secureClient.setNeedClientAuth(config.isClientAuthentication());
			}
			
			secureClient.setEnabledSessionCreation(config.isNewSession());
			if (config.getProtocols() != null) {
				secureClient.setEnabledProtocols(config.getProtocols().split("[,]"));
			}
		}
				
		client.connect(config.getHostName(), config.getPort());
		client.enterLocalPassiveMode();

		if (config.getProtocol() != null) {
			FTPSClient secureClient = (FTPSClient) client;
			secureClient.execPROT("P");
		}
		
		int reply = client.getReplyCode();

        if (!FTPReply.isPositiveCompletion(reply)) {
        	return null;
        }
		
		client.login(config.getUserName(), config.getPassword());
		
		if (config.getTransferMode().equals("STREAM")) {
			client.setFileTransferMode(FTP.STREAM_TRANSFER_MODE);
		} else if (config.getTransferMode().equals("BLOCK")) {
			client.setFileTransferMode(FTP.BLOCK_TRANSFER_MODE);
		} else if (config.getTransferMode().equals("COMPRESSED")) {
			client.setFileTransferMode(FTP.COMPRESSED_TRANSFER_MODE);
		}
		
		if (config.getFileType().equals("BINARY")) {
			client.setFileType(FTP.BINARY_FILE_TYPE);
		} else if (config.getFileType().equals("ASCII")) {
			client.setFileType(FTP.ASCII_FILE_TYPE);
		} else if (config.getFileType().equals("EBCDIC")) {
			client.setFileType(FTP.EBCDIC_FILE_TYPE);
		}
		
		return client;
	}
}
