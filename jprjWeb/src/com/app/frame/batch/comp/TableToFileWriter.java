package com.app.frame.batch.comp;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemStream;
import org.springframework.batch.item.ItemStreamException;
import org.springframework.batch.item.ItemWriter;

import com.app.core.app.bean.Student;
import com.frame.logging.Logger;
import com.frame.logging.LoggerFactory;

public class TableToFileWriter implements ItemWriter<Student>, ItemStream {
	private static final Logger log = LoggerFactory.getLog(TableToFileWriter.class.getName());
	
	private StepExecution stepExecution;
	private String layout;
	private String controlLayout;
	private Date runDate;
	
	public void setLayout(String layout) throws IOException {
		this.layout = layout;
	}

	@BeforeStep
	public void setStepExecution(StepExecution stepExecution) {
		this.stepExecution = stepExecution;
	}
	
	
	public String getControlLayout() {
		return controlLayout;
	}

	public void setControlLayout(String controlLayout) {
		this.controlLayout = controlLayout;
	}

	public void setRunDate(Date runDate) throws Exception {
		this.runDate = runDate;
	}

	/**
	 * Generates CMW921 file
	 */
	public void write(List<? extends Student> policies)throws Exception {
		String fileName = "files/H2Student.xls";
		log.info("*******write()");
		 try {

	            HSSFWorkbook myWorkBook = new HSSFWorkbook();
	            HSSFSheet mySheet = myWorkBook.createSheet();
	            HSSFRow myRow;
	            HSSFCell myCell;

	            for (int rowNum = 0; rowNum < 10; rowNum++) {
	            	int count = 0;
	                myRow = mySheet.createRow(rowNum);
	                for (int cellNum = 0; cellNum < 10; cellNum++) {
	                    myCell = myRow.createCell(cellNum+count);
	                    myCell.setCellValue(new HSSFRichTextString(rowNum + "___________" + cellNum));
	                    System.out.println(rowNum+"__"+cellNum);
	                }
	            }
	            
	            for (int i = 0; i < 10; i++) {
	            	mySheet.autoSizeColumn((short)i);
				}
	            
	            
	            FileOutputStream out = new FileOutputStream(fileName);
	            myWorkBook.write(out);
	            out.flush();
	            out.close();
	        } catch (Exception e) {
	            e.printStackTrace();
	        }	
}


	
	

	public void update(ExecutionContext arg0) throws ItemStreamException {
		
	}

	@Override
	public void close() throws ItemStreamException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void open(ExecutionContext arg0) throws ItemStreamException {
		// TODO Auto-generated method stub
		
	}

	

}
