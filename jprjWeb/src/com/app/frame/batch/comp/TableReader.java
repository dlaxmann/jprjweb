package com.app.frame.batch.comp;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemStream;
import org.springframework.batch.item.ItemStreamException;

import com.app.core.app.bean.Student;
import com.app.frame.springBatch.DateUtils;
import com.frame.dao.BaseDAO;
import com.frame.exception.CacheException;
import com.frame.logging.Logger;
import com.frame.logging.LoggerFactory;

public class TableReader implements ItemReader<Student>, ItemStream {
	private static final Logger log = LoggerFactory.getLog(TableReader.class.getName());
	
	private boolean readFlag = false;
	private Date runDate;
	private Date endDate;
	private Date fromDate;
	private StepExecution stepExecution;
	private List<Student> studentList;
	private int counter;
	private BaseDAO basedao;
	
	
	public Student read() throws Exception {
		Student student = null;
		
		if (studentList == null && !readFlag) {
			try {
				if(fromDate == null || endDate == null){
					fromDate = DateUtils.truncateTime(runDate);
					endDate = DateUtils.addTime(runDate);
				}
				studentList = basedao.getRegUserForDay(fromDate, endDate);
				readFlag = true;
			} catch(Exception e) {
				throw e;
			} 
		}
		
		
		if ( studentList != null && counter < studentList.size()) {
			log.debug("Processing key: " + studentList.get(counter));
			student = (Student) studentList.get(counter);
			counter++;
		}
		return student;
	}

	
	public void close() throws ItemStreamException {
	}

	public void open(ExecutionContext arg0) throws ItemStreamException {
		counter = 0;
	}

	public void update(ExecutionContext arg0) throws ItemStreamException {
	}
	public void setCdsdao(BaseDAO basedao) {
		this.basedao = basedao;
	}
	@BeforeStep
	public void setStepExecution(StepExecution stepExecution) {
		this.stepExecution = stepExecution;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public void setRunDate(Date runDate) throws CacheException {
		this.runDate = runDate;
	}

	
}
