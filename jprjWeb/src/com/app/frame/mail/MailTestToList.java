package com.app.frame.mail;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class MailTestToList extends InternetAddress {
	
	public static List<String> buildEmails(){
		List<String> addrsList = new ArrayList<String>();
		addrsList.add("ldevasani@csc.com");
		addrsList.add("ldevasani@csc.com");
		
		String addList = "ldevasani@csc.com"+","+"ldevasani@csc.com";
		StringBuffer str = new StringBuffer();
		
		for (String string : addrsList) {
			str.append(string+",");
		}
	
		
		try {
			InternetAddress.parse(str.toString());
		} catch (AddressException e) {
			e.printStackTrace();
		}
		return addrsList;
	}
	
	
   public static InternetAddress[]parse(List addrsListt){
		return null;
	}
   
	public static void main(String[] args) {
		
		//String host = "hm2ntns03.farmersinsurance.com";
        String host = "hm2ntns03.farmersinsurance.com";
        String from = "FBIExpress@farmersinsurance.com";
    

        try {
        Properties props = System.getProperties();
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.user", from);
        props.put("mail.debug", "true");

        Session session = Session.getDefaultInstance(props, null);
        session.setDebug(true);
        Transport transport = session.getTransport("smtp");

        MimeMessage message = new MimeMessage(session);
        
        
        Address fromAddress = new InternetAddress("FBIExpress@farmersinsurance.com");
        // Create the message part 
        BodyPart messageBodyPart = new MimeBodyPart();
        // Fill the message
        messageBodyPart.setText("This is my testing content.");
     // Create a multipar message
        Multipart multipart = new MimeMultipart();

        // Set text message part
        multipart.addBodyPart(messageBodyPart);

        // Part two is attachment
        messageBodyPart = new MimeBodyPart();
        String filename = "D:/CLST.CDS.PET.DMV.DAILY.dat";
        DataSource source = new FileDataSource(filename);
        messageBodyPart.setDataHandler(new DataHandler(source));
        messageBodyPart.setFileName(filename);
        multipart.addBodyPart(messageBodyPart);

        // Send the complete message parts
        message.setContent(multipart,"text/html");



        message.setFrom(fromAddress);

        InternetAddress to = new InternetAddress("ldevasani@csc.com");
        message.addRecipient(Message.RecipientType.TO, to);
      //  {"ldevasani@csc.com","ldevasani@csc.com"};
        InternetAddress[] toArray = {to,to}; 
        
        message.addRecipients(Message.RecipientType.BCC, toArray);
        
        
        message.setSubject("Email Details Sending");
        message.setText("This is my testing content.");
       

        transport.connect(host, from);
        message.saveChanges();
        Transport.send(message);
        transport.close();
    } catch(Exception e){
    }finally { 
    }
    //    out.close();
    }
}
