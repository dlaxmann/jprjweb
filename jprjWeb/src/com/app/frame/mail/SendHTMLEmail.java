package com.app.frame.mail;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class SendHTMLEmail {

	
	public static void main(String[] args) {

		String host="hm2ntns03.farmersinsurance.com";
		 String from = "FBIExpress@farmersinsurance.com"; //for testing
		String to="ldevasani@csc.com";
		final String user="";
		final String password="";
		
		Properties properties = System.getProperties();
		properties.setProperty("mail.smtp.host",host );
		properties.put("mail.smtp.user", from);
		properties.put("mail.smpt.auth", "false");
		
		Session session = Session.getDefaultInstance(properties,
				new javax.mail.Authenticator(){
			protected PasswordAuthentication getPasswordAuthentication(){
				return new  PasswordAuthentication(user,password);
			}
		});
		
		
		try{
			MimeMessage message =  new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
			
			message.setSubject("html message");
			message.setContent("<h2>Sending html content<h2>","text/html");
			
			Transport.send(message);
			System.out.println("message sent");
			
		}catch(MessagingException e){
			e.printStackTrace();
		}
	}

}
