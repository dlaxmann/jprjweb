package com.app.frame.mail;
import java.io.IOException;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;



public class MailTest1 {
	
	
	public static Session buildGoogleSession() {
		 
		String host = "smtp.gmail.com";
        String port = "587";
        String from = "imlaxman@gmail.com";
        String password = "titanic@123";
        String to = "dlaxman2014@gmail.com";
        String auth ="true";

        Properties props = System.getProperties();
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.port", port);
        props.put("mail.smpth.auth", auth);
		
		  Properties mailProps = new Properties();
		  mailProps.put("mail.transport.protocol", "smtp");
		  mailProps.put("mail.host", host);
		  mailProps.put("mail.from", from);
		  mailProps.put("mail.smtp.starttls.enable", "true");
		  mailProps.put("mail.smtp.port", port);
		  mailProps.put("mail.smtp.auth", auth);

		  // final, because we're using it in the   closure below
		  final PasswordAuthentication usernamePassword =
		    new PasswordAuthentication(from, password);
		  Authenticator authenticatior = new Authenticator() {
		    protected PasswordAuthentication getPasswordAuthentication() {
		      return usernamePassword;
		    }
		  };

		  Session session = Session.getInstance(mailProps, authenticatior);
		  session.setDebug(true);
		  
		  System.out.println("________Got Mail Session Object: "+session);
		  return session;
		}
	
	public static void main(String[] args)    {
	
		String from = "imlaxman@gmail.com";
	    String password = "titanic@123";
	    String to = "imlaxman@gmail.com";
	    
		Session session = buildGoogleSession();
        
	try{
        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress(from));
        message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
        message.setSubject("Hello JavaMail");

        // Handle attachment 1
        MimeBodyPart messageBodyPart1 = new MimeBodyPart();
        messageBodyPart1.attachFile("f:/SIBExl.txt");

        // Handle attachment 2
        MimeBodyPart messageBodyPart2 = new MimeBodyPart();
        messageBodyPart2.attachFile("f:/SIBExl.txt");

        FileDataSource fileDs = new FileDataSource("f:/bg.gif");
        MimeBodyPart imageBodypart = new MimeBodyPart();
        imageBodypart.setDataHandler(new DataHandler(fileDs));
        imageBodypart.setHeader("Content-ID", "<myimg>");
        imageBodypart.setDisposition(MimeBodyPart.INLINE);

        // Handle text
       // String body = "<html><body>Elotte<img src=\"cid:myimg\" width=\"600\" height=\"90\" alt=\"myimg\" />Utana</body></html>";

        String body = "<html><body bgcolor=\"red\">Elotte width=\"600\" height=\"90\" alt=\"myimg\" />Utana</body></html>";
        
        MimeBodyPart textPart = new MimeBodyPart();
        textPart.setHeader("Content-Type", "text/plain; charset=\"utf-8\"");
        textPart.setContent(body, "text/html; charset=utf-8");

        MimeMultipart multipart = new MimeMultipart("mixed");

        multipart.addBodyPart(textPart);
        multipart.addBodyPart(imageBodypart);
        multipart.addBodyPart(messageBodyPart1);
        multipart.addBodyPart(messageBodyPart2);

        message.setContent(multipart);

        // Send message
        Transport.send(message);
        
        
        
		  } catch (AddressException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		}catch(IOException e){
			  
		  }
    }

}
