package com.app.frame.mail;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.frame.logging.Logger;
import com.frame.logging.LoggerFactory;


public class MailSender {
private static final Logger LOGGER = LoggerFactory.getLog(MailSender.class.getName());
private String to;
private String from;
private String host;
private String subject;
private String body;
private int port = 25;
private boolean authenticationRequired = false;
private String userName;
private String password;
private boolean html;

public MailSender() {
}

public void setAuthenticationRequired(boolean authenticationRequired) {
	this.authenticationRequired = authenticationRequired;
}

public void setUserName(String userName) {
	this.userName = userName;
}

public void setPassword(String password) {
	this.password = password;
}

public void setHost(String host) {
	this.host = host;
}

public void setPort(int port) {
	this.port = port;
}

public void setTo(String to) {
	this.to = to;
}

public void setFrom(String from) {
	this.from = from;
}

public void setSubject(String subject) {
	this.subject = subject;
}

public void setBody(String body) {
	this.body = body;
}

public void setHtml(boolean html) {
	this.html = html;
}

public void send() throws MessagingException {
	send(from, to, subject, body);
}

public void send(String mailSub, String mailBody, List<String> attachments) throws MessagingException {
	send(from, to, mailSub, mailBody, attachments);
}

public Session getSession() {
	Properties props = new Properties();
	props.put("mail.smtp.host", host);
	props.put("mail.smtp.port", port + "");
	SMTPAuthenticator smtpAuth = null;
	
	props.put("mail.smtp.starttls.enable", "true");
	props.put("mail.smtp.ssl.enable", "true");

	if (authenticationRequired) {
		props.put("mail.smtp.auth", "true");
		smtpAuth = new SMTPAuthenticator(userName, password);
	}
	return Session.getInstance(props, smtpAuth); //getInstance(props, smtpAuth);
}


public void send(String fromAddress, String toAddress, String subjectStr, 
			String bodyStr, List<String> attachments) 
	throws MessagingException, AddressException {
	Session session = getSession();
		
	Message msg = new MimeMessage(session);
	
	msg.setFrom(new InternetAddress(fromAddress));
	msg.setRecipients(Message.RecipientType.TO, getToAddress(toAddress));
	msg.setSubject(subjectStr);
	msg.setSentDate(new Date());
	
	Multipart multipart = new MimeMultipart();
	
	// create the message part 
	MimeBodyPart messageBodyPart = new MimeBodyPart();
	if (html) {
		messageBodyPart.setContent(bodyStr, "text/html");
	} else {
		messageBodyPart.setText(bodyStr);
	}
	
	multipart.addBodyPart(messageBodyPart);

	for (int i = 0; i < attachments.size(); i++) {
		File f = new File ((String) attachments.get(i));
		// Part two is attachment
    	messageBodyPart = new MimeBodyPart();
    	DataSource source = new FileDataSource(f);
    	messageBodyPart.setDataHandler(new DataHandler(source));
    	messageBodyPart.setFileName(f.getName());
    	multipart.addBodyPart(messageBodyPart);
	}
	

	// Put parts in message
	msg.setContent(multipart);

	// Send the message
	Transport.send(msg);
}

public InternetAddress[] getToAddress(String toAddress) throws AddressException {
	String[] toAdrs = toAddress.split("[,]");
	InternetAddress[] address = new InternetAddress[toAdrs.length];
	int x = 0;
	for (String adrs: toAdrs) {
		adrs = adrs.trim();
		address[x++] = new InternetAddress(adrs);
	}
	
	return address;
}

public void send(String fromAddress, String toAddress, String subjectStr, String bodyStr) 
				throws MessagingException, AddressException {
	Session session = getSession();
	
	Message msg = new MimeMessage(session);
	
	msg.setFrom(new InternetAddress(fromAddress));
	msg.setRecipients(Message.RecipientType.TO, getToAddress(toAddress));
	msg.setSubject(subjectStr);
	msg.setSentDate(new Date());
	if (html) {
		msg.setContent(bodyStr, "text/html");
	} else {
		msg.setText(bodyStr);
	}
	
	Transport.send(msg);
}

	public void sendBcc(String fromAddress, String toAddress, String subjectStr, String bodyStr) 
		throws MessagingException, AddressException {
		Session session = getSession();
		
		Message msg = new MimeMessage(session);
		
		msg.setFrom(new InternetAddress(fromAddress));
		msg.setRecipients(Message.RecipientType.BCC, getToAddress(toAddress));
		msg.setSubject(subjectStr);
		msg.setSentDate(new Date());
		if (html) {
		msg.setContent(bodyStr, "text/html");
		} else {
		msg.setText(bodyStr);
		}
		
		Transport.send(msg);
		}

public static void main(String[] args) throws Exception {
	MailSender ms = new MailSender();
	/*String from = "FBIExpress@farmersinsurance.com";

	// Assuming you are sending email from localhost
	String host = "hm2ntns03.farmersinsurance.com";
	String to = "sdevarasetti@csc.com";
	String filename = "D:/PeachTreeWsAIT/CDSBal3B_DMVError/src/cdsfiles/CLST.CDS.AIT03.DMV.SRCHINFF.txt";
	sendEmail(host, from, to, "Test Mail!!",
			"Testing mail through java code", filename);*/
	
}
}