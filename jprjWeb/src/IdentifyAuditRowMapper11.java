



import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class IdentifyAuditRowMapper11  {
	
	private static final String MONTHLYCURRENT = "MONTHLY CURRENT";
	private static final String MONTHLYPRIOR = "MONTHLY PRIOR";
	private static final String QUARTERLY = "QUARTERLY";
	private static final String SEMIANNUAL = "SEMIANNUAL";
	private static final String ANNUAL = "ANNUAL";
	private static final String TWOMONTHTREND = "2 MONTH TREND";
	private static final String THREEMONTHTREND = "= 3 MONTH TREND";
	private static final String SIXMONTHTREND = "6 MONTH TREND";
	private static final String ONEMONTHSEMIANNUAL = "1 MONTH SEMIANNUAL";
	
	
	 public String dateFormat(String str){
		    Calendar cal = Calendar.getInstance();
		    SimpleDateFormat sdf = new SimpleDateFormat("dd-MMMMM-yyyy");
		    return sdf.format(cal.getTime());
		  }
	 
	public static void main (String args[]) throws SQLException, ParseException{
		
		
		AuditExecution11 auditexecution = new AuditExecution11();
		
			
		String auditPeriod = "MONTHLYCURRENT";
		
		 Calendar cal = new GregorianCalendar();
		
		 
		 int month = cal.get(Calendar.MONTH);
		 int year = cal.get(Calendar.YEAR);
		 int day = cal.get(Calendar.DAY_OF_MONTH);
		 
		 
		 cal.set(year, month, day);
		 Date d = cal.getTime();
		 System.out.println(d);
		 
		 StringBuffer buf = new StringBuffer();
		 buf.append(day );      
		 buf.append(month );                   
		 buf.append(year);  
		 String str = buf.toString();  
	     DateFormat formatter  = new SimpleDateFormat("dd-MMM-yy");
	     System.out.println(formatter);
	     Date date   = (Date)formatter.parse(str);    
	            
		
		if(auditPeriod !=null && auditPeriod.equals(MONTHLYCURRENT)){
			
			int startDateOfMonth = cal.getActualMinimum(Calendar.DAY_OF_MONTH);
		    int endDateOfMonth = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		    
		    
	            
	     //  Date startDateOfMonthFormat = (formatter);
		   Date endDateOfMonthFormat = (date); 
		    
			//auditexecution.setInv_Strt_Dt(startDateOfMonthFormat);
			auditexecution.setInv_End_Dt(endDateOfMonthFormat);
		}
		else if(auditPeriod !=null && auditPeriod.equals(MONTHLYPRIOR)){
			
			int priorMonth = cal.get(Calendar.MONTH) - 1;
			int startDateOfPriorMonth = cal.getActualMinimum(Calendar.DAY_OF_MONTH);
			int endDateOfPriorMonth = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
			
			Date startDateOfPriorMonthFormat = (date);
			Date endDateOfMonthPriorFormat = (date);
			
			auditexecution.setInv_Strt_Dt(startDateOfPriorMonthFormat);
			auditexecution.setInv_End_Dt(endDateOfMonthPriorFormat);
			
		}
		else if(auditPeriod !=null && auditPeriod.equals(QUARTERLY)){
			
		//	String st = TestDate.now("dd MMMMM yyyy");
		     //System.out.println(st);
		  //   String str = st.substring(3,6);
			 
		/*	auditexecution.setInv_Strt_Dt();
			auditexecution.setInv_End_Dt(invEndDt);
		}
		else if(auditPeriod !=null && auditPeriod.equals(SEMIANNUAL)){
			auditexecution.setInv_Strt_Dt(invStrtDt);
			auditexecution.setInv_End_Dt(invEndDt);
		}
		else if(auditPeriod !=null && auditPeriod.equals(ANNUAL)){
			auditexecution.setInv_Strt_Dt(invStrtDt);
			auditexecution.setInv_End_Dt(invEndDt);
		}
		else if(auditPeriod !=null && auditPeriod.equals(THREEMONTHTREND)){
			auditexecution.setInv_Strt_Dt(invStrtDt);
			auditexecution.setInv_End_Dt(invEndDt);
		}
		else if(auditPeriod !=null && auditPeriod.equals(SIXMONTHTREND)){
			auditexecution.setInv_Strt_Dt(invStrtDt);
			auditexecution.setInv_End_Dt(invEndDt);
		}
		else if(auditPeriod !=null && auditPeriod.equals(ONEMONTHSEMIANNUAL)){
			auditexecution.setInv_Strt_Dt(invStrtDt);
			auditexecution.setInv_End_Dt(invEndDt);
		}*/
		
		
		System.out.println(".........inserted in  auditexecution:" +auditexecution.getAdt_Exctn_Seq());
		System.out.println(".........inserted in  auditexecution:" +auditexecution.getTelco_Lvl_Cd());
				
				//return auditexecution;
		
	}
	}
}


/*int month = cal.get(Calendar.MONTH);
int year = cal.get(Calendar.YEAR);
int day = cal.get(Calendar.DAY_OF_MONTH);
 
StringBuffer buf = new StringBuffer();
buf.append(day );      // Java Almanac v1/
buf.append(month );                   
buf.append(year);  
String str = buf.toString();  
DateFormat formatter  = new SimpleDateFormat("dd-MMM-yy");
Date date   = (Date)formatter.parse(str);    */



//com.vzbi.verify.batch.domain.auditExecution.identifyAudit.IdentifyAuditRowMapper
/*
auditDefinition.setAdt_desc_txt(rs.getString("adt_desc_txt "));
auditDefinition.setAdt_excptn_tmplt_cd_txt(rs.getNString("adt_excptn_tmplt_cd_txt "));
auditDefinition.setAdt_nm_txt(rs.getString("adt_nm_txt "));
auditDefinition.setAdt_run_rate(rs.getInt("adt_run_rate "));
auditDefinition.setAdt_seq(rs.getLong("adt_seq "));
auditDefinition.setAdt_spcfc_fld_1(rs.getString("adt_spcfc_fld_1 "));
auditDefinition.setAdt_spcfc_fld_2(rs.getString("adt_spcfc_fld_2 "));
auditDefinition.setAdt_spcfc_fld_3(rs.getString("adt_spcfc_fld_3 "));
auditDefinition.setAdt_spcfc_fld_4(rs.getString("adt_spcfc_fld_4 "));
auditDefinition.setAdt_spcfc_fld_5(rs.getString("adt_spcfc_fld_5 "));
auditDefinition.setAdt_typ_seq(rs.getLong("adt_typ_seq"));
auditDefinition.setAudit_vrsn_no(rs.getLong("adt_vrsn_no "));
auditDefinition.setAudt_type_cd_txt(rs.getString("adt_typ_cd_txt "));
auditDefinition.setChrg_type_seq(rs.getLong("chrg_typ_seq"));
auditDefinition.setcreate_dt(rs.getDate("create_dt "));
auditDefinition.setDvlpr_user_id(rs.getLong("dvlpr_user_id "));
auditDefinition.setIss_cd_seq(rs.getLong("iss_cd_seq"));
auditDefinition.setMdfd_by_txt(rs.getString("mdfd_by_txt "));
auditDefinition.setStat_cd(rs.getInt("stat_cd"));
auditDefinition.setTstr_userid(rs.getLong("tstr_userid "));
auditDefinition.setUser_id(rs.getLong("user_id "));
auditDefinition.setMdfd_dt(rs.getDate("mdfd_dt"))*/

/*auditConfiguration.setAcna_param_set_row_seq(rs.getInt("acna_param_set_row_seq"));
auditConfiguration.setAdt_cat_param_set_row_seq(rs.getInt("adt_cat_param_set_row_seq"));
auditConfiguration.setAuditApprovalRs(rs.getBoolean("adt_aprv_rslt_ind"));
auditConfiguration.setAuditConfignDescText(rs.getString("adt_confign_desc_txt"));
auditConfiguration.setAuditConfignNo(rs.getString("adt_confign_nm"));
auditConfiguration.setAuditConfigSeq(rs.getInt("adt_confign_seq"));
auditConfiguration.setAuditConfigTypeCdText(rs.getString("adt_config_sched_typ_cd_txt"));
auditConfiguration.setAuditPeriodCdText(rs.getString("adt_period_cd_txt"));
auditConfiguration.setAuditRunDay(rs.getInt("adt_run_day"));
auditConfiguration.setCreate_dt(rs.getInt("create_dt"));
auditConfiguration.setIgnorInrInd(rs.getBoolean("ignor_inr_ind"));
auditConfiguration.setIlleagalSeaquence(rs.getLong("illgl_ent_seq"));
auditConfiguration.setInv_frmt_param_set_row_seq(rs.getInt("inv_frmt_param_set_row_seq"));
auditConfiguration.setMdfd_by_txt(rs.getInt("mdfd_by_txt"));
auditConfiguration.setMdfd_dt(rs.getInt("mdfd_dt"));
auditConfiguration.setOutputFormatCdText(rs.getString("outpt_frmt_cd_txt"));
auditConfiguration.setPayDeductCdText(rs.getString("pay_deduct_cd_txt"));
auditConfiguration.setProjSequence(rs.getLong("proj_seq"));
auditConfiguration.setStateCd(rs.getString("stat_cd")); 
auditConfiguration.setTelco_lvl_cd(rs.getInt("telco_lvl_cd"));
auditConfiguration.setTelco_param_set_row_seq(rs.getLong("telco_param_set_row_seq"));
auditConfiguration.setTelco_prnt_param_set_row_seq(rs.getLong("telco_prnt_param_set_row_seq"));
auditConfiguration.setTelco_rllp_param_set_row_seq(rs.getLong("telco_rllp_param_set_row_seq"));
auditConfiguration.setUsco_param_set_row_seq(rs.getInt("usco_param_set_row_seq"));
auditConfiguration.setWrk_grp_seq(rs.getLong("wrk_grp_seq"));*/

//mapRowObject.add(auditDefinition);
//mapRowObject.add(auditConfiguration);
//System.out.println("+++++++ row mapper list size:" +mapRowObject.size());
