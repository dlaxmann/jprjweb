import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.List;




public class DeleteCDSSt {

		 

		  public static void main(String[] args)  {
			  Connection conn;
		    try    {
		    	
		    	String schema = "DUR33U";
		      Class.forName("com.ibm.db2.jcc.DB2Driver").newInstance();
		      String url = "jdbc:db2://10.148.20.11:4052/DB2D";
		      conn = DriverManager.getConnection(url, "ideagent", "eauto1");
		      System.out.println("Got connectins" +conn);
		      Statement st = conn.createStatement();
		      List<String> tablesList = getTables();
				for (String tbl: tablesList){
					 st.executeUpdate("DELETE FROM " +schema+"."+tbl);
					 System.out.println("Deleted from "+tbl);
				}
		      conn.close();
		    } 
		    catch (ClassNotFoundException ex) {System.err.println(ex.getMessage());}
		    catch (IllegalAccessException ex) {System.err.println(ex.getMessage());}
		    catch (InstantiationException ex) {System.err.println(ex.getMessage());}
		    catch (SQLException ex)           {System.err.println(ex.getMessage());}
		  }
		  
		  public static List getTables(){
			  String[] tables =
			  {"ST_PLCY",
			  	"ST_PLCY_TRANS_CVG_PRM",
			  	"ST_DRV",
			  	"ST_DRV_VLTN",
			  	"ST_PLCY_WC_TRANS_ACTVY",
			  	"ST_INC",
			  	"ST_MVR",
			  	"ST_OTH_PLCY",
			  	"ST_OTH_PLCY_AUTO_DMGRPHCS",
			  	"ST_OTH_PLCY_CVG",
			  	"ST_PLCY_ADDR",
			  	"ST_PLCY_ADT_TRANS",
			  	"ST_PLCY_AGT",
			  	"ST_PLCY_BLNKT",
			  	"ST_PLCY_CD_TRSLN_HSTRY",
			  	"ST_PLCY_CLNT",

			  	"ST_PLCY_DTL",
			  	"ST_PLCY_DUNS",
			  	"ST_PLCY_ENTY",

			  	"ST_PLCY_LOC",
			  	"ST_PLCY_LOC_GEOG",
			  	"ST_PLCY_LOC_LOB_ASSCN",
			  	"ST_PLCY_LOC_RCP",
			  	
			  	"ST_PLCY_LOC_STRUC",
			  	"ST_PLCY_LOC_STRUC_LQR",
			  	"ST_PLCY_LOC_STRUC_RENOVTN",
			  	"ST_PLCY_LOC_STRUC_RST",
			  	"ST_PLCY_RTG",
			  	
			  	"ST_PLCY_SOI_CVG",
			  	"ST_PLCY_SOI_CVG_NP",
			  	"ST_PLCY_SOI_CVRD_ITEM",
			  	"ST_PLCY_SOI_TXT",
			  	"ST_PLCY_ST_AND_MISC_FEE",
			  	"ST_PLCY_TRANS",
			  	"ST_PLCY_TRANS_ACTVY",

			  	"ST_PLCY_VEH",
			  	"ST_PLCY_VEH_DMGRPHCS",
			  	"ST_PLCY_WC",
			  	"ST_SOI",
			  	"ST_SOI_CLNT_RLTNSH",
			  	"ST_SOI_OTH_INT",
			  	"ST_SOI_ST_WC_CHRG",
			  	"ST_SOI_ST_WC_PRM",
			  	"ST_SOI_UWRTNG_INFO",
			  	"ST_Plcy_Crss_Ref",
			  	"ST_PAF_PLCY",
			  	"ST_PAF_SOI_CVG"};
			  List<String> tablesList = Arrays.asList(tables);
			  	return   tablesList;
		  }
		  
}

