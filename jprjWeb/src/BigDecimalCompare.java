import java.math.BigDecimal;


public class BigDecimalCompare {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		BigDecimal bd1 = new BigDecimal("10");
System.out.println("bd1  "+bd1);
		// Create via a long
		BigDecimal bd2 = BigDecimal.valueOf(123L);
		BigDecimal bd3 = new BigDecimal("10");
		if(bd1.compareTo(bd3) ==0){
			System.out.println("in if 0");
		}if(bd1.compareTo(bd3) !=0){
			System.out.println("in if 1");
		}
		bd1 = bd1.add(bd2);
		bd1 = bd1.multiply(bd2);
		bd1 = bd1.subtract(bd2);
		bd1 = bd1.divide(bd2, BigDecimal.ROUND_UP);
		bd1 = bd1.negate();
		
		
		
	}

}
