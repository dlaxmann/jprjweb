

import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateUtilsSamples {
	public static Date truncateTime(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.set(Calendar.MILLISECOND, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.HOUR_OF_DAY, 0);
		
		return c.getTime();
	}
	
	public static Calendar truncateTime(Calendar c) {
		c.set(Calendar.MILLISECOND, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.HOUR_OF_DAY, 0);
		
		return c;
	}
	
	public static Date addTime(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.set(Calendar.MILLISECOND, c.getActualMaximum(Calendar.MILLISECOND));
		c.set(Calendar.SECOND, c.getActualMaximum(Calendar.SECOND));
		c.set(Calendar.MINUTE, c.getActualMaximum(Calendar.MINUTE));
		c.set(Calendar.HOUR_OF_DAY, c.getActualMaximum(Calendar.HOUR_OF_DAY));
		
		return c.getTime();
	}
	
	public static long diffDays(Calendar c1, Calendar c2) {
		long milliseconds1 = c1.getTimeInMillis();
		long milliseconds2 = c2.getTimeInMillis();
		long diff = milliseconds2 - milliseconds1;
		long diffDays = diff / (24 * 60 * 60 * 1000);
		
		return diffDays;
	}
	
	public static long daysBetweenDates(Date date1, Date date2) {
		long milliseconds1 = truncateTime(date1).getTime();
		long milliseconds2 = truncateTime(date2).getTime();
		long diff = milliseconds2 - milliseconds1;
		long diffDays = diff / (24 * 60 * 60 * 1000);
		
		return diffDays;
	}
	
	public static void testMethods(){
		Calendar c1 = Calendar.getInstance();
		Calendar c2 = Calendar.getInstance();
		c2.add(Calendar.DAY_OF_MONTH, 3);
		
		System.out.println("Diff Days: " + DateUtilsSamples.diffDays(c1, c1));
		System.out.println("Diff Days: " + DateUtilsSamples.diffDays(c1, c2));
		System.out.println("Diff Days: " + DateUtilsSamples.diffDays(c2, c1));
		
		Calendar c3 = Calendar.getInstance();
		Date d = c3.getTime();
		System.out.println("Before: " + d);
		d = DateUtilsSamples.truncateTime(d);
		System.out.println("After : " + d);
		
		d = DateUtilsSamples.addTime(d);
		System.out.println("After adding time: " + d);
		
		Date runDate = new Date();
		Calendar cq = Calendar.getInstance();
		cq.setTime(runDate);
		cq.add(Calendar.MONTH, -3);
		cq.add(Calendar.DATE, +1);
		Date prevCloseDate = cq.getTime();
		System.out.println("After adding time: " + prevCloseDate);
	}
	
	public static void ConvertDateToString(){
	    //Date dt = ConvertDateToString.dateFormat();
	    //Date dt = (Date)ConvertDateToString.dateFormat());
	    Calendar cal = new GregorianCalendar();
		 int month = cal.get(Calendar.MONTH);
		 int year = cal.get(Calendar.YEAR);
		 int day = cal.get(Calendar.DAY_OF_MONTH);
		 
		
		 SimpleDateFormat sdf = new SimpleDateFormat("MMMM");
		 String qmonth = sdf.format(cal.get(1));
		 System.out.println("qmonth= " +qmonth);
		
		 String endOfQMonth = sdf.format(cal.get(3));
		 
		 //int _startDateOfQuaterly = cal.getMinimum(1);
		 //System.out.println("qmonth= " +_startDateOfQuaterly);
		 int _endDateOfQuaterly = cal.get(1);
		 System.out.println("endOfQMonth= " +endOfQMonth);
		 
		// System.out.println(_startDateOfQuaterly);
		// int _endDateOfQuaterly = cal.getActualMaximum((month)+2);
		 int quarterendmonth = (cal.get((Calendar.MONTH)+2));
		 System.out.println(quarterendmonth);
		 
		// System.out.println(months);
		 System.out.println(year);
		 System.out.println(month);
		 System.out.println(day);
	    
	}
	
	public static void MonthMaxDays(){
		Calendar calendar = Calendar.getInstance();
	    SimpleDateFormat sdf = new SimpleDateFormat("dd-MMMMM-yyyy");
		  int year = 2009;
	        int month = Calendar.FEBRUARY;
	        int date = 1;

	        //calendar.set(year, month, date);
	        calendar.set(Calendar.MONTH, 3); 
	        int maxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
	        System.out.println("Max Day: " + maxDay);

	        calendar.set(2004, Calendar.FEBRUARY, 1);
	        maxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
	        System.out.println("Max Day: " + maxDay);
	}
	
	public static void dateFormattings(){
		Calendar cal = Calendar.getInstance();
	    SimpleDateFormat sdf = new SimpleDateFormat("dd-MMMMM-yyyy");
	  //first month of quater as aprial
		cal.set(Calendar.MONTH, 3); 			
		String firstMonthOfQuater = sdf.format(cal.getTime());
		System.out.println("the first of month of quater is   "+firstMonthOfQuater);
		int firstDayOffirstMonthOfQuater = cal.getMinimum(Calendar.DAY_OF_MONTH);
		System.out.println("first day of quater april is   "+firstDayOffirstMonthOfQuater);
		
		//last month of quater as june
		cal.set(Calendar.MONTH, 5);				
		SimpleDateFormat sdf1 = new SimpleDateFormat("MMMMM-yyyy");
		String lastMonthOfQuater = sdf1.format(cal.getTime());
		System.out.println("the last monthof quater is   "+lastMonthOfQuater);
		
		
		cal.set(Calendar.MONTH, 5);
		int lastDayOffirstMonthOfQuater = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		System.out.println("---lastDayOffirstMonthOfQuater:  " +lastDayOffirstMonthOfQuater);
		 String aString = Integer.toString(lastDayOffirstMonthOfQuater);
		 String  finaldate = aString.concat("-").concat(lastMonthOfQuater);
		
		cal.set(Calendar.YEAR,  lastDayOffirstMonthOfQuater);
		String lastMonthOfQuaterssss = sdf.format(cal.getTime());
		System.out.println("*****---finaldate   "+finaldate);
		
		
		
		cal.set(Calendar.MONTH, 6);				// gives beforelast Month
		String lastMonthOfQuater6 = sdf.format(cal.getTime());
		System.out.println("---lastMonthOfQuater6   "+lastMonthOfQuater6);
		int lastDayOffirstMonthOfQuater6 = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		System.out.println("---lastDayOffirstMonthOfQuater6:  " +lastDayOffirstMonthOfQuater6);
		
		cal.set(Calendar.MONTH, 7);				// gives beforelast Month
		String lastMonthOfQuater7 = sdf.format(cal.getTime());
		System.out.println("---lastMonthOfQuater7   "+lastMonthOfQuater7);
		int lastDayOffirstMonthOfQuater7 = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		System.out.println("---lastDayOffirstMonthOfQuater7:  " +lastDayOffirstMonthOfQuater7);
		
		//PRIOR MONTH
		/*String priorMonth = (cal.get(Calendar.MONTH));
		System.out.println("---priorMonth   "+priorMonth);
		String month = monthName[cal.get(Calendar.MONTH)];
		//cal.get(Calendar.MONTH - 1);

*/
		Calendar clander = new GregorianCalendar();
		clander.add(Calendar.MONTH,+1);
		Format formatter11= new SimpleDateFormat("MMMM");
        System.out.println(formatter11.format(clander.getTime()));
        System.out.println("11111  "+sdf.format(clander.getTime()));
        
        Calendar clander1 = new GregorianCalendar();
        clander1.add(Calendar.MONTH,0);
        Format formatter1= new SimpleDateFormat("MMMM");
        System.out.println(formatter1.format(clander1.getTime()));
        System.out.println("00000  "+sdf.format(clander1.getTime()));
        
        Calendar clander2 = new GregorianCalendar();
        clander2.add(Calendar.MONTH, -1);
        Format formatter = new SimpleDateFormat("MMMM");
        System.out.println(formatter.format(clander2.getTime()));
        System.out.println("---1111  "+sdf.format(clander2.getTime()));
        
        
		
		cal.set(Calendar.MONTH, - 1);;
        System.out.println(sdf.format(cal.getTime()));
        cal.set(Calendar.MONTH, - 2);
        System.out.println(sdf.format(cal.getTime()));
        cal.set(Calendar.MONTH, 0);
        System.out.println(sdf.format(cal.getTime()));
        cal.set(Calendar.MONTH, 1);
        System.out.println(sdf.format(cal.getTime()));
        cal.set(Calendar.MONTH,  2);
        System.out.println(sdf.format(cal.getTime()));
        cal.set(Calendar.MONTH, 3);
        System.out.println(sdf.format(cal.getTime()));
        cal.set(Calendar.MONTH, 4);
        System.out.println(sdf.format(cal.getTime()));
        cal.set(Calendar.MONTH,  5);
        System.out.println(sdf.format(cal.getTime()));
        cal.set(Calendar.MONTH, 6);
        System.out.println(sdf.format(cal.getTime()));
        cal.set(Calendar.MONTH, 7);
        System.out.println(sdf.format(cal.getTime()));
        cal.set(Calendar.MONTH,  8);
        System.out.println(sdf.format(cal.getTime()));
        cal.set(Calendar.MONTH, 9);
        System.out.println(sdf.format(cal.getTime()));
        cal.set(Calendar.MONTH, 10);
        System.out.println(sdf.format(cal.getTime()));
        cal.set(Calendar.MONTH,  11);
        System.out.println(sdf.format(cal.getTime()));
        
        Calendar cld = new GregorianCalendar();
   //     cld.get(Calendar.YEAR,-1);
        System.out.println(sdf.format(cld.getTime()));
	}
	
	private static String stringToDate(String sDate) {
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.s");
		SimpleDateFormat sdf2 = new SimpleDateFormat("ddMMyyyy");
		Date d1 = null;
		try {
			d1 = sdf1.parse(sDate);
		} catch (ParseException e1) {
			e1.printStackTrace();
			//java.text.ParseException: Unparseable date: "2010-June-07"
		}
        String formatedDate = sdf2.format(d1);
        System.out.println("Final date.."+formatedDate);
		return formatedDate;
	}
	
	public static void main(String[] args) {
		
		//ConvertDateToString();
		//MonthMaxDays();
		dateFormattings();
		Date d = new Date();
		//stringToDate(d);
	}
}
