

import java.util.Date;

public class AuditExecution11 {

	
	private int adt_Exctn_Seq;        
	private Date create_Dt;           
	private String mdfd_By_Txt;          
	private Date mdfd_Dt;              
	private String ignor_Inr_Ind;        
	private int adt_Seq;              
	private int adt_Confign_Seq;      
	private int user_Id;              
	private int excptn_Gen_Cnt;       
	private Date exctn_Strt_Dt;        
	private Date exctn_End_Dt;         
	private Date inv_Strt_Dt;          
	private Date inv_End_Dt;           
	private Date webfcs_Strt_Dt;       
	private Date webfcs_End_Dt;        
	private String webfcs_Stat_Txt;      
	private String telco_Lvl_Cd; 
	private String pay_Deduct_Cd_Text;
	
	private String tay_Deduct_Cd_Txt;    
	private String outpt_Frmt_Cd_Txt;    
	private String adt_Apprv_Rslt_Ind;   
	private int wrk_Grp_Seq;         
	private String app_Stat_Cd_Txt;      
	private int adt_Batch_Seq;       
	private int audit_Param_Set_Seq;
	
	
	/**
	 * @return the adt_Exctn_Seq
	 */
	public int getAdt_Exctn_Seq() {
		return adt_Exctn_Seq;
	}
	/**
	 * @param adtExctnSeq the adt_Exctn_Seq to set
	 */
	public void setAdt_Exctn_Seq(int adtExctnSeq) {
		adt_Exctn_Seq = adtExctnSeq;
	}
	/**
	 * @return the create_Dt
	 */
	public Date getCreate_Dt() {
		return create_Dt;
	}
	/**
	 * @param createDt the create_Dt to set
	 */
	public void setCreate_Dt(Date createDt) {
		create_Dt = createDt;
	}
	/**
	 * @return the mdfd_By_Txt
	 */
	public String getMdfd_By_Txt() {
		return mdfd_By_Txt;
	}
	/**
	 * @param mdfdByTxt the mdfd_By_Txt to set
	 */
	public void setMdfd_By_Txt(String mdfdByTxt) {
		mdfd_By_Txt = mdfdByTxt;
	}
	/**
	 * @return the mdfd_Dt
	 */
	public Date getMdfd_Dt() {
		return mdfd_Dt;
	}
	/**
	 * @param mdfdDt the mdfd_Dt to set
	 */
	public void setMdfd_Dt(Date mdfdDt) {
		mdfd_Dt = mdfdDt;
	}
	/**
	 * @return the ignor_Inr_Ind
	 */
	public String getIgnor_Inr_Ind() {
		return ignor_Inr_Ind;
	}
	/**
	 * @param ignorInrInd the ignor_Inr_Ind to set
	 */
	public void setIgnor_Inr_Ind(String ignorInrInd) {
		ignor_Inr_Ind = ignorInrInd;
	}
	/**
	 * @return the adt_Seq
	 */
	public int getAdt_Seq() {
		return adt_Seq;
	}
	/**
	 * @param adtSeq the adt_Seq to set
	 */
	public void setAdt_Seq(int adtSeq) {
		adt_Seq = adtSeq;
	}
	/**
	 * @return the adt_Confign_Seq
	 */
	public int getAdt_Confign_Seq() {
		return adt_Confign_Seq;
	}
	/**
	 * @param adtConfignSeq the adt_Confign_Seq to set
	 */
	public void setAdt_Confign_Seq(int adtConfignSeq) {
		adt_Confign_Seq = adtConfignSeq;
	}
	/**
	 * @return the user_Id
	 */
	public int getUser_Id() {
		return user_Id;
	}
	/**
	 * @param userId the user_Id to set
	 */
	public void setUser_Id(int userId) {
		user_Id = userId;
	}
	/**
	 * @return the excptn_Gen_Cnt
	 */
	public int getExcptn_Gen_Cnt() {
		return excptn_Gen_Cnt;
	}
	/**
	 * @param excptnGenCnt the excptn_Gen_Cnt to set
	 */
	public void setExcptn_Gen_Cnt(int excptnGenCnt) {
		excptn_Gen_Cnt = excptnGenCnt;
	}
	/**
	 * @return the exctn_Strt_Dt
	 */
	public Date getExctn_Strt_Dt() {
		return exctn_Strt_Dt;
	}
	/**
	 * @param exctnStrtDt the exctn_Strt_Dt to set
	 */
	public void setExctn_Strt_Dt(Date exctnStrtDt) {
		exctn_Strt_Dt = exctnStrtDt;
	}
	/**
	 * @return the exctn_End_Dt
	 */
	public Date getExctn_End_Dt() {
		return exctn_End_Dt;
	}
	/**
	 * @param exctnEndDt the exctn_End_Dt to set
	 */
	public void setExctn_End_Dt(Date exctnEndDt) {
		exctn_End_Dt = exctnEndDt;
	}
	/**
	 * @return the inv_Strt_Dt
	 */
	public Date getInv_Strt_Dt() {
		return inv_Strt_Dt;
	}
	/**
	 * @param invStrtDt the inv_Strt_Dt to set
	 */
	public void setInv_Strt_Dt(Date invStrtDt) {
		inv_Strt_Dt = invStrtDt;
	}
	/**
	 * @return the inv_End_Dt
	 */
	public Date getInv_End_Dt() {
		return inv_End_Dt;
	}
	/**
	 * @param invEndDt the inv_End_Dt to set
	 */
	public void setInv_End_Dt(Date invEndDt) {
		inv_End_Dt = invEndDt;
	}
	/**
	 * @return the webfcs_Strt_Dt
	 */
	public Date getWebfcs_Strt_Dt() {
		return webfcs_Strt_Dt;
	}
	/**
	 * @param webfcsStrtDt the webfcs_Strt_Dt to set
	 */
	public void setWebfcs_Strt_Dt(Date webfcsStrtDt) {
		webfcs_Strt_Dt = webfcsStrtDt;
	}
	/**
	 * @return the webfcs_End_Dt
	 */
	public Date getWebfcs_End_Dt() {
		return webfcs_End_Dt;
	}
	/**
	 * @param webfcsEndDt the webfcs_End_Dt to set
	 */
	public void setWebfcs_End_Dt(Date webfcsEndDt) {
		webfcs_End_Dt = webfcsEndDt;
	}
	/**
	 * @return the webfcs_Stat_Txt
	 */
	public String getWebfcs_Stat_Txt() {
		return webfcs_Stat_Txt;
	}
	/**
	 * @param webfcsStatTxt the webfcs_Stat_Txt to set
	 */
	public void setWebfcs_Stat_Txt(String webfcsStatTxt) {
		webfcs_Stat_Txt = webfcsStatTxt;
	}
	/**
	 * @return the telco_Lvl_Cd
	 */
	public String getTelco_Lvl_Cd() {
		return telco_Lvl_Cd;
	}
	/**
	 * @param telcoLvlCd the telco_Lvl_Cd to set
	 */
	public void setTelco_Lvl_Cd(String telcoLvlCd) {
		telco_Lvl_Cd = telcoLvlCd;
	}
	
	
		
	/**
	 * @return the pay_Deduct_Cd_Text
	 */
	public String getPay_Deduct_Cd_Text() {
		return pay_Deduct_Cd_Text;
	}
	/**
	 * @param payDeductCdText the pay_Deduct_Cd_Text to set
	 */
	public void setPay_Deduct_Cd_Text(String payDeductCdText) {
		pay_Deduct_Cd_Text = payDeductCdText;
	}
	
	
	
	/**
	 * @return the tay_Deduct_Cd_Txt
	 */
	public String getTay_Deduct_Cd_Txt() {
		return tay_Deduct_Cd_Txt;
	}
	/**
	 * @param tayDeductCdTxt the tay_Deduct_Cd_Txt to set
	 */
	public void setTay_Deduct_Cd_Txt(String tayDeductCdTxt) {
		tay_Deduct_Cd_Txt = tayDeductCdTxt;
	}
	/**
	 * @return the outpt_Frmt_Cd_Txt
	 */
	public String getOutpt_Frmt_Cd_Txt() {
		return outpt_Frmt_Cd_Txt;
	}
	/**
	 * @param outptFrmtCdTxt the outpt_Frmt_Cd_Txt to set
	 */
	public void setOutpt_Frmt_Cd_Txt(String outptFrmtCdTxt) {
		outpt_Frmt_Cd_Txt = outptFrmtCdTxt;
	}
	/**
	 * @return the adt_Apprv_Rslt_Ind
	 */
	public String getAdt_Apprv_Rslt_Ind() {
		return adt_Apprv_Rslt_Ind;
	}
	/**
	 * @param adtApprvRsltInd the adt_Apprv_Rslt_Ind to set
	 */
	public void setAdt_Apprv_Rslt_Ind(String adtApprvRsltInd) {
		adt_Apprv_Rslt_Ind = adtApprvRsltInd;
	}
	/**
	 * @return the wrk_Grp_Seq
	 */
	public int getWrk_Grp_Seq() {
		return wrk_Grp_Seq;
	}
	/**
	 * @param wrkGrpSeq the wrk_Grp_Seq to set
	 */
	public void setWrk_Grp_Seq(int wrkGrpSeq) {
		wrk_Grp_Seq = wrkGrpSeq;
	}
	/**
	 * @return the app_Stat_Cd_Txt
	 */
	public String getApp_Stat_Cd_Txt() {
		return app_Stat_Cd_Txt;
	}
	/**
	 * @param appStatCdTxt the app_Stat_Cd_Txt to set
	 */
	public void setApp_Stat_Cd_Txt(String appStatCdTxt) {
		app_Stat_Cd_Txt = appStatCdTxt;
	}
	/**
	 * @return the adt_Batch_Seq
	 */
	public int getAdt_Batch_Seq() {
		return adt_Batch_Seq;
	}
	/**
	 * @param adtBatchSeq the adt_Batch_Seq to set
	 */
	public void setAdt_Batch_Seq(int adtBatchSeq) {
		adt_Batch_Seq = adtBatchSeq;
	}
	/**
	 * @return the audit_Param_Set_Seq
	 */
	public int getAudit_Param_Set_Seq() {
		return audit_Param_Set_Seq;
	}
	/**
	 * @param auditParamSetSeq the audit_Param_Set_Seq to set
	 */
	public void setAudit_Param_Set_Seq(int auditParamSetSeq) {
		audit_Param_Set_Seq = auditParamSetSeq;
	}  

	
	
	
	
	
	
}
